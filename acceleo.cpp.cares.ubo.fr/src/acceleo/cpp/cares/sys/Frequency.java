package acceleo.cpp.cares.sys;

import fr.ubo.cares.dcl.TimeableObject;
import fr.ubo.cares.sys.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Frequency {
	
	int currentIndex = 0;
	
	public void setFrequency(CompositeComponent comp){
		double frequency = comp.getFrequency();
		for (Component it : comp.getComponents()) {
			if (it.isInheritFrequency()) {
				((TimeableObject)it).setFrequency(frequency);
			}
			if (it instanceof CompositeComponent) {
				setFrequency((CompositeComponent)it);
			}
		}
	}
}
