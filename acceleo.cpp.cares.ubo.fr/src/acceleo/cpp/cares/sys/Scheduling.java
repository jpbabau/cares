package acceleo.cpp.cares.sys;

import fr.ubo.cares.sys.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Scheduling {
	
	int currentIndex = 0;
	
	public List<LeafComponent> OrderPriority(CompositeComponent root){
		List<LeafComponent> lc = allLeaf(root);
		Collections.sort(lc,ComparatorPriority);
		return lc;
	}
	
	public List<LeafComponent> allLeaf(CompositeComponent root){
		List<LeafComponent> lc = new ArrayList<LeafComponent>();
		for (Component it : root.getComponents()) {
			if (it instanceof CompositeComponent) {
				lc.addAll(allLeaf((CompositeComponent)it));
			}
			if (it instanceof LeafComponent) {
				lc.add((LeafComponent)it);
			}
		}
		return lc;
	}
	
    public static Comparator<LeafComponent> ComparatorPriority = new Comparator<LeafComponent>() {
        @Override
        public int compare(LeafComponent e1, LeafComponent e2) {
            return (int) (e1.getSchedulingPriority() - e2.getSchedulingPriority());
        }
    };
	
}
