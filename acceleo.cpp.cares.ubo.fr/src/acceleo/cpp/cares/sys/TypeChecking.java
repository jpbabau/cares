package acceleo.cpp.cares.sys;

import fr.ubo.cares.dcl.Input;
import fr.ubo.cares.dcl.Interface;
import fr.ubo.cares.dcl.InterfacePort;
import fr.ubo.cares.dcl.Output;
import fr.ubo.cares.dcl.ParameterDeclaration;
import fr.ubo.cares.dcl.ParameterInstanciation;
import fr.ubo.cares.sys.*;
import java.util.stream.Stream;

public class TypeChecking {

	int currentIndex = 0;

	public boolean typeCheck(CompositeComponent root) {
		boolean isOk = true;
		for (Component it : root.getComponents()) {
			if (it instanceof CompositeComponent) {
				isOk = isOk && typeCheck((CompositeComponent) it);
			}
			if (it instanceof LeafComponent) {
				isOk = isOk && typeCheck((LeafComponent) it);
			}
		}
		if (isOk) {
			System.out.println("types are ok");
		}
		return isOk;
	}

	public boolean typeCheck(LeafComponent elt) {
		boolean isOk = true;
		System.out.println("type checking for component " + elt.getName());

		// is the component type is set
		if (elt.getType() == null) {
			isOk = false;
			System.err.println(elt.getName() + " has no type");
		}
		// component input types are ok
		for (Input inputType : elt.getType().getInputs()) {
			Stream<LeafInputPort> lip = elt.getInputPorts().stream().filter(input->input.getInput()==inputType);
			if (lip.count() != 1) {
				System.err.println(elt.getName() + " requires one and only one input of type " +inputType.getName());
				isOk = false;
			}
		}
		// component inputs have correct types
		for (LeafInputPort input : elt.getInputPorts()) {
			if (! elt.getType().getInputs().contains(input.getInput())) {
				System.err.println(elt.getName() + " has one input of bad type : " +input.getName());
				isOk = false;
			}
		}
		// component output types are ok
		for (Output outputType : elt.getType().getOutputs()) {
			Stream<LeafOutputPort> lop = elt.getOutputPorts().stream().filter(output->output.getOutput()==outputType);
			if (lop.count() != 1) {
				System.err.println(elt.getName() + " requires one and only one output of type " + outputType.getName());
				isOk = false;
			}
		}
		// component outputs have correct types
		for (LeafOutputPort output : elt.getOutputPorts()) {
			if (! elt.getType().getOutputs().contains(output.getOutput())) {
				System.err.println(elt.getName() + " has one output of bad type : " +output.getName());
				isOk = false;
			}
		}
		// component parameter types are ok
		for (ParameterDeclaration paramType : elt.getType().getParameters()) {
			Stream<ParameterInstanciation> lpa = elt.getDeclarations().stream().filter(param->param.getParameter()==paramType);
			if (lpa.count() != 1) {
				System.err.println(elt.getName() + " requires one and only one parameter of type " +paramType.getName());
				isOk = false;
			}
		}
		// component parameters have correct types
		for (ParameterInstanciation param : elt.getDeclarations()) {
			if (! elt.getType().getParameters().contains(param.getParameter())) {
				System.err.println(elt.getName() + " has one parameter of bad type : " +param.getName());
				isOk = false;
			}
		}
		// component required interfaces types are ok
		for (InterfacePort itfType : elt.getType().getRequiredInterface()) {
			Stream<RequiredInterfacePort> lri = elt.getRequiredPorts().stream().filter(itf->itf.getInterface()==itfType);
			if (lri.count() == 0) {
				System.err.println(elt.getName() + " requires required interface port of type " +itfType.getName());
				isOk = false;
			}
		}
		// component required interfaces have correct types
		for (RequiredInterfacePort itf : elt.getRequiredPorts()) {
			if (! elt.getType().getRequiredInterface().contains(itf.getInterface())) {
				System.err.println(elt.getName() + " has one required interface of bad type : " +itf.getName());
				isOk = false;
			}
		}
		// component provided interfaces types are ok
		for (InterfacePort itfType : elt.getType().getProvidedInterface()) {
			Stream<ProvidedInterfacePort> lpi = elt.getProvidedPorts().stream().filter(itf->itf.getInterface()==itfType);
			if (lpi.count() != 1) {
				System.err.println(elt.getName() + " requires one and only one provided interface port of type " +itfType.getInterface().getName());
				isOk = false;
			}
		}
		// component provided interfaces have correct types
		for (ProvidedInterfacePort itf : elt.getProvidedPorts()) {
			if (! elt.getType().getProvidedInterface().contains(itf.getInterface())) {
				System.err.println("the LeafComponent " + elt.getName() + " refers " + elt.getType().getName() + " has one provided interface of bad type : " +itf.getName() );
				isOk = false;
			}
		}
		// component required interfaces have correct types
		for (RequiredInterfacePort rItfP : elt.getRequiredPorts()) {
			if (rItfP.getCallLink() == null) {
				System.err.println(" link the required interface "+rItfP.getName()+" of "+elt.getName() +" to a provided one");
				isOk = false;
			} else {
				Interface rItf = rItfP.getInterface().getInterface();
				for(ProvidedInterfacePort pip : rItfP.getCallLink()) {
					Interface itf = pip.getInterface().getInterface();
					if ( itf == null || rItf == null) {
						System.err.println("link to the required interface  of " + elt.getName() +" does not call a provided interface of the same type null" );
						isOk = false;											
					}
					else if(itf != rItf)
					{
						System.err.println("link to the required interface "+ rItf.getName()+" of "+elt.getName() +" does not call a provided interface of the same type " + itf.getName());
						isOk = false;					
					}
					
				}
			}
		}
		return isOk;
	}
}
