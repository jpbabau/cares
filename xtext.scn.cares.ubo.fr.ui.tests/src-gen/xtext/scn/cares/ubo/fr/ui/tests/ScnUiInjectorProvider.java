/*
 * generated by Xtext 2.16.0-SNAPSHOT
 */
package xtext.scn.cares.ubo.fr.ui.tests;

import com.google.inject.Injector;
import org.eclipse.xtext.testing.IInjectorProvider;
import xtext.scn.cares.ubo.fr.ui.internal.FrActivator;

public class ScnUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return FrActivator.getInstance().getInjector("xtext.scn.cares.ubo.fr.Scn");
	}

}
