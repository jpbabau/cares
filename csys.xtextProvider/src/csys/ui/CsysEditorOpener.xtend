package csys.ui

import fr.ubo.cares.sys.presentation.SysEditor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EReference
import org.eclipse.ui.IEditorPart
import org.eclipse.xtext.ui.editor.LanguageSpecificURIEditorOpener

class CsysEditorOpener extends LanguageSpecificURIEditorOpener {
	
	override protected void selectAndReveal(
		IEditorPart openEditor, 
		URI uri, EReference crossReference, int indexInList, 
		boolean select) {
		if (uri.fragment !== null) {
			val ecoreEditor = openEditor.getAdapter(SysEditor)
			if (ecoreEditor instanceof SysEditor) {
				val eObject = ecoreEditor.editingDomain.resourceSet.getEObject(uri, true)
				ecoreEditor.setSelectionToViewer(#[eObject])
			}
		}
	}

	override protected String getEditorId() {
		return "io.typefox.xtextxmi.tree.presentation.TreeEditorID"
	}
}