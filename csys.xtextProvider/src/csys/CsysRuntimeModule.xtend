package csys

import org.eclipse.xtext.resource.generic.AbstractGenericResourceRuntimeModule
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider

class CsysRuntimeModule extends AbstractGenericResourceRuntimeModule {
	
	override protected getFileExtensions() {
		'csys'
	}
	
	override protected getLanguageName() {
		'csys.xtextProvider.Provider'
	}
	
	override bindIQualifiedNameProvider() {
		DefaultDeclarativeQualifiedNameProvider
	}
	
	// bind additional services here
}