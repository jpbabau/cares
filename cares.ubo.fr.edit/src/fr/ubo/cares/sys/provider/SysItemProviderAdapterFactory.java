/**
 */
package fr.ubo.cares.sys.provider;

import fr.ubo.cares.sys.util.SysAdapterFactory;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SysItemProviderAdapterFactory extends SysAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
	/**
	 * This keeps track of the root adapter factory that delegates to this adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComposedAdapterFactory parentAdapterFactory;

	/**
	 * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IChangeNotifier changeNotifier = new ChangeNotifier();

	/**
	 * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Collection<Object> supportedTypes = new ArrayList<Object>();

	/**
	 * This constructs an instance.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SysItemProviderAdapterFactory() {
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(IItemPropertySource.class);
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.CaresSystem} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaresSystemItemProvider caresSystemItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.CaresSystem}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCaresSystemAdapter() {
		if (caresSystemItemProvider == null) {
			caresSystemItemProvider = new CaresSystemItemProvider(this);
		}

		return caresSystemItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.CompositeComponent} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeComponentItemProvider compositeComponentItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.CompositeComponent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCompositeComponentAdapter() {
		if (compositeComponentItemProvider == null) {
			compositeComponentItemProvider = new CompositeComponentItemProvider(this);
		}

		return compositeComponentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.LeafComponent} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafComponentItemProvider leafComponentItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.LeafComponent}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLeafComponentAdapter() {
		if (leafComponentItemProvider == null) {
			leafComponentItemProvider = new LeafComponentItemProvider(this);
		}

		return leafComponentItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.DataLink} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataLinkItemProvider dataLinkItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.DataLink}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createDataLinkAdapter() {
		if (dataLinkItemProvider == null) {
			dataLinkItemProvider = new DataLinkItemProvider(this);
		}

		return dataLinkItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.CompositeInputPort} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeInputPortItemProvider compositeInputPortItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.CompositeInputPort}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCompositeInputPortAdapter() {
		if (compositeInputPortItemProvider == null) {
			compositeInputPortItemProvider = new CompositeInputPortItemProvider(this);
		}

		return compositeInputPortItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.CompositeOutputPort} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeOutputPortItemProvider compositeOutputPortItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.CompositeOutputPort}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createCompositeOutputPortAdapter() {
		if (compositeOutputPortItemProvider == null) {
			compositeOutputPortItemProvider = new CompositeOutputPortItemProvider(this);
		}

		return compositeOutputPortItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.LeafInputPort} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafInputPortItemProvider leafInputPortItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.LeafInputPort}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLeafInputPortAdapter() {
		if (leafInputPortItemProvider == null) {
			leafInputPortItemProvider = new LeafInputPortItemProvider(this);
		}

		return leafInputPortItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.LeafOutputPort} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafOutputPortItemProvider leafOutputPortItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.LeafOutputPort}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createLeafOutputPortAdapter() {
		if (leafOutputPortItemProvider == null) {
			leafOutputPortItemProvider = new LeafOutputPortItemProvider(this);
		}

		return leafOutputPortItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.RequiredInterfacePort} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredInterfacePortItemProvider requiredInterfacePortItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.RequiredInterfacePort}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createRequiredInterfacePortAdapter() {
		if (requiredInterfacePortItemProvider == null) {
			requiredInterfacePortItemProvider = new RequiredInterfacePortItemProvider(this);
		}

		return requiredInterfacePortItemProvider;
	}

	/**
	 * This keeps track of the one adapter used for all {@link fr.ubo.cares.sys.ProvidedInterfacePort} instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvidedInterfacePortItemProvider providedInterfacePortItemProvider;

	/**
	 * This creates an adapter for a {@link fr.ubo.cares.sys.ProvidedInterfacePort}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter createProvidedInterfacePortAdapter() {
		if (providedInterfacePortItemProvider == null) {
			providedInterfacePortItemProvider = new ProvidedInterfacePortItemProvider(this);
		}

		return providedInterfacePortItemProvider;
	}

	/**
	 * This returns the root adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComposeableAdapterFactory getRootAdapterFactory() {
		return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
	}

	/**
	 * This sets the composed adapter factory that contains this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
		this.parentAdapterFactory = parentAdapterFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object type) {
		return supportedTypes.contains(type) || super.isFactoryForType(type);
	}

	/**
	 * This implementation substitutes the factory itself as the key for the adapter.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Adapter adapt(Notifier notifier, Object type) {
		return super.adapt(notifier, this);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object adapt(Object object, Object type) {
		if (isFactoryForType(type)) {
			Object adapter = super.adapt(object, type);
			if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
				return adapter;
			}
		}

		return null;
	}

	/**
	 * This adds a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.addListener(notifyChangedListener);
	}

	/**
	 * This removes a listener.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeListener(INotifyChangedListener notifyChangedListener) {
		changeNotifier.removeListener(notifyChangedListener);
	}

	/**
	 * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void fireNotifyChanged(Notification notification) {
		changeNotifier.fireNotifyChanged(notification);

		if (parentAdapterFactory != null) {
			parentAdapterFactory.fireNotifyChanged(notification);
		}
	}

	/**
	 * This disposes all of the item providers created by this factory. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void dispose() {
		if (caresSystemItemProvider != null) caresSystemItemProvider.dispose();
		if (compositeComponentItemProvider != null) compositeComponentItemProvider.dispose();
		if (leafComponentItemProvider != null) leafComponentItemProvider.dispose();
		if (dataLinkItemProvider != null) dataLinkItemProvider.dispose();
		if (compositeInputPortItemProvider != null) compositeInputPortItemProvider.dispose();
		if (compositeOutputPortItemProvider != null) compositeOutputPortItemProvider.dispose();
		if (leafInputPortItemProvider != null) leafInputPortItemProvider.dispose();
		if (leafOutputPortItemProvider != null) leafOutputPortItemProvider.dispose();
		if (requiredInterfacePortItemProvider != null) requiredInterfacePortItemProvider.dispose();
		if (providedInterfacePortItemProvider != null) providedInterfacePortItemProvider.dispose();
	}

}
