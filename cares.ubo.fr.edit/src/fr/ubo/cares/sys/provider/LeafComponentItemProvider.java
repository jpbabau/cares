/**
 */
package fr.ubo.cares.sys.provider;


import fr.ubo.cares.dcl.DclFactory;
import fr.ubo.cares.dcl.DclPackage;

import fr.ubo.cares.sys.LeafComponent;
import fr.ubo.cares.sys.SysFactory;
import fr.ubo.cares.sys.SysPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.ubo.cares.sys.LeafComponent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LeafComponentItemProvider extends ComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFrequencyPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Frequency feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFrequencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeableObject_frequency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeableObject_frequency_feature", "_UI_TimeableObject_type"),
				 DclPackage.Literals.TIMEABLE_OBJECT__FREQUENCY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LeafComponent_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LeafComponent_type_feature", "_UI_LeafComponent_type"),
				 SysPackage.Literals.LEAF_COMPONENT__TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SysPackage.Literals.LEAF_COMPONENT__DECLARATIONS);
			childrenFeatures.add(SysPackage.Literals.LEAF_COMPONENT__INPUT_PORTS);
			childrenFeatures.add(SysPackage.Literals.LEAF_COMPONENT__OUTPUT_PORTS);
			childrenFeatures.add(SysPackage.Literals.LEAF_COMPONENT__PROVIDED_PORTS);
			childrenFeatures.add(SysPackage.Literals.LEAF_COMPONENT__REQUIRED_PORTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns LeafComponent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LeafComponent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((LeafComponent)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_LeafComponent_type") :
			getString("_UI_LeafComponent_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LeafComponent.class)) {
			case SysPackage.LEAF_COMPONENT__FREQUENCY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case SysPackage.LEAF_COMPONENT__DECLARATIONS:
			case SysPackage.LEAF_COMPONENT__INPUT_PORTS:
			case SysPackage.LEAF_COMPONENT__OUTPUT_PORTS:
			case SysPackage.LEAF_COMPONENT__PROVIDED_PORTS:
			case SysPackage.LEAF_COMPONENT__REQUIRED_PORTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.LEAF_COMPONENT__DECLARATIONS,
				 DclFactory.eINSTANCE.createParameterInstanciation()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.LEAF_COMPONENT__INPUT_PORTS,
				 SysFactory.eINSTANCE.createLeafInputPort()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.LEAF_COMPONENT__OUTPUT_PORTS,
				 SysFactory.eINSTANCE.createLeafOutputPort()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.LEAF_COMPONENT__PROVIDED_PORTS,
				 SysFactory.eINSTANCE.createProvidedInterfacePort()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.LEAF_COMPONENT__REQUIRED_PORTS,
				 SysFactory.eINSTANCE.createRequiredInterfacePort()));
	}

}
