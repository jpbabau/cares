/**
 */
package fr.ubo.cares.sys.provider;


import fr.ubo.cares.dcl.DclPackage;

import fr.ubo.cares.sys.CompositeComponent;
import fr.ubo.cares.sys.SysFactory;
import fr.ubo.cares.sys.SysPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.ubo.cares.sys.CompositeComponent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CompositeComponentItemProvider extends ComponentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeComponentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFrequencyPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Frequency feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFrequencyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TimeableObject_frequency_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TimeableObject_frequency_feature", "_UI_TimeableObject_type"),
				 DclPackage.Literals.TIMEABLE_OBJECT__FREQUENCY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SysPackage.Literals.COMPOSITE_COMPONENT__COMPONENTS);
			childrenFeatures.add(SysPackage.Literals.COMPOSITE_COMPONENT__LINKS);
			childrenFeatures.add(SysPackage.Literals.COMPOSITE_COMPONENT__INPUT_PORTS);
			childrenFeatures.add(SysPackage.Literals.COMPOSITE_COMPONENT__OUTPUT_PORTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CompositeComponent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CompositeComponent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CompositeComponent)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CompositeComponent_type") :
			getString("_UI_CompositeComponent_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CompositeComponent.class)) {
			case SysPackage.COMPOSITE_COMPONENT__FREQUENCY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case SysPackage.COMPOSITE_COMPONENT__COMPONENTS:
			case SysPackage.COMPOSITE_COMPONENT__LINKS:
			case SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS:
			case SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.COMPOSITE_COMPONENT__COMPONENTS,
				 SysFactory.eINSTANCE.createCompositeComponent()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.COMPOSITE_COMPONENT__COMPONENTS,
				 SysFactory.eINSTANCE.createLeafComponent()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.COMPOSITE_COMPONENT__LINKS,
				 SysFactory.eINSTANCE.createDataLink()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.COMPOSITE_COMPONENT__INPUT_PORTS,
				 SysFactory.eINSTANCE.createCompositeInputPort()));

		newChildDescriptors.add
			(createChildParameter
				(SysPackage.Literals.COMPOSITE_COMPONENT__OUTPUT_PORTS,
				 SysFactory.eINSTANCE.createCompositeOutputPort()));
	}

}
