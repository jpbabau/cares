/**
 */
package fr.ubo.cares.scn.provider;


import fr.ubo.cares.scn.BasicEvent;
import fr.ubo.cares.scn.ScnFactory;
import fr.ubo.cares.scn.ScnPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link fr.ubo.cares.scn.BasicEvent} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BasicEventItemProvider extends EventItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicEventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ScnPackage.Literals.BASIC_EVENT__ACTIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BasicEvent.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BasicEvent"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		BasicEvent basicEvent = (BasicEvent)object;
		return getString("_UI_BasicEvent_type") + " " + basicEvent.getTime();
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BasicEvent.class)) {
			case ScnPackage.BASIC_EVENT__ACTIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createComponentInitialization()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createComponentStart()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createComponentStop()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createServiceCall()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createSimpleDoubleParameterSet()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createSimpleIntParameterSet()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createSimpleBoolParameterSet()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createSimpleStringParameterSet()));

		newChildDescriptors.add
			(createChildParameter
				(ScnPackage.Literals.BASIC_EVENT__ACTIONS,
				 ScnFactory.eINSTANCE.createXOR()));
	}

}
