package xtext.dcl.cares.ubo.fr.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import xtext.dcl.cares.ubo.fr.services.DeclarationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDeclarationParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Declaration'", "'{'", "'}'", "'types'", "'interfaces'", "'leafComponentTypes'", "'('", "'Void'", "')'", "';'", "'CInt'", "'CDouble'", "'CBoolean'", "'CString'", "'.'", "'Record'", "'['", "','", "']'", "'-'", "'Interface'", "'LeafComponentType'", "'requiredInterfaces'", "'providedInterfaces'", "'inputs'", "'outputs'", "'parameters'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDeclarationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDeclarationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDeclarationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDeclaration.g"; }


    	private DeclarationGrammarAccess grammarAccess;

    	public void setGrammarAccess(DeclarationGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDeclaration"
    // InternalDeclaration.g:53:1: entryRuleDeclaration : ruleDeclaration EOF ;
    public final void entryRuleDeclaration() throws RecognitionException {
        try {
            // InternalDeclaration.g:54:1: ( ruleDeclaration EOF )
            // InternalDeclaration.g:55:1: ruleDeclaration EOF
            {
             before(grammarAccess.getDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclaration();

            state._fsp--;

             after(grammarAccess.getDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // InternalDeclaration.g:62:1: ruleDeclaration : ( ( rule__Declaration__Group__0 ) ) ;
    public final void ruleDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:66:2: ( ( ( rule__Declaration__Group__0 ) ) )
            // InternalDeclaration.g:67:2: ( ( rule__Declaration__Group__0 ) )
            {
            // InternalDeclaration.g:67:2: ( ( rule__Declaration__Group__0 ) )
            // InternalDeclaration.g:68:3: ( rule__Declaration__Group__0 )
            {
             before(grammarAccess.getDeclarationAccess().getGroup()); 
            // InternalDeclaration.g:69:3: ( rule__Declaration__Group__0 )
            // InternalDeclaration.g:69:4: rule__Declaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleType"
    // InternalDeclaration.g:78:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalDeclaration.g:79:1: ( ruleType EOF )
            // InternalDeclaration.g:80:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalDeclaration.g:87:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:91:2: ( ( ( rule__Type__Alternatives ) ) )
            // InternalDeclaration.g:92:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalDeclaration.g:92:2: ( ( rule__Type__Alternatives ) )
            // InternalDeclaration.g:93:3: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalDeclaration.g:94:3: ( rule__Type__Alternatives )
            // InternalDeclaration.g:94:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleVoid"
    // InternalDeclaration.g:103:1: entryRuleVoid : ruleVoid EOF ;
    public final void entryRuleVoid() throws RecognitionException {
        try {
            // InternalDeclaration.g:104:1: ( ruleVoid EOF )
            // InternalDeclaration.g:105:1: ruleVoid EOF
            {
             before(grammarAccess.getVoidRule()); 
            pushFollow(FOLLOW_1);
            ruleVoid();

            state._fsp--;

             after(grammarAccess.getVoidRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVoid"


    // $ANTLR start "ruleVoid"
    // InternalDeclaration.g:112:1: ruleVoid : ( ( rule__Void__Group__0 ) ) ;
    public final void ruleVoid() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:116:2: ( ( ( rule__Void__Group__0 ) ) )
            // InternalDeclaration.g:117:2: ( ( rule__Void__Group__0 ) )
            {
            // InternalDeclaration.g:117:2: ( ( rule__Void__Group__0 ) )
            // InternalDeclaration.g:118:3: ( rule__Void__Group__0 )
            {
             before(grammarAccess.getVoidAccess().getGroup()); 
            // InternalDeclaration.g:119:3: ( rule__Void__Group__0 )
            // InternalDeclaration.g:119:4: rule__Void__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Void__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVoidAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVoid"


    // $ANTLR start "entryRuleCInt"
    // InternalDeclaration.g:128:1: entryRuleCInt : ruleCInt EOF ;
    public final void entryRuleCInt() throws RecognitionException {
        try {
            // InternalDeclaration.g:129:1: ( ruleCInt EOF )
            // InternalDeclaration.g:130:1: ruleCInt EOF
            {
             before(grammarAccess.getCIntRule()); 
            pushFollow(FOLLOW_1);
            ruleCInt();

            state._fsp--;

             after(grammarAccess.getCIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCInt"


    // $ANTLR start "ruleCInt"
    // InternalDeclaration.g:137:1: ruleCInt : ( ( rule__CInt__Group__0 ) ) ;
    public final void ruleCInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:141:2: ( ( ( rule__CInt__Group__0 ) ) )
            // InternalDeclaration.g:142:2: ( ( rule__CInt__Group__0 ) )
            {
            // InternalDeclaration.g:142:2: ( ( rule__CInt__Group__0 ) )
            // InternalDeclaration.g:143:3: ( rule__CInt__Group__0 )
            {
             before(grammarAccess.getCIntAccess().getGroup()); 
            // InternalDeclaration.g:144:3: ( rule__CInt__Group__0 )
            // InternalDeclaration.g:144:4: rule__CInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCInt"


    // $ANTLR start "entryRuleCDouble"
    // InternalDeclaration.g:153:1: entryRuleCDouble : ruleCDouble EOF ;
    public final void entryRuleCDouble() throws RecognitionException {
        try {
            // InternalDeclaration.g:154:1: ( ruleCDouble EOF )
            // InternalDeclaration.g:155:1: ruleCDouble EOF
            {
             before(grammarAccess.getCDoubleRule()); 
            pushFollow(FOLLOW_1);
            ruleCDouble();

            state._fsp--;

             after(grammarAccess.getCDoubleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCDouble"


    // $ANTLR start "ruleCDouble"
    // InternalDeclaration.g:162:1: ruleCDouble : ( ( rule__CDouble__Group__0 ) ) ;
    public final void ruleCDouble() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:166:2: ( ( ( rule__CDouble__Group__0 ) ) )
            // InternalDeclaration.g:167:2: ( ( rule__CDouble__Group__0 ) )
            {
            // InternalDeclaration.g:167:2: ( ( rule__CDouble__Group__0 ) )
            // InternalDeclaration.g:168:3: ( rule__CDouble__Group__0 )
            {
             before(grammarAccess.getCDoubleAccess().getGroup()); 
            // InternalDeclaration.g:169:3: ( rule__CDouble__Group__0 )
            // InternalDeclaration.g:169:4: rule__CDouble__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CDouble__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCDoubleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCDouble"


    // $ANTLR start "entryRuleCBoolean"
    // InternalDeclaration.g:178:1: entryRuleCBoolean : ruleCBoolean EOF ;
    public final void entryRuleCBoolean() throws RecognitionException {
        try {
            // InternalDeclaration.g:179:1: ( ruleCBoolean EOF )
            // InternalDeclaration.g:180:1: ruleCBoolean EOF
            {
             before(grammarAccess.getCBooleanRule()); 
            pushFollow(FOLLOW_1);
            ruleCBoolean();

            state._fsp--;

             after(grammarAccess.getCBooleanRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCBoolean"


    // $ANTLR start "ruleCBoolean"
    // InternalDeclaration.g:187:1: ruleCBoolean : ( ( rule__CBoolean__Group__0 ) ) ;
    public final void ruleCBoolean() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:191:2: ( ( ( rule__CBoolean__Group__0 ) ) )
            // InternalDeclaration.g:192:2: ( ( rule__CBoolean__Group__0 ) )
            {
            // InternalDeclaration.g:192:2: ( ( rule__CBoolean__Group__0 ) )
            // InternalDeclaration.g:193:3: ( rule__CBoolean__Group__0 )
            {
             before(grammarAccess.getCBooleanAccess().getGroup()); 
            // InternalDeclaration.g:194:3: ( rule__CBoolean__Group__0 )
            // InternalDeclaration.g:194:4: rule__CBoolean__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCBooleanAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCBoolean"


    // $ANTLR start "entryRuleCString"
    // InternalDeclaration.g:203:1: entryRuleCString : ruleCString EOF ;
    public final void entryRuleCString() throws RecognitionException {
        try {
            // InternalDeclaration.g:204:1: ( ruleCString EOF )
            // InternalDeclaration.g:205:1: ruleCString EOF
            {
             before(grammarAccess.getCStringRule()); 
            pushFollow(FOLLOW_1);
            ruleCString();

            state._fsp--;

             after(grammarAccess.getCStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCString"


    // $ANTLR start "ruleCString"
    // InternalDeclaration.g:212:1: ruleCString : ( ( rule__CString__Group__0 ) ) ;
    public final void ruleCString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:216:2: ( ( ( rule__CString__Group__0 ) ) )
            // InternalDeclaration.g:217:2: ( ( rule__CString__Group__0 ) )
            {
            // InternalDeclaration.g:217:2: ( ( rule__CString__Group__0 ) )
            // InternalDeclaration.g:218:3: ( rule__CString__Group__0 )
            {
             before(grammarAccess.getCStringAccess().getGroup()); 
            // InternalDeclaration.g:219:3: ( rule__CString__Group__0 )
            // InternalDeclaration.g:219:4: rule__CString__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CString__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCStringAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCString"


    // $ANTLR start "entryRuleJavaType"
    // InternalDeclaration.g:228:1: entryRuleJavaType : ruleJavaType EOF ;
    public final void entryRuleJavaType() throws RecognitionException {
        try {
            // InternalDeclaration.g:229:1: ( ruleJavaType EOF )
            // InternalDeclaration.g:230:1: ruleJavaType EOF
            {
             before(grammarAccess.getJavaTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleJavaType();

            state._fsp--;

             after(grammarAccess.getJavaTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleJavaType"


    // $ANTLR start "ruleJavaType"
    // InternalDeclaration.g:237:1: ruleJavaType : ( ( rule__JavaType__Group__0 ) ) ;
    public final void ruleJavaType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:241:2: ( ( ( rule__JavaType__Group__0 ) ) )
            // InternalDeclaration.g:242:2: ( ( rule__JavaType__Group__0 ) )
            {
            // InternalDeclaration.g:242:2: ( ( rule__JavaType__Group__0 ) )
            // InternalDeclaration.g:243:3: ( rule__JavaType__Group__0 )
            {
             before(grammarAccess.getJavaTypeAccess().getGroup()); 
            // InternalDeclaration.g:244:3: ( rule__JavaType__Group__0 )
            // InternalDeclaration.g:244:4: rule__JavaType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__JavaType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getJavaTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleJavaType"


    // $ANTLR start "entryRuleStructType"
    // InternalDeclaration.g:253:1: entryRuleStructType : ruleStructType EOF ;
    public final void entryRuleStructType() throws RecognitionException {
        try {
            // InternalDeclaration.g:254:1: ( ruleStructType EOF )
            // InternalDeclaration.g:255:1: ruleStructType EOF
            {
             before(grammarAccess.getStructTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleStructType();

            state._fsp--;

             after(grammarAccess.getStructTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStructType"


    // $ANTLR start "ruleStructType"
    // InternalDeclaration.g:262:1: ruleStructType : ( ( rule__StructType__Group__0 ) ) ;
    public final void ruleStructType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:266:2: ( ( ( rule__StructType__Group__0 ) ) )
            // InternalDeclaration.g:267:2: ( ( rule__StructType__Group__0 ) )
            {
            // InternalDeclaration.g:267:2: ( ( rule__StructType__Group__0 ) )
            // InternalDeclaration.g:268:3: ( rule__StructType__Group__0 )
            {
             before(grammarAccess.getStructTypeAccess().getGroup()); 
            // InternalDeclaration.g:269:3: ( rule__StructType__Group__0 )
            // InternalDeclaration.g:269:4: rule__StructType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StructType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStructTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStructType"


    // $ANTLR start "entryRuleParameterDeclaration"
    // InternalDeclaration.g:278:1: entryRuleParameterDeclaration : ruleParameterDeclaration EOF ;
    public final void entryRuleParameterDeclaration() throws RecognitionException {
        try {
            // InternalDeclaration.g:279:1: ( ruleParameterDeclaration EOF )
            // InternalDeclaration.g:280:1: ruleParameterDeclaration EOF
            {
             before(grammarAccess.getParameterDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getParameterDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameterDeclaration"


    // $ANTLR start "ruleParameterDeclaration"
    // InternalDeclaration.g:287:1: ruleParameterDeclaration : ( ( rule__ParameterDeclaration__Group__0 ) ) ;
    public final void ruleParameterDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:291:2: ( ( ( rule__ParameterDeclaration__Group__0 ) ) )
            // InternalDeclaration.g:292:2: ( ( rule__ParameterDeclaration__Group__0 ) )
            {
            // InternalDeclaration.g:292:2: ( ( rule__ParameterDeclaration__Group__0 ) )
            // InternalDeclaration.g:293:3: ( rule__ParameterDeclaration__Group__0 )
            {
             before(grammarAccess.getParameterDeclarationAccess().getGroup()); 
            // InternalDeclaration.g:294:3: ( rule__ParameterDeclaration__Group__0 )
            // InternalDeclaration.g:294:4: rule__ParameterDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParameterDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameterDeclaration"


    // $ANTLR start "entryRuleNUMBER"
    // InternalDeclaration.g:303:1: entryRuleNUMBER : ruleNUMBER EOF ;
    public final void entryRuleNUMBER() throws RecognitionException {
        try {
            // InternalDeclaration.g:304:1: ( ruleNUMBER EOF )
            // InternalDeclaration.g:305:1: ruleNUMBER EOF
            {
             before(grammarAccess.getNUMBERRule()); 
            pushFollow(FOLLOW_1);
            ruleNUMBER();

            state._fsp--;

             after(grammarAccess.getNUMBERRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNUMBER"


    // $ANTLR start "ruleNUMBER"
    // InternalDeclaration.g:312:1: ruleNUMBER : ( ( rule__NUMBER__Group__0 ) ) ;
    public final void ruleNUMBER() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:316:2: ( ( ( rule__NUMBER__Group__0 ) ) )
            // InternalDeclaration.g:317:2: ( ( rule__NUMBER__Group__0 ) )
            {
            // InternalDeclaration.g:317:2: ( ( rule__NUMBER__Group__0 ) )
            // InternalDeclaration.g:318:3: ( rule__NUMBER__Group__0 )
            {
             before(grammarAccess.getNUMBERAccess().getGroup()); 
            // InternalDeclaration.g:319:3: ( rule__NUMBER__Group__0 )
            // InternalDeclaration.g:319:4: rule__NUMBER__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NUMBER__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNUMBERAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNUMBER"


    // $ANTLR start "entryRuleInterface"
    // InternalDeclaration.g:328:1: entryRuleInterface : ruleInterface EOF ;
    public final void entryRuleInterface() throws RecognitionException {
        try {
            // InternalDeclaration.g:329:1: ( ruleInterface EOF )
            // InternalDeclaration.g:330:1: ruleInterface EOF
            {
             before(grammarAccess.getInterfaceRule()); 
            pushFollow(FOLLOW_1);
            ruleInterface();

            state._fsp--;

             after(grammarAccess.getInterfaceRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterface"


    // $ANTLR start "ruleInterface"
    // InternalDeclaration.g:337:1: ruleInterface : ( ( rule__Interface__Group__0 ) ) ;
    public final void ruleInterface() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:341:2: ( ( ( rule__Interface__Group__0 ) ) )
            // InternalDeclaration.g:342:2: ( ( rule__Interface__Group__0 ) )
            {
            // InternalDeclaration.g:342:2: ( ( rule__Interface__Group__0 ) )
            // InternalDeclaration.g:343:3: ( rule__Interface__Group__0 )
            {
             before(grammarAccess.getInterfaceAccess().getGroup()); 
            // InternalDeclaration.g:344:3: ( rule__Interface__Group__0 )
            // InternalDeclaration.g:344:4: rule__Interface__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Interface__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterface"


    // $ANTLR start "entryRuleFunction"
    // InternalDeclaration.g:353:1: entryRuleFunction : ruleFunction EOF ;
    public final void entryRuleFunction() throws RecognitionException {
        try {
            // InternalDeclaration.g:354:1: ( ruleFunction EOF )
            // InternalDeclaration.g:355:1: ruleFunction EOF
            {
             before(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getFunctionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalDeclaration.g:362:1: ruleFunction : ( ( rule__Function__Group__0 ) ) ;
    public final void ruleFunction() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:366:2: ( ( ( rule__Function__Group__0 ) ) )
            // InternalDeclaration.g:367:2: ( ( rule__Function__Group__0 ) )
            {
            // InternalDeclaration.g:367:2: ( ( rule__Function__Group__0 ) )
            // InternalDeclaration.g:368:3: ( rule__Function__Group__0 )
            {
             before(grammarAccess.getFunctionAccess().getGroup()); 
            // InternalDeclaration.g:369:3: ( rule__Function__Group__0 )
            // InternalDeclaration.g:369:4: rule__Function__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleReturnType"
    // InternalDeclaration.g:378:1: entryRuleReturnType : ruleReturnType EOF ;
    public final void entryRuleReturnType() throws RecognitionException {
        try {
            // InternalDeclaration.g:379:1: ( ruleReturnType EOF )
            // InternalDeclaration.g:380:1: ruleReturnType EOF
            {
             before(grammarAccess.getReturnTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleReturnType();

            state._fsp--;

             after(grammarAccess.getReturnTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReturnType"


    // $ANTLR start "ruleReturnType"
    // InternalDeclaration.g:387:1: ruleReturnType : ( ( rule__ReturnType__Group__0 ) ) ;
    public final void ruleReturnType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:391:2: ( ( ( rule__ReturnType__Group__0 ) ) )
            // InternalDeclaration.g:392:2: ( ( rule__ReturnType__Group__0 ) )
            {
            // InternalDeclaration.g:392:2: ( ( rule__ReturnType__Group__0 ) )
            // InternalDeclaration.g:393:3: ( rule__ReturnType__Group__0 )
            {
             before(grammarAccess.getReturnTypeAccess().getGroup()); 
            // InternalDeclaration.g:394:3: ( rule__ReturnType__Group__0 )
            // InternalDeclaration.g:394:4: rule__ReturnType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ReturnType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReturnTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReturnType"


    // $ANTLR start "entryRuleInterfacePort"
    // InternalDeclaration.g:403:1: entryRuleInterfacePort : ruleInterfacePort EOF ;
    public final void entryRuleInterfacePort() throws RecognitionException {
        try {
            // InternalDeclaration.g:404:1: ( ruleInterfacePort EOF )
            // InternalDeclaration.g:405:1: ruleInterfacePort EOF
            {
             before(grammarAccess.getInterfacePortRule()); 
            pushFollow(FOLLOW_1);
            ruleInterfacePort();

            state._fsp--;

             after(grammarAccess.getInterfacePortRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInterfacePort"


    // $ANTLR start "ruleInterfacePort"
    // InternalDeclaration.g:412:1: ruleInterfacePort : ( ( rule__InterfacePort__Group__0 ) ) ;
    public final void ruleInterfacePort() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:416:2: ( ( ( rule__InterfacePort__Group__0 ) ) )
            // InternalDeclaration.g:417:2: ( ( rule__InterfacePort__Group__0 ) )
            {
            // InternalDeclaration.g:417:2: ( ( rule__InterfacePort__Group__0 ) )
            // InternalDeclaration.g:418:3: ( rule__InterfacePort__Group__0 )
            {
             before(grammarAccess.getInterfacePortAccess().getGroup()); 
            // InternalDeclaration.g:419:3: ( rule__InterfacePort__Group__0 )
            // InternalDeclaration.g:419:4: rule__InterfacePort__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InterfacePort__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInterfacePortAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInterfacePort"


    // $ANTLR start "entryRuleLeafComponentType"
    // InternalDeclaration.g:428:1: entryRuleLeafComponentType : ruleLeafComponentType EOF ;
    public final void entryRuleLeafComponentType() throws RecognitionException {
        try {
            // InternalDeclaration.g:429:1: ( ruleLeafComponentType EOF )
            // InternalDeclaration.g:430:1: ruleLeafComponentType EOF
            {
             before(grammarAccess.getLeafComponentTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleLeafComponentType();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLeafComponentType"


    // $ANTLR start "ruleLeafComponentType"
    // InternalDeclaration.g:437:1: ruleLeafComponentType : ( ( rule__LeafComponentType__Group__0 ) ) ;
    public final void ruleLeafComponentType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:441:2: ( ( ( rule__LeafComponentType__Group__0 ) ) )
            // InternalDeclaration.g:442:2: ( ( rule__LeafComponentType__Group__0 ) )
            {
            // InternalDeclaration.g:442:2: ( ( rule__LeafComponentType__Group__0 ) )
            // InternalDeclaration.g:443:3: ( rule__LeafComponentType__Group__0 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup()); 
            // InternalDeclaration.g:444:3: ( rule__LeafComponentType__Group__0 )
            // InternalDeclaration.g:444:4: rule__LeafComponentType__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLeafComponentType"


    // $ANTLR start "entryRuleInput"
    // InternalDeclaration.g:453:1: entryRuleInput : ruleInput EOF ;
    public final void entryRuleInput() throws RecognitionException {
        try {
            // InternalDeclaration.g:454:1: ( ruleInput EOF )
            // InternalDeclaration.g:455:1: ruleInput EOF
            {
             before(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_1);
            ruleInput();

            state._fsp--;

             after(grammarAccess.getInputRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // InternalDeclaration.g:462:1: ruleInput : ( ( rule__Input__Group__0 ) ) ;
    public final void ruleInput() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:466:2: ( ( ( rule__Input__Group__0 ) ) )
            // InternalDeclaration.g:467:2: ( ( rule__Input__Group__0 ) )
            {
            // InternalDeclaration.g:467:2: ( ( rule__Input__Group__0 ) )
            // InternalDeclaration.g:468:3: ( rule__Input__Group__0 )
            {
             before(grammarAccess.getInputAccess().getGroup()); 
            // InternalDeclaration.g:469:3: ( rule__Input__Group__0 )
            // InternalDeclaration.g:469:4: rule__Input__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Input__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInputAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "entryRuleOutput"
    // InternalDeclaration.g:478:1: entryRuleOutput : ruleOutput EOF ;
    public final void entryRuleOutput() throws RecognitionException {
        try {
            // InternalDeclaration.g:479:1: ( ruleOutput EOF )
            // InternalDeclaration.g:480:1: ruleOutput EOF
            {
             before(grammarAccess.getOutputRule()); 
            pushFollow(FOLLOW_1);
            ruleOutput();

            state._fsp--;

             after(grammarAccess.getOutputRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOutput"


    // $ANTLR start "ruleOutput"
    // InternalDeclaration.g:487:1: ruleOutput : ( ( rule__Output__Group__0 ) ) ;
    public final void ruleOutput() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:491:2: ( ( ( rule__Output__Group__0 ) ) )
            // InternalDeclaration.g:492:2: ( ( rule__Output__Group__0 ) )
            {
            // InternalDeclaration.g:492:2: ( ( rule__Output__Group__0 ) )
            // InternalDeclaration.g:493:3: ( rule__Output__Group__0 )
            {
             before(grammarAccess.getOutputAccess().getGroup()); 
            // InternalDeclaration.g:494:3: ( rule__Output__Group__0 )
            // InternalDeclaration.g:494:4: rule__Output__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Output__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOutputAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOutput"


    // $ANTLR start "entryRuleEString"
    // InternalDeclaration.g:503:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalDeclaration.g:504:1: ( ruleEString EOF )
            // InternalDeclaration.g:505:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDeclaration.g:512:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:516:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalDeclaration.g:517:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalDeclaration.g:517:2: ( ( rule__EString__Alternatives ) )
            // InternalDeclaration.g:518:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalDeclaration.g:519:3: ( rule__EString__Alternatives )
            // InternalDeclaration.g:519:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalDeclaration.g:527:1: rule__Type__Alternatives : ( ( ruleStructType ) | ( ruleCInt ) | ( ruleCDouble ) | ( ruleCBoolean ) | ( ruleCString ) | ( ruleJavaType ) | ( ruleVoid ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:531:1: ( ( ruleStructType ) | ( ruleCInt ) | ( ruleCDouble ) | ( ruleCBoolean ) | ( ruleCString ) | ( ruleJavaType ) | ( ruleVoid ) )
            int alt1=7;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // InternalDeclaration.g:532:2: ( ruleStructType )
                    {
                    // InternalDeclaration.g:532:2: ( ruleStructType )
                    // InternalDeclaration.g:533:3: ruleStructType
                    {
                     before(grammarAccess.getTypeAccess().getStructTypeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStructType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getStructTypeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDeclaration.g:538:2: ( ruleCInt )
                    {
                    // InternalDeclaration.g:538:2: ( ruleCInt )
                    // InternalDeclaration.g:539:3: ruleCInt
                    {
                     before(grammarAccess.getTypeAccess().getCIntParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCInt();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getCIntParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDeclaration.g:544:2: ( ruleCDouble )
                    {
                    // InternalDeclaration.g:544:2: ( ruleCDouble )
                    // InternalDeclaration.g:545:3: ruleCDouble
                    {
                     before(grammarAccess.getTypeAccess().getCDoubleParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleCDouble();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getCDoubleParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalDeclaration.g:550:2: ( ruleCBoolean )
                    {
                    // InternalDeclaration.g:550:2: ( ruleCBoolean )
                    // InternalDeclaration.g:551:3: ruleCBoolean
                    {
                     before(grammarAccess.getTypeAccess().getCBooleanParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleCBoolean();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getCBooleanParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalDeclaration.g:556:2: ( ruleCString )
                    {
                    // InternalDeclaration.g:556:2: ( ruleCString )
                    // InternalDeclaration.g:557:3: ruleCString
                    {
                     before(grammarAccess.getTypeAccess().getCStringParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleCString();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getCStringParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalDeclaration.g:562:2: ( ruleJavaType )
                    {
                    // InternalDeclaration.g:562:2: ( ruleJavaType )
                    // InternalDeclaration.g:563:3: ruleJavaType
                    {
                     before(grammarAccess.getTypeAccess().getJavaTypeParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleJavaType();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getJavaTypeParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalDeclaration.g:568:2: ( ruleVoid )
                    {
                    // InternalDeclaration.g:568:2: ( ruleVoid )
                    // InternalDeclaration.g:569:3: ruleVoid
                    {
                     before(grammarAccess.getTypeAccess().getVoidParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleVoid();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getVoidParserRuleCall_6()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalDeclaration.g:578:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:582:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==RULE_ID) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalDeclaration.g:583:2: ( RULE_STRING )
                    {
                    // InternalDeclaration.g:583:2: ( RULE_STRING )
                    // InternalDeclaration.g:584:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDeclaration.g:589:2: ( RULE_ID )
                    {
                    // InternalDeclaration.g:589:2: ( RULE_ID )
                    // InternalDeclaration.g:590:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__Declaration__Group__0"
    // InternalDeclaration.g:599:1: rule__Declaration__Group__0 : rule__Declaration__Group__0__Impl rule__Declaration__Group__1 ;
    public final void rule__Declaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:603:1: ( rule__Declaration__Group__0__Impl rule__Declaration__Group__1 )
            // InternalDeclaration.g:604:2: rule__Declaration__Group__0__Impl rule__Declaration__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Declaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__0"


    // $ANTLR start "rule__Declaration__Group__0__Impl"
    // InternalDeclaration.g:611:1: rule__Declaration__Group__0__Impl : ( () ) ;
    public final void rule__Declaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:615:1: ( ( () ) )
            // InternalDeclaration.g:616:1: ( () )
            {
            // InternalDeclaration.g:616:1: ( () )
            // InternalDeclaration.g:617:2: ()
            {
             before(grammarAccess.getDeclarationAccess().getDeclarationAction_0()); 
            // InternalDeclaration.g:618:2: ()
            // InternalDeclaration.g:618:3: 
            {
            }

             after(grammarAccess.getDeclarationAccess().getDeclarationAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__0__Impl"


    // $ANTLR start "rule__Declaration__Group__1"
    // InternalDeclaration.g:626:1: rule__Declaration__Group__1 : rule__Declaration__Group__1__Impl rule__Declaration__Group__2 ;
    public final void rule__Declaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:630:1: ( rule__Declaration__Group__1__Impl rule__Declaration__Group__2 )
            // InternalDeclaration.g:631:2: rule__Declaration__Group__1__Impl rule__Declaration__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Declaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__1"


    // $ANTLR start "rule__Declaration__Group__1__Impl"
    // InternalDeclaration.g:638:1: rule__Declaration__Group__1__Impl : ( 'Declaration' ) ;
    public final void rule__Declaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:642:1: ( ( 'Declaration' ) )
            // InternalDeclaration.g:643:1: ( 'Declaration' )
            {
            // InternalDeclaration.g:643:1: ( 'Declaration' )
            // InternalDeclaration.g:644:2: 'Declaration'
            {
             before(grammarAccess.getDeclarationAccess().getDeclarationKeyword_1()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getDeclarationKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__1__Impl"


    // $ANTLR start "rule__Declaration__Group__2"
    // InternalDeclaration.g:653:1: rule__Declaration__Group__2 : rule__Declaration__Group__2__Impl rule__Declaration__Group__3 ;
    public final void rule__Declaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:657:1: ( rule__Declaration__Group__2__Impl rule__Declaration__Group__3 )
            // InternalDeclaration.g:658:2: rule__Declaration__Group__2__Impl rule__Declaration__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Declaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__2"


    // $ANTLR start "rule__Declaration__Group__2__Impl"
    // InternalDeclaration.g:665:1: rule__Declaration__Group__2__Impl : ( ( rule__Declaration__NameAssignment_2 ) ) ;
    public final void rule__Declaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:669:1: ( ( ( rule__Declaration__NameAssignment_2 ) ) )
            // InternalDeclaration.g:670:1: ( ( rule__Declaration__NameAssignment_2 ) )
            {
            // InternalDeclaration.g:670:1: ( ( rule__Declaration__NameAssignment_2 ) )
            // InternalDeclaration.g:671:2: ( rule__Declaration__NameAssignment_2 )
            {
             before(grammarAccess.getDeclarationAccess().getNameAssignment_2()); 
            // InternalDeclaration.g:672:2: ( rule__Declaration__NameAssignment_2 )
            // InternalDeclaration.g:672:3: rule__Declaration__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__2__Impl"


    // $ANTLR start "rule__Declaration__Group__3"
    // InternalDeclaration.g:680:1: rule__Declaration__Group__3 : rule__Declaration__Group__3__Impl rule__Declaration__Group__4 ;
    public final void rule__Declaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:684:1: ( rule__Declaration__Group__3__Impl rule__Declaration__Group__4 )
            // InternalDeclaration.g:685:2: rule__Declaration__Group__3__Impl rule__Declaration__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Declaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__3"


    // $ANTLR start "rule__Declaration__Group__3__Impl"
    // InternalDeclaration.g:692:1: rule__Declaration__Group__3__Impl : ( '{' ) ;
    public final void rule__Declaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:696:1: ( ( '{' ) )
            // InternalDeclaration.g:697:1: ( '{' )
            {
            // InternalDeclaration.g:697:1: ( '{' )
            // InternalDeclaration.g:698:2: '{'
            {
             before(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__3__Impl"


    // $ANTLR start "rule__Declaration__Group__4"
    // InternalDeclaration.g:707:1: rule__Declaration__Group__4 : rule__Declaration__Group__4__Impl rule__Declaration__Group__5 ;
    public final void rule__Declaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:711:1: ( rule__Declaration__Group__4__Impl rule__Declaration__Group__5 )
            // InternalDeclaration.g:712:2: rule__Declaration__Group__4__Impl rule__Declaration__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Declaration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__4"


    // $ANTLR start "rule__Declaration__Group__4__Impl"
    // InternalDeclaration.g:719:1: rule__Declaration__Group__4__Impl : ( ( rule__Declaration__Group_4__0 )? ) ;
    public final void rule__Declaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:723:1: ( ( ( rule__Declaration__Group_4__0 )? ) )
            // InternalDeclaration.g:724:1: ( ( rule__Declaration__Group_4__0 )? )
            {
            // InternalDeclaration.g:724:1: ( ( rule__Declaration__Group_4__0 )? )
            // InternalDeclaration.g:725:2: ( rule__Declaration__Group_4__0 )?
            {
             before(grammarAccess.getDeclarationAccess().getGroup_4()); 
            // InternalDeclaration.g:726:2: ( rule__Declaration__Group_4__0 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==14) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalDeclaration.g:726:3: rule__Declaration__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Declaration__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeclarationAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__4__Impl"


    // $ANTLR start "rule__Declaration__Group__5"
    // InternalDeclaration.g:734:1: rule__Declaration__Group__5 : rule__Declaration__Group__5__Impl rule__Declaration__Group__6 ;
    public final void rule__Declaration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:738:1: ( rule__Declaration__Group__5__Impl rule__Declaration__Group__6 )
            // InternalDeclaration.g:739:2: rule__Declaration__Group__5__Impl rule__Declaration__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__Declaration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__5"


    // $ANTLR start "rule__Declaration__Group__5__Impl"
    // InternalDeclaration.g:746:1: rule__Declaration__Group__5__Impl : ( ( rule__Declaration__Group_5__0 )? ) ;
    public final void rule__Declaration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:750:1: ( ( ( rule__Declaration__Group_5__0 )? ) )
            // InternalDeclaration.g:751:1: ( ( rule__Declaration__Group_5__0 )? )
            {
            // InternalDeclaration.g:751:1: ( ( rule__Declaration__Group_5__0 )? )
            // InternalDeclaration.g:752:2: ( rule__Declaration__Group_5__0 )?
            {
             before(grammarAccess.getDeclarationAccess().getGroup_5()); 
            // InternalDeclaration.g:753:2: ( rule__Declaration__Group_5__0 )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalDeclaration.g:753:3: rule__Declaration__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Declaration__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeclarationAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__5__Impl"


    // $ANTLR start "rule__Declaration__Group__6"
    // InternalDeclaration.g:761:1: rule__Declaration__Group__6 : rule__Declaration__Group__6__Impl rule__Declaration__Group__7 ;
    public final void rule__Declaration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:765:1: ( rule__Declaration__Group__6__Impl rule__Declaration__Group__7 )
            // InternalDeclaration.g:766:2: rule__Declaration__Group__6__Impl rule__Declaration__Group__7
            {
            pushFollow(FOLLOW_6);
            rule__Declaration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__6"


    // $ANTLR start "rule__Declaration__Group__6__Impl"
    // InternalDeclaration.g:773:1: rule__Declaration__Group__6__Impl : ( ( rule__Declaration__Group_6__0 )? ) ;
    public final void rule__Declaration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:777:1: ( ( ( rule__Declaration__Group_6__0 )? ) )
            // InternalDeclaration.g:778:1: ( ( rule__Declaration__Group_6__0 )? )
            {
            // InternalDeclaration.g:778:1: ( ( rule__Declaration__Group_6__0 )? )
            // InternalDeclaration.g:779:2: ( rule__Declaration__Group_6__0 )?
            {
             before(grammarAccess.getDeclarationAccess().getGroup_6()); 
            // InternalDeclaration.g:780:2: ( rule__Declaration__Group_6__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==16) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalDeclaration.g:780:3: rule__Declaration__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Declaration__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeclarationAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__6__Impl"


    // $ANTLR start "rule__Declaration__Group__7"
    // InternalDeclaration.g:788:1: rule__Declaration__Group__7 : rule__Declaration__Group__7__Impl ;
    public final void rule__Declaration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:792:1: ( rule__Declaration__Group__7__Impl )
            // InternalDeclaration.g:793:2: rule__Declaration__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__7"


    // $ANTLR start "rule__Declaration__Group__7__Impl"
    // InternalDeclaration.g:799:1: rule__Declaration__Group__7__Impl : ( '}' ) ;
    public final void rule__Declaration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:803:1: ( ( '}' ) )
            // InternalDeclaration.g:804:1: ( '}' )
            {
            // InternalDeclaration.g:804:1: ( '}' )
            // InternalDeclaration.g:805:2: '}'
            {
             before(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group__7__Impl"


    // $ANTLR start "rule__Declaration__Group_4__0"
    // InternalDeclaration.g:815:1: rule__Declaration__Group_4__0 : rule__Declaration__Group_4__0__Impl rule__Declaration__Group_4__1 ;
    public final void rule__Declaration__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:819:1: ( rule__Declaration__Group_4__0__Impl rule__Declaration__Group_4__1 )
            // InternalDeclaration.g:820:2: rule__Declaration__Group_4__0__Impl rule__Declaration__Group_4__1
            {
            pushFollow(FOLLOW_5);
            rule__Declaration__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__0"


    // $ANTLR start "rule__Declaration__Group_4__0__Impl"
    // InternalDeclaration.g:827:1: rule__Declaration__Group_4__0__Impl : ( 'types' ) ;
    public final void rule__Declaration__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:831:1: ( ( 'types' ) )
            // InternalDeclaration.g:832:1: ( 'types' )
            {
            // InternalDeclaration.g:832:1: ( 'types' )
            // InternalDeclaration.g:833:2: 'types'
            {
             before(grammarAccess.getDeclarationAccess().getTypesKeyword_4_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getTypesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__0__Impl"


    // $ANTLR start "rule__Declaration__Group_4__1"
    // InternalDeclaration.g:842:1: rule__Declaration__Group_4__1 : rule__Declaration__Group_4__1__Impl rule__Declaration__Group_4__2 ;
    public final void rule__Declaration__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:846:1: ( rule__Declaration__Group_4__1__Impl rule__Declaration__Group_4__2 )
            // InternalDeclaration.g:847:2: rule__Declaration__Group_4__1__Impl rule__Declaration__Group_4__2
            {
            pushFollow(FOLLOW_7);
            rule__Declaration__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__1"


    // $ANTLR start "rule__Declaration__Group_4__1__Impl"
    // InternalDeclaration.g:854:1: rule__Declaration__Group_4__1__Impl : ( '{' ) ;
    public final void rule__Declaration__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:858:1: ( ( '{' ) )
            // InternalDeclaration.g:859:1: ( '{' )
            {
            // InternalDeclaration.g:859:1: ( '{' )
            // InternalDeclaration.g:860:2: '{'
            {
             before(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_4_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__1__Impl"


    // $ANTLR start "rule__Declaration__Group_4__2"
    // InternalDeclaration.g:869:1: rule__Declaration__Group_4__2 : rule__Declaration__Group_4__2__Impl rule__Declaration__Group_4__3 ;
    public final void rule__Declaration__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:873:1: ( rule__Declaration__Group_4__2__Impl rule__Declaration__Group_4__3 )
            // InternalDeclaration.g:874:2: rule__Declaration__Group_4__2__Impl rule__Declaration__Group_4__3
            {
            pushFollow(FOLLOW_8);
            rule__Declaration__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__2"


    // $ANTLR start "rule__Declaration__Group_4__2__Impl"
    // InternalDeclaration.g:881:1: rule__Declaration__Group_4__2__Impl : ( ( rule__Declaration__TypesAssignment_4_2 ) ) ;
    public final void rule__Declaration__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:885:1: ( ( ( rule__Declaration__TypesAssignment_4_2 ) ) )
            // InternalDeclaration.g:886:1: ( ( rule__Declaration__TypesAssignment_4_2 ) )
            {
            // InternalDeclaration.g:886:1: ( ( rule__Declaration__TypesAssignment_4_2 ) )
            // InternalDeclaration.g:887:2: ( rule__Declaration__TypesAssignment_4_2 )
            {
             before(grammarAccess.getDeclarationAccess().getTypesAssignment_4_2()); 
            // InternalDeclaration.g:888:2: ( rule__Declaration__TypesAssignment_4_2 )
            // InternalDeclaration.g:888:3: rule__Declaration__TypesAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__TypesAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getTypesAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__2__Impl"


    // $ANTLR start "rule__Declaration__Group_4__3"
    // InternalDeclaration.g:896:1: rule__Declaration__Group_4__3 : rule__Declaration__Group_4__3__Impl rule__Declaration__Group_4__4 ;
    public final void rule__Declaration__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:900:1: ( rule__Declaration__Group_4__3__Impl rule__Declaration__Group_4__4 )
            // InternalDeclaration.g:901:2: rule__Declaration__Group_4__3__Impl rule__Declaration__Group_4__4
            {
            pushFollow(FOLLOW_8);
            rule__Declaration__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__3"


    // $ANTLR start "rule__Declaration__Group_4__3__Impl"
    // InternalDeclaration.g:908:1: rule__Declaration__Group_4__3__Impl : ( ( rule__Declaration__TypesAssignment_4_3 )* ) ;
    public final void rule__Declaration__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:912:1: ( ( ( rule__Declaration__TypesAssignment_4_3 )* ) )
            // InternalDeclaration.g:913:1: ( ( rule__Declaration__TypesAssignment_4_3 )* )
            {
            // InternalDeclaration.g:913:1: ( ( rule__Declaration__TypesAssignment_4_3 )* )
            // InternalDeclaration.g:914:2: ( rule__Declaration__TypesAssignment_4_3 )*
            {
             before(grammarAccess.getDeclarationAccess().getTypesAssignment_4_3()); 
            // InternalDeclaration.g:915:2: ( rule__Declaration__TypesAssignment_4_3 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)||LA6_0==26) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDeclaration.g:915:3: rule__Declaration__TypesAssignment_4_3
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Declaration__TypesAssignment_4_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getDeclarationAccess().getTypesAssignment_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__3__Impl"


    // $ANTLR start "rule__Declaration__Group_4__4"
    // InternalDeclaration.g:923:1: rule__Declaration__Group_4__4 : rule__Declaration__Group_4__4__Impl ;
    public final void rule__Declaration__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:927:1: ( rule__Declaration__Group_4__4__Impl )
            // InternalDeclaration.g:928:2: rule__Declaration__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__4"


    // $ANTLR start "rule__Declaration__Group_4__4__Impl"
    // InternalDeclaration.g:934:1: rule__Declaration__Group_4__4__Impl : ( '}' ) ;
    public final void rule__Declaration__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:938:1: ( ( '}' ) )
            // InternalDeclaration.g:939:1: ( '}' )
            {
            // InternalDeclaration.g:939:1: ( '}' )
            // InternalDeclaration.g:940:2: '}'
            {
             before(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_4_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_4__4__Impl"


    // $ANTLR start "rule__Declaration__Group_5__0"
    // InternalDeclaration.g:950:1: rule__Declaration__Group_5__0 : rule__Declaration__Group_5__0__Impl rule__Declaration__Group_5__1 ;
    public final void rule__Declaration__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:954:1: ( rule__Declaration__Group_5__0__Impl rule__Declaration__Group_5__1 )
            // InternalDeclaration.g:955:2: rule__Declaration__Group_5__0__Impl rule__Declaration__Group_5__1
            {
            pushFollow(FOLLOW_5);
            rule__Declaration__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__0"


    // $ANTLR start "rule__Declaration__Group_5__0__Impl"
    // InternalDeclaration.g:962:1: rule__Declaration__Group_5__0__Impl : ( 'interfaces' ) ;
    public final void rule__Declaration__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:966:1: ( ( 'interfaces' ) )
            // InternalDeclaration.g:967:1: ( 'interfaces' )
            {
            // InternalDeclaration.g:967:1: ( 'interfaces' )
            // InternalDeclaration.g:968:2: 'interfaces'
            {
             before(grammarAccess.getDeclarationAccess().getInterfacesKeyword_5_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getInterfacesKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__0__Impl"


    // $ANTLR start "rule__Declaration__Group_5__1"
    // InternalDeclaration.g:977:1: rule__Declaration__Group_5__1 : rule__Declaration__Group_5__1__Impl rule__Declaration__Group_5__2 ;
    public final void rule__Declaration__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:981:1: ( rule__Declaration__Group_5__1__Impl rule__Declaration__Group_5__2 )
            // InternalDeclaration.g:982:2: rule__Declaration__Group_5__1__Impl rule__Declaration__Group_5__2
            {
            pushFollow(FOLLOW_10);
            rule__Declaration__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__1"


    // $ANTLR start "rule__Declaration__Group_5__1__Impl"
    // InternalDeclaration.g:989:1: rule__Declaration__Group_5__1__Impl : ( '{' ) ;
    public final void rule__Declaration__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:993:1: ( ( '{' ) )
            // InternalDeclaration.g:994:1: ( '{' )
            {
            // InternalDeclaration.g:994:1: ( '{' )
            // InternalDeclaration.g:995:2: '{'
            {
             before(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_5_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__1__Impl"


    // $ANTLR start "rule__Declaration__Group_5__2"
    // InternalDeclaration.g:1004:1: rule__Declaration__Group_5__2 : rule__Declaration__Group_5__2__Impl rule__Declaration__Group_5__3 ;
    public final void rule__Declaration__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1008:1: ( rule__Declaration__Group_5__2__Impl rule__Declaration__Group_5__3 )
            // InternalDeclaration.g:1009:2: rule__Declaration__Group_5__2__Impl rule__Declaration__Group_5__3
            {
            pushFollow(FOLLOW_11);
            rule__Declaration__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__2"


    // $ANTLR start "rule__Declaration__Group_5__2__Impl"
    // InternalDeclaration.g:1016:1: rule__Declaration__Group_5__2__Impl : ( ( rule__Declaration__InterfacesAssignment_5_2 ) ) ;
    public final void rule__Declaration__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1020:1: ( ( ( rule__Declaration__InterfacesAssignment_5_2 ) ) )
            // InternalDeclaration.g:1021:1: ( ( rule__Declaration__InterfacesAssignment_5_2 ) )
            {
            // InternalDeclaration.g:1021:1: ( ( rule__Declaration__InterfacesAssignment_5_2 ) )
            // InternalDeclaration.g:1022:2: ( rule__Declaration__InterfacesAssignment_5_2 )
            {
             before(grammarAccess.getDeclarationAccess().getInterfacesAssignment_5_2()); 
            // InternalDeclaration.g:1023:2: ( rule__Declaration__InterfacesAssignment_5_2 )
            // InternalDeclaration.g:1023:3: rule__Declaration__InterfacesAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__InterfacesAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getInterfacesAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__2__Impl"


    // $ANTLR start "rule__Declaration__Group_5__3"
    // InternalDeclaration.g:1031:1: rule__Declaration__Group_5__3 : rule__Declaration__Group_5__3__Impl rule__Declaration__Group_5__4 ;
    public final void rule__Declaration__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1035:1: ( rule__Declaration__Group_5__3__Impl rule__Declaration__Group_5__4 )
            // InternalDeclaration.g:1036:2: rule__Declaration__Group_5__3__Impl rule__Declaration__Group_5__4
            {
            pushFollow(FOLLOW_11);
            rule__Declaration__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__3"


    // $ANTLR start "rule__Declaration__Group_5__3__Impl"
    // InternalDeclaration.g:1043:1: rule__Declaration__Group_5__3__Impl : ( ( rule__Declaration__InterfacesAssignment_5_3 )* ) ;
    public final void rule__Declaration__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1047:1: ( ( ( rule__Declaration__InterfacesAssignment_5_3 )* ) )
            // InternalDeclaration.g:1048:1: ( ( rule__Declaration__InterfacesAssignment_5_3 )* )
            {
            // InternalDeclaration.g:1048:1: ( ( rule__Declaration__InterfacesAssignment_5_3 )* )
            // InternalDeclaration.g:1049:2: ( rule__Declaration__InterfacesAssignment_5_3 )*
            {
             before(grammarAccess.getDeclarationAccess().getInterfacesAssignment_5_3()); 
            // InternalDeclaration.g:1050:2: ( rule__Declaration__InterfacesAssignment_5_3 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==31) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalDeclaration.g:1050:3: rule__Declaration__InterfacesAssignment_5_3
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Declaration__InterfacesAssignment_5_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getDeclarationAccess().getInterfacesAssignment_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__3__Impl"


    // $ANTLR start "rule__Declaration__Group_5__4"
    // InternalDeclaration.g:1058:1: rule__Declaration__Group_5__4 : rule__Declaration__Group_5__4__Impl ;
    public final void rule__Declaration__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1062:1: ( rule__Declaration__Group_5__4__Impl )
            // InternalDeclaration.g:1063:2: rule__Declaration__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__4"


    // $ANTLR start "rule__Declaration__Group_5__4__Impl"
    // InternalDeclaration.g:1069:1: rule__Declaration__Group_5__4__Impl : ( '}' ) ;
    public final void rule__Declaration__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1073:1: ( ( '}' ) )
            // InternalDeclaration.g:1074:1: ( '}' )
            {
            // InternalDeclaration.g:1074:1: ( '}' )
            // InternalDeclaration.g:1075:2: '}'
            {
             before(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_5_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_5__4__Impl"


    // $ANTLR start "rule__Declaration__Group_6__0"
    // InternalDeclaration.g:1085:1: rule__Declaration__Group_6__0 : rule__Declaration__Group_6__0__Impl rule__Declaration__Group_6__1 ;
    public final void rule__Declaration__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1089:1: ( rule__Declaration__Group_6__0__Impl rule__Declaration__Group_6__1 )
            // InternalDeclaration.g:1090:2: rule__Declaration__Group_6__0__Impl rule__Declaration__Group_6__1
            {
            pushFollow(FOLLOW_5);
            rule__Declaration__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__0"


    // $ANTLR start "rule__Declaration__Group_6__0__Impl"
    // InternalDeclaration.g:1097:1: rule__Declaration__Group_6__0__Impl : ( 'leafComponentTypes' ) ;
    public final void rule__Declaration__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1101:1: ( ( 'leafComponentTypes' ) )
            // InternalDeclaration.g:1102:1: ( 'leafComponentTypes' )
            {
            // InternalDeclaration.g:1102:1: ( 'leafComponentTypes' )
            // InternalDeclaration.g:1103:2: 'leafComponentTypes'
            {
             before(grammarAccess.getDeclarationAccess().getLeafComponentTypesKeyword_6_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getLeafComponentTypesKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__0__Impl"


    // $ANTLR start "rule__Declaration__Group_6__1"
    // InternalDeclaration.g:1112:1: rule__Declaration__Group_6__1 : rule__Declaration__Group_6__1__Impl rule__Declaration__Group_6__2 ;
    public final void rule__Declaration__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1116:1: ( rule__Declaration__Group_6__1__Impl rule__Declaration__Group_6__2 )
            // InternalDeclaration.g:1117:2: rule__Declaration__Group_6__1__Impl rule__Declaration__Group_6__2
            {
            pushFollow(FOLLOW_13);
            rule__Declaration__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__1"


    // $ANTLR start "rule__Declaration__Group_6__1__Impl"
    // InternalDeclaration.g:1124:1: rule__Declaration__Group_6__1__Impl : ( '{' ) ;
    public final void rule__Declaration__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1128:1: ( ( '{' ) )
            // InternalDeclaration.g:1129:1: ( '{' )
            {
            // InternalDeclaration.g:1129:1: ( '{' )
            // InternalDeclaration.g:1130:2: '{'
            {
             before(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_6_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__1__Impl"


    // $ANTLR start "rule__Declaration__Group_6__2"
    // InternalDeclaration.g:1139:1: rule__Declaration__Group_6__2 : rule__Declaration__Group_6__2__Impl rule__Declaration__Group_6__3 ;
    public final void rule__Declaration__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1143:1: ( rule__Declaration__Group_6__2__Impl rule__Declaration__Group_6__3 )
            // InternalDeclaration.g:1144:2: rule__Declaration__Group_6__2__Impl rule__Declaration__Group_6__3
            {
            pushFollow(FOLLOW_14);
            rule__Declaration__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__2"


    // $ANTLR start "rule__Declaration__Group_6__2__Impl"
    // InternalDeclaration.g:1151:1: rule__Declaration__Group_6__2__Impl : ( ( rule__Declaration__LeafComponentTypesAssignment_6_2 ) ) ;
    public final void rule__Declaration__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1155:1: ( ( ( rule__Declaration__LeafComponentTypesAssignment_6_2 ) ) )
            // InternalDeclaration.g:1156:1: ( ( rule__Declaration__LeafComponentTypesAssignment_6_2 ) )
            {
            // InternalDeclaration.g:1156:1: ( ( rule__Declaration__LeafComponentTypesAssignment_6_2 ) )
            // InternalDeclaration.g:1157:2: ( rule__Declaration__LeafComponentTypesAssignment_6_2 )
            {
             before(grammarAccess.getDeclarationAccess().getLeafComponentTypesAssignment_6_2()); 
            // InternalDeclaration.g:1158:2: ( rule__Declaration__LeafComponentTypesAssignment_6_2 )
            // InternalDeclaration.g:1158:3: rule__Declaration__LeafComponentTypesAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__LeafComponentTypesAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationAccess().getLeafComponentTypesAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__2__Impl"


    // $ANTLR start "rule__Declaration__Group_6__3"
    // InternalDeclaration.g:1166:1: rule__Declaration__Group_6__3 : rule__Declaration__Group_6__3__Impl rule__Declaration__Group_6__4 ;
    public final void rule__Declaration__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1170:1: ( rule__Declaration__Group_6__3__Impl rule__Declaration__Group_6__4 )
            // InternalDeclaration.g:1171:2: rule__Declaration__Group_6__3__Impl rule__Declaration__Group_6__4
            {
            pushFollow(FOLLOW_14);
            rule__Declaration__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Declaration__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__3"


    // $ANTLR start "rule__Declaration__Group_6__3__Impl"
    // InternalDeclaration.g:1178:1: rule__Declaration__Group_6__3__Impl : ( ( rule__Declaration__LeafComponentTypesAssignment_6_3 )* ) ;
    public final void rule__Declaration__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1182:1: ( ( ( rule__Declaration__LeafComponentTypesAssignment_6_3 )* ) )
            // InternalDeclaration.g:1183:1: ( ( rule__Declaration__LeafComponentTypesAssignment_6_3 )* )
            {
            // InternalDeclaration.g:1183:1: ( ( rule__Declaration__LeafComponentTypesAssignment_6_3 )* )
            // InternalDeclaration.g:1184:2: ( rule__Declaration__LeafComponentTypesAssignment_6_3 )*
            {
             before(grammarAccess.getDeclarationAccess().getLeafComponentTypesAssignment_6_3()); 
            // InternalDeclaration.g:1185:2: ( rule__Declaration__LeafComponentTypesAssignment_6_3 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==32) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalDeclaration.g:1185:3: rule__Declaration__LeafComponentTypesAssignment_6_3
            	    {
            	    pushFollow(FOLLOW_15);
            	    rule__Declaration__LeafComponentTypesAssignment_6_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getDeclarationAccess().getLeafComponentTypesAssignment_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__3__Impl"


    // $ANTLR start "rule__Declaration__Group_6__4"
    // InternalDeclaration.g:1193:1: rule__Declaration__Group_6__4 : rule__Declaration__Group_6__4__Impl ;
    public final void rule__Declaration__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1197:1: ( rule__Declaration__Group_6__4__Impl )
            // InternalDeclaration.g:1198:2: rule__Declaration__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Declaration__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__4"


    // $ANTLR start "rule__Declaration__Group_6__4__Impl"
    // InternalDeclaration.g:1204:1: rule__Declaration__Group_6__4__Impl : ( '}' ) ;
    public final void rule__Declaration__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1208:1: ( ( '}' ) )
            // InternalDeclaration.g:1209:1: ( '}' )
            {
            // InternalDeclaration.g:1209:1: ( '}' )
            // InternalDeclaration.g:1210:2: '}'
            {
             before(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_6_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__Group_6__4__Impl"


    // $ANTLR start "rule__Void__Group__0"
    // InternalDeclaration.g:1220:1: rule__Void__Group__0 : rule__Void__Group__0__Impl rule__Void__Group__1 ;
    public final void rule__Void__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1224:1: ( rule__Void__Group__0__Impl rule__Void__Group__1 )
            // InternalDeclaration.g:1225:2: rule__Void__Group__0__Impl rule__Void__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Void__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Void__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__0"


    // $ANTLR start "rule__Void__Group__0__Impl"
    // InternalDeclaration.g:1232:1: rule__Void__Group__0__Impl : ( () ) ;
    public final void rule__Void__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1236:1: ( ( () ) )
            // InternalDeclaration.g:1237:1: ( () )
            {
            // InternalDeclaration.g:1237:1: ( () )
            // InternalDeclaration.g:1238:2: ()
            {
             before(grammarAccess.getVoidAccess().getVoidAction_0()); 
            // InternalDeclaration.g:1239:2: ()
            // InternalDeclaration.g:1239:3: 
            {
            }

             after(grammarAccess.getVoidAccess().getVoidAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__0__Impl"


    // $ANTLR start "rule__Void__Group__1"
    // InternalDeclaration.g:1247:1: rule__Void__Group__1 : rule__Void__Group__1__Impl rule__Void__Group__2 ;
    public final void rule__Void__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1251:1: ( rule__Void__Group__1__Impl rule__Void__Group__2 )
            // InternalDeclaration.g:1252:2: rule__Void__Group__1__Impl rule__Void__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Void__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Void__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__1"


    // $ANTLR start "rule__Void__Group__1__Impl"
    // InternalDeclaration.g:1259:1: rule__Void__Group__1__Impl : ( ( rule__Void__NameAssignment_1 ) ) ;
    public final void rule__Void__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1263:1: ( ( ( rule__Void__NameAssignment_1 ) ) )
            // InternalDeclaration.g:1264:1: ( ( rule__Void__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:1264:1: ( ( rule__Void__NameAssignment_1 ) )
            // InternalDeclaration.g:1265:2: ( rule__Void__NameAssignment_1 )
            {
             before(grammarAccess.getVoidAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:1266:2: ( rule__Void__NameAssignment_1 )
            // InternalDeclaration.g:1266:3: rule__Void__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Void__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVoidAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__1__Impl"


    // $ANTLR start "rule__Void__Group__2"
    // InternalDeclaration.g:1274:1: rule__Void__Group__2 : rule__Void__Group__2__Impl rule__Void__Group__3 ;
    public final void rule__Void__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1278:1: ( rule__Void__Group__2__Impl rule__Void__Group__3 )
            // InternalDeclaration.g:1279:2: rule__Void__Group__2__Impl rule__Void__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__Void__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Void__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__2"


    // $ANTLR start "rule__Void__Group__2__Impl"
    // InternalDeclaration.g:1286:1: rule__Void__Group__2__Impl : ( '(' ) ;
    public final void rule__Void__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1290:1: ( ( '(' ) )
            // InternalDeclaration.g:1291:1: ( '(' )
            {
            // InternalDeclaration.g:1291:1: ( '(' )
            // InternalDeclaration.g:1292:2: '('
            {
             before(grammarAccess.getVoidAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getVoidAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__2__Impl"


    // $ANTLR start "rule__Void__Group__3"
    // InternalDeclaration.g:1301:1: rule__Void__Group__3 : rule__Void__Group__3__Impl rule__Void__Group__4 ;
    public final void rule__Void__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1305:1: ( rule__Void__Group__3__Impl rule__Void__Group__4 )
            // InternalDeclaration.g:1306:2: rule__Void__Group__3__Impl rule__Void__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__Void__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Void__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__3"


    // $ANTLR start "rule__Void__Group__3__Impl"
    // InternalDeclaration.g:1313:1: rule__Void__Group__3__Impl : ( 'Void' ) ;
    public final void rule__Void__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1317:1: ( ( 'Void' ) )
            // InternalDeclaration.g:1318:1: ( 'Void' )
            {
            // InternalDeclaration.g:1318:1: ( 'Void' )
            // InternalDeclaration.g:1319:2: 'Void'
            {
             before(grammarAccess.getVoidAccess().getVoidKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getVoidAccess().getVoidKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__3__Impl"


    // $ANTLR start "rule__Void__Group__4"
    // InternalDeclaration.g:1328:1: rule__Void__Group__4 : rule__Void__Group__4__Impl rule__Void__Group__5 ;
    public final void rule__Void__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1332:1: ( rule__Void__Group__4__Impl rule__Void__Group__5 )
            // InternalDeclaration.g:1333:2: rule__Void__Group__4__Impl rule__Void__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__Void__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Void__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__4"


    // $ANTLR start "rule__Void__Group__4__Impl"
    // InternalDeclaration.g:1340:1: rule__Void__Group__4__Impl : ( ')' ) ;
    public final void rule__Void__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1344:1: ( ( ')' ) )
            // InternalDeclaration.g:1345:1: ( ')' )
            {
            // InternalDeclaration.g:1345:1: ( ')' )
            // InternalDeclaration.g:1346:2: ')'
            {
             before(grammarAccess.getVoidAccess().getRightParenthesisKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getVoidAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__4__Impl"


    // $ANTLR start "rule__Void__Group__5"
    // InternalDeclaration.g:1355:1: rule__Void__Group__5 : rule__Void__Group__5__Impl ;
    public final void rule__Void__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1359:1: ( rule__Void__Group__5__Impl )
            // InternalDeclaration.g:1360:2: rule__Void__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Void__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__5"


    // $ANTLR start "rule__Void__Group__5__Impl"
    // InternalDeclaration.g:1366:1: rule__Void__Group__5__Impl : ( ';' ) ;
    public final void rule__Void__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1370:1: ( ( ';' ) )
            // InternalDeclaration.g:1371:1: ( ';' )
            {
            // InternalDeclaration.g:1371:1: ( ';' )
            // InternalDeclaration.g:1372:2: ';'
            {
             before(grammarAccess.getVoidAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getVoidAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__Group__5__Impl"


    // $ANTLR start "rule__CInt__Group__0"
    // InternalDeclaration.g:1382:1: rule__CInt__Group__0 : rule__CInt__Group__0__Impl rule__CInt__Group__1 ;
    public final void rule__CInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1386:1: ( rule__CInt__Group__0__Impl rule__CInt__Group__1 )
            // InternalDeclaration.g:1387:2: rule__CInt__Group__0__Impl rule__CInt__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__CInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__0"


    // $ANTLR start "rule__CInt__Group__0__Impl"
    // InternalDeclaration.g:1394:1: rule__CInt__Group__0__Impl : ( () ) ;
    public final void rule__CInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1398:1: ( ( () ) )
            // InternalDeclaration.g:1399:1: ( () )
            {
            // InternalDeclaration.g:1399:1: ( () )
            // InternalDeclaration.g:1400:2: ()
            {
             before(grammarAccess.getCIntAccess().getCIntAction_0()); 
            // InternalDeclaration.g:1401:2: ()
            // InternalDeclaration.g:1401:3: 
            {
            }

             after(grammarAccess.getCIntAccess().getCIntAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__0__Impl"


    // $ANTLR start "rule__CInt__Group__1"
    // InternalDeclaration.g:1409:1: rule__CInt__Group__1 : rule__CInt__Group__1__Impl rule__CInt__Group__2 ;
    public final void rule__CInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1413:1: ( rule__CInt__Group__1__Impl rule__CInt__Group__2 )
            // InternalDeclaration.g:1414:2: rule__CInt__Group__1__Impl rule__CInt__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__CInt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CInt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__1"


    // $ANTLR start "rule__CInt__Group__1__Impl"
    // InternalDeclaration.g:1421:1: rule__CInt__Group__1__Impl : ( ( rule__CInt__NameAssignment_1 ) ) ;
    public final void rule__CInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1425:1: ( ( ( rule__CInt__NameAssignment_1 ) ) )
            // InternalDeclaration.g:1426:1: ( ( rule__CInt__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:1426:1: ( ( rule__CInt__NameAssignment_1 ) )
            // InternalDeclaration.g:1427:2: ( rule__CInt__NameAssignment_1 )
            {
             before(grammarAccess.getCIntAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:1428:2: ( rule__CInt__NameAssignment_1 )
            // InternalDeclaration.g:1428:3: rule__CInt__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CInt__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCIntAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__1__Impl"


    // $ANTLR start "rule__CInt__Group__2"
    // InternalDeclaration.g:1436:1: rule__CInt__Group__2 : rule__CInt__Group__2__Impl rule__CInt__Group__3 ;
    public final void rule__CInt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1440:1: ( rule__CInt__Group__2__Impl rule__CInt__Group__3 )
            // InternalDeclaration.g:1441:2: rule__CInt__Group__2__Impl rule__CInt__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__CInt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CInt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__2"


    // $ANTLR start "rule__CInt__Group__2__Impl"
    // InternalDeclaration.g:1448:1: rule__CInt__Group__2__Impl : ( '(' ) ;
    public final void rule__CInt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1452:1: ( ( '(' ) )
            // InternalDeclaration.g:1453:1: ( '(' )
            {
            // InternalDeclaration.g:1453:1: ( '(' )
            // InternalDeclaration.g:1454:2: '('
            {
             before(grammarAccess.getCIntAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCIntAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__2__Impl"


    // $ANTLR start "rule__CInt__Group__3"
    // InternalDeclaration.g:1463:1: rule__CInt__Group__3 : rule__CInt__Group__3__Impl rule__CInt__Group__4 ;
    public final void rule__CInt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1467:1: ( rule__CInt__Group__3__Impl rule__CInt__Group__4 )
            // InternalDeclaration.g:1468:2: rule__CInt__Group__3__Impl rule__CInt__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__CInt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CInt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__3"


    // $ANTLR start "rule__CInt__Group__3__Impl"
    // InternalDeclaration.g:1475:1: rule__CInt__Group__3__Impl : ( 'CInt' ) ;
    public final void rule__CInt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1479:1: ( ( 'CInt' ) )
            // InternalDeclaration.g:1480:1: ( 'CInt' )
            {
            // InternalDeclaration.g:1480:1: ( 'CInt' )
            // InternalDeclaration.g:1481:2: 'CInt'
            {
             before(grammarAccess.getCIntAccess().getCIntKeyword_3()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getCIntAccess().getCIntKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__3__Impl"


    // $ANTLR start "rule__CInt__Group__4"
    // InternalDeclaration.g:1490:1: rule__CInt__Group__4 : rule__CInt__Group__4__Impl rule__CInt__Group__5 ;
    public final void rule__CInt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1494:1: ( rule__CInt__Group__4__Impl rule__CInt__Group__5 )
            // InternalDeclaration.g:1495:2: rule__CInt__Group__4__Impl rule__CInt__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__CInt__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CInt__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__4"


    // $ANTLR start "rule__CInt__Group__4__Impl"
    // InternalDeclaration.g:1502:1: rule__CInt__Group__4__Impl : ( ')' ) ;
    public final void rule__CInt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1506:1: ( ( ')' ) )
            // InternalDeclaration.g:1507:1: ( ')' )
            {
            // InternalDeclaration.g:1507:1: ( ')' )
            // InternalDeclaration.g:1508:2: ')'
            {
             before(grammarAccess.getCIntAccess().getRightParenthesisKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCIntAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__4__Impl"


    // $ANTLR start "rule__CInt__Group__5"
    // InternalDeclaration.g:1517:1: rule__CInt__Group__5 : rule__CInt__Group__5__Impl ;
    public final void rule__CInt__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1521:1: ( rule__CInt__Group__5__Impl )
            // InternalDeclaration.g:1522:2: rule__CInt__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CInt__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__5"


    // $ANTLR start "rule__CInt__Group__5__Impl"
    // InternalDeclaration.g:1528:1: rule__CInt__Group__5__Impl : ( ';' ) ;
    public final void rule__CInt__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1532:1: ( ( ';' ) )
            // InternalDeclaration.g:1533:1: ( ';' )
            {
            // InternalDeclaration.g:1533:1: ( ';' )
            // InternalDeclaration.g:1534:2: ';'
            {
             before(grammarAccess.getCIntAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCIntAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__Group__5__Impl"


    // $ANTLR start "rule__CDouble__Group__0"
    // InternalDeclaration.g:1544:1: rule__CDouble__Group__0 : rule__CDouble__Group__0__Impl rule__CDouble__Group__1 ;
    public final void rule__CDouble__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1548:1: ( rule__CDouble__Group__0__Impl rule__CDouble__Group__1 )
            // InternalDeclaration.g:1549:2: rule__CDouble__Group__0__Impl rule__CDouble__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__CDouble__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CDouble__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__0"


    // $ANTLR start "rule__CDouble__Group__0__Impl"
    // InternalDeclaration.g:1556:1: rule__CDouble__Group__0__Impl : ( () ) ;
    public final void rule__CDouble__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1560:1: ( ( () ) )
            // InternalDeclaration.g:1561:1: ( () )
            {
            // InternalDeclaration.g:1561:1: ( () )
            // InternalDeclaration.g:1562:2: ()
            {
             before(grammarAccess.getCDoubleAccess().getCDoubleAction_0()); 
            // InternalDeclaration.g:1563:2: ()
            // InternalDeclaration.g:1563:3: 
            {
            }

             after(grammarAccess.getCDoubleAccess().getCDoubleAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__0__Impl"


    // $ANTLR start "rule__CDouble__Group__1"
    // InternalDeclaration.g:1571:1: rule__CDouble__Group__1 : rule__CDouble__Group__1__Impl rule__CDouble__Group__2 ;
    public final void rule__CDouble__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1575:1: ( rule__CDouble__Group__1__Impl rule__CDouble__Group__2 )
            // InternalDeclaration.g:1576:2: rule__CDouble__Group__1__Impl rule__CDouble__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__CDouble__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CDouble__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__1"


    // $ANTLR start "rule__CDouble__Group__1__Impl"
    // InternalDeclaration.g:1583:1: rule__CDouble__Group__1__Impl : ( ( rule__CDouble__NameAssignment_1 ) ) ;
    public final void rule__CDouble__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1587:1: ( ( ( rule__CDouble__NameAssignment_1 ) ) )
            // InternalDeclaration.g:1588:1: ( ( rule__CDouble__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:1588:1: ( ( rule__CDouble__NameAssignment_1 ) )
            // InternalDeclaration.g:1589:2: ( rule__CDouble__NameAssignment_1 )
            {
             before(grammarAccess.getCDoubleAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:1590:2: ( rule__CDouble__NameAssignment_1 )
            // InternalDeclaration.g:1590:3: rule__CDouble__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CDouble__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCDoubleAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__1__Impl"


    // $ANTLR start "rule__CDouble__Group__2"
    // InternalDeclaration.g:1598:1: rule__CDouble__Group__2 : rule__CDouble__Group__2__Impl rule__CDouble__Group__3 ;
    public final void rule__CDouble__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1602:1: ( rule__CDouble__Group__2__Impl rule__CDouble__Group__3 )
            // InternalDeclaration.g:1603:2: rule__CDouble__Group__2__Impl rule__CDouble__Group__3
            {
            pushFollow(FOLLOW_21);
            rule__CDouble__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CDouble__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__2"


    // $ANTLR start "rule__CDouble__Group__2__Impl"
    // InternalDeclaration.g:1610:1: rule__CDouble__Group__2__Impl : ( '(' ) ;
    public final void rule__CDouble__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1614:1: ( ( '(' ) )
            // InternalDeclaration.g:1615:1: ( '(' )
            {
            // InternalDeclaration.g:1615:1: ( '(' )
            // InternalDeclaration.g:1616:2: '('
            {
             before(grammarAccess.getCDoubleAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCDoubleAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__2__Impl"


    // $ANTLR start "rule__CDouble__Group__3"
    // InternalDeclaration.g:1625:1: rule__CDouble__Group__3 : rule__CDouble__Group__3__Impl rule__CDouble__Group__4 ;
    public final void rule__CDouble__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1629:1: ( rule__CDouble__Group__3__Impl rule__CDouble__Group__4 )
            // InternalDeclaration.g:1630:2: rule__CDouble__Group__3__Impl rule__CDouble__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__CDouble__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CDouble__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__3"


    // $ANTLR start "rule__CDouble__Group__3__Impl"
    // InternalDeclaration.g:1637:1: rule__CDouble__Group__3__Impl : ( 'CDouble' ) ;
    public final void rule__CDouble__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1641:1: ( ( 'CDouble' ) )
            // InternalDeclaration.g:1642:1: ( 'CDouble' )
            {
            // InternalDeclaration.g:1642:1: ( 'CDouble' )
            // InternalDeclaration.g:1643:2: 'CDouble'
            {
             before(grammarAccess.getCDoubleAccess().getCDoubleKeyword_3()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCDoubleAccess().getCDoubleKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__3__Impl"


    // $ANTLR start "rule__CDouble__Group__4"
    // InternalDeclaration.g:1652:1: rule__CDouble__Group__4 : rule__CDouble__Group__4__Impl rule__CDouble__Group__5 ;
    public final void rule__CDouble__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1656:1: ( rule__CDouble__Group__4__Impl rule__CDouble__Group__5 )
            // InternalDeclaration.g:1657:2: rule__CDouble__Group__4__Impl rule__CDouble__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__CDouble__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CDouble__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__4"


    // $ANTLR start "rule__CDouble__Group__4__Impl"
    // InternalDeclaration.g:1664:1: rule__CDouble__Group__4__Impl : ( ')' ) ;
    public final void rule__CDouble__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1668:1: ( ( ')' ) )
            // InternalDeclaration.g:1669:1: ( ')' )
            {
            // InternalDeclaration.g:1669:1: ( ')' )
            // InternalDeclaration.g:1670:2: ')'
            {
             before(grammarAccess.getCDoubleAccess().getRightParenthesisKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCDoubleAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__4__Impl"


    // $ANTLR start "rule__CDouble__Group__5"
    // InternalDeclaration.g:1679:1: rule__CDouble__Group__5 : rule__CDouble__Group__5__Impl ;
    public final void rule__CDouble__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1683:1: ( rule__CDouble__Group__5__Impl )
            // InternalDeclaration.g:1684:2: rule__CDouble__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CDouble__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__5"


    // $ANTLR start "rule__CDouble__Group__5__Impl"
    // InternalDeclaration.g:1690:1: rule__CDouble__Group__5__Impl : ( ';' ) ;
    public final void rule__CDouble__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1694:1: ( ( ';' ) )
            // InternalDeclaration.g:1695:1: ( ';' )
            {
            // InternalDeclaration.g:1695:1: ( ';' )
            // InternalDeclaration.g:1696:2: ';'
            {
             before(grammarAccess.getCDoubleAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCDoubleAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__Group__5__Impl"


    // $ANTLR start "rule__CBoolean__Group__0"
    // InternalDeclaration.g:1706:1: rule__CBoolean__Group__0 : rule__CBoolean__Group__0__Impl rule__CBoolean__Group__1 ;
    public final void rule__CBoolean__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1710:1: ( rule__CBoolean__Group__0__Impl rule__CBoolean__Group__1 )
            // InternalDeclaration.g:1711:2: rule__CBoolean__Group__0__Impl rule__CBoolean__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__CBoolean__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__0"


    // $ANTLR start "rule__CBoolean__Group__0__Impl"
    // InternalDeclaration.g:1718:1: rule__CBoolean__Group__0__Impl : ( () ) ;
    public final void rule__CBoolean__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1722:1: ( ( () ) )
            // InternalDeclaration.g:1723:1: ( () )
            {
            // InternalDeclaration.g:1723:1: ( () )
            // InternalDeclaration.g:1724:2: ()
            {
             before(grammarAccess.getCBooleanAccess().getCBooleanAction_0()); 
            // InternalDeclaration.g:1725:2: ()
            // InternalDeclaration.g:1725:3: 
            {
            }

             after(grammarAccess.getCBooleanAccess().getCBooleanAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__0__Impl"


    // $ANTLR start "rule__CBoolean__Group__1"
    // InternalDeclaration.g:1733:1: rule__CBoolean__Group__1 : rule__CBoolean__Group__1__Impl rule__CBoolean__Group__2 ;
    public final void rule__CBoolean__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1737:1: ( rule__CBoolean__Group__1__Impl rule__CBoolean__Group__2 )
            // InternalDeclaration.g:1738:2: rule__CBoolean__Group__1__Impl rule__CBoolean__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__CBoolean__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__1"


    // $ANTLR start "rule__CBoolean__Group__1__Impl"
    // InternalDeclaration.g:1745:1: rule__CBoolean__Group__1__Impl : ( ( rule__CBoolean__NameAssignment_1 ) ) ;
    public final void rule__CBoolean__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1749:1: ( ( ( rule__CBoolean__NameAssignment_1 ) ) )
            // InternalDeclaration.g:1750:1: ( ( rule__CBoolean__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:1750:1: ( ( rule__CBoolean__NameAssignment_1 ) )
            // InternalDeclaration.g:1751:2: ( rule__CBoolean__NameAssignment_1 )
            {
             before(grammarAccess.getCBooleanAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:1752:2: ( rule__CBoolean__NameAssignment_1 )
            // InternalDeclaration.g:1752:3: rule__CBoolean__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CBoolean__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCBooleanAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__1__Impl"


    // $ANTLR start "rule__CBoolean__Group__2"
    // InternalDeclaration.g:1760:1: rule__CBoolean__Group__2 : rule__CBoolean__Group__2__Impl rule__CBoolean__Group__3 ;
    public final void rule__CBoolean__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1764:1: ( rule__CBoolean__Group__2__Impl rule__CBoolean__Group__3 )
            // InternalDeclaration.g:1765:2: rule__CBoolean__Group__2__Impl rule__CBoolean__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__CBoolean__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__2"


    // $ANTLR start "rule__CBoolean__Group__2__Impl"
    // InternalDeclaration.g:1772:1: rule__CBoolean__Group__2__Impl : ( '(' ) ;
    public final void rule__CBoolean__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1776:1: ( ( '(' ) )
            // InternalDeclaration.g:1777:1: ( '(' )
            {
            // InternalDeclaration.g:1777:1: ( '(' )
            // InternalDeclaration.g:1778:2: '('
            {
             before(grammarAccess.getCBooleanAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCBooleanAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__2__Impl"


    // $ANTLR start "rule__CBoolean__Group__3"
    // InternalDeclaration.g:1787:1: rule__CBoolean__Group__3 : rule__CBoolean__Group__3__Impl rule__CBoolean__Group__4 ;
    public final void rule__CBoolean__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1791:1: ( rule__CBoolean__Group__3__Impl rule__CBoolean__Group__4 )
            // InternalDeclaration.g:1792:2: rule__CBoolean__Group__3__Impl rule__CBoolean__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__CBoolean__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__3"


    // $ANTLR start "rule__CBoolean__Group__3__Impl"
    // InternalDeclaration.g:1799:1: rule__CBoolean__Group__3__Impl : ( 'CBoolean' ) ;
    public final void rule__CBoolean__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1803:1: ( ( 'CBoolean' ) )
            // InternalDeclaration.g:1804:1: ( 'CBoolean' )
            {
            // InternalDeclaration.g:1804:1: ( 'CBoolean' )
            // InternalDeclaration.g:1805:2: 'CBoolean'
            {
             before(grammarAccess.getCBooleanAccess().getCBooleanKeyword_3()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getCBooleanAccess().getCBooleanKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__3__Impl"


    // $ANTLR start "rule__CBoolean__Group__4"
    // InternalDeclaration.g:1814:1: rule__CBoolean__Group__4 : rule__CBoolean__Group__4__Impl rule__CBoolean__Group__5 ;
    public final void rule__CBoolean__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1818:1: ( rule__CBoolean__Group__4__Impl rule__CBoolean__Group__5 )
            // InternalDeclaration.g:1819:2: rule__CBoolean__Group__4__Impl rule__CBoolean__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__CBoolean__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__4"


    // $ANTLR start "rule__CBoolean__Group__4__Impl"
    // InternalDeclaration.g:1826:1: rule__CBoolean__Group__4__Impl : ( ')' ) ;
    public final void rule__CBoolean__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1830:1: ( ( ')' ) )
            // InternalDeclaration.g:1831:1: ( ')' )
            {
            // InternalDeclaration.g:1831:1: ( ')' )
            // InternalDeclaration.g:1832:2: ')'
            {
             before(grammarAccess.getCBooleanAccess().getRightParenthesisKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCBooleanAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__4__Impl"


    // $ANTLR start "rule__CBoolean__Group__5"
    // InternalDeclaration.g:1841:1: rule__CBoolean__Group__5 : rule__CBoolean__Group__5__Impl ;
    public final void rule__CBoolean__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1845:1: ( rule__CBoolean__Group__5__Impl )
            // InternalDeclaration.g:1846:2: rule__CBoolean__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CBoolean__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__5"


    // $ANTLR start "rule__CBoolean__Group__5__Impl"
    // InternalDeclaration.g:1852:1: rule__CBoolean__Group__5__Impl : ( ';' ) ;
    public final void rule__CBoolean__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1856:1: ( ( ';' ) )
            // InternalDeclaration.g:1857:1: ( ';' )
            {
            // InternalDeclaration.g:1857:1: ( ';' )
            // InternalDeclaration.g:1858:2: ';'
            {
             before(grammarAccess.getCBooleanAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCBooleanAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__Group__5__Impl"


    // $ANTLR start "rule__CString__Group__0"
    // InternalDeclaration.g:1868:1: rule__CString__Group__0 : rule__CString__Group__0__Impl rule__CString__Group__1 ;
    public final void rule__CString__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1872:1: ( rule__CString__Group__0__Impl rule__CString__Group__1 )
            // InternalDeclaration.g:1873:2: rule__CString__Group__0__Impl rule__CString__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__CString__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CString__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__0"


    // $ANTLR start "rule__CString__Group__0__Impl"
    // InternalDeclaration.g:1880:1: rule__CString__Group__0__Impl : ( () ) ;
    public final void rule__CString__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1884:1: ( ( () ) )
            // InternalDeclaration.g:1885:1: ( () )
            {
            // InternalDeclaration.g:1885:1: ( () )
            // InternalDeclaration.g:1886:2: ()
            {
             before(grammarAccess.getCStringAccess().getCStringAction_0()); 
            // InternalDeclaration.g:1887:2: ()
            // InternalDeclaration.g:1887:3: 
            {
            }

             after(grammarAccess.getCStringAccess().getCStringAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__0__Impl"


    // $ANTLR start "rule__CString__Group__1"
    // InternalDeclaration.g:1895:1: rule__CString__Group__1 : rule__CString__Group__1__Impl rule__CString__Group__2 ;
    public final void rule__CString__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1899:1: ( rule__CString__Group__1__Impl rule__CString__Group__2 )
            // InternalDeclaration.g:1900:2: rule__CString__Group__1__Impl rule__CString__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__CString__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CString__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__1"


    // $ANTLR start "rule__CString__Group__1__Impl"
    // InternalDeclaration.g:1907:1: rule__CString__Group__1__Impl : ( ( rule__CString__NameAssignment_1 ) ) ;
    public final void rule__CString__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1911:1: ( ( ( rule__CString__NameAssignment_1 ) ) )
            // InternalDeclaration.g:1912:1: ( ( rule__CString__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:1912:1: ( ( rule__CString__NameAssignment_1 ) )
            // InternalDeclaration.g:1913:2: ( rule__CString__NameAssignment_1 )
            {
             before(grammarAccess.getCStringAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:1914:2: ( rule__CString__NameAssignment_1 )
            // InternalDeclaration.g:1914:3: rule__CString__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CString__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCStringAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__1__Impl"


    // $ANTLR start "rule__CString__Group__2"
    // InternalDeclaration.g:1922:1: rule__CString__Group__2 : rule__CString__Group__2__Impl rule__CString__Group__3 ;
    public final void rule__CString__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1926:1: ( rule__CString__Group__2__Impl rule__CString__Group__3 )
            // InternalDeclaration.g:1927:2: rule__CString__Group__2__Impl rule__CString__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__CString__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CString__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__2"


    // $ANTLR start "rule__CString__Group__2__Impl"
    // InternalDeclaration.g:1934:1: rule__CString__Group__2__Impl : ( '(' ) ;
    public final void rule__CString__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1938:1: ( ( '(' ) )
            // InternalDeclaration.g:1939:1: ( '(' )
            {
            // InternalDeclaration.g:1939:1: ( '(' )
            // InternalDeclaration.g:1940:2: '('
            {
             before(grammarAccess.getCStringAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCStringAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__2__Impl"


    // $ANTLR start "rule__CString__Group__3"
    // InternalDeclaration.g:1949:1: rule__CString__Group__3 : rule__CString__Group__3__Impl rule__CString__Group__4 ;
    public final void rule__CString__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1953:1: ( rule__CString__Group__3__Impl rule__CString__Group__4 )
            // InternalDeclaration.g:1954:2: rule__CString__Group__3__Impl rule__CString__Group__4
            {
            pushFollow(FOLLOW_18);
            rule__CString__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CString__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__3"


    // $ANTLR start "rule__CString__Group__3__Impl"
    // InternalDeclaration.g:1961:1: rule__CString__Group__3__Impl : ( 'CString' ) ;
    public final void rule__CString__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1965:1: ( ( 'CString' ) )
            // InternalDeclaration.g:1966:1: ( 'CString' )
            {
            // InternalDeclaration.g:1966:1: ( 'CString' )
            // InternalDeclaration.g:1967:2: 'CString'
            {
             before(grammarAccess.getCStringAccess().getCStringKeyword_3()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getCStringAccess().getCStringKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__3__Impl"


    // $ANTLR start "rule__CString__Group__4"
    // InternalDeclaration.g:1976:1: rule__CString__Group__4 : rule__CString__Group__4__Impl rule__CString__Group__5 ;
    public final void rule__CString__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1980:1: ( rule__CString__Group__4__Impl rule__CString__Group__5 )
            // InternalDeclaration.g:1981:2: rule__CString__Group__4__Impl rule__CString__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__CString__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CString__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__4"


    // $ANTLR start "rule__CString__Group__4__Impl"
    // InternalDeclaration.g:1988:1: rule__CString__Group__4__Impl : ( ')' ) ;
    public final void rule__CString__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:1992:1: ( ( ')' ) )
            // InternalDeclaration.g:1993:1: ( ')' )
            {
            // InternalDeclaration.g:1993:1: ( ')' )
            // InternalDeclaration.g:1994:2: ')'
            {
             before(grammarAccess.getCStringAccess().getRightParenthesisKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCStringAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__4__Impl"


    // $ANTLR start "rule__CString__Group__5"
    // InternalDeclaration.g:2003:1: rule__CString__Group__5 : rule__CString__Group__5__Impl ;
    public final void rule__CString__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2007:1: ( rule__CString__Group__5__Impl )
            // InternalDeclaration.g:2008:2: rule__CString__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CString__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__5"


    // $ANTLR start "rule__CString__Group__5__Impl"
    // InternalDeclaration.g:2014:1: rule__CString__Group__5__Impl : ( ';' ) ;
    public final void rule__CString__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2018:1: ( ( ';' ) )
            // InternalDeclaration.g:2019:1: ( ';' )
            {
            // InternalDeclaration.g:2019:1: ( ';' )
            // InternalDeclaration.g:2020:2: ';'
            {
             before(grammarAccess.getCStringAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCStringAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__Group__5__Impl"


    // $ANTLR start "rule__JavaType__Group__0"
    // InternalDeclaration.g:2030:1: rule__JavaType__Group__0 : rule__JavaType__Group__0__Impl rule__JavaType__Group__1 ;
    public final void rule__JavaType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2034:1: ( rule__JavaType__Group__0__Impl rule__JavaType__Group__1 )
            // InternalDeclaration.g:2035:2: rule__JavaType__Group__0__Impl rule__JavaType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__JavaType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__0"


    // $ANTLR start "rule__JavaType__Group__0__Impl"
    // InternalDeclaration.g:2042:1: rule__JavaType__Group__0__Impl : ( () ) ;
    public final void rule__JavaType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2046:1: ( ( () ) )
            // InternalDeclaration.g:2047:1: ( () )
            {
            // InternalDeclaration.g:2047:1: ( () )
            // InternalDeclaration.g:2048:2: ()
            {
             before(grammarAccess.getJavaTypeAccess().getJavaTypeAction_0()); 
            // InternalDeclaration.g:2049:2: ()
            // InternalDeclaration.g:2049:3: 
            {
            }

             after(grammarAccess.getJavaTypeAccess().getJavaTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__0__Impl"


    // $ANTLR start "rule__JavaType__Group__1"
    // InternalDeclaration.g:2057:1: rule__JavaType__Group__1 : rule__JavaType__Group__1__Impl rule__JavaType__Group__2 ;
    public final void rule__JavaType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2061:1: ( rule__JavaType__Group__1__Impl rule__JavaType__Group__2 )
            // InternalDeclaration.g:2062:2: rule__JavaType__Group__1__Impl rule__JavaType__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__JavaType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__1"


    // $ANTLR start "rule__JavaType__Group__1__Impl"
    // InternalDeclaration.g:2069:1: rule__JavaType__Group__1__Impl : ( ( rule__JavaType__NameAssignment_1 ) ) ;
    public final void rule__JavaType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2073:1: ( ( ( rule__JavaType__NameAssignment_1 ) ) )
            // InternalDeclaration.g:2074:1: ( ( rule__JavaType__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:2074:1: ( ( rule__JavaType__NameAssignment_1 ) )
            // InternalDeclaration.g:2075:2: ( rule__JavaType__NameAssignment_1 )
            {
             before(grammarAccess.getJavaTypeAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:2076:2: ( rule__JavaType__NameAssignment_1 )
            // InternalDeclaration.g:2076:3: rule__JavaType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__JavaType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getJavaTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__1__Impl"


    // $ANTLR start "rule__JavaType__Group__2"
    // InternalDeclaration.g:2084:1: rule__JavaType__Group__2 : rule__JavaType__Group__2__Impl rule__JavaType__Group__3 ;
    public final void rule__JavaType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2088:1: ( rule__JavaType__Group__2__Impl rule__JavaType__Group__3 )
            // InternalDeclaration.g:2089:2: rule__JavaType__Group__2__Impl rule__JavaType__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__JavaType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__2"


    // $ANTLR start "rule__JavaType__Group__2__Impl"
    // InternalDeclaration.g:2096:1: rule__JavaType__Group__2__Impl : ( '(' ) ;
    public final void rule__JavaType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2100:1: ( ( '(' ) )
            // InternalDeclaration.g:2101:1: ( '(' )
            {
            // InternalDeclaration.g:2101:1: ( '(' )
            // InternalDeclaration.g:2102:2: '('
            {
             before(grammarAccess.getJavaTypeAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getJavaTypeAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__2__Impl"


    // $ANTLR start "rule__JavaType__Group__3"
    // InternalDeclaration.g:2111:1: rule__JavaType__Group__3 : rule__JavaType__Group__3__Impl rule__JavaType__Group__4 ;
    public final void rule__JavaType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2115:1: ( rule__JavaType__Group__3__Impl rule__JavaType__Group__4 )
            // InternalDeclaration.g:2116:2: rule__JavaType__Group__3__Impl rule__JavaType__Group__4
            {
            pushFollow(FOLLOW_24);
            rule__JavaType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__3"


    // $ANTLR start "rule__JavaType__Group__3__Impl"
    // InternalDeclaration.g:2123:1: rule__JavaType__Group__3__Impl : ( ( rule__JavaType__PackageNameAssignment_3 ) ) ;
    public final void rule__JavaType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2127:1: ( ( ( rule__JavaType__PackageNameAssignment_3 ) ) )
            // InternalDeclaration.g:2128:1: ( ( rule__JavaType__PackageNameAssignment_3 ) )
            {
            // InternalDeclaration.g:2128:1: ( ( rule__JavaType__PackageNameAssignment_3 ) )
            // InternalDeclaration.g:2129:2: ( rule__JavaType__PackageNameAssignment_3 )
            {
             before(grammarAccess.getJavaTypeAccess().getPackageNameAssignment_3()); 
            // InternalDeclaration.g:2130:2: ( rule__JavaType__PackageNameAssignment_3 )
            // InternalDeclaration.g:2130:3: rule__JavaType__PackageNameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__JavaType__PackageNameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getJavaTypeAccess().getPackageNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__3__Impl"


    // $ANTLR start "rule__JavaType__Group__4"
    // InternalDeclaration.g:2138:1: rule__JavaType__Group__4 : rule__JavaType__Group__4__Impl rule__JavaType__Group__5 ;
    public final void rule__JavaType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2142:1: ( rule__JavaType__Group__4__Impl rule__JavaType__Group__5 )
            // InternalDeclaration.g:2143:2: rule__JavaType__Group__4__Impl rule__JavaType__Group__5
            {
            pushFollow(FOLLOW_4);
            rule__JavaType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__4"


    // $ANTLR start "rule__JavaType__Group__4__Impl"
    // InternalDeclaration.g:2150:1: rule__JavaType__Group__4__Impl : ( '.' ) ;
    public final void rule__JavaType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2154:1: ( ( '.' ) )
            // InternalDeclaration.g:2155:1: ( '.' )
            {
            // InternalDeclaration.g:2155:1: ( '.' )
            // InternalDeclaration.g:2156:2: '.'
            {
             before(grammarAccess.getJavaTypeAccess().getFullStopKeyword_4()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getJavaTypeAccess().getFullStopKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__4__Impl"


    // $ANTLR start "rule__JavaType__Group__5"
    // InternalDeclaration.g:2165:1: rule__JavaType__Group__5 : rule__JavaType__Group__5__Impl rule__JavaType__Group__6 ;
    public final void rule__JavaType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2169:1: ( rule__JavaType__Group__5__Impl rule__JavaType__Group__6 )
            // InternalDeclaration.g:2170:2: rule__JavaType__Group__5__Impl rule__JavaType__Group__6
            {
            pushFollow(FOLLOW_18);
            rule__JavaType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__5"


    // $ANTLR start "rule__JavaType__Group__5__Impl"
    // InternalDeclaration.g:2177:1: rule__JavaType__Group__5__Impl : ( ( rule__JavaType__ClassNameAssignment_5 ) ) ;
    public final void rule__JavaType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2181:1: ( ( ( rule__JavaType__ClassNameAssignment_5 ) ) )
            // InternalDeclaration.g:2182:1: ( ( rule__JavaType__ClassNameAssignment_5 ) )
            {
            // InternalDeclaration.g:2182:1: ( ( rule__JavaType__ClassNameAssignment_5 ) )
            // InternalDeclaration.g:2183:2: ( rule__JavaType__ClassNameAssignment_5 )
            {
             before(grammarAccess.getJavaTypeAccess().getClassNameAssignment_5()); 
            // InternalDeclaration.g:2184:2: ( rule__JavaType__ClassNameAssignment_5 )
            // InternalDeclaration.g:2184:3: rule__JavaType__ClassNameAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__JavaType__ClassNameAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getJavaTypeAccess().getClassNameAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__5__Impl"


    // $ANTLR start "rule__JavaType__Group__6"
    // InternalDeclaration.g:2192:1: rule__JavaType__Group__6 : rule__JavaType__Group__6__Impl rule__JavaType__Group__7 ;
    public final void rule__JavaType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2196:1: ( rule__JavaType__Group__6__Impl rule__JavaType__Group__7 )
            // InternalDeclaration.g:2197:2: rule__JavaType__Group__6__Impl rule__JavaType__Group__7
            {
            pushFollow(FOLLOW_19);
            rule__JavaType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__JavaType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__6"


    // $ANTLR start "rule__JavaType__Group__6__Impl"
    // InternalDeclaration.g:2204:1: rule__JavaType__Group__6__Impl : ( ')' ) ;
    public final void rule__JavaType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2208:1: ( ( ')' ) )
            // InternalDeclaration.g:2209:1: ( ')' )
            {
            // InternalDeclaration.g:2209:1: ( ')' )
            // InternalDeclaration.g:2210:2: ')'
            {
             before(grammarAccess.getJavaTypeAccess().getRightParenthesisKeyword_6()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getJavaTypeAccess().getRightParenthesisKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__6__Impl"


    // $ANTLR start "rule__JavaType__Group__7"
    // InternalDeclaration.g:2219:1: rule__JavaType__Group__7 : rule__JavaType__Group__7__Impl ;
    public final void rule__JavaType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2223:1: ( rule__JavaType__Group__7__Impl )
            // InternalDeclaration.g:2224:2: rule__JavaType__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__JavaType__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__7"


    // $ANTLR start "rule__JavaType__Group__7__Impl"
    // InternalDeclaration.g:2230:1: rule__JavaType__Group__7__Impl : ( ';' ) ;
    public final void rule__JavaType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2234:1: ( ( ';' ) )
            // InternalDeclaration.g:2235:1: ( ';' )
            {
            // InternalDeclaration.g:2235:1: ( ';' )
            // InternalDeclaration.g:2236:2: ';'
            {
             before(grammarAccess.getJavaTypeAccess().getSemicolonKeyword_7()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getJavaTypeAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__Group__7__Impl"


    // $ANTLR start "rule__StructType__Group__0"
    // InternalDeclaration.g:2246:1: rule__StructType__Group__0 : rule__StructType__Group__0__Impl rule__StructType__Group__1 ;
    public final void rule__StructType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2250:1: ( rule__StructType__Group__0__Impl rule__StructType__Group__1 )
            // InternalDeclaration.g:2251:2: rule__StructType__Group__0__Impl rule__StructType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__StructType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__0"


    // $ANTLR start "rule__StructType__Group__0__Impl"
    // InternalDeclaration.g:2258:1: rule__StructType__Group__0__Impl : ( 'Record' ) ;
    public final void rule__StructType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2262:1: ( ( 'Record' ) )
            // InternalDeclaration.g:2263:1: ( 'Record' )
            {
            // InternalDeclaration.g:2263:1: ( 'Record' )
            // InternalDeclaration.g:2264:2: 'Record'
            {
             before(grammarAccess.getStructTypeAccess().getRecordKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getRecordKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__0__Impl"


    // $ANTLR start "rule__StructType__Group__1"
    // InternalDeclaration.g:2273:1: rule__StructType__Group__1 : rule__StructType__Group__1__Impl rule__StructType__Group__2 ;
    public final void rule__StructType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2277:1: ( rule__StructType__Group__1__Impl rule__StructType__Group__2 )
            // InternalDeclaration.g:2278:2: rule__StructType__Group__1__Impl rule__StructType__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__StructType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__1"


    // $ANTLR start "rule__StructType__Group__1__Impl"
    // InternalDeclaration.g:2285:1: rule__StructType__Group__1__Impl : ( ( rule__StructType__NameAssignment_1 ) ) ;
    public final void rule__StructType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2289:1: ( ( ( rule__StructType__NameAssignment_1 ) ) )
            // InternalDeclaration.g:2290:1: ( ( rule__StructType__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:2290:1: ( ( rule__StructType__NameAssignment_1 ) )
            // InternalDeclaration.g:2291:2: ( rule__StructType__NameAssignment_1 )
            {
             before(grammarAccess.getStructTypeAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:2292:2: ( rule__StructType__NameAssignment_1 )
            // InternalDeclaration.g:2292:3: rule__StructType__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__StructType__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStructTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__1__Impl"


    // $ANTLR start "rule__StructType__Group__2"
    // InternalDeclaration.g:2300:1: rule__StructType__Group__2 : rule__StructType__Group__2__Impl rule__StructType__Group__3 ;
    public final void rule__StructType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2304:1: ( rule__StructType__Group__2__Impl rule__StructType__Group__3 )
            // InternalDeclaration.g:2305:2: rule__StructType__Group__2__Impl rule__StructType__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__StructType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__2"


    // $ANTLR start "rule__StructType__Group__2__Impl"
    // InternalDeclaration.g:2312:1: rule__StructType__Group__2__Impl : ( '{' ) ;
    public final void rule__StructType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2316:1: ( ( '{' ) )
            // InternalDeclaration.g:2317:1: ( '{' )
            {
            // InternalDeclaration.g:2317:1: ( '{' )
            // InternalDeclaration.g:2318:2: '{'
            {
             before(grammarAccess.getStructTypeAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__2__Impl"


    // $ANTLR start "rule__StructType__Group__3"
    // InternalDeclaration.g:2327:1: rule__StructType__Group__3 : rule__StructType__Group__3__Impl rule__StructType__Group__4 ;
    public final void rule__StructType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2331:1: ( rule__StructType__Group__3__Impl rule__StructType__Group__4 )
            // InternalDeclaration.g:2332:2: rule__StructType__Group__3__Impl rule__StructType__Group__4
            {
            pushFollow(FOLLOW_19);
            rule__StructType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__3"


    // $ANTLR start "rule__StructType__Group__3__Impl"
    // InternalDeclaration.g:2339:1: rule__StructType__Group__3__Impl : ( ( rule__StructType__FieldsAssignment_3 ) ) ;
    public final void rule__StructType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2343:1: ( ( ( rule__StructType__FieldsAssignment_3 ) ) )
            // InternalDeclaration.g:2344:1: ( ( rule__StructType__FieldsAssignment_3 ) )
            {
            // InternalDeclaration.g:2344:1: ( ( rule__StructType__FieldsAssignment_3 ) )
            // InternalDeclaration.g:2345:2: ( rule__StructType__FieldsAssignment_3 )
            {
             before(grammarAccess.getStructTypeAccess().getFieldsAssignment_3()); 
            // InternalDeclaration.g:2346:2: ( rule__StructType__FieldsAssignment_3 )
            // InternalDeclaration.g:2346:3: rule__StructType__FieldsAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__StructType__FieldsAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getStructTypeAccess().getFieldsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__3__Impl"


    // $ANTLR start "rule__StructType__Group__4"
    // InternalDeclaration.g:2354:1: rule__StructType__Group__4 : rule__StructType__Group__4__Impl rule__StructType__Group__5 ;
    public final void rule__StructType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2358:1: ( rule__StructType__Group__4__Impl rule__StructType__Group__5 )
            // InternalDeclaration.g:2359:2: rule__StructType__Group__4__Impl rule__StructType__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__StructType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__4"


    // $ANTLR start "rule__StructType__Group__4__Impl"
    // InternalDeclaration.g:2366:1: rule__StructType__Group__4__Impl : ( ( rule__StructType__Group_4__0 )* ) ;
    public final void rule__StructType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2370:1: ( ( ( rule__StructType__Group_4__0 )* ) )
            // InternalDeclaration.g:2371:1: ( ( rule__StructType__Group_4__0 )* )
            {
            // InternalDeclaration.g:2371:1: ( ( rule__StructType__Group_4__0 )* )
            // InternalDeclaration.g:2372:2: ( rule__StructType__Group_4__0 )*
            {
             before(grammarAccess.getStructTypeAccess().getGroup_4()); 
            // InternalDeclaration.g:2373:2: ( rule__StructType__Group_4__0 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==20) ) {
                    int LA9_1 = input.LA(2);

                    if ( ((LA9_1>=RULE_STRING && LA9_1<=RULE_ID)) ) {
                        alt9=1;
                    }


                }


                switch (alt9) {
            	case 1 :
            	    // InternalDeclaration.g:2373:3: rule__StructType__Group_4__0
            	    {
            	    pushFollow(FOLLOW_25);
            	    rule__StructType__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getStructTypeAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__4__Impl"


    // $ANTLR start "rule__StructType__Group__5"
    // InternalDeclaration.g:2381:1: rule__StructType__Group__5 : rule__StructType__Group__5__Impl rule__StructType__Group__6 ;
    public final void rule__StructType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2385:1: ( rule__StructType__Group__5__Impl rule__StructType__Group__6 )
            // InternalDeclaration.g:2386:2: rule__StructType__Group__5__Impl rule__StructType__Group__6
            {
            pushFollow(FOLLOW_26);
            rule__StructType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__5"


    // $ANTLR start "rule__StructType__Group__5__Impl"
    // InternalDeclaration.g:2393:1: rule__StructType__Group__5__Impl : ( ';' ) ;
    public final void rule__StructType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2397:1: ( ( ';' ) )
            // InternalDeclaration.g:2398:1: ( ';' )
            {
            // InternalDeclaration.g:2398:1: ( ';' )
            // InternalDeclaration.g:2399:2: ';'
            {
             before(grammarAccess.getStructTypeAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__5__Impl"


    // $ANTLR start "rule__StructType__Group__6"
    // InternalDeclaration.g:2408:1: rule__StructType__Group__6 : rule__StructType__Group__6__Impl rule__StructType__Group__7 ;
    public final void rule__StructType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2412:1: ( rule__StructType__Group__6__Impl rule__StructType__Group__7 )
            // InternalDeclaration.g:2413:2: rule__StructType__Group__6__Impl rule__StructType__Group__7
            {
            pushFollow(FOLLOW_19);
            rule__StructType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__6"


    // $ANTLR start "rule__StructType__Group__6__Impl"
    // InternalDeclaration.g:2420:1: rule__StructType__Group__6__Impl : ( '}' ) ;
    public final void rule__StructType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2424:1: ( ( '}' ) )
            // InternalDeclaration.g:2425:1: ( '}' )
            {
            // InternalDeclaration.g:2425:1: ( '}' )
            // InternalDeclaration.g:2426:2: '}'
            {
             before(grammarAccess.getStructTypeAccess().getRightCurlyBracketKeyword_6()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__6__Impl"


    // $ANTLR start "rule__StructType__Group__7"
    // InternalDeclaration.g:2435:1: rule__StructType__Group__7 : rule__StructType__Group__7__Impl ;
    public final void rule__StructType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2439:1: ( rule__StructType__Group__7__Impl )
            // InternalDeclaration.g:2440:2: rule__StructType__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructType__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__7"


    // $ANTLR start "rule__StructType__Group__7__Impl"
    // InternalDeclaration.g:2446:1: rule__StructType__Group__7__Impl : ( ';' ) ;
    public final void rule__StructType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2450:1: ( ( ';' ) )
            // InternalDeclaration.g:2451:1: ( ';' )
            {
            // InternalDeclaration.g:2451:1: ( ';' )
            // InternalDeclaration.g:2452:2: ';'
            {
             before(grammarAccess.getStructTypeAccess().getSemicolonKeyword_7()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getSemicolonKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group__7__Impl"


    // $ANTLR start "rule__StructType__Group_4__0"
    // InternalDeclaration.g:2462:1: rule__StructType__Group_4__0 : rule__StructType__Group_4__0__Impl rule__StructType__Group_4__1 ;
    public final void rule__StructType__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2466:1: ( rule__StructType__Group_4__0__Impl rule__StructType__Group_4__1 )
            // InternalDeclaration.g:2467:2: rule__StructType__Group_4__0__Impl rule__StructType__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__StructType__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StructType__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group_4__0"


    // $ANTLR start "rule__StructType__Group_4__0__Impl"
    // InternalDeclaration.g:2474:1: rule__StructType__Group_4__0__Impl : ( ';' ) ;
    public final void rule__StructType__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2478:1: ( ( ';' ) )
            // InternalDeclaration.g:2479:1: ( ';' )
            {
            // InternalDeclaration.g:2479:1: ( ';' )
            // InternalDeclaration.g:2480:2: ';'
            {
             before(grammarAccess.getStructTypeAccess().getSemicolonKeyword_4_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getStructTypeAccess().getSemicolonKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group_4__0__Impl"


    // $ANTLR start "rule__StructType__Group_4__1"
    // InternalDeclaration.g:2489:1: rule__StructType__Group_4__1 : rule__StructType__Group_4__1__Impl ;
    public final void rule__StructType__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2493:1: ( rule__StructType__Group_4__1__Impl )
            // InternalDeclaration.g:2494:2: rule__StructType__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StructType__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group_4__1"


    // $ANTLR start "rule__StructType__Group_4__1__Impl"
    // InternalDeclaration.g:2500:1: rule__StructType__Group_4__1__Impl : ( ( rule__StructType__FieldsAssignment_4_1 ) ) ;
    public final void rule__StructType__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2504:1: ( ( ( rule__StructType__FieldsAssignment_4_1 ) ) )
            // InternalDeclaration.g:2505:1: ( ( rule__StructType__FieldsAssignment_4_1 ) )
            {
            // InternalDeclaration.g:2505:1: ( ( rule__StructType__FieldsAssignment_4_1 ) )
            // InternalDeclaration.g:2506:2: ( rule__StructType__FieldsAssignment_4_1 )
            {
             before(grammarAccess.getStructTypeAccess().getFieldsAssignment_4_1()); 
            // InternalDeclaration.g:2507:2: ( rule__StructType__FieldsAssignment_4_1 )
            // InternalDeclaration.g:2507:3: rule__StructType__FieldsAssignment_4_1
            {
            pushFollow(FOLLOW_2);
            rule__StructType__FieldsAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getStructTypeAccess().getFieldsAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__Group_4__1__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group__0"
    // InternalDeclaration.g:2516:1: rule__ParameterDeclaration__Group__0 : rule__ParameterDeclaration__Group__0__Impl rule__ParameterDeclaration__Group__1 ;
    public final void rule__ParameterDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2520:1: ( rule__ParameterDeclaration__Group__0__Impl rule__ParameterDeclaration__Group__1 )
            // InternalDeclaration.g:2521:2: rule__ParameterDeclaration__Group__0__Impl rule__ParameterDeclaration__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__ParameterDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group__0"


    // $ANTLR start "rule__ParameterDeclaration__Group__0__Impl"
    // InternalDeclaration.g:2528:1: rule__ParameterDeclaration__Group__0__Impl : ( ( rule__ParameterDeclaration__TypeAssignment_0 ) ) ;
    public final void rule__ParameterDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2532:1: ( ( ( rule__ParameterDeclaration__TypeAssignment_0 ) ) )
            // InternalDeclaration.g:2533:1: ( ( rule__ParameterDeclaration__TypeAssignment_0 ) )
            {
            // InternalDeclaration.g:2533:1: ( ( rule__ParameterDeclaration__TypeAssignment_0 ) )
            // InternalDeclaration.g:2534:2: ( rule__ParameterDeclaration__TypeAssignment_0 )
            {
             before(grammarAccess.getParameterDeclarationAccess().getTypeAssignment_0()); 
            // InternalDeclaration.g:2535:2: ( rule__ParameterDeclaration__TypeAssignment_0 )
            // InternalDeclaration.g:2535:3: rule__ParameterDeclaration__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getParameterDeclarationAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group__0__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group__1"
    // InternalDeclaration.g:2543:1: rule__ParameterDeclaration__Group__1 : rule__ParameterDeclaration__Group__1__Impl rule__ParameterDeclaration__Group__2 ;
    public final void rule__ParameterDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2547:1: ( rule__ParameterDeclaration__Group__1__Impl rule__ParameterDeclaration__Group__2 )
            // InternalDeclaration.g:2548:2: rule__ParameterDeclaration__Group__1__Impl rule__ParameterDeclaration__Group__2
            {
            pushFollow(FOLLOW_27);
            rule__ParameterDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group__1"


    // $ANTLR start "rule__ParameterDeclaration__Group__1__Impl"
    // InternalDeclaration.g:2555:1: rule__ParameterDeclaration__Group__1__Impl : ( ( rule__ParameterDeclaration__Group_1__0 )? ) ;
    public final void rule__ParameterDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2559:1: ( ( ( rule__ParameterDeclaration__Group_1__0 )? ) )
            // InternalDeclaration.g:2560:1: ( ( rule__ParameterDeclaration__Group_1__0 )? )
            {
            // InternalDeclaration.g:2560:1: ( ( rule__ParameterDeclaration__Group_1__0 )? )
            // InternalDeclaration.g:2561:2: ( rule__ParameterDeclaration__Group_1__0 )?
            {
             before(grammarAccess.getParameterDeclarationAccess().getGroup_1()); 
            // InternalDeclaration.g:2562:2: ( rule__ParameterDeclaration__Group_1__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==27) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDeclaration.g:2562:3: rule__ParameterDeclaration__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ParameterDeclaration__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getParameterDeclarationAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group__1__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group__2"
    // InternalDeclaration.g:2570:1: rule__ParameterDeclaration__Group__2 : rule__ParameterDeclaration__Group__2__Impl ;
    public final void rule__ParameterDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2574:1: ( rule__ParameterDeclaration__Group__2__Impl )
            // InternalDeclaration.g:2575:2: rule__ParameterDeclaration__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group__2"


    // $ANTLR start "rule__ParameterDeclaration__Group__2__Impl"
    // InternalDeclaration.g:2581:1: rule__ParameterDeclaration__Group__2__Impl : ( ( rule__ParameterDeclaration__NameAssignment_2 ) ) ;
    public final void rule__ParameterDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2585:1: ( ( ( rule__ParameterDeclaration__NameAssignment_2 ) ) )
            // InternalDeclaration.g:2586:1: ( ( rule__ParameterDeclaration__NameAssignment_2 ) )
            {
            // InternalDeclaration.g:2586:1: ( ( rule__ParameterDeclaration__NameAssignment_2 ) )
            // InternalDeclaration.g:2587:2: ( rule__ParameterDeclaration__NameAssignment_2 )
            {
             before(grammarAccess.getParameterDeclarationAccess().getNameAssignment_2()); 
            // InternalDeclaration.g:2588:2: ( rule__ParameterDeclaration__NameAssignment_2 )
            // InternalDeclaration.g:2588:3: rule__ParameterDeclaration__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getParameterDeclarationAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group__2__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__0"
    // InternalDeclaration.g:2597:1: rule__ParameterDeclaration__Group_1__0 : rule__ParameterDeclaration__Group_1__0__Impl rule__ParameterDeclaration__Group_1__1 ;
    public final void rule__ParameterDeclaration__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2601:1: ( rule__ParameterDeclaration__Group_1__0__Impl rule__ParameterDeclaration__Group_1__1 )
            // InternalDeclaration.g:2602:2: rule__ParameterDeclaration__Group_1__0__Impl rule__ParameterDeclaration__Group_1__1
            {
            pushFollow(FOLLOW_28);
            rule__ParameterDeclaration__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__0"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__0__Impl"
    // InternalDeclaration.g:2609:1: rule__ParameterDeclaration__Group_1__0__Impl : ( '[' ) ;
    public final void rule__ParameterDeclaration__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2613:1: ( ( '[' ) )
            // InternalDeclaration.g:2614:1: ( '[' )
            {
            // InternalDeclaration.g:2614:1: ( '[' )
            // InternalDeclaration.g:2615:2: '['
            {
             before(grammarAccess.getParameterDeclarationAccess().getLeftSquareBracketKeyword_1_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getParameterDeclarationAccess().getLeftSquareBracketKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__0__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__1"
    // InternalDeclaration.g:2624:1: rule__ParameterDeclaration__Group_1__1 : rule__ParameterDeclaration__Group_1__1__Impl rule__ParameterDeclaration__Group_1__2 ;
    public final void rule__ParameterDeclaration__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2628:1: ( rule__ParameterDeclaration__Group_1__1__Impl rule__ParameterDeclaration__Group_1__2 )
            // InternalDeclaration.g:2629:2: rule__ParameterDeclaration__Group_1__1__Impl rule__ParameterDeclaration__Group_1__2
            {
            pushFollow(FOLLOW_29);
            rule__ParameterDeclaration__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__1"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__1__Impl"
    // InternalDeclaration.g:2636:1: rule__ParameterDeclaration__Group_1__1__Impl : ( ( rule__ParameterDeclaration__LowerBoundAssignment_1_1 ) ) ;
    public final void rule__ParameterDeclaration__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2640:1: ( ( ( rule__ParameterDeclaration__LowerBoundAssignment_1_1 ) ) )
            // InternalDeclaration.g:2641:1: ( ( rule__ParameterDeclaration__LowerBoundAssignment_1_1 ) )
            {
            // InternalDeclaration.g:2641:1: ( ( rule__ParameterDeclaration__LowerBoundAssignment_1_1 ) )
            // InternalDeclaration.g:2642:2: ( rule__ParameterDeclaration__LowerBoundAssignment_1_1 )
            {
             before(grammarAccess.getParameterDeclarationAccess().getLowerBoundAssignment_1_1()); 
            // InternalDeclaration.g:2643:2: ( rule__ParameterDeclaration__LowerBoundAssignment_1_1 )
            // InternalDeclaration.g:2643:3: rule__ParameterDeclaration__LowerBoundAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__LowerBoundAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getParameterDeclarationAccess().getLowerBoundAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__1__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__2"
    // InternalDeclaration.g:2651:1: rule__ParameterDeclaration__Group_1__2 : rule__ParameterDeclaration__Group_1__2__Impl rule__ParameterDeclaration__Group_1__3 ;
    public final void rule__ParameterDeclaration__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2655:1: ( rule__ParameterDeclaration__Group_1__2__Impl rule__ParameterDeclaration__Group_1__3 )
            // InternalDeclaration.g:2656:2: rule__ParameterDeclaration__Group_1__2__Impl rule__ParameterDeclaration__Group_1__3
            {
            pushFollow(FOLLOW_30);
            rule__ParameterDeclaration__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__2"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__2__Impl"
    // InternalDeclaration.g:2663:1: rule__ParameterDeclaration__Group_1__2__Impl : ( ',' ) ;
    public final void rule__ParameterDeclaration__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2667:1: ( ( ',' ) )
            // InternalDeclaration.g:2668:1: ( ',' )
            {
            // InternalDeclaration.g:2668:1: ( ',' )
            // InternalDeclaration.g:2669:2: ','
            {
             before(grammarAccess.getParameterDeclarationAccess().getCommaKeyword_1_2()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getParameterDeclarationAccess().getCommaKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__2__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__3"
    // InternalDeclaration.g:2678:1: rule__ParameterDeclaration__Group_1__3 : rule__ParameterDeclaration__Group_1__3__Impl rule__ParameterDeclaration__Group_1__4 ;
    public final void rule__ParameterDeclaration__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2682:1: ( rule__ParameterDeclaration__Group_1__3__Impl rule__ParameterDeclaration__Group_1__4 )
            // InternalDeclaration.g:2683:2: rule__ParameterDeclaration__Group_1__3__Impl rule__ParameterDeclaration__Group_1__4
            {
            pushFollow(FOLLOW_31);
            rule__ParameterDeclaration__Group_1__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group_1__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__3"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__3__Impl"
    // InternalDeclaration.g:2690:1: rule__ParameterDeclaration__Group_1__3__Impl : ( ( rule__ParameterDeclaration__UpperBoundAssignment_1_3 ) ) ;
    public final void rule__ParameterDeclaration__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2694:1: ( ( ( rule__ParameterDeclaration__UpperBoundAssignment_1_3 ) ) )
            // InternalDeclaration.g:2695:1: ( ( rule__ParameterDeclaration__UpperBoundAssignment_1_3 ) )
            {
            // InternalDeclaration.g:2695:1: ( ( rule__ParameterDeclaration__UpperBoundAssignment_1_3 ) )
            // InternalDeclaration.g:2696:2: ( rule__ParameterDeclaration__UpperBoundAssignment_1_3 )
            {
             before(grammarAccess.getParameterDeclarationAccess().getUpperBoundAssignment_1_3()); 
            // InternalDeclaration.g:2697:2: ( rule__ParameterDeclaration__UpperBoundAssignment_1_3 )
            // InternalDeclaration.g:2697:3: rule__ParameterDeclaration__UpperBoundAssignment_1_3
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__UpperBoundAssignment_1_3();

            state._fsp--;


            }

             after(grammarAccess.getParameterDeclarationAccess().getUpperBoundAssignment_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__3__Impl"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__4"
    // InternalDeclaration.g:2705:1: rule__ParameterDeclaration__Group_1__4 : rule__ParameterDeclaration__Group_1__4__Impl ;
    public final void rule__ParameterDeclaration__Group_1__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2709:1: ( rule__ParameterDeclaration__Group_1__4__Impl )
            // InternalDeclaration.g:2710:2: rule__ParameterDeclaration__Group_1__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ParameterDeclaration__Group_1__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__4"


    // $ANTLR start "rule__ParameterDeclaration__Group_1__4__Impl"
    // InternalDeclaration.g:2716:1: rule__ParameterDeclaration__Group_1__4__Impl : ( ']' ) ;
    public final void rule__ParameterDeclaration__Group_1__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2720:1: ( ( ']' ) )
            // InternalDeclaration.g:2721:1: ( ']' )
            {
            // InternalDeclaration.g:2721:1: ( ']' )
            // InternalDeclaration.g:2722:2: ']'
            {
             before(grammarAccess.getParameterDeclarationAccess().getRightSquareBracketKeyword_1_4()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getParameterDeclarationAccess().getRightSquareBracketKeyword_1_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__Group_1__4__Impl"


    // $ANTLR start "rule__NUMBER__Group__0"
    // InternalDeclaration.g:2732:1: rule__NUMBER__Group__0 : rule__NUMBER__Group__0__Impl rule__NUMBER__Group__1 ;
    public final void rule__NUMBER__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2736:1: ( rule__NUMBER__Group__0__Impl rule__NUMBER__Group__1 )
            // InternalDeclaration.g:2737:2: rule__NUMBER__Group__0__Impl rule__NUMBER__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__NUMBER__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NUMBER__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NUMBER__Group__0"


    // $ANTLR start "rule__NUMBER__Group__0__Impl"
    // InternalDeclaration.g:2744:1: rule__NUMBER__Group__0__Impl : ( ( '-' )? ) ;
    public final void rule__NUMBER__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2748:1: ( ( ( '-' )? ) )
            // InternalDeclaration.g:2749:1: ( ( '-' )? )
            {
            // InternalDeclaration.g:2749:1: ( ( '-' )? )
            // InternalDeclaration.g:2750:2: ( '-' )?
            {
             before(grammarAccess.getNUMBERAccess().getHyphenMinusKeyword_0()); 
            // InternalDeclaration.g:2751:2: ( '-' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==30) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDeclaration.g:2751:3: '-'
                    {
                    match(input,30,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getNUMBERAccess().getHyphenMinusKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NUMBER__Group__0__Impl"


    // $ANTLR start "rule__NUMBER__Group__1"
    // InternalDeclaration.g:2759:1: rule__NUMBER__Group__1 : rule__NUMBER__Group__1__Impl ;
    public final void rule__NUMBER__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2763:1: ( rule__NUMBER__Group__1__Impl )
            // InternalDeclaration.g:2764:2: rule__NUMBER__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NUMBER__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NUMBER__Group__1"


    // $ANTLR start "rule__NUMBER__Group__1__Impl"
    // InternalDeclaration.g:2770:1: rule__NUMBER__Group__1__Impl : ( RULE_INT ) ;
    public final void rule__NUMBER__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2774:1: ( ( RULE_INT ) )
            // InternalDeclaration.g:2775:1: ( RULE_INT )
            {
            // InternalDeclaration.g:2775:1: ( RULE_INT )
            // InternalDeclaration.g:2776:2: RULE_INT
            {
             before(grammarAccess.getNUMBERAccess().getINTTerminalRuleCall_1()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getNUMBERAccess().getINTTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NUMBER__Group__1__Impl"


    // $ANTLR start "rule__Interface__Group__0"
    // InternalDeclaration.g:2786:1: rule__Interface__Group__0 : rule__Interface__Group__0__Impl rule__Interface__Group__1 ;
    public final void rule__Interface__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2790:1: ( rule__Interface__Group__0__Impl rule__Interface__Group__1 )
            // InternalDeclaration.g:2791:2: rule__Interface__Group__0__Impl rule__Interface__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Interface__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interface__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__0"


    // $ANTLR start "rule__Interface__Group__0__Impl"
    // InternalDeclaration.g:2798:1: rule__Interface__Group__0__Impl : ( () ) ;
    public final void rule__Interface__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2802:1: ( ( () ) )
            // InternalDeclaration.g:2803:1: ( () )
            {
            // InternalDeclaration.g:2803:1: ( () )
            // InternalDeclaration.g:2804:2: ()
            {
             before(grammarAccess.getInterfaceAccess().getInterfaceAction_0()); 
            // InternalDeclaration.g:2805:2: ()
            // InternalDeclaration.g:2805:3: 
            {
            }

             after(grammarAccess.getInterfaceAccess().getInterfaceAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__0__Impl"


    // $ANTLR start "rule__Interface__Group__1"
    // InternalDeclaration.g:2813:1: rule__Interface__Group__1 : rule__Interface__Group__1__Impl rule__Interface__Group__2 ;
    public final void rule__Interface__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2817:1: ( rule__Interface__Group__1__Impl rule__Interface__Group__2 )
            // InternalDeclaration.g:2818:2: rule__Interface__Group__1__Impl rule__Interface__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Interface__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interface__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__1"


    // $ANTLR start "rule__Interface__Group__1__Impl"
    // InternalDeclaration.g:2825:1: rule__Interface__Group__1__Impl : ( 'Interface' ) ;
    public final void rule__Interface__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2829:1: ( ( 'Interface' ) )
            // InternalDeclaration.g:2830:1: ( 'Interface' )
            {
            // InternalDeclaration.g:2830:1: ( 'Interface' )
            // InternalDeclaration.g:2831:2: 'Interface'
            {
             before(grammarAccess.getInterfaceAccess().getInterfaceKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getInterfaceAccess().getInterfaceKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__1__Impl"


    // $ANTLR start "rule__Interface__Group__2"
    // InternalDeclaration.g:2840:1: rule__Interface__Group__2 : rule__Interface__Group__2__Impl rule__Interface__Group__3 ;
    public final void rule__Interface__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2844:1: ( rule__Interface__Group__2__Impl rule__Interface__Group__3 )
            // InternalDeclaration.g:2845:2: rule__Interface__Group__2__Impl rule__Interface__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Interface__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interface__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__2"


    // $ANTLR start "rule__Interface__Group__2__Impl"
    // InternalDeclaration.g:2852:1: rule__Interface__Group__2__Impl : ( ( rule__Interface__NameAssignment_2 ) ) ;
    public final void rule__Interface__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2856:1: ( ( ( rule__Interface__NameAssignment_2 ) ) )
            // InternalDeclaration.g:2857:1: ( ( rule__Interface__NameAssignment_2 ) )
            {
            // InternalDeclaration.g:2857:1: ( ( rule__Interface__NameAssignment_2 ) )
            // InternalDeclaration.g:2858:2: ( rule__Interface__NameAssignment_2 )
            {
             before(grammarAccess.getInterfaceAccess().getNameAssignment_2()); 
            // InternalDeclaration.g:2859:2: ( rule__Interface__NameAssignment_2 )
            // InternalDeclaration.g:2859:3: rule__Interface__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Interface__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__2__Impl"


    // $ANTLR start "rule__Interface__Group__3"
    // InternalDeclaration.g:2867:1: rule__Interface__Group__3 : rule__Interface__Group__3__Impl rule__Interface__Group__4 ;
    public final void rule__Interface__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2871:1: ( rule__Interface__Group__3__Impl rule__Interface__Group__4 )
            // InternalDeclaration.g:2872:2: rule__Interface__Group__3__Impl rule__Interface__Group__4
            {
            pushFollow(FOLLOW_32);
            rule__Interface__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interface__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__3"


    // $ANTLR start "rule__Interface__Group__3__Impl"
    // InternalDeclaration.g:2879:1: rule__Interface__Group__3__Impl : ( '{' ) ;
    public final void rule__Interface__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2883:1: ( ( '{' ) )
            // InternalDeclaration.g:2884:1: ( '{' )
            {
            // InternalDeclaration.g:2884:1: ( '{' )
            // InternalDeclaration.g:2885:2: '{'
            {
             before(grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__3__Impl"


    // $ANTLR start "rule__Interface__Group__4"
    // InternalDeclaration.g:2894:1: rule__Interface__Group__4 : rule__Interface__Group__4__Impl rule__Interface__Group__5 ;
    public final void rule__Interface__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2898:1: ( rule__Interface__Group__4__Impl rule__Interface__Group__5 )
            // InternalDeclaration.g:2899:2: rule__Interface__Group__4__Impl rule__Interface__Group__5
            {
            pushFollow(FOLLOW_32);
            rule__Interface__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interface__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__4"


    // $ANTLR start "rule__Interface__Group__4__Impl"
    // InternalDeclaration.g:2906:1: rule__Interface__Group__4__Impl : ( ( rule__Interface__Group_4__0 )? ) ;
    public final void rule__Interface__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2910:1: ( ( ( rule__Interface__Group_4__0 )? ) )
            // InternalDeclaration.g:2911:1: ( ( rule__Interface__Group_4__0 )? )
            {
            // InternalDeclaration.g:2911:1: ( ( rule__Interface__Group_4__0 )? )
            // InternalDeclaration.g:2912:2: ( rule__Interface__Group_4__0 )?
            {
             before(grammarAccess.getInterfaceAccess().getGroup_4()); 
            // InternalDeclaration.g:2913:2: ( rule__Interface__Group_4__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=RULE_STRING && LA12_0<=RULE_ID)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDeclaration.g:2913:3: rule__Interface__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Interface__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getInterfaceAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__4__Impl"


    // $ANTLR start "rule__Interface__Group__5"
    // InternalDeclaration.g:2921:1: rule__Interface__Group__5 : rule__Interface__Group__5__Impl ;
    public final void rule__Interface__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2925:1: ( rule__Interface__Group__5__Impl )
            // InternalDeclaration.g:2926:2: rule__Interface__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interface__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__5"


    // $ANTLR start "rule__Interface__Group__5__Impl"
    // InternalDeclaration.g:2932:1: rule__Interface__Group__5__Impl : ( '}' ) ;
    public final void rule__Interface__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2936:1: ( ( '}' ) )
            // InternalDeclaration.g:2937:1: ( '}' )
            {
            // InternalDeclaration.g:2937:1: ( '}' )
            // InternalDeclaration.g:2938:2: '}'
            {
             before(grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group__5__Impl"


    // $ANTLR start "rule__Interface__Group_4__0"
    // InternalDeclaration.g:2948:1: rule__Interface__Group_4__0 : rule__Interface__Group_4__0__Impl rule__Interface__Group_4__1 ;
    public final void rule__Interface__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2952:1: ( rule__Interface__Group_4__0__Impl rule__Interface__Group_4__1 )
            // InternalDeclaration.g:2953:2: rule__Interface__Group_4__0__Impl rule__Interface__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__Interface__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Interface__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group_4__0"


    // $ANTLR start "rule__Interface__Group_4__0__Impl"
    // InternalDeclaration.g:2960:1: rule__Interface__Group_4__0__Impl : ( ( rule__Interface__FunctionsAssignment_4_0 ) ) ;
    public final void rule__Interface__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2964:1: ( ( ( rule__Interface__FunctionsAssignment_4_0 ) ) )
            // InternalDeclaration.g:2965:1: ( ( rule__Interface__FunctionsAssignment_4_0 ) )
            {
            // InternalDeclaration.g:2965:1: ( ( rule__Interface__FunctionsAssignment_4_0 ) )
            // InternalDeclaration.g:2966:2: ( rule__Interface__FunctionsAssignment_4_0 )
            {
             before(grammarAccess.getInterfaceAccess().getFunctionsAssignment_4_0()); 
            // InternalDeclaration.g:2967:2: ( rule__Interface__FunctionsAssignment_4_0 )
            // InternalDeclaration.g:2967:3: rule__Interface__FunctionsAssignment_4_0
            {
            pushFollow(FOLLOW_2);
            rule__Interface__FunctionsAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getInterfaceAccess().getFunctionsAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group_4__0__Impl"


    // $ANTLR start "rule__Interface__Group_4__1"
    // InternalDeclaration.g:2975:1: rule__Interface__Group_4__1 : rule__Interface__Group_4__1__Impl ;
    public final void rule__Interface__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2979:1: ( rule__Interface__Group_4__1__Impl )
            // InternalDeclaration.g:2980:2: rule__Interface__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Interface__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group_4__1"


    // $ANTLR start "rule__Interface__Group_4__1__Impl"
    // InternalDeclaration.g:2986:1: rule__Interface__Group_4__1__Impl : ( ( rule__Interface__FunctionsAssignment_4_1 )* ) ;
    public final void rule__Interface__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:2990:1: ( ( ( rule__Interface__FunctionsAssignment_4_1 )* ) )
            // InternalDeclaration.g:2991:1: ( ( rule__Interface__FunctionsAssignment_4_1 )* )
            {
            // InternalDeclaration.g:2991:1: ( ( rule__Interface__FunctionsAssignment_4_1 )* )
            // InternalDeclaration.g:2992:2: ( rule__Interface__FunctionsAssignment_4_1 )*
            {
             before(grammarAccess.getInterfaceAccess().getFunctionsAssignment_4_1()); 
            // InternalDeclaration.g:2993:2: ( rule__Interface__FunctionsAssignment_4_1 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>=RULE_STRING && LA13_0<=RULE_ID)) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalDeclaration.g:2993:3: rule__Interface__FunctionsAssignment_4_1
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__Interface__FunctionsAssignment_4_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getInterfaceAccess().getFunctionsAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__Group_4__1__Impl"


    // $ANTLR start "rule__Function__Group__0"
    // InternalDeclaration.g:3002:1: rule__Function__Group__0 : rule__Function__Group__0__Impl rule__Function__Group__1 ;
    public final void rule__Function__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3006:1: ( rule__Function__Group__0__Impl rule__Function__Group__1 )
            // InternalDeclaration.g:3007:2: rule__Function__Group__0__Impl rule__Function__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Function__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0"


    // $ANTLR start "rule__Function__Group__0__Impl"
    // InternalDeclaration.g:3014:1: rule__Function__Group__0__Impl : ( ( rule__Function__ReturnTypeAssignment_0 )? ) ;
    public final void rule__Function__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3018:1: ( ( ( rule__Function__ReturnTypeAssignment_0 )? ) )
            // InternalDeclaration.g:3019:1: ( ( rule__Function__ReturnTypeAssignment_0 )? )
            {
            // InternalDeclaration.g:3019:1: ( ( rule__Function__ReturnTypeAssignment_0 )? )
            // InternalDeclaration.g:3020:2: ( rule__Function__ReturnTypeAssignment_0 )?
            {
             before(grammarAccess.getFunctionAccess().getReturnTypeAssignment_0()); 
            // InternalDeclaration.g:3021:2: ( rule__Function__ReturnTypeAssignment_0 )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_STRING) ) {
                int LA14_1 = input.LA(2);

                if ( ((LA14_1>=RULE_STRING && LA14_1<=RULE_ID)) ) {
                    alt14=1;
                }
            }
            else if ( (LA14_0==RULE_ID) ) {
                int LA14_2 = input.LA(2);

                if ( ((LA14_2>=RULE_STRING && LA14_2<=RULE_ID)) ) {
                    alt14=1;
                }
            }
            switch (alt14) {
                case 1 :
                    // InternalDeclaration.g:3021:3: rule__Function__ReturnTypeAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__ReturnTypeAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionAccess().getReturnTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__0__Impl"


    // $ANTLR start "rule__Function__Group__1"
    // InternalDeclaration.g:3029:1: rule__Function__Group__1 : rule__Function__Group__1__Impl rule__Function__Group__2 ;
    public final void rule__Function__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3033:1: ( rule__Function__Group__1__Impl rule__Function__Group__2 )
            // InternalDeclaration.g:3034:2: rule__Function__Group__1__Impl rule__Function__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Function__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1"


    // $ANTLR start "rule__Function__Group__1__Impl"
    // InternalDeclaration.g:3041:1: rule__Function__Group__1__Impl : ( ( rule__Function__NameAssignment_1 ) ) ;
    public final void rule__Function__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3045:1: ( ( ( rule__Function__NameAssignment_1 ) ) )
            // InternalDeclaration.g:3046:1: ( ( rule__Function__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:3046:1: ( ( rule__Function__NameAssignment_1 ) )
            // InternalDeclaration.g:3047:2: ( rule__Function__NameAssignment_1 )
            {
             before(grammarAccess.getFunctionAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:3048:2: ( rule__Function__NameAssignment_1 )
            // InternalDeclaration.g:3048:3: rule__Function__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Function__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__1__Impl"


    // $ANTLR start "rule__Function__Group__2"
    // InternalDeclaration.g:3056:1: rule__Function__Group__2 : rule__Function__Group__2__Impl rule__Function__Group__3 ;
    public final void rule__Function__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3060:1: ( rule__Function__Group__2__Impl rule__Function__Group__3 )
            // InternalDeclaration.g:3061:2: rule__Function__Group__2__Impl rule__Function__Group__3
            {
            pushFollow(FOLLOW_34);
            rule__Function__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2"


    // $ANTLR start "rule__Function__Group__2__Impl"
    // InternalDeclaration.g:3068:1: rule__Function__Group__2__Impl : ( '(' ) ;
    public final void rule__Function__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3072:1: ( ( '(' ) )
            // InternalDeclaration.g:3073:1: ( '(' )
            {
            // InternalDeclaration.g:3073:1: ( '(' )
            // InternalDeclaration.g:3074:2: '('
            {
             before(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__2__Impl"


    // $ANTLR start "rule__Function__Group__3"
    // InternalDeclaration.g:3083:1: rule__Function__Group__3 : rule__Function__Group__3__Impl rule__Function__Group__4 ;
    public final void rule__Function__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3087:1: ( rule__Function__Group__3__Impl rule__Function__Group__4 )
            // InternalDeclaration.g:3088:2: rule__Function__Group__3__Impl rule__Function__Group__4
            {
            pushFollow(FOLLOW_34);
            rule__Function__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3"


    // $ANTLR start "rule__Function__Group__3__Impl"
    // InternalDeclaration.g:3095:1: rule__Function__Group__3__Impl : ( ( rule__Function__Group_3__0 )? ) ;
    public final void rule__Function__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3099:1: ( ( ( rule__Function__Group_3__0 )? ) )
            // InternalDeclaration.g:3100:1: ( ( rule__Function__Group_3__0 )? )
            {
            // InternalDeclaration.g:3100:1: ( ( rule__Function__Group_3__0 )? )
            // InternalDeclaration.g:3101:2: ( rule__Function__Group_3__0 )?
            {
             before(grammarAccess.getFunctionAccess().getGroup_3()); 
            // InternalDeclaration.g:3102:2: ( rule__Function__Group_3__0 )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=RULE_STRING && LA15_0<=RULE_ID)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDeclaration.g:3102:3: rule__Function__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Function__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__3__Impl"


    // $ANTLR start "rule__Function__Group__4"
    // InternalDeclaration.g:3110:1: rule__Function__Group__4 : rule__Function__Group__4__Impl rule__Function__Group__5 ;
    public final void rule__Function__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3114:1: ( rule__Function__Group__4__Impl rule__Function__Group__5 )
            // InternalDeclaration.g:3115:2: rule__Function__Group__4__Impl rule__Function__Group__5
            {
            pushFollow(FOLLOW_19);
            rule__Function__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4"


    // $ANTLR start "rule__Function__Group__4__Impl"
    // InternalDeclaration.g:3122:1: rule__Function__Group__4__Impl : ( ')' ) ;
    public final void rule__Function__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3126:1: ( ( ')' ) )
            // InternalDeclaration.g:3127:1: ( ')' )
            {
            // InternalDeclaration.g:3127:1: ( ')' )
            // InternalDeclaration.g:3128:2: ')'
            {
             before(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__4__Impl"


    // $ANTLR start "rule__Function__Group__5"
    // InternalDeclaration.g:3137:1: rule__Function__Group__5 : rule__Function__Group__5__Impl ;
    public final void rule__Function__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3141:1: ( rule__Function__Group__5__Impl )
            // InternalDeclaration.g:3142:2: rule__Function__Group__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5"


    // $ANTLR start "rule__Function__Group__5__Impl"
    // InternalDeclaration.g:3148:1: rule__Function__Group__5__Impl : ( ';' ) ;
    public final void rule__Function__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3152:1: ( ( ';' ) )
            // InternalDeclaration.g:3153:1: ( ';' )
            {
            // InternalDeclaration.g:3153:1: ( ';' )
            // InternalDeclaration.g:3154:2: ';'
            {
             before(grammarAccess.getFunctionAccess().getSemicolonKeyword_5()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getSemicolonKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group__5__Impl"


    // $ANTLR start "rule__Function__Group_3__0"
    // InternalDeclaration.g:3164:1: rule__Function__Group_3__0 : rule__Function__Group_3__0__Impl rule__Function__Group_3__1 ;
    public final void rule__Function__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3168:1: ( rule__Function__Group_3__0__Impl rule__Function__Group_3__1 )
            // InternalDeclaration.g:3169:2: rule__Function__Group_3__0__Impl rule__Function__Group_3__1
            {
            pushFollow(FOLLOW_29);
            rule__Function__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__0"


    // $ANTLR start "rule__Function__Group_3__0__Impl"
    // InternalDeclaration.g:3176:1: rule__Function__Group_3__0__Impl : ( ( rule__Function__ParametersAssignment_3_0 ) ) ;
    public final void rule__Function__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3180:1: ( ( ( rule__Function__ParametersAssignment_3_0 ) ) )
            // InternalDeclaration.g:3181:1: ( ( rule__Function__ParametersAssignment_3_0 ) )
            {
            // InternalDeclaration.g:3181:1: ( ( rule__Function__ParametersAssignment_3_0 ) )
            // InternalDeclaration.g:3182:2: ( rule__Function__ParametersAssignment_3_0 )
            {
             before(grammarAccess.getFunctionAccess().getParametersAssignment_3_0()); 
            // InternalDeclaration.g:3183:2: ( rule__Function__ParametersAssignment_3_0 )
            // InternalDeclaration.g:3183:3: rule__Function__ParametersAssignment_3_0
            {
            pushFollow(FOLLOW_2);
            rule__Function__ParametersAssignment_3_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParametersAssignment_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__0__Impl"


    // $ANTLR start "rule__Function__Group_3__1"
    // InternalDeclaration.g:3191:1: rule__Function__Group_3__1 : rule__Function__Group_3__1__Impl ;
    public final void rule__Function__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3195:1: ( rule__Function__Group_3__1__Impl )
            // InternalDeclaration.g:3196:2: rule__Function__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__1"


    // $ANTLR start "rule__Function__Group_3__1__Impl"
    // InternalDeclaration.g:3202:1: rule__Function__Group_3__1__Impl : ( ( rule__Function__Group_3_1__0 )* ) ;
    public final void rule__Function__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3206:1: ( ( ( rule__Function__Group_3_1__0 )* ) )
            // InternalDeclaration.g:3207:1: ( ( rule__Function__Group_3_1__0 )* )
            {
            // InternalDeclaration.g:3207:1: ( ( rule__Function__Group_3_1__0 )* )
            // InternalDeclaration.g:3208:2: ( rule__Function__Group_3_1__0 )*
            {
             before(grammarAccess.getFunctionAccess().getGroup_3_1()); 
            // InternalDeclaration.g:3209:2: ( rule__Function__Group_3_1__0 )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==28) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalDeclaration.g:3209:3: rule__Function__Group_3_1__0
            	    {
            	    pushFollow(FOLLOW_35);
            	    rule__Function__Group_3_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getFunctionAccess().getGroup_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3__1__Impl"


    // $ANTLR start "rule__Function__Group_3_1__0"
    // InternalDeclaration.g:3218:1: rule__Function__Group_3_1__0 : rule__Function__Group_3_1__0__Impl rule__Function__Group_3_1__1 ;
    public final void rule__Function__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3222:1: ( rule__Function__Group_3_1__0__Impl rule__Function__Group_3_1__1 )
            // InternalDeclaration.g:3223:2: rule__Function__Group_3_1__0__Impl rule__Function__Group_3_1__1
            {
            pushFollow(FOLLOW_4);
            rule__Function__Group_3_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Function__Group_3_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3_1__0"


    // $ANTLR start "rule__Function__Group_3_1__0__Impl"
    // InternalDeclaration.g:3230:1: rule__Function__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__Function__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3234:1: ( ( ',' ) )
            // InternalDeclaration.g:3235:1: ( ',' )
            {
            // InternalDeclaration.g:3235:1: ( ',' )
            // InternalDeclaration.g:3236:2: ','
            {
             before(grammarAccess.getFunctionAccess().getCommaKeyword_3_1_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getFunctionAccess().getCommaKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3_1__0__Impl"


    // $ANTLR start "rule__Function__Group_3_1__1"
    // InternalDeclaration.g:3245:1: rule__Function__Group_3_1__1 : rule__Function__Group_3_1__1__Impl ;
    public final void rule__Function__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3249:1: ( rule__Function__Group_3_1__1__Impl )
            // InternalDeclaration.g:3250:2: rule__Function__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Function__Group_3_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3_1__1"


    // $ANTLR start "rule__Function__Group_3_1__1__Impl"
    // InternalDeclaration.g:3256:1: rule__Function__Group_3_1__1__Impl : ( ( rule__Function__ParametersAssignment_3_1_1 ) ) ;
    public final void rule__Function__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3260:1: ( ( ( rule__Function__ParametersAssignment_3_1_1 ) ) )
            // InternalDeclaration.g:3261:1: ( ( rule__Function__ParametersAssignment_3_1_1 ) )
            {
            // InternalDeclaration.g:3261:1: ( ( rule__Function__ParametersAssignment_3_1_1 ) )
            // InternalDeclaration.g:3262:2: ( rule__Function__ParametersAssignment_3_1_1 )
            {
             before(grammarAccess.getFunctionAccess().getParametersAssignment_3_1_1()); 
            // InternalDeclaration.g:3263:2: ( rule__Function__ParametersAssignment_3_1_1 )
            // InternalDeclaration.g:3263:3: rule__Function__ParametersAssignment_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__Function__ParametersAssignment_3_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionAccess().getParametersAssignment_3_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__Group_3_1__1__Impl"


    // $ANTLR start "rule__ReturnType__Group__0"
    // InternalDeclaration.g:3272:1: rule__ReturnType__Group__0 : rule__ReturnType__Group__0__Impl rule__ReturnType__Group__1 ;
    public final void rule__ReturnType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3276:1: ( rule__ReturnType__Group__0__Impl rule__ReturnType__Group__1 )
            // InternalDeclaration.g:3277:2: rule__ReturnType__Group__0__Impl rule__ReturnType__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__ReturnType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReturnType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnType__Group__0"


    // $ANTLR start "rule__ReturnType__Group__0__Impl"
    // InternalDeclaration.g:3284:1: rule__ReturnType__Group__0__Impl : ( () ) ;
    public final void rule__ReturnType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3288:1: ( ( () ) )
            // InternalDeclaration.g:3289:1: ( () )
            {
            // InternalDeclaration.g:3289:1: ( () )
            // InternalDeclaration.g:3290:2: ()
            {
             before(grammarAccess.getReturnTypeAccess().getReturnTypeAction_0()); 
            // InternalDeclaration.g:3291:2: ()
            // InternalDeclaration.g:3291:3: 
            {
            }

             after(grammarAccess.getReturnTypeAccess().getReturnTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnType__Group__0__Impl"


    // $ANTLR start "rule__ReturnType__Group__1"
    // InternalDeclaration.g:3299:1: rule__ReturnType__Group__1 : rule__ReturnType__Group__1__Impl ;
    public final void rule__ReturnType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3303:1: ( rule__ReturnType__Group__1__Impl )
            // InternalDeclaration.g:3304:2: rule__ReturnType__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReturnType__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnType__Group__1"


    // $ANTLR start "rule__ReturnType__Group__1__Impl"
    // InternalDeclaration.g:3310:1: rule__ReturnType__Group__1__Impl : ( ( rule__ReturnType__TypeAssignment_1 ) ) ;
    public final void rule__ReturnType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3314:1: ( ( ( rule__ReturnType__TypeAssignment_1 ) ) )
            // InternalDeclaration.g:3315:1: ( ( rule__ReturnType__TypeAssignment_1 ) )
            {
            // InternalDeclaration.g:3315:1: ( ( rule__ReturnType__TypeAssignment_1 ) )
            // InternalDeclaration.g:3316:2: ( rule__ReturnType__TypeAssignment_1 )
            {
             before(grammarAccess.getReturnTypeAccess().getTypeAssignment_1()); 
            // InternalDeclaration.g:3317:2: ( rule__ReturnType__TypeAssignment_1 )
            // InternalDeclaration.g:3317:3: rule__ReturnType__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ReturnType__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReturnTypeAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnType__Group__1__Impl"


    // $ANTLR start "rule__InterfacePort__Group__0"
    // InternalDeclaration.g:3326:1: rule__InterfacePort__Group__0 : rule__InterfacePort__Group__0__Impl rule__InterfacePort__Group__1 ;
    public final void rule__InterfacePort__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3330:1: ( rule__InterfacePort__Group__0__Impl rule__InterfacePort__Group__1 )
            // InternalDeclaration.g:3331:2: rule__InterfacePort__Group__0__Impl rule__InterfacePort__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__InterfacePort__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfacePort__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__Group__0"


    // $ANTLR start "rule__InterfacePort__Group__0__Impl"
    // InternalDeclaration.g:3338:1: rule__InterfacePort__Group__0__Impl : ( () ) ;
    public final void rule__InterfacePort__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3342:1: ( ( () ) )
            // InternalDeclaration.g:3343:1: ( () )
            {
            // InternalDeclaration.g:3343:1: ( () )
            // InternalDeclaration.g:3344:2: ()
            {
             before(grammarAccess.getInterfacePortAccess().getInterfacePortAction_0()); 
            // InternalDeclaration.g:3345:2: ()
            // InternalDeclaration.g:3345:3: 
            {
            }

             after(grammarAccess.getInterfacePortAccess().getInterfacePortAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__Group__0__Impl"


    // $ANTLR start "rule__InterfacePort__Group__1"
    // InternalDeclaration.g:3353:1: rule__InterfacePort__Group__1 : rule__InterfacePort__Group__1__Impl rule__InterfacePort__Group__2 ;
    public final void rule__InterfacePort__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3357:1: ( rule__InterfacePort__Group__1__Impl rule__InterfacePort__Group__2 )
            // InternalDeclaration.g:3358:2: rule__InterfacePort__Group__1__Impl rule__InterfacePort__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__InterfacePort__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InterfacePort__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__Group__1"


    // $ANTLR start "rule__InterfacePort__Group__1__Impl"
    // InternalDeclaration.g:3365:1: rule__InterfacePort__Group__1__Impl : ( ( rule__InterfacePort__InterfaceAssignment_1 ) ) ;
    public final void rule__InterfacePort__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3369:1: ( ( ( rule__InterfacePort__InterfaceAssignment_1 ) ) )
            // InternalDeclaration.g:3370:1: ( ( rule__InterfacePort__InterfaceAssignment_1 ) )
            {
            // InternalDeclaration.g:3370:1: ( ( rule__InterfacePort__InterfaceAssignment_1 ) )
            // InternalDeclaration.g:3371:2: ( rule__InterfacePort__InterfaceAssignment_1 )
            {
             before(grammarAccess.getInterfacePortAccess().getInterfaceAssignment_1()); 
            // InternalDeclaration.g:3372:2: ( rule__InterfacePort__InterfaceAssignment_1 )
            // InternalDeclaration.g:3372:3: rule__InterfacePort__InterfaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__InterfacePort__InterfaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInterfacePortAccess().getInterfaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__Group__1__Impl"


    // $ANTLR start "rule__InterfacePort__Group__2"
    // InternalDeclaration.g:3380:1: rule__InterfacePort__Group__2 : rule__InterfacePort__Group__2__Impl ;
    public final void rule__InterfacePort__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3384:1: ( rule__InterfacePort__Group__2__Impl )
            // InternalDeclaration.g:3385:2: rule__InterfacePort__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InterfacePort__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__Group__2"


    // $ANTLR start "rule__InterfacePort__Group__2__Impl"
    // InternalDeclaration.g:3391:1: rule__InterfacePort__Group__2__Impl : ( ( rule__InterfacePort__NameAssignment_2 ) ) ;
    public final void rule__InterfacePort__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3395:1: ( ( ( rule__InterfacePort__NameAssignment_2 ) ) )
            // InternalDeclaration.g:3396:1: ( ( rule__InterfacePort__NameAssignment_2 ) )
            {
            // InternalDeclaration.g:3396:1: ( ( rule__InterfacePort__NameAssignment_2 ) )
            // InternalDeclaration.g:3397:2: ( rule__InterfacePort__NameAssignment_2 )
            {
             before(grammarAccess.getInterfacePortAccess().getNameAssignment_2()); 
            // InternalDeclaration.g:3398:2: ( rule__InterfacePort__NameAssignment_2 )
            // InternalDeclaration.g:3398:3: rule__InterfacePort__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__InterfacePort__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getInterfacePortAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__Group__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__0"
    // InternalDeclaration.g:3407:1: rule__LeafComponentType__Group__0 : rule__LeafComponentType__Group__0__Impl rule__LeafComponentType__Group__1 ;
    public final void rule__LeafComponentType__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3411:1: ( rule__LeafComponentType__Group__0__Impl rule__LeafComponentType__Group__1 )
            // InternalDeclaration.g:3412:2: rule__LeafComponentType__Group__0__Impl rule__LeafComponentType__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__LeafComponentType__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__0"


    // $ANTLR start "rule__LeafComponentType__Group__0__Impl"
    // InternalDeclaration.g:3419:1: rule__LeafComponentType__Group__0__Impl : ( () ) ;
    public final void rule__LeafComponentType__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3423:1: ( ( () ) )
            // InternalDeclaration.g:3424:1: ( () )
            {
            // InternalDeclaration.g:3424:1: ( () )
            // InternalDeclaration.g:3425:2: ()
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeafComponentTypeAction_0()); 
            // InternalDeclaration.g:3426:2: ()
            // InternalDeclaration.g:3426:3: 
            {
            }

             after(grammarAccess.getLeafComponentTypeAccess().getLeafComponentTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__1"
    // InternalDeclaration.g:3434:1: rule__LeafComponentType__Group__1 : rule__LeafComponentType__Group__1__Impl rule__LeafComponentType__Group__2 ;
    public final void rule__LeafComponentType__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3438:1: ( rule__LeafComponentType__Group__1__Impl rule__LeafComponentType__Group__2 )
            // InternalDeclaration.g:3439:2: rule__LeafComponentType__Group__1__Impl rule__LeafComponentType__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__1"


    // $ANTLR start "rule__LeafComponentType__Group__1__Impl"
    // InternalDeclaration.g:3446:1: rule__LeafComponentType__Group__1__Impl : ( 'LeafComponentType' ) ;
    public final void rule__LeafComponentType__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3450:1: ( ( 'LeafComponentType' ) )
            // InternalDeclaration.g:3451:1: ( 'LeafComponentType' )
            {
            // InternalDeclaration.g:3451:1: ( 'LeafComponentType' )
            // InternalDeclaration.g:3452:2: 'LeafComponentType'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeafComponentTypeKeyword_1()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeafComponentTypeKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__2"
    // InternalDeclaration.g:3461:1: rule__LeafComponentType__Group__2 : rule__LeafComponentType__Group__2__Impl rule__LeafComponentType__Group__3 ;
    public final void rule__LeafComponentType__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3465:1: ( rule__LeafComponentType__Group__2__Impl rule__LeafComponentType__Group__3 )
            // InternalDeclaration.g:3466:2: rule__LeafComponentType__Group__2__Impl rule__LeafComponentType__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__LeafComponentType__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__2"


    // $ANTLR start "rule__LeafComponentType__Group__2__Impl"
    // InternalDeclaration.g:3473:1: rule__LeafComponentType__Group__2__Impl : ( ( rule__LeafComponentType__NameAssignment_2 ) ) ;
    public final void rule__LeafComponentType__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3477:1: ( ( ( rule__LeafComponentType__NameAssignment_2 ) ) )
            // InternalDeclaration.g:3478:1: ( ( rule__LeafComponentType__NameAssignment_2 ) )
            {
            // InternalDeclaration.g:3478:1: ( ( rule__LeafComponentType__NameAssignment_2 ) )
            // InternalDeclaration.g:3479:2: ( rule__LeafComponentType__NameAssignment_2 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getNameAssignment_2()); 
            // InternalDeclaration.g:3480:2: ( rule__LeafComponentType__NameAssignment_2 )
            // InternalDeclaration.g:3480:3: rule__LeafComponentType__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__3"
    // InternalDeclaration.g:3488:1: rule__LeafComponentType__Group__3 : rule__LeafComponentType__Group__3__Impl rule__LeafComponentType__Group__4 ;
    public final void rule__LeafComponentType__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3492:1: ( rule__LeafComponentType__Group__3__Impl rule__LeafComponentType__Group__4 )
            // InternalDeclaration.g:3493:2: rule__LeafComponentType__Group__3__Impl rule__LeafComponentType__Group__4
            {
            pushFollow(FOLLOW_36);
            rule__LeafComponentType__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__3"


    // $ANTLR start "rule__LeafComponentType__Group__3__Impl"
    // InternalDeclaration.g:3500:1: rule__LeafComponentType__Group__3__Impl : ( '{' ) ;
    public final void rule__LeafComponentType__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3504:1: ( ( '{' ) )
            // InternalDeclaration.g:3505:1: ( '{' )
            {
            // InternalDeclaration.g:3505:1: ( '{' )
            // InternalDeclaration.g:3506:2: '{'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_3()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__3__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__4"
    // InternalDeclaration.g:3515:1: rule__LeafComponentType__Group__4 : rule__LeafComponentType__Group__4__Impl rule__LeafComponentType__Group__5 ;
    public final void rule__LeafComponentType__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3519:1: ( rule__LeafComponentType__Group__4__Impl rule__LeafComponentType__Group__5 )
            // InternalDeclaration.g:3520:2: rule__LeafComponentType__Group__4__Impl rule__LeafComponentType__Group__5
            {
            pushFollow(FOLLOW_36);
            rule__LeafComponentType__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__4"


    // $ANTLR start "rule__LeafComponentType__Group__4__Impl"
    // InternalDeclaration.g:3527:1: rule__LeafComponentType__Group__4__Impl : ( ( rule__LeafComponentType__Group_4__0 )? ) ;
    public final void rule__LeafComponentType__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3531:1: ( ( ( rule__LeafComponentType__Group_4__0 )? ) )
            // InternalDeclaration.g:3532:1: ( ( rule__LeafComponentType__Group_4__0 )? )
            {
            // InternalDeclaration.g:3532:1: ( ( rule__LeafComponentType__Group_4__0 )? )
            // InternalDeclaration.g:3533:2: ( rule__LeafComponentType__Group_4__0 )?
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_4()); 
            // InternalDeclaration.g:3534:2: ( rule__LeafComponentType__Group_4__0 )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==33) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDeclaration.g:3534:3: rule__LeafComponentType__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LeafComponentType__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__4__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__5"
    // InternalDeclaration.g:3542:1: rule__LeafComponentType__Group__5 : rule__LeafComponentType__Group__5__Impl rule__LeafComponentType__Group__6 ;
    public final void rule__LeafComponentType__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3546:1: ( rule__LeafComponentType__Group__5__Impl rule__LeafComponentType__Group__6 )
            // InternalDeclaration.g:3547:2: rule__LeafComponentType__Group__5__Impl rule__LeafComponentType__Group__6
            {
            pushFollow(FOLLOW_36);
            rule__LeafComponentType__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__5"


    // $ANTLR start "rule__LeafComponentType__Group__5__Impl"
    // InternalDeclaration.g:3554:1: rule__LeafComponentType__Group__5__Impl : ( ( rule__LeafComponentType__Group_5__0 )? ) ;
    public final void rule__LeafComponentType__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3558:1: ( ( ( rule__LeafComponentType__Group_5__0 )? ) )
            // InternalDeclaration.g:3559:1: ( ( rule__LeafComponentType__Group_5__0 )? )
            {
            // InternalDeclaration.g:3559:1: ( ( rule__LeafComponentType__Group_5__0 )? )
            // InternalDeclaration.g:3560:2: ( rule__LeafComponentType__Group_5__0 )?
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_5()); 
            // InternalDeclaration.g:3561:2: ( rule__LeafComponentType__Group_5__0 )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==34) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalDeclaration.g:3561:3: rule__LeafComponentType__Group_5__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LeafComponentType__Group_5__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__5__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__6"
    // InternalDeclaration.g:3569:1: rule__LeafComponentType__Group__6 : rule__LeafComponentType__Group__6__Impl rule__LeafComponentType__Group__7 ;
    public final void rule__LeafComponentType__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3573:1: ( rule__LeafComponentType__Group__6__Impl rule__LeafComponentType__Group__7 )
            // InternalDeclaration.g:3574:2: rule__LeafComponentType__Group__6__Impl rule__LeafComponentType__Group__7
            {
            pushFollow(FOLLOW_36);
            rule__LeafComponentType__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__6"


    // $ANTLR start "rule__LeafComponentType__Group__6__Impl"
    // InternalDeclaration.g:3581:1: rule__LeafComponentType__Group__6__Impl : ( ( rule__LeafComponentType__Group_6__0 )? ) ;
    public final void rule__LeafComponentType__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3585:1: ( ( ( rule__LeafComponentType__Group_6__0 )? ) )
            // InternalDeclaration.g:3586:1: ( ( rule__LeafComponentType__Group_6__0 )? )
            {
            // InternalDeclaration.g:3586:1: ( ( rule__LeafComponentType__Group_6__0 )? )
            // InternalDeclaration.g:3587:2: ( rule__LeafComponentType__Group_6__0 )?
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_6()); 
            // InternalDeclaration.g:3588:2: ( rule__LeafComponentType__Group_6__0 )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==35) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalDeclaration.g:3588:3: rule__LeafComponentType__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LeafComponentType__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__6__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__7"
    // InternalDeclaration.g:3596:1: rule__LeafComponentType__Group__7 : rule__LeafComponentType__Group__7__Impl rule__LeafComponentType__Group__8 ;
    public final void rule__LeafComponentType__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3600:1: ( rule__LeafComponentType__Group__7__Impl rule__LeafComponentType__Group__8 )
            // InternalDeclaration.g:3601:2: rule__LeafComponentType__Group__7__Impl rule__LeafComponentType__Group__8
            {
            pushFollow(FOLLOW_36);
            rule__LeafComponentType__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__7"


    // $ANTLR start "rule__LeafComponentType__Group__7__Impl"
    // InternalDeclaration.g:3608:1: rule__LeafComponentType__Group__7__Impl : ( ( rule__LeafComponentType__Group_7__0 )? ) ;
    public final void rule__LeafComponentType__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3612:1: ( ( ( rule__LeafComponentType__Group_7__0 )? ) )
            // InternalDeclaration.g:3613:1: ( ( rule__LeafComponentType__Group_7__0 )? )
            {
            // InternalDeclaration.g:3613:1: ( ( rule__LeafComponentType__Group_7__0 )? )
            // InternalDeclaration.g:3614:2: ( rule__LeafComponentType__Group_7__0 )?
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_7()); 
            // InternalDeclaration.g:3615:2: ( rule__LeafComponentType__Group_7__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==36) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalDeclaration.g:3615:3: rule__LeafComponentType__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LeafComponentType__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__7__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__8"
    // InternalDeclaration.g:3623:1: rule__LeafComponentType__Group__8 : rule__LeafComponentType__Group__8__Impl rule__LeafComponentType__Group__9 ;
    public final void rule__LeafComponentType__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3627:1: ( rule__LeafComponentType__Group__8__Impl rule__LeafComponentType__Group__9 )
            // InternalDeclaration.g:3628:2: rule__LeafComponentType__Group__8__Impl rule__LeafComponentType__Group__9
            {
            pushFollow(FOLLOW_36);
            rule__LeafComponentType__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__8"


    // $ANTLR start "rule__LeafComponentType__Group__8__Impl"
    // InternalDeclaration.g:3635:1: rule__LeafComponentType__Group__8__Impl : ( ( rule__LeafComponentType__Group_8__0 )? ) ;
    public final void rule__LeafComponentType__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3639:1: ( ( ( rule__LeafComponentType__Group_8__0 )? ) )
            // InternalDeclaration.g:3640:1: ( ( rule__LeafComponentType__Group_8__0 )? )
            {
            // InternalDeclaration.g:3640:1: ( ( rule__LeafComponentType__Group_8__0 )? )
            // InternalDeclaration.g:3641:2: ( rule__LeafComponentType__Group_8__0 )?
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_8()); 
            // InternalDeclaration.g:3642:2: ( rule__LeafComponentType__Group_8__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==37) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalDeclaration.g:3642:3: rule__LeafComponentType__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LeafComponentType__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__8__Impl"


    // $ANTLR start "rule__LeafComponentType__Group__9"
    // InternalDeclaration.g:3650:1: rule__LeafComponentType__Group__9 : rule__LeafComponentType__Group__9__Impl ;
    public final void rule__LeafComponentType__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3654:1: ( rule__LeafComponentType__Group__9__Impl )
            // InternalDeclaration.g:3655:2: rule__LeafComponentType__Group__9__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group__9__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__9"


    // $ANTLR start "rule__LeafComponentType__Group__9__Impl"
    // InternalDeclaration.g:3661:1: rule__LeafComponentType__Group__9__Impl : ( '}' ) ;
    public final void rule__LeafComponentType__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3665:1: ( ( '}' ) )
            // InternalDeclaration.g:3666:1: ( '}' )
            {
            // InternalDeclaration.g:3666:1: ( '}' )
            // InternalDeclaration.g:3667:2: '}'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_9()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group__9__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4__0"
    // InternalDeclaration.g:3677:1: rule__LeafComponentType__Group_4__0 : rule__LeafComponentType__Group_4__0__Impl rule__LeafComponentType__Group_4__1 ;
    public final void rule__LeafComponentType__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3681:1: ( rule__LeafComponentType__Group_4__0__Impl rule__LeafComponentType__Group_4__1 )
            // InternalDeclaration.g:3682:2: rule__LeafComponentType__Group_4__0__Impl rule__LeafComponentType__Group_4__1
            {
            pushFollow(FOLLOW_16);
            rule__LeafComponentType__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__0"


    // $ANTLR start "rule__LeafComponentType__Group_4__0__Impl"
    // InternalDeclaration.g:3689:1: rule__LeafComponentType__Group_4__0__Impl : ( 'requiredInterfaces' ) ;
    public final void rule__LeafComponentType__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3693:1: ( ( 'requiredInterfaces' ) )
            // InternalDeclaration.g:3694:1: ( 'requiredInterfaces' )
            {
            // InternalDeclaration.g:3694:1: ( 'requiredInterfaces' )
            // InternalDeclaration.g:3695:2: 'requiredInterfaces'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfacesKeyword_4_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfacesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4__1"
    // InternalDeclaration.g:3704:1: rule__LeafComponentType__Group_4__1 : rule__LeafComponentType__Group_4__1__Impl rule__LeafComponentType__Group_4__2 ;
    public final void rule__LeafComponentType__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3708:1: ( rule__LeafComponentType__Group_4__1__Impl rule__LeafComponentType__Group_4__2 )
            // InternalDeclaration.g:3709:2: rule__LeafComponentType__Group_4__1__Impl rule__LeafComponentType__Group_4__2
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__1"


    // $ANTLR start "rule__LeafComponentType__Group_4__1__Impl"
    // InternalDeclaration.g:3716:1: rule__LeafComponentType__Group_4__1__Impl : ( '(' ) ;
    public final void rule__LeafComponentType__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3720:1: ( ( '(' ) )
            // InternalDeclaration.g:3721:1: ( '(' )
            {
            // InternalDeclaration.g:3721:1: ( '(' )
            // InternalDeclaration.g:3722:2: '('
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeftParenthesisKeyword_4_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeftParenthesisKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4__2"
    // InternalDeclaration.g:3731:1: rule__LeafComponentType__Group_4__2 : rule__LeafComponentType__Group_4__2__Impl rule__LeafComponentType__Group_4__3 ;
    public final void rule__LeafComponentType__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3735:1: ( rule__LeafComponentType__Group_4__2__Impl rule__LeafComponentType__Group_4__3 )
            // InternalDeclaration.g:3736:2: rule__LeafComponentType__Group_4__2__Impl rule__LeafComponentType__Group_4__3
            {
            pushFollow(FOLLOW_37);
            rule__LeafComponentType__Group_4__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__2"


    // $ANTLR start "rule__LeafComponentType__Group_4__2__Impl"
    // InternalDeclaration.g:3743:1: rule__LeafComponentType__Group_4__2__Impl : ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_2 ) ) ;
    public final void rule__LeafComponentType__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3747:1: ( ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_2 ) ) )
            // InternalDeclaration.g:3748:1: ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_2 ) )
            {
            // InternalDeclaration.g:3748:1: ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_2 ) )
            // InternalDeclaration.g:3749:2: ( rule__LeafComponentType__RequiredInterfaceAssignment_4_2 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceAssignment_4_2()); 
            // InternalDeclaration.g:3750:2: ( rule__LeafComponentType__RequiredInterfaceAssignment_4_2 )
            // InternalDeclaration.g:3750:3: rule__LeafComponentType__RequiredInterfaceAssignment_4_2
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__RequiredInterfaceAssignment_4_2();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceAssignment_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4__3"
    // InternalDeclaration.g:3758:1: rule__LeafComponentType__Group_4__3 : rule__LeafComponentType__Group_4__3__Impl rule__LeafComponentType__Group_4__4 ;
    public final void rule__LeafComponentType__Group_4__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3762:1: ( rule__LeafComponentType__Group_4__3__Impl rule__LeafComponentType__Group_4__4 )
            // InternalDeclaration.g:3763:2: rule__LeafComponentType__Group_4__3__Impl rule__LeafComponentType__Group_4__4
            {
            pushFollow(FOLLOW_37);
            rule__LeafComponentType__Group_4__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__3"


    // $ANTLR start "rule__LeafComponentType__Group_4__3__Impl"
    // InternalDeclaration.g:3770:1: rule__LeafComponentType__Group_4__3__Impl : ( ( rule__LeafComponentType__Group_4_3__0 )* ) ;
    public final void rule__LeafComponentType__Group_4__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3774:1: ( ( ( rule__LeafComponentType__Group_4_3__0 )* ) )
            // InternalDeclaration.g:3775:1: ( ( rule__LeafComponentType__Group_4_3__0 )* )
            {
            // InternalDeclaration.g:3775:1: ( ( rule__LeafComponentType__Group_4_3__0 )* )
            // InternalDeclaration.g:3776:2: ( rule__LeafComponentType__Group_4_3__0 )*
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_4_3()); 
            // InternalDeclaration.g:3777:2: ( rule__LeafComponentType__Group_4_3__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==28) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalDeclaration.g:3777:3: rule__LeafComponentType__Group_4_3__0
            	    {
            	    pushFollow(FOLLOW_35);
            	    rule__LeafComponentType__Group_4_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_4_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__3__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4__4"
    // InternalDeclaration.g:3785:1: rule__LeafComponentType__Group_4__4 : rule__LeafComponentType__Group_4__4__Impl ;
    public final void rule__LeafComponentType__Group_4__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3789:1: ( rule__LeafComponentType__Group_4__4__Impl )
            // InternalDeclaration.g:3790:2: rule__LeafComponentType__Group_4__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__4"


    // $ANTLR start "rule__LeafComponentType__Group_4__4__Impl"
    // InternalDeclaration.g:3796:1: rule__LeafComponentType__Group_4__4__Impl : ( ')' ) ;
    public final void rule__LeafComponentType__Group_4__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3800:1: ( ( ')' ) )
            // InternalDeclaration.g:3801:1: ( ')' )
            {
            // InternalDeclaration.g:3801:1: ( ')' )
            // InternalDeclaration.g:3802:2: ')'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRightParenthesisKeyword_4_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRightParenthesisKeyword_4_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4__4__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4_3__0"
    // InternalDeclaration.g:3812:1: rule__LeafComponentType__Group_4_3__0 : rule__LeafComponentType__Group_4_3__0__Impl rule__LeafComponentType__Group_4_3__1 ;
    public final void rule__LeafComponentType__Group_4_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3816:1: ( rule__LeafComponentType__Group_4_3__0__Impl rule__LeafComponentType__Group_4_3__1 )
            // InternalDeclaration.g:3817:2: rule__LeafComponentType__Group_4_3__0__Impl rule__LeafComponentType__Group_4_3__1
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_4_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4_3__0"


    // $ANTLR start "rule__LeafComponentType__Group_4_3__0__Impl"
    // InternalDeclaration.g:3824:1: rule__LeafComponentType__Group_4_3__0__Impl : ( ',' ) ;
    public final void rule__LeafComponentType__Group_4_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3828:1: ( ( ',' ) )
            // InternalDeclaration.g:3829:1: ( ',' )
            {
            // InternalDeclaration.g:3829:1: ( ',' )
            // InternalDeclaration.g:3830:2: ','
            {
             before(grammarAccess.getLeafComponentTypeAccess().getCommaKeyword_4_3_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getCommaKeyword_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4_3__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_4_3__1"
    // InternalDeclaration.g:3839:1: rule__LeafComponentType__Group_4_3__1 : rule__LeafComponentType__Group_4_3__1__Impl ;
    public final void rule__LeafComponentType__Group_4_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3843:1: ( rule__LeafComponentType__Group_4_3__1__Impl )
            // InternalDeclaration.g:3844:2: rule__LeafComponentType__Group_4_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_4_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4_3__1"


    // $ANTLR start "rule__LeafComponentType__Group_4_3__1__Impl"
    // InternalDeclaration.g:3850:1: rule__LeafComponentType__Group_4_3__1__Impl : ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 ) ) ;
    public final void rule__LeafComponentType__Group_4_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3854:1: ( ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 ) ) )
            // InternalDeclaration.g:3855:1: ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 ) )
            {
            // InternalDeclaration.g:3855:1: ( ( rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 ) )
            // InternalDeclaration.g:3856:2: ( rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceAssignment_4_3_1()); 
            // InternalDeclaration.g:3857:2: ( rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 )
            // InternalDeclaration.g:3857:3: rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceAssignment_4_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_4_3__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5__0"
    // InternalDeclaration.g:3866:1: rule__LeafComponentType__Group_5__0 : rule__LeafComponentType__Group_5__0__Impl rule__LeafComponentType__Group_5__1 ;
    public final void rule__LeafComponentType__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3870:1: ( rule__LeafComponentType__Group_5__0__Impl rule__LeafComponentType__Group_5__1 )
            // InternalDeclaration.g:3871:2: rule__LeafComponentType__Group_5__0__Impl rule__LeafComponentType__Group_5__1
            {
            pushFollow(FOLLOW_16);
            rule__LeafComponentType__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__0"


    // $ANTLR start "rule__LeafComponentType__Group_5__0__Impl"
    // InternalDeclaration.g:3878:1: rule__LeafComponentType__Group_5__0__Impl : ( 'providedInterfaces' ) ;
    public final void rule__LeafComponentType__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3882:1: ( ( 'providedInterfaces' ) )
            // InternalDeclaration.g:3883:1: ( 'providedInterfaces' )
            {
            // InternalDeclaration.g:3883:1: ( 'providedInterfaces' )
            // InternalDeclaration.g:3884:2: 'providedInterfaces'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfacesKeyword_5_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfacesKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5__1"
    // InternalDeclaration.g:3893:1: rule__LeafComponentType__Group_5__1 : rule__LeafComponentType__Group_5__1__Impl rule__LeafComponentType__Group_5__2 ;
    public final void rule__LeafComponentType__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3897:1: ( rule__LeafComponentType__Group_5__1__Impl rule__LeafComponentType__Group_5__2 )
            // InternalDeclaration.g:3898:2: rule__LeafComponentType__Group_5__1__Impl rule__LeafComponentType__Group_5__2
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_5__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__1"


    // $ANTLR start "rule__LeafComponentType__Group_5__1__Impl"
    // InternalDeclaration.g:3905:1: rule__LeafComponentType__Group_5__1__Impl : ( '(' ) ;
    public final void rule__LeafComponentType__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3909:1: ( ( '(' ) )
            // InternalDeclaration.g:3910:1: ( '(' )
            {
            // InternalDeclaration.g:3910:1: ( '(' )
            // InternalDeclaration.g:3911:2: '('
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeftParenthesisKeyword_5_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeftParenthesisKeyword_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5__2"
    // InternalDeclaration.g:3920:1: rule__LeafComponentType__Group_5__2 : rule__LeafComponentType__Group_5__2__Impl rule__LeafComponentType__Group_5__3 ;
    public final void rule__LeafComponentType__Group_5__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3924:1: ( rule__LeafComponentType__Group_5__2__Impl rule__LeafComponentType__Group_5__3 )
            // InternalDeclaration.g:3925:2: rule__LeafComponentType__Group_5__2__Impl rule__LeafComponentType__Group_5__3
            {
            pushFollow(FOLLOW_37);
            rule__LeafComponentType__Group_5__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__2"


    // $ANTLR start "rule__LeafComponentType__Group_5__2__Impl"
    // InternalDeclaration.g:3932:1: rule__LeafComponentType__Group_5__2__Impl : ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 ) ) ;
    public final void rule__LeafComponentType__Group_5__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3936:1: ( ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 ) ) )
            // InternalDeclaration.g:3937:1: ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 ) )
            {
            // InternalDeclaration.g:3937:1: ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 ) )
            // InternalDeclaration.g:3938:2: ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceAssignment_5_2()); 
            // InternalDeclaration.g:3939:2: ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 )
            // InternalDeclaration.g:3939:3: rule__LeafComponentType__ProvidedInterfaceAssignment_5_2
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__ProvidedInterfaceAssignment_5_2();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceAssignment_5_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5__3"
    // InternalDeclaration.g:3947:1: rule__LeafComponentType__Group_5__3 : rule__LeafComponentType__Group_5__3__Impl rule__LeafComponentType__Group_5__4 ;
    public final void rule__LeafComponentType__Group_5__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3951:1: ( rule__LeafComponentType__Group_5__3__Impl rule__LeafComponentType__Group_5__4 )
            // InternalDeclaration.g:3952:2: rule__LeafComponentType__Group_5__3__Impl rule__LeafComponentType__Group_5__4
            {
            pushFollow(FOLLOW_37);
            rule__LeafComponentType__Group_5__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__3"


    // $ANTLR start "rule__LeafComponentType__Group_5__3__Impl"
    // InternalDeclaration.g:3959:1: rule__LeafComponentType__Group_5__3__Impl : ( ( rule__LeafComponentType__Group_5_3__0 )* ) ;
    public final void rule__LeafComponentType__Group_5__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3963:1: ( ( ( rule__LeafComponentType__Group_5_3__0 )* ) )
            // InternalDeclaration.g:3964:1: ( ( rule__LeafComponentType__Group_5_3__0 )* )
            {
            // InternalDeclaration.g:3964:1: ( ( rule__LeafComponentType__Group_5_3__0 )* )
            // InternalDeclaration.g:3965:2: ( rule__LeafComponentType__Group_5_3__0 )*
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_5_3()); 
            // InternalDeclaration.g:3966:2: ( rule__LeafComponentType__Group_5_3__0 )*
            loop23:
            do {
                int alt23=2;
                int LA23_0 = input.LA(1);

                if ( (LA23_0==28) ) {
                    alt23=1;
                }


                switch (alt23) {
            	case 1 :
            	    // InternalDeclaration.g:3966:3: rule__LeafComponentType__Group_5_3__0
            	    {
            	    pushFollow(FOLLOW_35);
            	    rule__LeafComponentType__Group_5_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop23;
                }
            } while (true);

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_5_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__3__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5__4"
    // InternalDeclaration.g:3974:1: rule__LeafComponentType__Group_5__4 : rule__LeafComponentType__Group_5__4__Impl ;
    public final void rule__LeafComponentType__Group_5__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3978:1: ( rule__LeafComponentType__Group_5__4__Impl )
            // InternalDeclaration.g:3979:2: rule__LeafComponentType__Group_5__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__4"


    // $ANTLR start "rule__LeafComponentType__Group_5__4__Impl"
    // InternalDeclaration.g:3985:1: rule__LeafComponentType__Group_5__4__Impl : ( ')' ) ;
    public final void rule__LeafComponentType__Group_5__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:3989:1: ( ( ')' ) )
            // InternalDeclaration.g:3990:1: ( ')' )
            {
            // InternalDeclaration.g:3990:1: ( ')' )
            // InternalDeclaration.g:3991:2: ')'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRightParenthesisKeyword_5_4()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRightParenthesisKeyword_5_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5__4__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5_3__0"
    // InternalDeclaration.g:4001:1: rule__LeafComponentType__Group_5_3__0 : rule__LeafComponentType__Group_5_3__0__Impl rule__LeafComponentType__Group_5_3__1 ;
    public final void rule__LeafComponentType__Group_5_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4005:1: ( rule__LeafComponentType__Group_5_3__0__Impl rule__LeafComponentType__Group_5_3__1 )
            // InternalDeclaration.g:4006:2: rule__LeafComponentType__Group_5_3__0__Impl rule__LeafComponentType__Group_5_3__1
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_5_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5_3__0"


    // $ANTLR start "rule__LeafComponentType__Group_5_3__0__Impl"
    // InternalDeclaration.g:4013:1: rule__LeafComponentType__Group_5_3__0__Impl : ( ',' ) ;
    public final void rule__LeafComponentType__Group_5_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4017:1: ( ( ',' ) )
            // InternalDeclaration.g:4018:1: ( ',' )
            {
            // InternalDeclaration.g:4018:1: ( ',' )
            // InternalDeclaration.g:4019:2: ','
            {
             before(grammarAccess.getLeafComponentTypeAccess().getCommaKeyword_5_3_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getCommaKeyword_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5_3__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_5_3__1"
    // InternalDeclaration.g:4028:1: rule__LeafComponentType__Group_5_3__1 : rule__LeafComponentType__Group_5_3__1__Impl ;
    public final void rule__LeafComponentType__Group_5_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4032:1: ( rule__LeafComponentType__Group_5_3__1__Impl )
            // InternalDeclaration.g:4033:2: rule__LeafComponentType__Group_5_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_5_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5_3__1"


    // $ANTLR start "rule__LeafComponentType__Group_5_3__1__Impl"
    // InternalDeclaration.g:4039:1: rule__LeafComponentType__Group_5_3__1__Impl : ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 ) ) ;
    public final void rule__LeafComponentType__Group_5_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4043:1: ( ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 ) ) )
            // InternalDeclaration.g:4044:1: ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 ) )
            {
            // InternalDeclaration.g:4044:1: ( ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 ) )
            // InternalDeclaration.g:4045:2: ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceAssignment_5_3_1()); 
            // InternalDeclaration.g:4046:2: ( rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 )
            // InternalDeclaration.g:4046:3: rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceAssignment_5_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_5_3__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_6__0"
    // InternalDeclaration.g:4055:1: rule__LeafComponentType__Group_6__0 : rule__LeafComponentType__Group_6__0__Impl rule__LeafComponentType__Group_6__1 ;
    public final void rule__LeafComponentType__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4059:1: ( rule__LeafComponentType__Group_6__0__Impl rule__LeafComponentType__Group_6__1 )
            // InternalDeclaration.g:4060:2: rule__LeafComponentType__Group_6__0__Impl rule__LeafComponentType__Group_6__1
            {
            pushFollow(FOLLOW_5);
            rule__LeafComponentType__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__0"


    // $ANTLR start "rule__LeafComponentType__Group_6__0__Impl"
    // InternalDeclaration.g:4067:1: rule__LeafComponentType__Group_6__0__Impl : ( 'inputs' ) ;
    public final void rule__LeafComponentType__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4071:1: ( ( 'inputs' ) )
            // InternalDeclaration.g:4072:1: ( 'inputs' )
            {
            // InternalDeclaration.g:4072:1: ( 'inputs' )
            // InternalDeclaration.g:4073:2: 'inputs'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getInputsKeyword_6_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getInputsKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_6__1"
    // InternalDeclaration.g:4082:1: rule__LeafComponentType__Group_6__1 : rule__LeafComponentType__Group_6__1__Impl rule__LeafComponentType__Group_6__2 ;
    public final void rule__LeafComponentType__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4086:1: ( rule__LeafComponentType__Group_6__1__Impl rule__LeafComponentType__Group_6__2 )
            // InternalDeclaration.g:4087:2: rule__LeafComponentType__Group_6__1__Impl rule__LeafComponentType__Group_6__2
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_6__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_6__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__1"


    // $ANTLR start "rule__LeafComponentType__Group_6__1__Impl"
    // InternalDeclaration.g:4094:1: rule__LeafComponentType__Group_6__1__Impl : ( '{' ) ;
    public final void rule__LeafComponentType__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4098:1: ( ( '{' ) )
            // InternalDeclaration.g:4099:1: ( '{' )
            {
            // InternalDeclaration.g:4099:1: ( '{' )
            // InternalDeclaration.g:4100:2: '{'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_6_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_6__2"
    // InternalDeclaration.g:4109:1: rule__LeafComponentType__Group_6__2 : rule__LeafComponentType__Group_6__2__Impl rule__LeafComponentType__Group_6__3 ;
    public final void rule__LeafComponentType__Group_6__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4113:1: ( rule__LeafComponentType__Group_6__2__Impl rule__LeafComponentType__Group_6__3 )
            // InternalDeclaration.g:4114:2: rule__LeafComponentType__Group_6__2__Impl rule__LeafComponentType__Group_6__3
            {
            pushFollow(FOLLOW_32);
            rule__LeafComponentType__Group_6__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_6__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__2"


    // $ANTLR start "rule__LeafComponentType__Group_6__2__Impl"
    // InternalDeclaration.g:4121:1: rule__LeafComponentType__Group_6__2__Impl : ( ( rule__LeafComponentType__InputsAssignment_6_2 ) ) ;
    public final void rule__LeafComponentType__Group_6__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4125:1: ( ( ( rule__LeafComponentType__InputsAssignment_6_2 ) ) )
            // InternalDeclaration.g:4126:1: ( ( rule__LeafComponentType__InputsAssignment_6_2 ) )
            {
            // InternalDeclaration.g:4126:1: ( ( rule__LeafComponentType__InputsAssignment_6_2 ) )
            // InternalDeclaration.g:4127:2: ( rule__LeafComponentType__InputsAssignment_6_2 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getInputsAssignment_6_2()); 
            // InternalDeclaration.g:4128:2: ( rule__LeafComponentType__InputsAssignment_6_2 )
            // InternalDeclaration.g:4128:3: rule__LeafComponentType__InputsAssignment_6_2
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__InputsAssignment_6_2();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getInputsAssignment_6_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_6__3"
    // InternalDeclaration.g:4136:1: rule__LeafComponentType__Group_6__3 : rule__LeafComponentType__Group_6__3__Impl rule__LeafComponentType__Group_6__4 ;
    public final void rule__LeafComponentType__Group_6__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4140:1: ( rule__LeafComponentType__Group_6__3__Impl rule__LeafComponentType__Group_6__4 )
            // InternalDeclaration.g:4141:2: rule__LeafComponentType__Group_6__3__Impl rule__LeafComponentType__Group_6__4
            {
            pushFollow(FOLLOW_32);
            rule__LeafComponentType__Group_6__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_6__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__3"


    // $ANTLR start "rule__LeafComponentType__Group_6__3__Impl"
    // InternalDeclaration.g:4148:1: rule__LeafComponentType__Group_6__3__Impl : ( ( rule__LeafComponentType__InputsAssignment_6_3 )* ) ;
    public final void rule__LeafComponentType__Group_6__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4152:1: ( ( ( rule__LeafComponentType__InputsAssignment_6_3 )* ) )
            // InternalDeclaration.g:4153:1: ( ( rule__LeafComponentType__InputsAssignment_6_3 )* )
            {
            // InternalDeclaration.g:4153:1: ( ( rule__LeafComponentType__InputsAssignment_6_3 )* )
            // InternalDeclaration.g:4154:2: ( rule__LeafComponentType__InputsAssignment_6_3 )*
            {
             before(grammarAccess.getLeafComponentTypeAccess().getInputsAssignment_6_3()); 
            // InternalDeclaration.g:4155:2: ( rule__LeafComponentType__InputsAssignment_6_3 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( ((LA24_0>=RULE_STRING && LA24_0<=RULE_ID)) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalDeclaration.g:4155:3: rule__LeafComponentType__InputsAssignment_6_3
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__LeafComponentType__InputsAssignment_6_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getLeafComponentTypeAccess().getInputsAssignment_6_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__3__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_6__4"
    // InternalDeclaration.g:4163:1: rule__LeafComponentType__Group_6__4 : rule__LeafComponentType__Group_6__4__Impl ;
    public final void rule__LeafComponentType__Group_6__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4167:1: ( rule__LeafComponentType__Group_6__4__Impl )
            // InternalDeclaration.g:4168:2: rule__LeafComponentType__Group_6__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_6__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__4"


    // $ANTLR start "rule__LeafComponentType__Group_6__4__Impl"
    // InternalDeclaration.g:4174:1: rule__LeafComponentType__Group_6__4__Impl : ( '}' ) ;
    public final void rule__LeafComponentType__Group_6__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4178:1: ( ( '}' ) )
            // InternalDeclaration.g:4179:1: ( '}' )
            {
            // InternalDeclaration.g:4179:1: ( '}' )
            // InternalDeclaration.g:4180:2: '}'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_6_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_6_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_6__4__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_7__0"
    // InternalDeclaration.g:4190:1: rule__LeafComponentType__Group_7__0 : rule__LeafComponentType__Group_7__0__Impl rule__LeafComponentType__Group_7__1 ;
    public final void rule__LeafComponentType__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4194:1: ( rule__LeafComponentType__Group_7__0__Impl rule__LeafComponentType__Group_7__1 )
            // InternalDeclaration.g:4195:2: rule__LeafComponentType__Group_7__0__Impl rule__LeafComponentType__Group_7__1
            {
            pushFollow(FOLLOW_5);
            rule__LeafComponentType__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__0"


    // $ANTLR start "rule__LeafComponentType__Group_7__0__Impl"
    // InternalDeclaration.g:4202:1: rule__LeafComponentType__Group_7__0__Impl : ( 'outputs' ) ;
    public final void rule__LeafComponentType__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4206:1: ( ( 'outputs' ) )
            // InternalDeclaration.g:4207:1: ( 'outputs' )
            {
            // InternalDeclaration.g:4207:1: ( 'outputs' )
            // InternalDeclaration.g:4208:2: 'outputs'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getOutputsKeyword_7_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getOutputsKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_7__1"
    // InternalDeclaration.g:4217:1: rule__LeafComponentType__Group_7__1 : rule__LeafComponentType__Group_7__1__Impl rule__LeafComponentType__Group_7__2 ;
    public final void rule__LeafComponentType__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4221:1: ( rule__LeafComponentType__Group_7__1__Impl rule__LeafComponentType__Group_7__2 )
            // InternalDeclaration.g:4222:2: rule__LeafComponentType__Group_7__1__Impl rule__LeafComponentType__Group_7__2
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_7__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_7__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__1"


    // $ANTLR start "rule__LeafComponentType__Group_7__1__Impl"
    // InternalDeclaration.g:4229:1: rule__LeafComponentType__Group_7__1__Impl : ( '{' ) ;
    public final void rule__LeafComponentType__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4233:1: ( ( '{' ) )
            // InternalDeclaration.g:4234:1: ( '{' )
            {
            // InternalDeclaration.g:4234:1: ( '{' )
            // InternalDeclaration.g:4235:2: '{'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_7_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_7__2"
    // InternalDeclaration.g:4244:1: rule__LeafComponentType__Group_7__2 : rule__LeafComponentType__Group_7__2__Impl rule__LeafComponentType__Group_7__3 ;
    public final void rule__LeafComponentType__Group_7__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4248:1: ( rule__LeafComponentType__Group_7__2__Impl rule__LeafComponentType__Group_7__3 )
            // InternalDeclaration.g:4249:2: rule__LeafComponentType__Group_7__2__Impl rule__LeafComponentType__Group_7__3
            {
            pushFollow(FOLLOW_32);
            rule__LeafComponentType__Group_7__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_7__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__2"


    // $ANTLR start "rule__LeafComponentType__Group_7__2__Impl"
    // InternalDeclaration.g:4256:1: rule__LeafComponentType__Group_7__2__Impl : ( ( rule__LeafComponentType__OutputsAssignment_7_2 ) ) ;
    public final void rule__LeafComponentType__Group_7__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4260:1: ( ( ( rule__LeafComponentType__OutputsAssignment_7_2 ) ) )
            // InternalDeclaration.g:4261:1: ( ( rule__LeafComponentType__OutputsAssignment_7_2 ) )
            {
            // InternalDeclaration.g:4261:1: ( ( rule__LeafComponentType__OutputsAssignment_7_2 ) )
            // InternalDeclaration.g:4262:2: ( rule__LeafComponentType__OutputsAssignment_7_2 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getOutputsAssignment_7_2()); 
            // InternalDeclaration.g:4263:2: ( rule__LeafComponentType__OutputsAssignment_7_2 )
            // InternalDeclaration.g:4263:3: rule__LeafComponentType__OutputsAssignment_7_2
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__OutputsAssignment_7_2();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getOutputsAssignment_7_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_7__3"
    // InternalDeclaration.g:4271:1: rule__LeafComponentType__Group_7__3 : rule__LeafComponentType__Group_7__3__Impl rule__LeafComponentType__Group_7__4 ;
    public final void rule__LeafComponentType__Group_7__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4275:1: ( rule__LeafComponentType__Group_7__3__Impl rule__LeafComponentType__Group_7__4 )
            // InternalDeclaration.g:4276:2: rule__LeafComponentType__Group_7__3__Impl rule__LeafComponentType__Group_7__4
            {
            pushFollow(FOLLOW_32);
            rule__LeafComponentType__Group_7__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_7__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__3"


    // $ANTLR start "rule__LeafComponentType__Group_7__3__Impl"
    // InternalDeclaration.g:4283:1: rule__LeafComponentType__Group_7__3__Impl : ( ( rule__LeafComponentType__OutputsAssignment_7_3 )* ) ;
    public final void rule__LeafComponentType__Group_7__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4287:1: ( ( ( rule__LeafComponentType__OutputsAssignment_7_3 )* ) )
            // InternalDeclaration.g:4288:1: ( ( rule__LeafComponentType__OutputsAssignment_7_3 )* )
            {
            // InternalDeclaration.g:4288:1: ( ( rule__LeafComponentType__OutputsAssignment_7_3 )* )
            // InternalDeclaration.g:4289:2: ( rule__LeafComponentType__OutputsAssignment_7_3 )*
            {
             before(grammarAccess.getLeafComponentTypeAccess().getOutputsAssignment_7_3()); 
            // InternalDeclaration.g:4290:2: ( rule__LeafComponentType__OutputsAssignment_7_3 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0>=RULE_STRING && LA25_0<=RULE_ID)) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalDeclaration.g:4290:3: rule__LeafComponentType__OutputsAssignment_7_3
            	    {
            	    pushFollow(FOLLOW_33);
            	    rule__LeafComponentType__OutputsAssignment_7_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getLeafComponentTypeAccess().getOutputsAssignment_7_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__3__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_7__4"
    // InternalDeclaration.g:4298:1: rule__LeafComponentType__Group_7__4 : rule__LeafComponentType__Group_7__4__Impl ;
    public final void rule__LeafComponentType__Group_7__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4302:1: ( rule__LeafComponentType__Group_7__4__Impl )
            // InternalDeclaration.g:4303:2: rule__LeafComponentType__Group_7__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_7__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__4"


    // $ANTLR start "rule__LeafComponentType__Group_7__4__Impl"
    // InternalDeclaration.g:4309:1: rule__LeafComponentType__Group_7__4__Impl : ( '}' ) ;
    public final void rule__LeafComponentType__Group_7__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4313:1: ( ( '}' ) )
            // InternalDeclaration.g:4314:1: ( '}' )
            {
            // InternalDeclaration.g:4314:1: ( '}' )
            // InternalDeclaration.g:4315:2: '}'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_7_4()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_7_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_7__4__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8__0"
    // InternalDeclaration.g:4325:1: rule__LeafComponentType__Group_8__0 : rule__LeafComponentType__Group_8__0__Impl rule__LeafComponentType__Group_8__1 ;
    public final void rule__LeafComponentType__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4329:1: ( rule__LeafComponentType__Group_8__0__Impl rule__LeafComponentType__Group_8__1 )
            // InternalDeclaration.g:4330:2: rule__LeafComponentType__Group_8__0__Impl rule__LeafComponentType__Group_8__1
            {
            pushFollow(FOLLOW_5);
            rule__LeafComponentType__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__0"


    // $ANTLR start "rule__LeafComponentType__Group_8__0__Impl"
    // InternalDeclaration.g:4337:1: rule__LeafComponentType__Group_8__0__Impl : ( 'parameters' ) ;
    public final void rule__LeafComponentType__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4341:1: ( ( 'parameters' ) )
            // InternalDeclaration.g:4342:1: ( 'parameters' )
            {
            // InternalDeclaration.g:4342:1: ( 'parameters' )
            // InternalDeclaration.g:4343:2: 'parameters'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getParametersKeyword_8_0()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getParametersKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8__1"
    // InternalDeclaration.g:4352:1: rule__LeafComponentType__Group_8__1 : rule__LeafComponentType__Group_8__1__Impl rule__LeafComponentType__Group_8__2 ;
    public final void rule__LeafComponentType__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4356:1: ( rule__LeafComponentType__Group_8__1__Impl rule__LeafComponentType__Group_8__2 )
            // InternalDeclaration.g:4357:2: rule__LeafComponentType__Group_8__1__Impl rule__LeafComponentType__Group_8__2
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__1"


    // $ANTLR start "rule__LeafComponentType__Group_8__1__Impl"
    // InternalDeclaration.g:4364:1: rule__LeafComponentType__Group_8__1__Impl : ( '{' ) ;
    public final void rule__LeafComponentType__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4368:1: ( ( '{' ) )
            // InternalDeclaration.g:4369:1: ( '{' )
            {
            // InternalDeclaration.g:4369:1: ( '{' )
            // InternalDeclaration.g:4370:2: '{'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_8_1()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__1__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8__2"
    // InternalDeclaration.g:4379:1: rule__LeafComponentType__Group_8__2 : rule__LeafComponentType__Group_8__2__Impl rule__LeafComponentType__Group_8__3 ;
    public final void rule__LeafComponentType__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4383:1: ( rule__LeafComponentType__Group_8__2__Impl rule__LeafComponentType__Group_8__3 )
            // InternalDeclaration.g:4384:2: rule__LeafComponentType__Group_8__2__Impl rule__LeafComponentType__Group_8__3
            {
            pushFollow(FOLLOW_19);
            rule__LeafComponentType__Group_8__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__2"


    // $ANTLR start "rule__LeafComponentType__Group_8__2__Impl"
    // InternalDeclaration.g:4391:1: rule__LeafComponentType__Group_8__2__Impl : ( ( rule__LeafComponentType__ParametersAssignment_8_2 ) ) ;
    public final void rule__LeafComponentType__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4395:1: ( ( ( rule__LeafComponentType__ParametersAssignment_8_2 ) ) )
            // InternalDeclaration.g:4396:1: ( ( rule__LeafComponentType__ParametersAssignment_8_2 ) )
            {
            // InternalDeclaration.g:4396:1: ( ( rule__LeafComponentType__ParametersAssignment_8_2 ) )
            // InternalDeclaration.g:4397:2: ( rule__LeafComponentType__ParametersAssignment_8_2 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getParametersAssignment_8_2()); 
            // InternalDeclaration.g:4398:2: ( rule__LeafComponentType__ParametersAssignment_8_2 )
            // InternalDeclaration.g:4398:3: rule__LeafComponentType__ParametersAssignment_8_2
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__ParametersAssignment_8_2();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getParametersAssignment_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__2__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8__3"
    // InternalDeclaration.g:4406:1: rule__LeafComponentType__Group_8__3 : rule__LeafComponentType__Group_8__3__Impl rule__LeafComponentType__Group_8__4 ;
    public final void rule__LeafComponentType__Group_8__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4410:1: ( rule__LeafComponentType__Group_8__3__Impl rule__LeafComponentType__Group_8__4 )
            // InternalDeclaration.g:4411:2: rule__LeafComponentType__Group_8__3__Impl rule__LeafComponentType__Group_8__4
            {
            pushFollow(FOLLOW_19);
            rule__LeafComponentType__Group_8__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__3"


    // $ANTLR start "rule__LeafComponentType__Group_8__3__Impl"
    // InternalDeclaration.g:4418:1: rule__LeafComponentType__Group_8__3__Impl : ( ( rule__LeafComponentType__Group_8_3__0 )* ) ;
    public final void rule__LeafComponentType__Group_8__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4422:1: ( ( ( rule__LeafComponentType__Group_8_3__0 )* ) )
            // InternalDeclaration.g:4423:1: ( ( rule__LeafComponentType__Group_8_3__0 )* )
            {
            // InternalDeclaration.g:4423:1: ( ( rule__LeafComponentType__Group_8_3__0 )* )
            // InternalDeclaration.g:4424:2: ( rule__LeafComponentType__Group_8_3__0 )*
            {
             before(grammarAccess.getLeafComponentTypeAccess().getGroup_8_3()); 
            // InternalDeclaration.g:4425:2: ( rule__LeafComponentType__Group_8_3__0 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==20) ) {
                    int LA26_1 = input.LA(2);

                    if ( ((LA26_1>=RULE_STRING && LA26_1<=RULE_ID)) ) {
                        alt26=1;
                    }


                }


                switch (alt26) {
            	case 1 :
            	    // InternalDeclaration.g:4425:3: rule__LeafComponentType__Group_8_3__0
            	    {
            	    pushFollow(FOLLOW_25);
            	    rule__LeafComponentType__Group_8_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getLeafComponentTypeAccess().getGroup_8_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__3__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8__4"
    // InternalDeclaration.g:4433:1: rule__LeafComponentType__Group_8__4 : rule__LeafComponentType__Group_8__4__Impl rule__LeafComponentType__Group_8__5 ;
    public final void rule__LeafComponentType__Group_8__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4437:1: ( rule__LeafComponentType__Group_8__4__Impl rule__LeafComponentType__Group_8__5 )
            // InternalDeclaration.g:4438:2: rule__LeafComponentType__Group_8__4__Impl rule__LeafComponentType__Group_8__5
            {
            pushFollow(FOLLOW_26);
            rule__LeafComponentType__Group_8__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__4"


    // $ANTLR start "rule__LeafComponentType__Group_8__4__Impl"
    // InternalDeclaration.g:4445:1: rule__LeafComponentType__Group_8__4__Impl : ( ';' ) ;
    public final void rule__LeafComponentType__Group_8__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4449:1: ( ( ';' ) )
            // InternalDeclaration.g:4450:1: ( ';' )
            {
            // InternalDeclaration.g:4450:1: ( ';' )
            // InternalDeclaration.g:4451:2: ';'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getSemicolonKeyword_8_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getSemicolonKeyword_8_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__4__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8__5"
    // InternalDeclaration.g:4460:1: rule__LeafComponentType__Group_8__5 : rule__LeafComponentType__Group_8__5__Impl ;
    public final void rule__LeafComponentType__Group_8__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4464:1: ( rule__LeafComponentType__Group_8__5__Impl )
            // InternalDeclaration.g:4465:2: rule__LeafComponentType__Group_8__5__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8__5__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__5"


    // $ANTLR start "rule__LeafComponentType__Group_8__5__Impl"
    // InternalDeclaration.g:4471:1: rule__LeafComponentType__Group_8__5__Impl : ( '}' ) ;
    public final void rule__LeafComponentType__Group_8__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4475:1: ( ( '}' ) )
            // InternalDeclaration.g:4476:1: ( '}' )
            {
            // InternalDeclaration.g:4476:1: ( '}' )
            // InternalDeclaration.g:4477:2: '}'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_8_5()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_8_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8__5__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8_3__0"
    // InternalDeclaration.g:4487:1: rule__LeafComponentType__Group_8_3__0 : rule__LeafComponentType__Group_8_3__0__Impl rule__LeafComponentType__Group_8_3__1 ;
    public final void rule__LeafComponentType__Group_8_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4491:1: ( rule__LeafComponentType__Group_8_3__0__Impl rule__LeafComponentType__Group_8_3__1 )
            // InternalDeclaration.g:4492:2: rule__LeafComponentType__Group_8_3__0__Impl rule__LeafComponentType__Group_8_3__1
            {
            pushFollow(FOLLOW_4);
            rule__LeafComponentType__Group_8_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8_3__0"


    // $ANTLR start "rule__LeafComponentType__Group_8_3__0__Impl"
    // InternalDeclaration.g:4499:1: rule__LeafComponentType__Group_8_3__0__Impl : ( ';' ) ;
    public final void rule__LeafComponentType__Group_8_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4503:1: ( ( ';' ) )
            // InternalDeclaration.g:4504:1: ( ';' )
            {
            // InternalDeclaration.g:4504:1: ( ';' )
            // InternalDeclaration.g:4505:2: ';'
            {
             before(grammarAccess.getLeafComponentTypeAccess().getSemicolonKeyword_8_3_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getLeafComponentTypeAccess().getSemicolonKeyword_8_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8_3__0__Impl"


    // $ANTLR start "rule__LeafComponentType__Group_8_3__1"
    // InternalDeclaration.g:4514:1: rule__LeafComponentType__Group_8_3__1 : rule__LeafComponentType__Group_8_3__1__Impl ;
    public final void rule__LeafComponentType__Group_8_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4518:1: ( rule__LeafComponentType__Group_8_3__1__Impl )
            // InternalDeclaration.g:4519:2: rule__LeafComponentType__Group_8_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__Group_8_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8_3__1"


    // $ANTLR start "rule__LeafComponentType__Group_8_3__1__Impl"
    // InternalDeclaration.g:4525:1: rule__LeafComponentType__Group_8_3__1__Impl : ( ( rule__LeafComponentType__ParametersAssignment_8_3_1 ) ) ;
    public final void rule__LeafComponentType__Group_8_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4529:1: ( ( ( rule__LeafComponentType__ParametersAssignment_8_3_1 ) ) )
            // InternalDeclaration.g:4530:1: ( ( rule__LeafComponentType__ParametersAssignment_8_3_1 ) )
            {
            // InternalDeclaration.g:4530:1: ( ( rule__LeafComponentType__ParametersAssignment_8_3_1 ) )
            // InternalDeclaration.g:4531:2: ( rule__LeafComponentType__ParametersAssignment_8_3_1 )
            {
             before(grammarAccess.getLeafComponentTypeAccess().getParametersAssignment_8_3_1()); 
            // InternalDeclaration.g:4532:2: ( rule__LeafComponentType__ParametersAssignment_8_3_1 )
            // InternalDeclaration.g:4532:3: rule__LeafComponentType__ParametersAssignment_8_3_1
            {
            pushFollow(FOLLOW_2);
            rule__LeafComponentType__ParametersAssignment_8_3_1();

            state._fsp--;


            }

             after(grammarAccess.getLeafComponentTypeAccess().getParametersAssignment_8_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__Group_8_3__1__Impl"


    // $ANTLR start "rule__Input__Group__0"
    // InternalDeclaration.g:4541:1: rule__Input__Group__0 : rule__Input__Group__0__Impl rule__Input__Group__1 ;
    public final void rule__Input__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4545:1: ( rule__Input__Group__0__Impl rule__Input__Group__1 )
            // InternalDeclaration.g:4546:2: rule__Input__Group__0__Impl rule__Input__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Input__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Input__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__0"


    // $ANTLR start "rule__Input__Group__0__Impl"
    // InternalDeclaration.g:4553:1: rule__Input__Group__0__Impl : ( ( rule__Input__TypeAssignment_0 ) ) ;
    public final void rule__Input__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4557:1: ( ( ( rule__Input__TypeAssignment_0 ) ) )
            // InternalDeclaration.g:4558:1: ( ( rule__Input__TypeAssignment_0 ) )
            {
            // InternalDeclaration.g:4558:1: ( ( rule__Input__TypeAssignment_0 ) )
            // InternalDeclaration.g:4559:2: ( rule__Input__TypeAssignment_0 )
            {
             before(grammarAccess.getInputAccess().getTypeAssignment_0()); 
            // InternalDeclaration.g:4560:2: ( rule__Input__TypeAssignment_0 )
            // InternalDeclaration.g:4560:3: rule__Input__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Input__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getInputAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__0__Impl"


    // $ANTLR start "rule__Input__Group__1"
    // InternalDeclaration.g:4568:1: rule__Input__Group__1 : rule__Input__Group__1__Impl rule__Input__Group__2 ;
    public final void rule__Input__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4572:1: ( rule__Input__Group__1__Impl rule__Input__Group__2 )
            // InternalDeclaration.g:4573:2: rule__Input__Group__1__Impl rule__Input__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__Input__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Input__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__1"


    // $ANTLR start "rule__Input__Group__1__Impl"
    // InternalDeclaration.g:4580:1: rule__Input__Group__1__Impl : ( ( rule__Input__NameAssignment_1 ) ) ;
    public final void rule__Input__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4584:1: ( ( ( rule__Input__NameAssignment_1 ) ) )
            // InternalDeclaration.g:4585:1: ( ( rule__Input__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:4585:1: ( ( rule__Input__NameAssignment_1 ) )
            // InternalDeclaration.g:4586:2: ( rule__Input__NameAssignment_1 )
            {
             before(grammarAccess.getInputAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:4587:2: ( rule__Input__NameAssignment_1 )
            // InternalDeclaration.g:4587:3: rule__Input__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Input__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInputAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__1__Impl"


    // $ANTLR start "rule__Input__Group__2"
    // InternalDeclaration.g:4595:1: rule__Input__Group__2 : rule__Input__Group__2__Impl ;
    public final void rule__Input__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4599:1: ( rule__Input__Group__2__Impl )
            // InternalDeclaration.g:4600:2: rule__Input__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Input__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__2"


    // $ANTLR start "rule__Input__Group__2__Impl"
    // InternalDeclaration.g:4606:1: rule__Input__Group__2__Impl : ( ';' ) ;
    public final void rule__Input__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4610:1: ( ( ';' ) )
            // InternalDeclaration.g:4611:1: ( ';' )
            {
            // InternalDeclaration.g:4611:1: ( ';' )
            // InternalDeclaration.g:4612:2: ';'
            {
             before(grammarAccess.getInputAccess().getSemicolonKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getInputAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__Group__2__Impl"


    // $ANTLR start "rule__Output__Group__0"
    // InternalDeclaration.g:4622:1: rule__Output__Group__0 : rule__Output__Group__0__Impl rule__Output__Group__1 ;
    public final void rule__Output__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4626:1: ( rule__Output__Group__0__Impl rule__Output__Group__1 )
            // InternalDeclaration.g:4627:2: rule__Output__Group__0__Impl rule__Output__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Output__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Output__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__Group__0"


    // $ANTLR start "rule__Output__Group__0__Impl"
    // InternalDeclaration.g:4634:1: rule__Output__Group__0__Impl : ( ( rule__Output__TypeAssignment_0 ) ) ;
    public final void rule__Output__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4638:1: ( ( ( rule__Output__TypeAssignment_0 ) ) )
            // InternalDeclaration.g:4639:1: ( ( rule__Output__TypeAssignment_0 ) )
            {
            // InternalDeclaration.g:4639:1: ( ( rule__Output__TypeAssignment_0 ) )
            // InternalDeclaration.g:4640:2: ( rule__Output__TypeAssignment_0 )
            {
             before(grammarAccess.getOutputAccess().getTypeAssignment_0()); 
            // InternalDeclaration.g:4641:2: ( rule__Output__TypeAssignment_0 )
            // InternalDeclaration.g:4641:3: rule__Output__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Output__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getOutputAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__Group__0__Impl"


    // $ANTLR start "rule__Output__Group__1"
    // InternalDeclaration.g:4649:1: rule__Output__Group__1 : rule__Output__Group__1__Impl rule__Output__Group__2 ;
    public final void rule__Output__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4653:1: ( rule__Output__Group__1__Impl rule__Output__Group__2 )
            // InternalDeclaration.g:4654:2: rule__Output__Group__1__Impl rule__Output__Group__2
            {
            pushFollow(FOLLOW_19);
            rule__Output__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Output__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__Group__1"


    // $ANTLR start "rule__Output__Group__1__Impl"
    // InternalDeclaration.g:4661:1: rule__Output__Group__1__Impl : ( ( rule__Output__NameAssignment_1 ) ) ;
    public final void rule__Output__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4665:1: ( ( ( rule__Output__NameAssignment_1 ) ) )
            // InternalDeclaration.g:4666:1: ( ( rule__Output__NameAssignment_1 ) )
            {
            // InternalDeclaration.g:4666:1: ( ( rule__Output__NameAssignment_1 ) )
            // InternalDeclaration.g:4667:2: ( rule__Output__NameAssignment_1 )
            {
             before(grammarAccess.getOutputAccess().getNameAssignment_1()); 
            // InternalDeclaration.g:4668:2: ( rule__Output__NameAssignment_1 )
            // InternalDeclaration.g:4668:3: rule__Output__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Output__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOutputAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__Group__1__Impl"


    // $ANTLR start "rule__Output__Group__2"
    // InternalDeclaration.g:4676:1: rule__Output__Group__2 : rule__Output__Group__2__Impl ;
    public final void rule__Output__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4680:1: ( rule__Output__Group__2__Impl )
            // InternalDeclaration.g:4681:2: rule__Output__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Output__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__Group__2"


    // $ANTLR start "rule__Output__Group__2__Impl"
    // InternalDeclaration.g:4687:1: rule__Output__Group__2__Impl : ( ';' ) ;
    public final void rule__Output__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4691:1: ( ( ';' ) )
            // InternalDeclaration.g:4692:1: ( ';' )
            {
            // InternalDeclaration.g:4692:1: ( ';' )
            // InternalDeclaration.g:4693:2: ';'
            {
             before(grammarAccess.getOutputAccess().getSemicolonKeyword_2()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getOutputAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__Group__2__Impl"


    // $ANTLR start "rule__Declaration__NameAssignment_2"
    // InternalDeclaration.g:4703:1: rule__Declaration__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Declaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4707:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4708:2: ( ruleEString )
            {
            // InternalDeclaration.g:4708:2: ( ruleEString )
            // InternalDeclaration.g:4709:3: ruleEString
            {
             before(grammarAccess.getDeclarationAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__NameAssignment_2"


    // $ANTLR start "rule__Declaration__TypesAssignment_4_2"
    // InternalDeclaration.g:4718:1: rule__Declaration__TypesAssignment_4_2 : ( ruleType ) ;
    public final void rule__Declaration__TypesAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4722:1: ( ( ruleType ) )
            // InternalDeclaration.g:4723:2: ( ruleType )
            {
            // InternalDeclaration.g:4723:2: ( ruleType )
            // InternalDeclaration.g:4724:3: ruleType
            {
             before(grammarAccess.getDeclarationAccess().getTypesTypeParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getTypesTypeParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__TypesAssignment_4_2"


    // $ANTLR start "rule__Declaration__TypesAssignment_4_3"
    // InternalDeclaration.g:4733:1: rule__Declaration__TypesAssignment_4_3 : ( ruleType ) ;
    public final void rule__Declaration__TypesAssignment_4_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4737:1: ( ( ruleType ) )
            // InternalDeclaration.g:4738:2: ( ruleType )
            {
            // InternalDeclaration.g:4738:2: ( ruleType )
            // InternalDeclaration.g:4739:3: ruleType
            {
             before(grammarAccess.getDeclarationAccess().getTypesTypeParserRuleCall_4_3_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getTypesTypeParserRuleCall_4_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__TypesAssignment_4_3"


    // $ANTLR start "rule__Declaration__InterfacesAssignment_5_2"
    // InternalDeclaration.g:4748:1: rule__Declaration__InterfacesAssignment_5_2 : ( ruleInterface ) ;
    public final void rule__Declaration__InterfacesAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4752:1: ( ( ruleInterface ) )
            // InternalDeclaration.g:4753:2: ( ruleInterface )
            {
            // InternalDeclaration.g:4753:2: ( ruleInterface )
            // InternalDeclaration.g:4754:3: ruleInterface
            {
             before(grammarAccess.getDeclarationAccess().getInterfacesInterfaceParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInterface();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getInterfacesInterfaceParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__InterfacesAssignment_5_2"


    // $ANTLR start "rule__Declaration__InterfacesAssignment_5_3"
    // InternalDeclaration.g:4763:1: rule__Declaration__InterfacesAssignment_5_3 : ( ruleInterface ) ;
    public final void rule__Declaration__InterfacesAssignment_5_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4767:1: ( ( ruleInterface ) )
            // InternalDeclaration.g:4768:2: ( ruleInterface )
            {
            // InternalDeclaration.g:4768:2: ( ruleInterface )
            // InternalDeclaration.g:4769:3: ruleInterface
            {
             before(grammarAccess.getDeclarationAccess().getInterfacesInterfaceParserRuleCall_5_3_0()); 
            pushFollow(FOLLOW_2);
            ruleInterface();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getInterfacesInterfaceParserRuleCall_5_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__InterfacesAssignment_5_3"


    // $ANTLR start "rule__Declaration__LeafComponentTypesAssignment_6_2"
    // InternalDeclaration.g:4778:1: rule__Declaration__LeafComponentTypesAssignment_6_2 : ( ruleLeafComponentType ) ;
    public final void rule__Declaration__LeafComponentTypesAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4782:1: ( ( ruleLeafComponentType ) )
            // InternalDeclaration.g:4783:2: ( ruleLeafComponentType )
            {
            // InternalDeclaration.g:4783:2: ( ruleLeafComponentType )
            // InternalDeclaration.g:4784:3: ruleLeafComponentType
            {
             before(grammarAccess.getDeclarationAccess().getLeafComponentTypesLeafComponentTypeParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLeafComponentType();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getLeafComponentTypesLeafComponentTypeParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__LeafComponentTypesAssignment_6_2"


    // $ANTLR start "rule__Declaration__LeafComponentTypesAssignment_6_3"
    // InternalDeclaration.g:4793:1: rule__Declaration__LeafComponentTypesAssignment_6_3 : ( ruleLeafComponentType ) ;
    public final void rule__Declaration__LeafComponentTypesAssignment_6_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4797:1: ( ( ruleLeafComponentType ) )
            // InternalDeclaration.g:4798:2: ( ruleLeafComponentType )
            {
            // InternalDeclaration.g:4798:2: ( ruleLeafComponentType )
            // InternalDeclaration.g:4799:3: ruleLeafComponentType
            {
             before(grammarAccess.getDeclarationAccess().getLeafComponentTypesLeafComponentTypeParserRuleCall_6_3_0()); 
            pushFollow(FOLLOW_2);
            ruleLeafComponentType();

            state._fsp--;

             after(grammarAccess.getDeclarationAccess().getLeafComponentTypesLeafComponentTypeParserRuleCall_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration__LeafComponentTypesAssignment_6_3"


    // $ANTLR start "rule__Void__NameAssignment_1"
    // InternalDeclaration.g:4808:1: rule__Void__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Void__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4812:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4813:2: ( ruleEString )
            {
            // InternalDeclaration.g:4813:2: ( ruleEString )
            // InternalDeclaration.g:4814:3: ruleEString
            {
             before(grammarAccess.getVoidAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getVoidAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Void__NameAssignment_1"


    // $ANTLR start "rule__CInt__NameAssignment_1"
    // InternalDeclaration.g:4823:1: rule__CInt__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__CInt__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4827:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4828:2: ( ruleEString )
            {
            // InternalDeclaration.g:4828:2: ( ruleEString )
            // InternalDeclaration.g:4829:3: ruleEString
            {
             before(grammarAccess.getCIntAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCIntAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CInt__NameAssignment_1"


    // $ANTLR start "rule__CDouble__NameAssignment_1"
    // InternalDeclaration.g:4838:1: rule__CDouble__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__CDouble__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4842:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4843:2: ( ruleEString )
            {
            // InternalDeclaration.g:4843:2: ( ruleEString )
            // InternalDeclaration.g:4844:3: ruleEString
            {
             before(grammarAccess.getCDoubleAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCDoubleAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CDouble__NameAssignment_1"


    // $ANTLR start "rule__CBoolean__NameAssignment_1"
    // InternalDeclaration.g:4853:1: rule__CBoolean__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__CBoolean__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4857:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4858:2: ( ruleEString )
            {
            // InternalDeclaration.g:4858:2: ( ruleEString )
            // InternalDeclaration.g:4859:3: ruleEString
            {
             before(grammarAccess.getCBooleanAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCBooleanAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CBoolean__NameAssignment_1"


    // $ANTLR start "rule__CString__NameAssignment_1"
    // InternalDeclaration.g:4868:1: rule__CString__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__CString__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4872:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4873:2: ( ruleEString )
            {
            // InternalDeclaration.g:4873:2: ( ruleEString )
            // InternalDeclaration.g:4874:3: ruleEString
            {
             before(grammarAccess.getCStringAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCStringAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CString__NameAssignment_1"


    // $ANTLR start "rule__JavaType__NameAssignment_1"
    // InternalDeclaration.g:4883:1: rule__JavaType__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__JavaType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4887:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4888:2: ( ruleEString )
            {
            // InternalDeclaration.g:4888:2: ( ruleEString )
            // InternalDeclaration.g:4889:3: ruleEString
            {
             before(grammarAccess.getJavaTypeAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getJavaTypeAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__NameAssignment_1"


    // $ANTLR start "rule__JavaType__PackageNameAssignment_3"
    // InternalDeclaration.g:4898:1: rule__JavaType__PackageNameAssignment_3 : ( ruleEString ) ;
    public final void rule__JavaType__PackageNameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4902:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4903:2: ( ruleEString )
            {
            // InternalDeclaration.g:4903:2: ( ruleEString )
            // InternalDeclaration.g:4904:3: ruleEString
            {
             before(grammarAccess.getJavaTypeAccess().getPackageNameEStringParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getJavaTypeAccess().getPackageNameEStringParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__PackageNameAssignment_3"


    // $ANTLR start "rule__JavaType__ClassNameAssignment_5"
    // InternalDeclaration.g:4913:1: rule__JavaType__ClassNameAssignment_5 : ( ruleEString ) ;
    public final void rule__JavaType__ClassNameAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4917:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4918:2: ( ruleEString )
            {
            // InternalDeclaration.g:4918:2: ( ruleEString )
            // InternalDeclaration.g:4919:3: ruleEString
            {
             before(grammarAccess.getJavaTypeAccess().getClassNameEStringParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getJavaTypeAccess().getClassNameEStringParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__JavaType__ClassNameAssignment_5"


    // $ANTLR start "rule__StructType__NameAssignment_1"
    // InternalDeclaration.g:4928:1: rule__StructType__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__StructType__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4932:1: ( ( ruleEString ) )
            // InternalDeclaration.g:4933:2: ( ruleEString )
            {
            // InternalDeclaration.g:4933:2: ( ruleEString )
            // InternalDeclaration.g:4934:3: ruleEString
            {
             before(grammarAccess.getStructTypeAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getStructTypeAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__NameAssignment_1"


    // $ANTLR start "rule__StructType__FieldsAssignment_3"
    // InternalDeclaration.g:4943:1: rule__StructType__FieldsAssignment_3 : ( ruleParameterDeclaration ) ;
    public final void rule__StructType__FieldsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4947:1: ( ( ruleParameterDeclaration ) )
            // InternalDeclaration.g:4948:2: ( ruleParameterDeclaration )
            {
            // InternalDeclaration.g:4948:2: ( ruleParameterDeclaration )
            // InternalDeclaration.g:4949:3: ruleParameterDeclaration
            {
             before(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__FieldsAssignment_3"


    // $ANTLR start "rule__StructType__FieldsAssignment_4_1"
    // InternalDeclaration.g:4958:1: rule__StructType__FieldsAssignment_4_1 : ( ruleParameterDeclaration ) ;
    public final void rule__StructType__FieldsAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4962:1: ( ( ruleParameterDeclaration ) )
            // InternalDeclaration.g:4963:2: ( ruleParameterDeclaration )
            {
            // InternalDeclaration.g:4963:2: ( ruleParameterDeclaration )
            // InternalDeclaration.g:4964:3: ruleParameterDeclaration
            {
             before(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StructType__FieldsAssignment_4_1"


    // $ANTLR start "rule__ParameterDeclaration__TypeAssignment_0"
    // InternalDeclaration.g:4973:1: rule__ParameterDeclaration__TypeAssignment_0 : ( ( ruleEString ) ) ;
    public final void rule__ParameterDeclaration__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4977:1: ( ( ( ruleEString ) ) )
            // InternalDeclaration.g:4978:2: ( ( ruleEString ) )
            {
            // InternalDeclaration.g:4978:2: ( ( ruleEString ) )
            // InternalDeclaration.g:4979:3: ( ruleEString )
            {
             before(grammarAccess.getParameterDeclarationAccess().getTypeTypeCrossReference_0_0()); 
            // InternalDeclaration.g:4980:3: ( ruleEString )
            // InternalDeclaration.g:4981:4: ruleEString
            {
             before(grammarAccess.getParameterDeclarationAccess().getTypeTypeEStringParserRuleCall_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getParameterDeclarationAccess().getTypeTypeEStringParserRuleCall_0_0_1()); 

            }

             after(grammarAccess.getParameterDeclarationAccess().getTypeTypeCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__TypeAssignment_0"


    // $ANTLR start "rule__ParameterDeclaration__LowerBoundAssignment_1_1"
    // InternalDeclaration.g:4992:1: rule__ParameterDeclaration__LowerBoundAssignment_1_1 : ( RULE_INT ) ;
    public final void rule__ParameterDeclaration__LowerBoundAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:4996:1: ( ( RULE_INT ) )
            // InternalDeclaration.g:4997:2: ( RULE_INT )
            {
            // InternalDeclaration.g:4997:2: ( RULE_INT )
            // InternalDeclaration.g:4998:3: RULE_INT
            {
             before(grammarAccess.getParameterDeclarationAccess().getLowerBoundINTTerminalRuleCall_1_1_0()); 
            match(input,RULE_INT,FOLLOW_2); 
             after(grammarAccess.getParameterDeclarationAccess().getLowerBoundINTTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__LowerBoundAssignment_1_1"


    // $ANTLR start "rule__ParameterDeclaration__UpperBoundAssignment_1_3"
    // InternalDeclaration.g:5007:1: rule__ParameterDeclaration__UpperBoundAssignment_1_3 : ( ruleNUMBER ) ;
    public final void rule__ParameterDeclaration__UpperBoundAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5011:1: ( ( ruleNUMBER ) )
            // InternalDeclaration.g:5012:2: ( ruleNUMBER )
            {
            // InternalDeclaration.g:5012:2: ( ruleNUMBER )
            // InternalDeclaration.g:5013:3: ruleNUMBER
            {
             before(grammarAccess.getParameterDeclarationAccess().getUpperBoundNUMBERParserRuleCall_1_3_0()); 
            pushFollow(FOLLOW_2);
            ruleNUMBER();

            state._fsp--;

             after(grammarAccess.getParameterDeclarationAccess().getUpperBoundNUMBERParserRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__UpperBoundAssignment_1_3"


    // $ANTLR start "rule__ParameterDeclaration__NameAssignment_2"
    // InternalDeclaration.g:5022:1: rule__ParameterDeclaration__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__ParameterDeclaration__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5026:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5027:2: ( ruleEString )
            {
            // InternalDeclaration.g:5027:2: ( ruleEString )
            // InternalDeclaration.g:5028:3: ruleEString
            {
             before(grammarAccess.getParameterDeclarationAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getParameterDeclarationAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ParameterDeclaration__NameAssignment_2"


    // $ANTLR start "rule__Interface__NameAssignment_2"
    // InternalDeclaration.g:5037:1: rule__Interface__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__Interface__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5041:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5042:2: ( ruleEString )
            {
            // InternalDeclaration.g:5042:2: ( ruleEString )
            // InternalDeclaration.g:5043:3: ruleEString
            {
             before(grammarAccess.getInterfaceAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getInterfaceAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__NameAssignment_2"


    // $ANTLR start "rule__Interface__FunctionsAssignment_4_0"
    // InternalDeclaration.g:5052:1: rule__Interface__FunctionsAssignment_4_0 : ( ruleFunction ) ;
    public final void rule__Interface__FunctionsAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5056:1: ( ( ruleFunction ) )
            // InternalDeclaration.g:5057:2: ( ruleFunction )
            {
            // InternalDeclaration.g:5057:2: ( ruleFunction )
            // InternalDeclaration.g:5058:3: ruleFunction
            {
             before(grammarAccess.getInterfaceAccess().getFunctionsFunctionParserRuleCall_4_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getInterfaceAccess().getFunctionsFunctionParserRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__FunctionsAssignment_4_0"


    // $ANTLR start "rule__Interface__FunctionsAssignment_4_1"
    // InternalDeclaration.g:5067:1: rule__Interface__FunctionsAssignment_4_1 : ( ruleFunction ) ;
    public final void rule__Interface__FunctionsAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5071:1: ( ( ruleFunction ) )
            // InternalDeclaration.g:5072:2: ( ruleFunction )
            {
            // InternalDeclaration.g:5072:2: ( ruleFunction )
            // InternalDeclaration.g:5073:3: ruleFunction
            {
             before(grammarAccess.getInterfaceAccess().getFunctionsFunctionParserRuleCall_4_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFunction();

            state._fsp--;

             after(grammarAccess.getInterfaceAccess().getFunctionsFunctionParserRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Interface__FunctionsAssignment_4_1"


    // $ANTLR start "rule__Function__ReturnTypeAssignment_0"
    // InternalDeclaration.g:5082:1: rule__Function__ReturnTypeAssignment_0 : ( ruleReturnType ) ;
    public final void rule__Function__ReturnTypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5086:1: ( ( ruleReturnType ) )
            // InternalDeclaration.g:5087:2: ( ruleReturnType )
            {
            // InternalDeclaration.g:5087:2: ( ruleReturnType )
            // InternalDeclaration.g:5088:3: ruleReturnType
            {
             before(grammarAccess.getFunctionAccess().getReturnTypeReturnTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleReturnType();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getReturnTypeReturnTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ReturnTypeAssignment_0"


    // $ANTLR start "rule__Function__NameAssignment_1"
    // InternalDeclaration.g:5097:1: rule__Function__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Function__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5101:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5102:2: ( ruleEString )
            {
            // InternalDeclaration.g:5102:2: ( ruleEString )
            // InternalDeclaration.g:5103:3: ruleEString
            {
             before(grammarAccess.getFunctionAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__NameAssignment_1"


    // $ANTLR start "rule__Function__ParametersAssignment_3_0"
    // InternalDeclaration.g:5112:1: rule__Function__ParametersAssignment_3_0 : ( ruleParameterDeclaration ) ;
    public final void rule__Function__ParametersAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5116:1: ( ( ruleParameterDeclaration ) )
            // InternalDeclaration.g:5117:2: ( ruleParameterDeclaration )
            {
            // InternalDeclaration.g:5117:2: ( ruleParameterDeclaration )
            // InternalDeclaration.g:5118:3: ruleParameterDeclaration
            {
             before(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_3_0_0()); 
            pushFollow(FOLLOW_2);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParametersAssignment_3_0"


    // $ANTLR start "rule__Function__ParametersAssignment_3_1_1"
    // InternalDeclaration.g:5127:1: rule__Function__ParametersAssignment_3_1_1 : ( ruleParameterDeclaration ) ;
    public final void rule__Function__ParametersAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5131:1: ( ( ruleParameterDeclaration ) )
            // InternalDeclaration.g:5132:2: ( ruleParameterDeclaration )
            {
            // InternalDeclaration.g:5132:2: ( ruleParameterDeclaration )
            // InternalDeclaration.g:5133:3: ruleParameterDeclaration
            {
             before(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_3_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_3_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Function__ParametersAssignment_3_1_1"


    // $ANTLR start "rule__ReturnType__TypeAssignment_1"
    // InternalDeclaration.g:5142:1: rule__ReturnType__TypeAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__ReturnType__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5146:1: ( ( ( ruleEString ) ) )
            // InternalDeclaration.g:5147:2: ( ( ruleEString ) )
            {
            // InternalDeclaration.g:5147:2: ( ( ruleEString ) )
            // InternalDeclaration.g:5148:3: ( ruleEString )
            {
             before(grammarAccess.getReturnTypeAccess().getTypeTypeCrossReference_1_0()); 
            // InternalDeclaration.g:5149:3: ( ruleEString )
            // InternalDeclaration.g:5150:4: ruleEString
            {
             before(grammarAccess.getReturnTypeAccess().getTypeTypeEStringParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getReturnTypeAccess().getTypeTypeEStringParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getReturnTypeAccess().getTypeTypeCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnType__TypeAssignment_1"


    // $ANTLR start "rule__InterfacePort__InterfaceAssignment_1"
    // InternalDeclaration.g:5161:1: rule__InterfacePort__InterfaceAssignment_1 : ( ( ruleEString ) ) ;
    public final void rule__InterfacePort__InterfaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5165:1: ( ( ( ruleEString ) ) )
            // InternalDeclaration.g:5166:2: ( ( ruleEString ) )
            {
            // InternalDeclaration.g:5166:2: ( ( ruleEString ) )
            // InternalDeclaration.g:5167:3: ( ruleEString )
            {
             before(grammarAccess.getInterfacePortAccess().getInterfaceInterfaceCrossReference_1_0()); 
            // InternalDeclaration.g:5168:3: ( ruleEString )
            // InternalDeclaration.g:5169:4: ruleEString
            {
             before(grammarAccess.getInterfacePortAccess().getInterfaceInterfaceEStringParserRuleCall_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getInterfacePortAccess().getInterfaceInterfaceEStringParserRuleCall_1_0_1()); 

            }

             after(grammarAccess.getInterfacePortAccess().getInterfaceInterfaceCrossReference_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__InterfaceAssignment_1"


    // $ANTLR start "rule__InterfacePort__NameAssignment_2"
    // InternalDeclaration.g:5180:1: rule__InterfacePort__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__InterfacePort__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5184:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5185:2: ( ruleEString )
            {
            // InternalDeclaration.g:5185:2: ( ruleEString )
            // InternalDeclaration.g:5186:3: ruleEString
            {
             before(grammarAccess.getInterfacePortAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getInterfacePortAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InterfacePort__NameAssignment_2"


    // $ANTLR start "rule__LeafComponentType__NameAssignment_2"
    // InternalDeclaration.g:5195:1: rule__LeafComponentType__NameAssignment_2 : ( ruleEString ) ;
    public final void rule__LeafComponentType__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5199:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5200:2: ( ruleEString )
            {
            // InternalDeclaration.g:5200:2: ( ruleEString )
            // InternalDeclaration.g:5201:3: ruleEString
            {
             before(grammarAccess.getLeafComponentTypeAccess().getNameEStringParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getNameEStringParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__NameAssignment_2"


    // $ANTLR start "rule__LeafComponentType__RequiredInterfaceAssignment_4_2"
    // InternalDeclaration.g:5210:1: rule__LeafComponentType__RequiredInterfaceAssignment_4_2 : ( ruleInterfacePort ) ;
    public final void rule__LeafComponentType__RequiredInterfaceAssignment_4_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5214:1: ( ( ruleInterfacePort ) )
            // InternalDeclaration.g:5215:2: ( ruleInterfacePort )
            {
            // InternalDeclaration.g:5215:2: ( ruleInterfacePort )
            // InternalDeclaration.g:5216:3: ruleInterfacePort
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceInterfacePortParserRuleCall_4_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfacePort();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceInterfacePortParserRuleCall_4_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__RequiredInterfaceAssignment_4_2"


    // $ANTLR start "rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1"
    // InternalDeclaration.g:5225:1: rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1 : ( ruleInterfacePort ) ;
    public final void rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5229:1: ( ( ruleInterfacePort ) )
            // InternalDeclaration.g:5230:2: ( ruleInterfacePort )
            {
            // InternalDeclaration.g:5230:2: ( ruleInterfacePort )
            // InternalDeclaration.g:5231:3: ruleInterfacePort
            {
             before(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceInterfacePortParserRuleCall_4_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfacePort();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceInterfacePortParserRuleCall_4_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__RequiredInterfaceAssignment_4_3_1"


    // $ANTLR start "rule__LeafComponentType__ProvidedInterfaceAssignment_5_2"
    // InternalDeclaration.g:5240:1: rule__LeafComponentType__ProvidedInterfaceAssignment_5_2 : ( ruleInterfacePort ) ;
    public final void rule__LeafComponentType__ProvidedInterfaceAssignment_5_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5244:1: ( ( ruleInterfacePort ) )
            // InternalDeclaration.g:5245:2: ( ruleInterfacePort )
            {
            // InternalDeclaration.g:5245:2: ( ruleInterfacePort )
            // InternalDeclaration.g:5246:3: ruleInterfacePort
            {
             before(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceInterfacePortParserRuleCall_5_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfacePort();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceInterfacePortParserRuleCall_5_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__ProvidedInterfaceAssignment_5_2"


    // $ANTLR start "rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1"
    // InternalDeclaration.g:5255:1: rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1 : ( ruleInterfacePort ) ;
    public final void rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5259:1: ( ( ruleInterfacePort ) )
            // InternalDeclaration.g:5260:2: ( ruleInterfacePort )
            {
            // InternalDeclaration.g:5260:2: ( ruleInterfacePort )
            // InternalDeclaration.g:5261:3: ruleInterfacePort
            {
             before(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceInterfacePortParserRuleCall_5_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleInterfacePort();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceInterfacePortParserRuleCall_5_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__ProvidedInterfaceAssignment_5_3_1"


    // $ANTLR start "rule__LeafComponentType__InputsAssignment_6_2"
    // InternalDeclaration.g:5270:1: rule__LeafComponentType__InputsAssignment_6_2 : ( ruleInput ) ;
    public final void rule__LeafComponentType__InputsAssignment_6_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5274:1: ( ( ruleInput ) )
            // InternalDeclaration.g:5275:2: ( ruleInput )
            {
            // InternalDeclaration.g:5275:2: ( ruleInput )
            // InternalDeclaration.g:5276:3: ruleInput
            {
             before(grammarAccess.getLeafComponentTypeAccess().getInputsInputParserRuleCall_6_2_0()); 
            pushFollow(FOLLOW_2);
            ruleInput();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getInputsInputParserRuleCall_6_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__InputsAssignment_6_2"


    // $ANTLR start "rule__LeafComponentType__InputsAssignment_6_3"
    // InternalDeclaration.g:5285:1: rule__LeafComponentType__InputsAssignment_6_3 : ( ruleInput ) ;
    public final void rule__LeafComponentType__InputsAssignment_6_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5289:1: ( ( ruleInput ) )
            // InternalDeclaration.g:5290:2: ( ruleInput )
            {
            // InternalDeclaration.g:5290:2: ( ruleInput )
            // InternalDeclaration.g:5291:3: ruleInput
            {
             before(grammarAccess.getLeafComponentTypeAccess().getInputsInputParserRuleCall_6_3_0()); 
            pushFollow(FOLLOW_2);
            ruleInput();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getInputsInputParserRuleCall_6_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__InputsAssignment_6_3"


    // $ANTLR start "rule__LeafComponentType__OutputsAssignment_7_2"
    // InternalDeclaration.g:5300:1: rule__LeafComponentType__OutputsAssignment_7_2 : ( ruleOutput ) ;
    public final void rule__LeafComponentType__OutputsAssignment_7_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5304:1: ( ( ruleOutput ) )
            // InternalDeclaration.g:5305:2: ( ruleOutput )
            {
            // InternalDeclaration.g:5305:2: ( ruleOutput )
            // InternalDeclaration.g:5306:3: ruleOutput
            {
             before(grammarAccess.getLeafComponentTypeAccess().getOutputsOutputParserRuleCall_7_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOutput();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getOutputsOutputParserRuleCall_7_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__OutputsAssignment_7_2"


    // $ANTLR start "rule__LeafComponentType__OutputsAssignment_7_3"
    // InternalDeclaration.g:5315:1: rule__LeafComponentType__OutputsAssignment_7_3 : ( ruleOutput ) ;
    public final void rule__LeafComponentType__OutputsAssignment_7_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5319:1: ( ( ruleOutput ) )
            // InternalDeclaration.g:5320:2: ( ruleOutput )
            {
            // InternalDeclaration.g:5320:2: ( ruleOutput )
            // InternalDeclaration.g:5321:3: ruleOutput
            {
             before(grammarAccess.getLeafComponentTypeAccess().getOutputsOutputParserRuleCall_7_3_0()); 
            pushFollow(FOLLOW_2);
            ruleOutput();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getOutputsOutputParserRuleCall_7_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__OutputsAssignment_7_3"


    // $ANTLR start "rule__LeafComponentType__ParametersAssignment_8_2"
    // InternalDeclaration.g:5330:1: rule__LeafComponentType__ParametersAssignment_8_2 : ( ruleParameterDeclaration ) ;
    public final void rule__LeafComponentType__ParametersAssignment_8_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5334:1: ( ( ruleParameterDeclaration ) )
            // InternalDeclaration.g:5335:2: ( ruleParameterDeclaration )
            {
            // InternalDeclaration.g:5335:2: ( ruleParameterDeclaration )
            // InternalDeclaration.g:5336:3: ruleParameterDeclaration
            {
             before(grammarAccess.getLeafComponentTypeAccess().getParametersParameterDeclarationParserRuleCall_8_2_0()); 
            pushFollow(FOLLOW_2);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getParametersParameterDeclarationParserRuleCall_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__ParametersAssignment_8_2"


    // $ANTLR start "rule__LeafComponentType__ParametersAssignment_8_3_1"
    // InternalDeclaration.g:5345:1: rule__LeafComponentType__ParametersAssignment_8_3_1 : ( ruleParameterDeclaration ) ;
    public final void rule__LeafComponentType__ParametersAssignment_8_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5349:1: ( ( ruleParameterDeclaration ) )
            // InternalDeclaration.g:5350:2: ( ruleParameterDeclaration )
            {
            // InternalDeclaration.g:5350:2: ( ruleParameterDeclaration )
            // InternalDeclaration.g:5351:3: ruleParameterDeclaration
            {
             before(grammarAccess.getLeafComponentTypeAccess().getParametersParameterDeclarationParserRuleCall_8_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameterDeclaration();

            state._fsp--;

             after(grammarAccess.getLeafComponentTypeAccess().getParametersParameterDeclarationParserRuleCall_8_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LeafComponentType__ParametersAssignment_8_3_1"


    // $ANTLR start "rule__Input__TypeAssignment_0"
    // InternalDeclaration.g:5360:1: rule__Input__TypeAssignment_0 : ( ( ruleEString ) ) ;
    public final void rule__Input__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5364:1: ( ( ( ruleEString ) ) )
            // InternalDeclaration.g:5365:2: ( ( ruleEString ) )
            {
            // InternalDeclaration.g:5365:2: ( ( ruleEString ) )
            // InternalDeclaration.g:5366:3: ( ruleEString )
            {
             before(grammarAccess.getInputAccess().getTypeTypeCrossReference_0_0()); 
            // InternalDeclaration.g:5367:3: ( ruleEString )
            // InternalDeclaration.g:5368:4: ruleEString
            {
             before(grammarAccess.getInputAccess().getTypeTypeEStringParserRuleCall_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getInputAccess().getTypeTypeEStringParserRuleCall_0_0_1()); 

            }

             after(grammarAccess.getInputAccess().getTypeTypeCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__TypeAssignment_0"


    // $ANTLR start "rule__Input__NameAssignment_1"
    // InternalDeclaration.g:5379:1: rule__Input__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Input__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5383:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5384:2: ( ruleEString )
            {
            // InternalDeclaration.g:5384:2: ( ruleEString )
            // InternalDeclaration.g:5385:3: ruleEString
            {
             before(grammarAccess.getInputAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getInputAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Input__NameAssignment_1"


    // $ANTLR start "rule__Output__TypeAssignment_0"
    // InternalDeclaration.g:5394:1: rule__Output__TypeAssignment_0 : ( ( ruleEString ) ) ;
    public final void rule__Output__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5398:1: ( ( ( ruleEString ) ) )
            // InternalDeclaration.g:5399:2: ( ( ruleEString ) )
            {
            // InternalDeclaration.g:5399:2: ( ( ruleEString ) )
            // InternalDeclaration.g:5400:3: ( ruleEString )
            {
             before(grammarAccess.getOutputAccess().getTypeTypeCrossReference_0_0()); 
            // InternalDeclaration.g:5401:3: ( ruleEString )
            // InternalDeclaration.g:5402:4: ruleEString
            {
             before(grammarAccess.getOutputAccess().getTypeTypeEStringParserRuleCall_0_0_1()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOutputAccess().getTypeTypeEStringParserRuleCall_0_0_1()); 

            }

             after(grammarAccess.getOutputAccess().getTypeTypeCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__TypeAssignment_0"


    // $ANTLR start "rule__Output__NameAssignment_1"
    // InternalDeclaration.g:5413:1: rule__Output__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Output__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDeclaration.g:5417:1: ( ( ruleEString ) )
            // InternalDeclaration.g:5418:2: ( ruleEString )
            {
            // InternalDeclaration.g:5418:2: ( ruleEString )
            // InternalDeclaration.g:5419:3: ruleEString
            {
             before(grammarAccess.getOutputAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getOutputAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Output__NameAssignment_1"

    // Delegated rules


    protected DFA1 dfa1 = new DFA1(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\4\1\uffff\2\21\1\4\6\uffff";
    static final String dfa_3s = "\1\32\1\uffff\2\21\1\30\6\uffff";
    static final String dfa_4s = "\1\uffff\1\1\3\uffff\1\7\1\3\1\4\1\5\1\6\1\2";
    static final String dfa_5s = "\13\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\1\3\24\uffff\1\1",
            "",
            "\1\4",
            "\1\4",
            "\2\11\14\uffff\1\5\2\uffff\1\12\1\6\1\7\1\10",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "527:1: rule__Type__Alternatives : ( ( ruleStructType ) | ( ruleCInt ) | ( ruleCDouble ) | ( ruleCBoolean ) | ( ruleCString ) | ( ruleJavaType ) | ( ruleVoid ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000004000030L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000004002030L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000004000032L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000080002000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000100002000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000100002L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000008000030L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000040000040L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000002030L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000080030L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000010000002L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000003E00002000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000010080000L});

}