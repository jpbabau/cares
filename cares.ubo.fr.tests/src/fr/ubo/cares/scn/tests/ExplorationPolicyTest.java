/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.ExplorationPolicy;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Exploration Policy</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ExplorationPolicyTest extends TestCase {

	/**
	 * The fixture for this Exploration Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExplorationPolicy fixture = null;

	/**
	 * Constructs a new Exploration Policy test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExplorationPolicyTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Exploration Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ExplorationPolicy fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Exploration Policy test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExplorationPolicy getFixture() {
		return fixture;
	}

} //ExplorationPolicyTest
