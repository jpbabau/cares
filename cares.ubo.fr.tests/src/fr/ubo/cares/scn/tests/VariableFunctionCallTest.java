/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.ScnFactory;
import fr.ubo.cares.scn.VariableFunctionCall;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Variable Function Call</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class VariableFunctionCallTest extends TestCase {

	/**
	 * The fixture for this Variable Function Call test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableFunctionCall fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VariableFunctionCallTest.class);
	}

	/**
	 * Constructs a new Variable Function Call test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableFunctionCallTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Variable Function Call test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(VariableFunctionCall fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Variable Function Call test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariableFunctionCall getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ScnFactory.eINSTANCE.createVariableFunctionCall());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //VariableFunctionCallTest
