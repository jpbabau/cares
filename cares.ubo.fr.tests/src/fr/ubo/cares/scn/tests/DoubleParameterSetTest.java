/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.DoubleParameterSet;
import fr.ubo.cares.scn.ScnFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Double Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DoubleParameterSetTest extends TestCase {

	/**
	 * The fixture for this Double Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoubleParameterSet fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DoubleParameterSetTest.class);
	}

	/**
	 * Constructs a new Double Parameter Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoubleParameterSetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Double Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DoubleParameterSet fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Double Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoubleParameterSet getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ScnFactory.eINSTANCE.createDoubleParameterSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DoubleParameterSetTest
