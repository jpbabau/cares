/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.ScnFactory;
import fr.ubo.cares.scn.SimpleBoolParameterSet;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple Bool Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleBoolParameterSetTest extends ParameterChangeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SimpleBoolParameterSetTest.class);
	}

	/**
	 * Constructs a new Simple Bool Parameter Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleBoolParameterSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Simple Bool Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SimpleBoolParameterSet getFixture() {
		return (SimpleBoolParameterSet)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ScnFactory.eINSTANCE.createSimpleBoolParameterSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SimpleBoolParameterSetTest
