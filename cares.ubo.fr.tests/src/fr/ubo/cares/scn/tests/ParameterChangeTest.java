/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.ParameterChange;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Parameter Change</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ParameterChangeTest extends ActionTest {

	/**
	 * Constructs a new Parameter Change test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterChangeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Parameter Change test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ParameterChange getFixture() {
		return (ParameterChange)fixture;
	}

} //ParameterChangeTest
