/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.ScnFactory;
import fr.ubo.cares.scn.SimpleStringParameterSet;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple String Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleStringParameterSetTest extends ParameterChangeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SimpleStringParameterSetTest.class);
	}

	/**
	 * Constructs a new Simple String Parameter Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleStringParameterSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Simple String Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SimpleStringParameterSet getFixture() {
		return (SimpleStringParameterSet)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ScnFactory.eINSTANCE.createSimpleStringParameterSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SimpleStringParameterSetTest
