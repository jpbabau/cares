/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.ScnFactory;
import fr.ubo.cares.scn.SimpleDoubleParameterSet;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple Double Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimpleDoubleParameterSetTest extends ParameterChangeTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SimpleDoubleParameterSetTest.class);
	}

	/**
	 * Constructs a new Simple Double Parameter Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleDoubleParameterSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Simple Double Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SimpleDoubleParameterSet getFixture() {
		return (SimpleDoubleParameterSet)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ScnFactory.eINSTANCE.createSimpleDoubleParameterSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SimpleDoubleParameterSetTest
