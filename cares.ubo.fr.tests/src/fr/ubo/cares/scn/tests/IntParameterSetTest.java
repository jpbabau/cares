/**
 */
package fr.ubo.cares.scn.tests;

import fr.ubo.cares.scn.IntParameterSet;
import fr.ubo.cares.scn.ScnFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Int Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class IntParameterSetTest extends TestCase {

	/**
	 * The fixture for this Int Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntParameterSet fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(IntParameterSetTest.class);
	}

	/**
	 * Constructs a new Int Parameter Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntParameterSetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Int Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(IntParameterSet fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Int Parameter Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntParameterSet getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ScnFactory.eINSTANCE.createIntParameterSet());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //IntParameterSetTest
