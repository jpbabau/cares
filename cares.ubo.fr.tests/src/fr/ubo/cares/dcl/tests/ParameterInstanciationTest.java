/**
 */
package fr.ubo.cares.dcl.tests;

import fr.ubo.cares.dcl.DclFactory;
import fr.ubo.cares.dcl.ParameterInstanciation;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Parameter Instanciation</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ParameterInstanciationTest extends NamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ParameterInstanciationTest.class);
	}

	/**
	 * Constructs a new Parameter Instanciation test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterInstanciationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Parameter Instanciation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ParameterInstanciation getFixture() {
		return (ParameterInstanciation)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DclFactory.eINSTANCE.createParameterInstanciation());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ParameterInstanciationTest
