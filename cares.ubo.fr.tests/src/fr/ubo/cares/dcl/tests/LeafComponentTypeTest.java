/**
 */
package fr.ubo.cares.dcl.tests;

import fr.ubo.cares.dcl.DclFactory;
import fr.ubo.cares.dcl.LeafComponentType;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Leaf Component Type</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LeafComponentTypeTest extends NamedElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LeafComponentTypeTest.class);
	}

	/**
	 * Constructs a new Leaf Component Type test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponentTypeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Leaf Component Type test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LeafComponentType getFixture() {
		return (LeafComponentType)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DclFactory.eINSTANCE.createLeafComponentType());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LeafComponentTypeTest
