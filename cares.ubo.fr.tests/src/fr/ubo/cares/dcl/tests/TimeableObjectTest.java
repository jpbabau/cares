/**
 */
package fr.ubo.cares.dcl.tests;

import fr.ubo.cares.dcl.DclFactory;
import fr.ubo.cares.dcl.TimeableObject;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Timeable Object</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TimeableObjectTest extends TestCase {

	/**
	 * The fixture for this Timeable Object test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeableObject fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TimeableObjectTest.class);
	}

	/**
	 * Constructs a new Timeable Object test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeableObjectTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Timeable Object test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(TimeableObject fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Timeable Object test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeableObject getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DclFactory.eINSTANCE.createTimeableObject());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TimeableObjectTest
