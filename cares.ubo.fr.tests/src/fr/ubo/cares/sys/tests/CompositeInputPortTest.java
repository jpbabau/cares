/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.CompositeInputPort;
import fr.ubo.cares.sys.SysFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Composite Input Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CompositeInputPortTest extends DataLinkSourceTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CompositeInputPortTest.class);
	}

	/**
	 * Constructs a new Composite Input Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeInputPortTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Composite Input Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CompositeInputPort getFixture() {
		return (CompositeInputPort)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SysFactory.eINSTANCE.createCompositeInputPort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CompositeInputPortTest
