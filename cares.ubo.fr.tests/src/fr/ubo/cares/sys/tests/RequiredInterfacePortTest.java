/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.RequiredInterfacePort;
import fr.ubo.cares.sys.SysFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Required Interface Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RequiredInterfacePortTest extends PortTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RequiredInterfacePortTest.class);
	}

	/**
	 * Constructs a new Required Interface Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredInterfacePortTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Required Interface Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected RequiredInterfacePort getFixture() {
		return (RequiredInterfacePort)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SysFactory.eINSTANCE.createRequiredInterfacePort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RequiredInterfacePortTest
