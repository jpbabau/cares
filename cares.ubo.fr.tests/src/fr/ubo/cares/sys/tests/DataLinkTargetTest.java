/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.DataLinkTarget;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Data Link Target</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DataLinkTargetTest extends TestCase {

	/**
	 * The fixture for this Data Link Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataLinkTarget fixture = null;

	/**
	 * Constructs a new Data Link Target test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataLinkTargetTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Data Link Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DataLinkTarget fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Data Link Target test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataLinkTarget getFixture() {
		return fixture;
	}

} //DataLinkTargetTest
