/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.DataLinkSource;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Data Link Source</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DataLinkSourceTest extends TestCase {

	/**
	 * The fixture for this Data Link Source test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataLinkSource fixture = null;

	/**
	 * Constructs a new Data Link Source test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataLinkSourceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Data Link Source test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DataLinkSource fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Data Link Source test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataLinkSource getFixture() {
		return fixture;
	}

} //DataLinkSourceTest
