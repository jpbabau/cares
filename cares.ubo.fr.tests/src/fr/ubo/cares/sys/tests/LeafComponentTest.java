/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.LeafComponent;
import fr.ubo.cares.sys.SysFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Leaf Component</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LeafComponentTest extends ComponentTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LeafComponentTest.class);
	}

	/**
	 * Constructs a new Leaf Component test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponentTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Leaf Component test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LeafComponent getFixture() {
		return (LeafComponent)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SysFactory.eINSTANCE.createLeafComponent());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LeafComponentTest
