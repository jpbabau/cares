/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.SysFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Provided Interface Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProvidedInterfacePortTest extends PortTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ProvidedInterfacePortTest.class);
	}

	/**
	 * Constructs a new Provided Interface Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedInterfacePortTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Provided Interface Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ProvidedInterfacePort getFixture() {
		return (ProvidedInterfacePort)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SysFactory.eINSTANCE.createProvidedInterfacePort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ProvidedInterfacePortTest
