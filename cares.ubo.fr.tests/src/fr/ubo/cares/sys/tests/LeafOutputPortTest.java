/**
 */
package fr.ubo.cares.sys.tests;

import fr.ubo.cares.sys.LeafOutputPort;
import fr.ubo.cares.sys.SysFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Leaf Output Port</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LeafOutputPortTest extends DataLinkSourceTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LeafOutputPortTest.class);
	}

	/**
	 * Constructs a new Leaf Output Port test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafOutputPortTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Leaf Output Port test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LeafOutputPort getFixture() {
		return (LeafOutputPort)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(SysFactory.eINSTANCE.createLeafOutputPort());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LeafOutputPortTest
