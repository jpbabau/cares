/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.TimeableObject;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.CompositeComponent#getComponents <em>Components</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.CompositeComponent#getLinks <em>Links</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.CompositeComponent#getInputPorts <em>Input Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.CompositeComponent#getOutputPorts <em>Output Ports</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getCompositeComponent()
 * @model
 * @generated
 */
public interface CompositeComponent extends Component, TimeableObject {
	/**
	 * Returns the value of the '<em><b>Components</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.Component}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Components</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getCompositeComponent_Components()
	 * @model containment="true"
	 * @generated
	 */
	EList<Component> getComponents();

	/**
	 * Returns the value of the '<em><b>Links</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.DataLink}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Links</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Links</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getCompositeComponent_Links()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataLink> getLinks();

	/**
	 * Returns the value of the '<em><b>Input Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.CompositeInputPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Ports</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getCompositeComponent_InputPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<CompositeInputPort> getInputPorts();

	/**
	 * Returns the value of the '<em><b>Output Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.CompositeOutputPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Ports</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getCompositeComponent_OutputPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<CompositeOutputPort> getOutputPorts();

} // CompositeComponent
