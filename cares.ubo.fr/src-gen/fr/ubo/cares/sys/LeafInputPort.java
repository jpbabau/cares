/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.Input;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf Input Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.LeafInputPort#getInput <em>Input</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getLeafInputPort()
 * @model
 * @generated
 */
public interface LeafInputPort extends DataLinkTarget, Port {
	/**
	 * Returns the value of the '<em><b>Input</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input</em>' reference.
	 * @see #setInput(Input)
	 * @see fr.ubo.cares.sys.SysPackage#getLeafInputPort_Input()
	 * @model
	 * @generated
	 */
	Input getInput();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.LeafInputPort#getInput <em>Input</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input</em>' reference.
	 * @see #getInput()
	 * @generated
	 */
	void setInput(Input value);

} // LeafInputPort
