/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.NamedElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.DataLink#getSource <em>Source</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.DataLink#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getDataLink()
 * @model
 * @generated
 */
public interface DataLink extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.ubo.cares.sys.DataLinkSource#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(DataLinkSource)
	 * @see fr.ubo.cares.sys.SysPackage#getDataLink_Source()
	 * @see fr.ubo.cares.sys.DataLinkSource#getTarget
	 * @model opposite="target" required="true"
	 * @generated
	 */
	DataLinkSource getSource();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.DataLink#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(DataLinkSource value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.DataLinkTarget}.
	 * It is bidirectional and its opposite is '{@link fr.ubo.cares.sys.DataLinkTarget#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getDataLink_Target()
	 * @see fr.ubo.cares.sys.DataLinkTarget#getSource
	 * @model opposite="source"
	 * @generated
	 */
	EList<DataLinkTarget> getTarget();

} // DataLink
