/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.Component#getSchedulingPriority <em>Scheduling Priority</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.Component#isInheritFrequency <em>Inherit Frequency</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getComponent()
 * @model abstract="true"
 * @generated
 */
public interface Component extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Scheduling Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduling Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduling Priority</em>' attribute.
	 * @see #setSchedulingPriority(int)
	 * @see fr.ubo.cares.sys.SysPackage#getComponent_SchedulingPriority()
	 * @model required="true"
	 * @generated
	 */
	int getSchedulingPriority();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.Component#getSchedulingPriority <em>Scheduling Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduling Priority</em>' attribute.
	 * @see #getSchedulingPriority()
	 * @generated
	 */
	void setSchedulingPriority(int value);

	/**
	 * Returns the value of the '<em><b>Inherit Frequency</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inherit Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inherit Frequency</em>' attribute.
	 * @see #setInheritFrequency(boolean)
	 * @see fr.ubo.cares.sys.SysPackage#getComponent_InheritFrequency()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isInheritFrequency();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.Component#isInheritFrequency <em>Inherit Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inherit Frequency</em>' attribute.
	 * @see #isInheritFrequency()
	 * @generated
	 */
	void setInheritFrequency(boolean value);

} // Component
