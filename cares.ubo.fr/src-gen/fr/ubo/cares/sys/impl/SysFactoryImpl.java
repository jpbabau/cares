/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.sys.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SysFactoryImpl extends EFactoryImpl implements SysFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SysFactory init() {
		try {
			SysFactory theSysFactory = (SysFactory)EPackage.Registry.INSTANCE.getEFactory(SysPackage.eNS_URI);
			if (theSysFactory != null) {
				return theSysFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SysFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SysFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SysPackage.CARES_SYSTEM: return createCaresSystem();
			case SysPackage.COMPOSITE_COMPONENT: return createCompositeComponent();
			case SysPackage.LEAF_COMPONENT: return createLeafComponent();
			case SysPackage.DATA_LINK: return createDataLink();
			case SysPackage.COMPOSITE_INPUT_PORT: return createCompositeInputPort();
			case SysPackage.COMPOSITE_OUTPUT_PORT: return createCompositeOutputPort();
			case SysPackage.LEAF_INPUT_PORT: return createLeafInputPort();
			case SysPackage.LEAF_OUTPUT_PORT: return createLeafOutputPort();
			case SysPackage.REQUIRED_INTERFACE_PORT: return createRequiredInterfacePort();
			case SysPackage.PROVIDED_INTERFACE_PORT: return createProvidedInterfacePort();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaresSystem createCaresSystem() {
		CaresSystemImpl caresSystem = new CaresSystemImpl();
		return caresSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeComponent createCompositeComponent() {
		CompositeComponentImpl compositeComponent = new CompositeComponentImpl();
		return compositeComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponent createLeafComponent() {
		LeafComponentImpl leafComponent = new LeafComponentImpl();
		return leafComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataLink createDataLink() {
		DataLinkImpl dataLink = new DataLinkImpl();
		return dataLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeInputPort createCompositeInputPort() {
		CompositeInputPortImpl compositeInputPort = new CompositeInputPortImpl();
		return compositeInputPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeOutputPort createCompositeOutputPort() {
		CompositeOutputPortImpl compositeOutputPort = new CompositeOutputPortImpl();
		return compositeOutputPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafInputPort createLeafInputPort() {
		LeafInputPortImpl leafInputPort = new LeafInputPortImpl();
		return leafInputPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafOutputPort createLeafOutputPort() {
		LeafOutputPortImpl leafOutputPort = new LeafOutputPortImpl();
		return leafOutputPort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredInterfacePort createRequiredInterfacePort() {
		RequiredInterfacePortImpl requiredInterfacePort = new RequiredInterfacePortImpl();
		return requiredInterfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedInterfacePort createProvidedInterfacePort() {
		ProvidedInterfacePortImpl providedInterfacePort = new ProvidedInterfacePortImpl();
		return providedInterfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SysPackage getSysPackage() {
		return (SysPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SysPackage getPackage() {
		return SysPackage.eINSTANCE;
	}

} //SysFactoryImpl
