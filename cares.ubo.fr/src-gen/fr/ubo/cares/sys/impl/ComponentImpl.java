/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.dcl.impl.NamedElementImpl;

import fr.ubo.cares.sys.Component;
import fr.ubo.cares.sys.SysPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.impl.ComponentImpl#getSchedulingPriority <em>Scheduling Priority</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.ComponentImpl#isInheritFrequency <em>Inherit Frequency</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ComponentImpl extends NamedElementImpl implements Component {
	/**
	 * The default value of the '{@link #getSchedulingPriority() <em>Scheduling Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulingPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int SCHEDULING_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSchedulingPriority() <em>Scheduling Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulingPriority()
	 * @generated
	 * @ordered
	 */
	protected int schedulingPriority = SCHEDULING_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #isInheritFrequency() <em>Inherit Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInheritFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INHERIT_FREQUENCY_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInheritFrequency() <em>Inherit Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInheritFrequency()
	 * @generated
	 * @ordered
	 */
	protected boolean inheritFrequency = INHERIT_FREQUENCY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysPackage.Literals.COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSchedulingPriority() {
		return schedulingPriority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSchedulingPriority(int newSchedulingPriority) {
		int oldSchedulingPriority = schedulingPriority;
		schedulingPriority = newSchedulingPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.COMPONENT__SCHEDULING_PRIORITY, oldSchedulingPriority, schedulingPriority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isInheritFrequency() {
		return inheritFrequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInheritFrequency(boolean newInheritFrequency) {
		boolean oldInheritFrequency = inheritFrequency;
		inheritFrequency = newInheritFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.COMPONENT__INHERIT_FREQUENCY, oldInheritFrequency, inheritFrequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SysPackage.COMPONENT__SCHEDULING_PRIORITY:
				return getSchedulingPriority();
			case SysPackage.COMPONENT__INHERIT_FREQUENCY:
				return isInheritFrequency();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SysPackage.COMPONENT__SCHEDULING_PRIORITY:
				setSchedulingPriority((Integer)newValue);
				return;
			case SysPackage.COMPONENT__INHERIT_FREQUENCY:
				setInheritFrequency((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SysPackage.COMPONENT__SCHEDULING_PRIORITY:
				setSchedulingPriority(SCHEDULING_PRIORITY_EDEFAULT);
				return;
			case SysPackage.COMPONENT__INHERIT_FREQUENCY:
				setInheritFrequency(INHERIT_FREQUENCY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SysPackage.COMPONENT__SCHEDULING_PRIORITY:
				return schedulingPriority != SCHEDULING_PRIORITY_EDEFAULT;
			case SysPackage.COMPONENT__INHERIT_FREQUENCY:
				return inheritFrequency != INHERIT_FREQUENCY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (schedulingPriority: ");
		result.append(schedulingPriority);
		result.append(", inheritFrequency: ");
		result.append(inheritFrequency);
		result.append(')');
		return result.toString();
	}

} //ComponentImpl
