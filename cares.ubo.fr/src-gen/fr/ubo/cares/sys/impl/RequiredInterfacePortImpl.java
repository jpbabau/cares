/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.dcl.InterfacePort;
import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.RequiredInterfacePort;
import fr.ubo.cares.sys.SysPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Required Interface Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.impl.RequiredInterfacePortImpl#getInterface <em>Interface</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.RequiredInterfacePortImpl#getCallLink <em>Call Link</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequiredInterfacePortImpl extends PortImpl implements RequiredInterfacePort {
	/**
	 * The cached value of the '{@link #getInterface() <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterface()
	 * @generated
	 * @ordered
	 */
	protected InterfacePort interface_;

	/**
	 * The cached value of the '{@link #getCallLink() <em>Call Link</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallLink()
	 * @generated
	 * @ordered
	 */
	protected EList<ProvidedInterfacePort> callLink;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredInterfacePortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysPackage.Literals.REQUIRED_INTERFACE_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfacePort getInterface() {
		if (interface_ != null && interface_.eIsProxy()) {
			InternalEObject oldInterface = (InternalEObject)interface_;
			interface_ = (InterfacePort)eResolveProxy(oldInterface);
			if (interface_ != oldInterface) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SysPackage.REQUIRED_INTERFACE_PORT__INTERFACE, oldInterface, interface_));
			}
		}
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfacePort basicGetInterface() {
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterface(InterfacePort newInterface) {
		InterfacePort oldInterface = interface_;
		interface_ = newInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.REQUIRED_INTERFACE_PORT__INTERFACE, oldInterface, interface_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProvidedInterfacePort> getCallLink() {
		if (callLink == null) {
			callLink = new EObjectResolvingEList<ProvidedInterfacePort>(ProvidedInterfacePort.class, this, SysPackage.REQUIRED_INTERFACE_PORT__CALL_LINK);
		}
		return callLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SysPackage.REQUIRED_INTERFACE_PORT__INTERFACE:
				if (resolve) return getInterface();
				return basicGetInterface();
			case SysPackage.REQUIRED_INTERFACE_PORT__CALL_LINK:
				return getCallLink();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SysPackage.REQUIRED_INTERFACE_PORT__INTERFACE:
				setInterface((InterfacePort)newValue);
				return;
			case SysPackage.REQUIRED_INTERFACE_PORT__CALL_LINK:
				getCallLink().clear();
				getCallLink().addAll((Collection<? extends ProvidedInterfacePort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SysPackage.REQUIRED_INTERFACE_PORT__INTERFACE:
				setInterface((InterfacePort)null);
				return;
			case SysPackage.REQUIRED_INTERFACE_PORT__CALL_LINK:
				getCallLink().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SysPackage.REQUIRED_INTERFACE_PORT__INTERFACE:
				return interface_ != null;
			case SysPackage.REQUIRED_INTERFACE_PORT__CALL_LINK:
				return callLink != null && !callLink.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RequiredInterfacePortImpl
