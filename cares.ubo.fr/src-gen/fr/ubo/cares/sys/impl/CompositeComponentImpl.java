/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.dcl.DclPackage;
import fr.ubo.cares.dcl.TimeableObject;
import fr.ubo.cares.sys.Component;
import fr.ubo.cares.sys.CompositeComponent;
import fr.ubo.cares.sys.CompositeInputPort;
import fr.ubo.cares.sys.CompositeOutputPort;
import fr.ubo.cares.sys.DataLink;
import fr.ubo.cares.sys.SysPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.impl.CompositeComponentImpl#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CompositeComponentImpl#getComponents <em>Components</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CompositeComponentImpl#getLinks <em>Links</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CompositeComponentImpl#getInputPorts <em>Input Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CompositeComponentImpl#getOutputPorts <em>Output Ports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeComponentImpl extends ComponentImpl implements CompositeComponent {
	/**
	 * The default value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final double FREQUENCY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected double frequency = FREQUENCY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> components;

	/**
	 * The cached value of the '{@link #getLinks() <em>Links</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<DataLink> links;

	/**
	 * The cached value of the '{@link #getInputPorts() <em>Input Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<CompositeInputPort> inputPorts;

	/**
	 * The cached value of the '{@link #getOutputPorts() <em>Output Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<CompositeOutputPort> outputPorts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysPackage.Literals.COMPOSITE_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getFrequency() {
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrequency(double newFrequency) {
		double oldFrequency = frequency;
		frequency = newFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.COMPOSITE_COMPONENT__FREQUENCY, oldFrequency, frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Component> getComponents() {
		if (components == null) {
			components = new EObjectContainmentEList<Component>(Component.class, this, SysPackage.COMPOSITE_COMPONENT__COMPONENTS);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataLink> getLinks() {
		if (links == null) {
			links = new EObjectContainmentEList<DataLink>(DataLink.class, this, SysPackage.COMPOSITE_COMPONENT__LINKS);
		}
		return links;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CompositeInputPort> getInputPorts() {
		if (inputPorts == null) {
			inputPorts = new EObjectContainmentEList<CompositeInputPort>(CompositeInputPort.class, this, SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS);
		}
		return inputPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CompositeOutputPort> getOutputPorts() {
		if (outputPorts == null) {
			outputPorts = new EObjectContainmentEList<CompositeOutputPort>(CompositeOutputPort.class, this, SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS);
		}
		return outputPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SysPackage.COMPOSITE_COMPONENT__COMPONENTS:
				return ((InternalEList<?>)getComponents()).basicRemove(otherEnd, msgs);
			case SysPackage.COMPOSITE_COMPONENT__LINKS:
				return ((InternalEList<?>)getLinks()).basicRemove(otherEnd, msgs);
			case SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS:
				return ((InternalEList<?>)getInputPorts()).basicRemove(otherEnd, msgs);
			case SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS:
				return ((InternalEList<?>)getOutputPorts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SysPackage.COMPOSITE_COMPONENT__FREQUENCY:
				return getFrequency();
			case SysPackage.COMPOSITE_COMPONENT__COMPONENTS:
				return getComponents();
			case SysPackage.COMPOSITE_COMPONENT__LINKS:
				return getLinks();
			case SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS:
				return getInputPorts();
			case SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS:
				return getOutputPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SysPackage.COMPOSITE_COMPONENT__FREQUENCY:
				setFrequency((Double)newValue);
				return;
			case SysPackage.COMPOSITE_COMPONENT__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends Component>)newValue);
				return;
			case SysPackage.COMPOSITE_COMPONENT__LINKS:
				getLinks().clear();
				getLinks().addAll((Collection<? extends DataLink>)newValue);
				return;
			case SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS:
				getInputPorts().clear();
				getInputPorts().addAll((Collection<? extends CompositeInputPort>)newValue);
				return;
			case SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS:
				getOutputPorts().clear();
				getOutputPorts().addAll((Collection<? extends CompositeOutputPort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SysPackage.COMPOSITE_COMPONENT__FREQUENCY:
				setFrequency(FREQUENCY_EDEFAULT);
				return;
			case SysPackage.COMPOSITE_COMPONENT__COMPONENTS:
				getComponents().clear();
				return;
			case SysPackage.COMPOSITE_COMPONENT__LINKS:
				getLinks().clear();
				return;
			case SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS:
				getInputPorts().clear();
				return;
			case SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS:
				getOutputPorts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SysPackage.COMPOSITE_COMPONENT__FREQUENCY:
				return frequency != FREQUENCY_EDEFAULT;
			case SysPackage.COMPOSITE_COMPONENT__COMPONENTS:
				return components != null && !components.isEmpty();
			case SysPackage.COMPOSITE_COMPONENT__LINKS:
				return links != null && !links.isEmpty();
			case SysPackage.COMPOSITE_COMPONENT__INPUT_PORTS:
				return inputPorts != null && !inputPorts.isEmpty();
			case SysPackage.COMPOSITE_COMPONENT__OUTPUT_PORTS:
				return outputPorts != null && !outputPorts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TimeableObject.class) {
			switch (derivedFeatureID) {
				case SysPackage.COMPOSITE_COMPONENT__FREQUENCY: return DclPackage.TIMEABLE_OBJECT__FREQUENCY;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TimeableObject.class) {
			switch (baseFeatureID) {
				case DclPackage.TIMEABLE_OBJECT__FREQUENCY: return SysPackage.COMPOSITE_COMPONENT__FREQUENCY;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (frequency: ");
		result.append(frequency);
		result.append(')');
		return result.toString();
	}

} //CompositeComponentImpl
