/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.dcl.DclPackage;

import fr.ubo.cares.sys.CaresSystem;
import fr.ubo.cares.sys.Component;
import fr.ubo.cares.sys.CompositeComponent;
import fr.ubo.cares.sys.CompositeInputPort;
import fr.ubo.cares.sys.CompositeOutputPort;
import fr.ubo.cares.sys.DataLink;
import fr.ubo.cares.sys.DataLinkSource;
import fr.ubo.cares.sys.DataLinkTarget;
import fr.ubo.cares.sys.LeafComponent;
import fr.ubo.cares.sys.LeafInputPort;
import fr.ubo.cares.sys.LeafOutputPort;
import fr.ubo.cares.sys.Port;
import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.RequiredInterfacePort;
import fr.ubo.cares.sys.SysFactory;
import fr.ubo.cares.sys.SysPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SysPackageImpl extends EPackageImpl implements SysPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass caresSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leafComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeInputPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeOutputPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leafInputPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leafOutputPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataLinkSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataLinkTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredInterfacePortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providedInterfacePortEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.ubo.cares.sys.SysPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SysPackageImpl() {
		super(eNS_URI, SysFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link SysPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SysPackage init() {
		if (isInited) return (SysPackage)EPackage.Registry.INSTANCE.getEPackage(SysPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredSysPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		SysPackageImpl theSysPackage = registeredSysPackage instanceof SysPackageImpl ? (SysPackageImpl)registeredSysPackage : new SysPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		DclPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theSysPackage.createPackageContents();

		// Initialize created meta-data
		theSysPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSysPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SysPackage.eNS_URI, theSysPackage);
		return theSysPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCaresSystem() {
		return caresSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaresSystem_Declarations() {
		return (EReference)caresSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCaresSystem_RootSystemComponent() {
		return (EReference)caresSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCaresSystem_StartTime() {
		return (EAttribute)caresSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCaresSystem_StepTime() {
		return (EAttribute)caresSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponent() {
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComponent_SchedulingPriority() {
		return (EAttribute)componentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComponent_InheritFrequency() {
		return (EAttribute)componentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeComponent() {
		return compositeComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeComponent_Components() {
		return (EReference)compositeComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeComponent_Links() {
		return (EReference)compositeComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeComponent_InputPorts() {
		return (EReference)compositeComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeComponent_OutputPorts() {
		return (EReference)compositeComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLeafComponent() {
		return leafComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafComponent_Type() {
		return (EReference)leafComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafComponent_Declarations() {
		return (EReference)leafComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafComponent_InputPorts() {
		return (EReference)leafComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafComponent_OutputPorts() {
		return (EReference)leafComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafComponent_ProvidedPorts() {
		return (EReference)leafComponentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafComponent_RequiredPorts() {
		return (EReference)leafComponentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataLink() {
		return dataLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLink_Source() {
		return (EReference)dataLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLink_Target() {
		return (EReference)dataLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeInputPort() {
		return compositeInputPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeInputPort_Type() {
		return (EReference)compositeInputPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeOutputPort() {
		return compositeOutputPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeOutputPort_Type() {
		return (EReference)compositeOutputPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLeafInputPort() {
		return leafInputPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafInputPort_Input() {
		return (EReference)leafInputPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLeafOutputPort() {
		return leafOutputPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLeafOutputPort_Output() {
		return (EReference)leafOutputPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataLinkSource() {
		return dataLinkSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLinkSource_Target() {
		return (EReference)dataLinkSourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataLinkTarget() {
		return dataLinkTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataLinkTarget_Source() {
		return (EReference)dataLinkTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPort() {
		return portEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredInterfacePort() {
		return requiredInterfacePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequiredInterfacePort_Interface() {
		return (EReference)requiredInterfacePortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequiredInterfacePort_CallLink() {
		return (EReference)requiredInterfacePortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvidedInterfacePort() {
		return providedInterfacePortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProvidedInterfacePort_Interface() {
		return (EReference)providedInterfacePortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SysFactory getSysFactory() {
		return (SysFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		caresSystemEClass = createEClass(CARES_SYSTEM);
		createEReference(caresSystemEClass, CARES_SYSTEM__DECLARATIONS);
		createEReference(caresSystemEClass, CARES_SYSTEM__ROOT_SYSTEM_COMPONENT);
		createEAttribute(caresSystemEClass, CARES_SYSTEM__START_TIME);
		createEAttribute(caresSystemEClass, CARES_SYSTEM__STEP_TIME);

		componentEClass = createEClass(COMPONENT);
		createEAttribute(componentEClass, COMPONENT__SCHEDULING_PRIORITY);
		createEAttribute(componentEClass, COMPONENT__INHERIT_FREQUENCY);

		compositeComponentEClass = createEClass(COMPOSITE_COMPONENT);
		createEReference(compositeComponentEClass, COMPOSITE_COMPONENT__COMPONENTS);
		createEReference(compositeComponentEClass, COMPOSITE_COMPONENT__LINKS);
		createEReference(compositeComponentEClass, COMPOSITE_COMPONENT__INPUT_PORTS);
		createEReference(compositeComponentEClass, COMPOSITE_COMPONENT__OUTPUT_PORTS);

		leafComponentEClass = createEClass(LEAF_COMPONENT);
		createEReference(leafComponentEClass, LEAF_COMPONENT__TYPE);
		createEReference(leafComponentEClass, LEAF_COMPONENT__DECLARATIONS);
		createEReference(leafComponentEClass, LEAF_COMPONENT__INPUT_PORTS);
		createEReference(leafComponentEClass, LEAF_COMPONENT__OUTPUT_PORTS);
		createEReference(leafComponentEClass, LEAF_COMPONENT__PROVIDED_PORTS);
		createEReference(leafComponentEClass, LEAF_COMPONENT__REQUIRED_PORTS);

		dataLinkEClass = createEClass(DATA_LINK);
		createEReference(dataLinkEClass, DATA_LINK__SOURCE);
		createEReference(dataLinkEClass, DATA_LINK__TARGET);

		compositeInputPortEClass = createEClass(COMPOSITE_INPUT_PORT);
		createEReference(compositeInputPortEClass, COMPOSITE_INPUT_PORT__TYPE);

		compositeOutputPortEClass = createEClass(COMPOSITE_OUTPUT_PORT);
		createEReference(compositeOutputPortEClass, COMPOSITE_OUTPUT_PORT__TYPE);

		leafInputPortEClass = createEClass(LEAF_INPUT_PORT);
		createEReference(leafInputPortEClass, LEAF_INPUT_PORT__INPUT);

		leafOutputPortEClass = createEClass(LEAF_OUTPUT_PORT);
		createEReference(leafOutputPortEClass, LEAF_OUTPUT_PORT__OUTPUT);

		dataLinkSourceEClass = createEClass(DATA_LINK_SOURCE);
		createEReference(dataLinkSourceEClass, DATA_LINK_SOURCE__TARGET);

		dataLinkTargetEClass = createEClass(DATA_LINK_TARGET);
		createEReference(dataLinkTargetEClass, DATA_LINK_TARGET__SOURCE);

		portEClass = createEClass(PORT);

		requiredInterfacePortEClass = createEClass(REQUIRED_INTERFACE_PORT);
		createEReference(requiredInterfacePortEClass, REQUIRED_INTERFACE_PORT__INTERFACE);
		createEReference(requiredInterfacePortEClass, REQUIRED_INTERFACE_PORT__CALL_LINK);

		providedInterfacePortEClass = createEClass(PROVIDED_INTERFACE_PORT);
		createEReference(providedInterfacePortEClass, PROVIDED_INTERFACE_PORT__INTERFACE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DclPackage theDclPackage = (DclPackage)EPackage.Registry.INSTANCE.getEPackage(DclPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		caresSystemEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		componentEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		compositeComponentEClass.getESuperTypes().add(this.getComponent());
		compositeComponentEClass.getESuperTypes().add(theDclPackage.getTimeableObject());
		leafComponentEClass.getESuperTypes().add(this.getComponent());
		leafComponentEClass.getESuperTypes().add(theDclPackage.getTimeableObject());
		dataLinkEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		compositeInputPortEClass.getESuperTypes().add(this.getDataLinkSource());
		compositeInputPortEClass.getESuperTypes().add(this.getDataLinkTarget());
		compositeInputPortEClass.getESuperTypes().add(this.getPort());
		compositeOutputPortEClass.getESuperTypes().add(this.getDataLinkTarget());
		compositeOutputPortEClass.getESuperTypes().add(this.getDataLinkSource());
		compositeOutputPortEClass.getESuperTypes().add(this.getPort());
		leafInputPortEClass.getESuperTypes().add(this.getDataLinkTarget());
		leafInputPortEClass.getESuperTypes().add(this.getPort());
		leafOutputPortEClass.getESuperTypes().add(this.getDataLinkSource());
		leafOutputPortEClass.getESuperTypes().add(this.getPort());
		portEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		requiredInterfacePortEClass.getESuperTypes().add(this.getPort());
		providedInterfacePortEClass.getESuperTypes().add(this.getPort());

		// Initialize classes, features, and operations; add parameters
		initEClass(caresSystemEClass, CaresSystem.class, "CaresSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCaresSystem_Declarations(), theDclPackage.getDeclaration(), null, "declarations", null, 0, -1, CaresSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCaresSystem_RootSystemComponent(), this.getCompositeComponent(), null, "rootSystemComponent", null, 1, 1, CaresSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCaresSystem_StartTime(), ecorePackage.getELong(), "startTime", "0", 1, 1, CaresSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCaresSystem_StepTime(), ecorePackage.getELong(), "stepTime", "1", 1, 1, CaresSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentEClass, Component.class, "Component", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComponent_SchedulingPriority(), ecorePackage.getEInt(), "schedulingPriority", null, 1, 1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComponent_InheritFrequency(), ecorePackage.getEBoolean(), "inheritFrequency", "false", 1, 1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compositeComponentEClass, CompositeComponent.class, "CompositeComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeComponent_Components(), this.getComponent(), null, "components", null, 0, -1, CompositeComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompositeComponent_Links(), this.getDataLink(), null, "links", null, 0, -1, CompositeComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompositeComponent_InputPorts(), this.getCompositeInputPort(), null, "inputPorts", null, 0, -1, CompositeComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompositeComponent_OutputPorts(), this.getCompositeOutputPort(), null, "outputPorts", null, 0, -1, CompositeComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(leafComponentEClass, LeafComponent.class, "LeafComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLeafComponent_Type(), theDclPackage.getLeafComponentType(), null, "type", null, 0, 1, LeafComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLeafComponent_Declarations(), theDclPackage.getParameterInstanciation(), null, "declarations", null, 0, -1, LeafComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLeafComponent_InputPorts(), this.getLeafInputPort(), null, "inputPorts", null, 0, -1, LeafComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLeafComponent_OutputPorts(), this.getLeafOutputPort(), null, "outputPorts", null, 0, -1, LeafComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLeafComponent_ProvidedPorts(), this.getProvidedInterfacePort(), null, "providedPorts", null, 0, -1, LeafComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLeafComponent_RequiredPorts(), this.getRequiredInterfacePort(), null, "requiredPorts", null, 0, -1, LeafComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataLinkEClass, DataLink.class, "DataLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataLink_Source(), this.getDataLinkSource(), this.getDataLinkSource_Target(), "source", null, 1, 1, DataLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataLink_Target(), this.getDataLinkTarget(), this.getDataLinkTarget_Source(), "target", null, 0, -1, DataLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compositeInputPortEClass, CompositeInputPort.class, "CompositeInputPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeInputPort_Type(), theDclPackage.getType(), null, "type", null, 0, 1, CompositeInputPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compositeOutputPortEClass, CompositeOutputPort.class, "CompositeOutputPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeOutputPort_Type(), theDclPackage.getType(), null, "type", null, 1, 1, CompositeOutputPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(leafInputPortEClass, LeafInputPort.class, "LeafInputPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLeafInputPort_Input(), theDclPackage.getInput(), null, "input", null, 0, 1, LeafInputPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(leafOutputPortEClass, LeafOutputPort.class, "LeafOutputPort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLeafOutputPort_Output(), theDclPackage.getOutput(), null, "output", null, 0, 1, LeafOutputPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataLinkSourceEClass, DataLinkSource.class, "DataLinkSource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataLinkSource_Target(), this.getDataLink(), this.getDataLink_Source(), "target", null, 0, 1, DataLinkSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataLinkTargetEClass, DataLinkTarget.class, "DataLinkTarget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataLinkTarget_Source(), this.getDataLink(), this.getDataLink_Target(), "source", null, 0, 1, DataLinkTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portEClass, Port.class, "Port", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requiredInterfacePortEClass, RequiredInterfacePort.class, "RequiredInterfacePort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRequiredInterfacePort_Interface(), theDclPackage.getInterfacePort(), null, "interface", null, 0, 1, RequiredInterfacePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequiredInterfacePort_CallLink(), this.getProvidedInterfacePort(), null, "callLink", null, 1, -1, RequiredInterfacePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providedInterfacePortEClass, ProvidedInterfacePort.class, "ProvidedInterfacePort", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getProvidedInterfacePort_Interface(), theDclPackage.getInterfacePort(), null, "interface", null, 0, 1, ProvidedInterfacePort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //SysPackageImpl
