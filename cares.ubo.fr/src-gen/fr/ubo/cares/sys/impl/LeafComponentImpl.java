/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.dcl.DclPackage;
import fr.ubo.cares.dcl.LeafComponentType;
import fr.ubo.cares.dcl.ParameterInstanciation;
import fr.ubo.cares.dcl.TimeableObject;

import fr.ubo.cares.sys.LeafComponent;
import fr.ubo.cares.sys.LeafInputPort;
import fr.ubo.cares.sys.LeafOutputPort;
import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.RequiredInterfacePort;
import fr.ubo.cares.sys.SysPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Leaf Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getFrequency <em>Frequency</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getInputPorts <em>Input Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getOutputPorts <em>Output Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getProvidedPorts <em>Provided Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.LeafComponentImpl#getRequiredPorts <em>Required Ports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LeafComponentImpl extends ComponentImpl implements LeafComponent {
	/**
	 * The default value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected static final double FREQUENCY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getFrequency() <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFrequency()
	 * @generated
	 * @ordered
	 */
	protected double frequency = FREQUENCY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected LeafComponentType type;

	/**
	 * The cached value of the '{@link #getDeclarations() <em>Declarations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeclarations()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterInstanciation> declarations;

	/**
	 * The cached value of the '{@link #getInputPorts() <em>Input Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<LeafInputPort> inputPorts;

	/**
	 * The cached value of the '{@link #getOutputPorts() <em>Output Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<LeafOutputPort> outputPorts;

	/**
	 * The cached value of the '{@link #getProvidedPorts() <em>Provided Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvidedPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<ProvidedInterfacePort> providedPorts;

	/**
	 * The cached value of the '{@link #getRequiredPorts() <em>Required Ports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredPorts()
	 * @generated
	 * @ordered
	 */
	protected EList<RequiredInterfacePort> requiredPorts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysPackage.Literals.LEAF_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getFrequency() {
		return frequency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFrequency(double newFrequency) {
		double oldFrequency = frequency;
		frequency = newFrequency;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.LEAF_COMPONENT__FREQUENCY, oldFrequency, frequency));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponentType getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject)type;
			type = (LeafComponentType)eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SysPackage.LEAF_COMPONENT__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponentType basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(LeafComponentType newType) {
		LeafComponentType oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.LEAF_COMPONENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterInstanciation> getDeclarations() {
		if (declarations == null) {
			declarations = new EObjectContainmentEList<ParameterInstanciation>(ParameterInstanciation.class, this, SysPackage.LEAF_COMPONENT__DECLARATIONS);
		}
		return declarations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LeafInputPort> getInputPorts() {
		if (inputPorts == null) {
			inputPorts = new EObjectContainmentEList<LeafInputPort>(LeafInputPort.class, this, SysPackage.LEAF_COMPONENT__INPUT_PORTS);
		}
		return inputPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LeafOutputPort> getOutputPorts() {
		if (outputPorts == null) {
			outputPorts = new EObjectContainmentEList<LeafOutputPort>(LeafOutputPort.class, this, SysPackage.LEAF_COMPONENT__OUTPUT_PORTS);
		}
		return outputPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ProvidedInterfacePort> getProvidedPorts() {
		if (providedPorts == null) {
			providedPorts = new EObjectContainmentEList<ProvidedInterfacePort>(ProvidedInterfacePort.class, this, SysPackage.LEAF_COMPONENT__PROVIDED_PORTS);
		}
		return providedPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RequiredInterfacePort> getRequiredPorts() {
		if (requiredPorts == null) {
			requiredPorts = new EObjectContainmentEList<RequiredInterfacePort>(RequiredInterfacePort.class, this, SysPackage.LEAF_COMPONENT__REQUIRED_PORTS);
		}
		return requiredPorts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SysPackage.LEAF_COMPONENT__DECLARATIONS:
				return ((InternalEList<?>)getDeclarations()).basicRemove(otherEnd, msgs);
			case SysPackage.LEAF_COMPONENT__INPUT_PORTS:
				return ((InternalEList<?>)getInputPorts()).basicRemove(otherEnd, msgs);
			case SysPackage.LEAF_COMPONENT__OUTPUT_PORTS:
				return ((InternalEList<?>)getOutputPorts()).basicRemove(otherEnd, msgs);
			case SysPackage.LEAF_COMPONENT__PROVIDED_PORTS:
				return ((InternalEList<?>)getProvidedPorts()).basicRemove(otherEnd, msgs);
			case SysPackage.LEAF_COMPONENT__REQUIRED_PORTS:
				return ((InternalEList<?>)getRequiredPorts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SysPackage.LEAF_COMPONENT__FREQUENCY:
				return getFrequency();
			case SysPackage.LEAF_COMPONENT__TYPE:
				if (resolve) return getType();
				return basicGetType();
			case SysPackage.LEAF_COMPONENT__DECLARATIONS:
				return getDeclarations();
			case SysPackage.LEAF_COMPONENT__INPUT_PORTS:
				return getInputPorts();
			case SysPackage.LEAF_COMPONENT__OUTPUT_PORTS:
				return getOutputPorts();
			case SysPackage.LEAF_COMPONENT__PROVIDED_PORTS:
				return getProvidedPorts();
			case SysPackage.LEAF_COMPONENT__REQUIRED_PORTS:
				return getRequiredPorts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SysPackage.LEAF_COMPONENT__FREQUENCY:
				setFrequency((Double)newValue);
				return;
			case SysPackage.LEAF_COMPONENT__TYPE:
				setType((LeafComponentType)newValue);
				return;
			case SysPackage.LEAF_COMPONENT__DECLARATIONS:
				getDeclarations().clear();
				getDeclarations().addAll((Collection<? extends ParameterInstanciation>)newValue);
				return;
			case SysPackage.LEAF_COMPONENT__INPUT_PORTS:
				getInputPorts().clear();
				getInputPorts().addAll((Collection<? extends LeafInputPort>)newValue);
				return;
			case SysPackage.LEAF_COMPONENT__OUTPUT_PORTS:
				getOutputPorts().clear();
				getOutputPorts().addAll((Collection<? extends LeafOutputPort>)newValue);
				return;
			case SysPackage.LEAF_COMPONENT__PROVIDED_PORTS:
				getProvidedPorts().clear();
				getProvidedPorts().addAll((Collection<? extends ProvidedInterfacePort>)newValue);
				return;
			case SysPackage.LEAF_COMPONENT__REQUIRED_PORTS:
				getRequiredPorts().clear();
				getRequiredPorts().addAll((Collection<? extends RequiredInterfacePort>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SysPackage.LEAF_COMPONENT__FREQUENCY:
				setFrequency(FREQUENCY_EDEFAULT);
				return;
			case SysPackage.LEAF_COMPONENT__TYPE:
				setType((LeafComponentType)null);
				return;
			case SysPackage.LEAF_COMPONENT__DECLARATIONS:
				getDeclarations().clear();
				return;
			case SysPackage.LEAF_COMPONENT__INPUT_PORTS:
				getInputPorts().clear();
				return;
			case SysPackage.LEAF_COMPONENT__OUTPUT_PORTS:
				getOutputPorts().clear();
				return;
			case SysPackage.LEAF_COMPONENT__PROVIDED_PORTS:
				getProvidedPorts().clear();
				return;
			case SysPackage.LEAF_COMPONENT__REQUIRED_PORTS:
				getRequiredPorts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SysPackage.LEAF_COMPONENT__FREQUENCY:
				return frequency != FREQUENCY_EDEFAULT;
			case SysPackage.LEAF_COMPONENT__TYPE:
				return type != null;
			case SysPackage.LEAF_COMPONENT__DECLARATIONS:
				return declarations != null && !declarations.isEmpty();
			case SysPackage.LEAF_COMPONENT__INPUT_PORTS:
				return inputPorts != null && !inputPorts.isEmpty();
			case SysPackage.LEAF_COMPONENT__OUTPUT_PORTS:
				return outputPorts != null && !outputPorts.isEmpty();
			case SysPackage.LEAF_COMPONENT__PROVIDED_PORTS:
				return providedPorts != null && !providedPorts.isEmpty();
			case SysPackage.LEAF_COMPONENT__REQUIRED_PORTS:
				return requiredPorts != null && !requiredPorts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == TimeableObject.class) {
			switch (derivedFeatureID) {
				case SysPackage.LEAF_COMPONENT__FREQUENCY: return DclPackage.TIMEABLE_OBJECT__FREQUENCY;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == TimeableObject.class) {
			switch (baseFeatureID) {
				case DclPackage.TIMEABLE_OBJECT__FREQUENCY: return SysPackage.LEAF_COMPONENT__FREQUENCY;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (frequency: ");
		result.append(frequency);
		result.append(')');
		return result.toString();
	}

} //LeafComponentImpl
