/**
 */
package fr.ubo.cares.sys.impl;

import fr.ubo.cares.dcl.Declaration;

import fr.ubo.cares.dcl.impl.NamedElementImpl;

import fr.ubo.cares.sys.CaresSystem;
import fr.ubo.cares.sys.CompositeComponent;
import fr.ubo.cares.sys.SysPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cares System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.impl.CaresSystemImpl#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CaresSystemImpl#getRootSystemComponent <em>Root System Component</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CaresSystemImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.impl.CaresSystemImpl#getStepTime <em>Step Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CaresSystemImpl extends NamedElementImpl implements CaresSystem {
	/**
	 * The cached value of the '{@link #getDeclarations() <em>Declarations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeclarations()
	 * @generated
	 * @ordered
	 */
	protected EList<Declaration> declarations;

	/**
	 * The cached value of the '{@link #getRootSystemComponent() <em>Root System Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootSystemComponent()
	 * @generated
	 * @ordered
	 */
	protected CompositeComponent rootSystemComponent;

	/**
	 * The default value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected static final long START_TIME_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getStartTime() <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartTime()
	 * @generated
	 * @ordered
	 */
	protected long startTime = START_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStepTime() <em>Step Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepTime()
	 * @generated
	 * @ordered
	 */
	protected static final long STEP_TIME_EDEFAULT = 1L;

	/**
	 * The cached value of the '{@link #getStepTime() <em>Step Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepTime()
	 * @generated
	 * @ordered
	 */
	protected long stepTime = STEP_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CaresSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SysPackage.Literals.CARES_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Declaration> getDeclarations() {
		if (declarations == null) {
			declarations = new EObjectResolvingEList<Declaration>(Declaration.class, this, SysPackage.CARES_SYSTEM__DECLARATIONS);
		}
		return declarations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeComponent getRootSystemComponent() {
		return rootSystemComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRootSystemComponent(CompositeComponent newRootSystemComponent, NotificationChain msgs) {
		CompositeComponent oldRootSystemComponent = rootSystemComponent;
		rootSystemComponent = newRootSystemComponent;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT, oldRootSystemComponent, newRootSystemComponent);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRootSystemComponent(CompositeComponent newRootSystemComponent) {
		if (newRootSystemComponent != rootSystemComponent) {
			NotificationChain msgs = null;
			if (rootSystemComponent != null)
				msgs = ((InternalEObject)rootSystemComponent).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT, null, msgs);
			if (newRootSystemComponent != null)
				msgs = ((InternalEObject)newRootSystemComponent).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT, null, msgs);
			msgs = basicSetRootSystemComponent(newRootSystemComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT, newRootSystemComponent, newRootSystemComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(long newStartTime) {
		long oldStartTime = startTime;
		startTime = newStartTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.CARES_SYSTEM__START_TIME, oldStartTime, startTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStepTime() {
		return stepTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStepTime(long newStepTime) {
		long oldStepTime = stepTime;
		stepTime = newStepTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SysPackage.CARES_SYSTEM__STEP_TIME, oldStepTime, stepTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT:
				return basicSetRootSystemComponent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SysPackage.CARES_SYSTEM__DECLARATIONS:
				return getDeclarations();
			case SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT:
				return getRootSystemComponent();
			case SysPackage.CARES_SYSTEM__START_TIME:
				return getStartTime();
			case SysPackage.CARES_SYSTEM__STEP_TIME:
				return getStepTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SysPackage.CARES_SYSTEM__DECLARATIONS:
				getDeclarations().clear();
				getDeclarations().addAll((Collection<? extends Declaration>)newValue);
				return;
			case SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT:
				setRootSystemComponent((CompositeComponent)newValue);
				return;
			case SysPackage.CARES_SYSTEM__START_TIME:
				setStartTime((Long)newValue);
				return;
			case SysPackage.CARES_SYSTEM__STEP_TIME:
				setStepTime((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SysPackage.CARES_SYSTEM__DECLARATIONS:
				getDeclarations().clear();
				return;
			case SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT:
				setRootSystemComponent((CompositeComponent)null);
				return;
			case SysPackage.CARES_SYSTEM__START_TIME:
				setStartTime(START_TIME_EDEFAULT);
				return;
			case SysPackage.CARES_SYSTEM__STEP_TIME:
				setStepTime(STEP_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SysPackage.CARES_SYSTEM__DECLARATIONS:
				return declarations != null && !declarations.isEmpty();
			case SysPackage.CARES_SYSTEM__ROOT_SYSTEM_COMPONENT:
				return rootSystemComponent != null;
			case SysPackage.CARES_SYSTEM__START_TIME:
				return startTime != START_TIME_EDEFAULT;
			case SysPackage.CARES_SYSTEM__STEP_TIME:
				return stepTime != STEP_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startTime: ");
		result.append(startTime);
		result.append(", stepTime: ");
		result.append(stepTime);
		result.append(')');
		return result.toString();
	}

} //CaresSystemImpl
