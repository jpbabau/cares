/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.InterfacePort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Interface Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.ProvidedInterfacePort#getInterface <em>Interface</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getProvidedInterfacePort()
 * @model
 * @generated
 */
public interface ProvidedInterfacePort extends Port {
	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(InterfacePort)
	 * @see fr.ubo.cares.sys.SysPackage#getProvidedInterfacePort_Interface()
	 * @model
	 * @generated
	 */
	InterfacePort getInterface();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.ProvidedInterfacePort#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(InterfacePort value);

} // ProvidedInterfacePort
