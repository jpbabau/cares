/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.sys.SysPackage#getPort()
 * @model abstract="true"
 * @generated
 */
public interface Port extends NamedElement {
} // Port
