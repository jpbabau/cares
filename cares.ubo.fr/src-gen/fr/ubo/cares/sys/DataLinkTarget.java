/**
 */
package fr.ubo.cares.sys;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Link Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.DataLinkTarget#getSource <em>Source</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getDataLinkTarget()
 * @model abstract="true"
 * @generated
 */
public interface DataLinkTarget extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.ubo.cares.sys.DataLink#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(DataLink)
	 * @see fr.ubo.cares.sys.SysPackage#getDataLinkTarget_Source()
	 * @see fr.ubo.cares.sys.DataLink#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	DataLink getSource();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.DataLinkTarget#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(DataLink value);

} // DataLinkTarget
