/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Output Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.CompositeOutputPort#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getCompositeOutputPort()
 * @model
 * @generated
 */
public interface CompositeOutputPort extends DataLinkTarget, DataLinkSource, Port {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see fr.ubo.cares.sys.SysPackage#getCompositeOutputPort_Type()
	 * @model required="true"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.CompositeOutputPort#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

} // CompositeOutputPort
