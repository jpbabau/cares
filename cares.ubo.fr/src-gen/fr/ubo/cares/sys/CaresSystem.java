/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.Declaration;
import fr.ubo.cares.dcl.NamedElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cares System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.CaresSystem#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.CaresSystem#getRootSystemComponent <em>Root System Component</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.CaresSystem#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.CaresSystem#getStepTime <em>Step Time</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getCaresSystem()
 * @model
 * @generated
 */
public interface CaresSystem extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Declarations</b></em>' reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Declaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declarations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declarations</em>' reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getCaresSystem_Declarations()
	 * @model
	 * @generated
	 */
	EList<Declaration> getDeclarations();

	/**
	 * Returns the value of the '<em><b>Root System Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root System Component</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root System Component</em>' containment reference.
	 * @see #setRootSystemComponent(CompositeComponent)
	 * @see fr.ubo.cares.sys.SysPackage#getCaresSystem_RootSystemComponent()
	 * @model containment="true" required="true"
	 * @generated
	 */
	CompositeComponent getRootSystemComponent();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.CaresSystem#getRootSystemComponent <em>Root System Component</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root System Component</em>' containment reference.
	 * @see #getRootSystemComponent()
	 * @generated
	 */
	void setRootSystemComponent(CompositeComponent value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(long)
	 * @see fr.ubo.cares.sys.SysPackage#getCaresSystem_StartTime()
	 * @model default="0" required="true"
	 * @generated
	 */
	long getStartTime();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.CaresSystem#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(long value);

	/**
	 * Returns the value of the '<em><b>Step Time</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Time</em>' attribute.
	 * @see #setStepTime(long)
	 * @see fr.ubo.cares.sys.SysPackage#getCaresSystem_StepTime()
	 * @model default="1" required="true"
	 * @generated
	 */
	long getStepTime();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.CaresSystem#getStepTime <em>Step Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Time</em>' attribute.
	 * @see #getStepTime()
	 * @generated
	 */
	void setStepTime(long value);

} // CaresSystem
