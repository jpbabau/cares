/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.DclPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.sys.SysFactory
 * @model kind="package"
 * @generated
 */
public interface SysPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sys";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.ubo.mde.cares/sys";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sys";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SysPackage eINSTANCE = fr.ubo.cares.sys.impl.SysPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.CaresSystemImpl <em>Cares System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.CaresSystemImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCaresSystem()
	 * @generated
	 */
	int CARES_SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM__DECLARATIONS = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root System Component</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM__ROOT_SYSTEM_COMPONENT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM__START_TIME = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Step Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM__STEP_TIME = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Cares System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Cares System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CARES_SYSTEM_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.ComponentImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Scheduling Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__SCHEDULING_PRIORITY = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Inherit Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__INHERIT_FREQUENCY = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.CompositeComponentImpl <em>Composite Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.CompositeComponentImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCompositeComponent()
	 * @generated
	 */
	int COMPOSITE_COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Scheduling Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__SCHEDULING_PRIORITY = COMPONENT__SCHEDULING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Inherit Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__INHERIT_FREQUENCY = COMPONENT__INHERIT_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__FREQUENCY = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__COMPONENTS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__LINKS = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Input Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__INPUT_PORTS = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Output Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT__OUTPUT_PORTS = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Composite Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Composite Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.LeafComponentImpl <em>Leaf Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.LeafComponentImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getLeafComponent()
	 * @generated
	 */
	int LEAF_COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Scheduling Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__SCHEDULING_PRIORITY = COMPONENT__SCHEDULING_PRIORITY;

	/**
	 * The feature id for the '<em><b>Inherit Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__INHERIT_FREQUENCY = COMPONENT__INHERIT_FREQUENCY;

	/**
	 * The feature id for the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__FREQUENCY = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__TYPE = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Declarations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__DECLARATIONS = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Input Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__INPUT_PORTS = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Output Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__OUTPUT_PORTS = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Provided Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__PROVIDED_PORTS = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Required Ports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT__REQUIRED_PORTS = COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Leaf Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Leaf Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.DataLinkImpl <em>Data Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.DataLinkImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getDataLink()
	 * @generated
	 */
	int DATA_LINK = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK__SOURCE = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK__TARGET = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Data Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.DataLinkSourceImpl <em>Data Link Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.DataLinkSourceImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getDataLinkSource()
	 * @generated
	 */
	int DATA_LINK_SOURCE = 9;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_SOURCE__TARGET = 0;

	/**
	 * The number of structural features of the '<em>Data Link Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_SOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Link Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_SOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.CompositeInputPortImpl <em>Composite Input Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.CompositeInputPortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCompositeInputPort()
	 * @generated
	 */
	int COMPOSITE_INPUT_PORT = 5;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INPUT_PORT__TARGET = DATA_LINK_SOURCE__TARGET;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INPUT_PORT__SOURCE = DATA_LINK_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INPUT_PORT__NAME = DATA_LINK_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INPUT_PORT__TYPE = DATA_LINK_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Composite Input Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INPUT_PORT_FEATURE_COUNT = DATA_LINK_SOURCE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Composite Input Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_INPUT_PORT_OPERATION_COUNT = DATA_LINK_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.DataLinkTargetImpl <em>Data Link Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.DataLinkTargetImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getDataLinkTarget()
	 * @generated
	 */
	int DATA_LINK_TARGET = 10;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_TARGET__SOURCE = 0;

	/**
	 * The number of structural features of the '<em>Data Link Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_TARGET_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Link Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_LINK_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.CompositeOutputPortImpl <em>Composite Output Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.CompositeOutputPortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCompositeOutputPort()
	 * @generated
	 */
	int COMPOSITE_OUTPUT_PORT = 6;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OUTPUT_PORT__SOURCE = DATA_LINK_TARGET__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OUTPUT_PORT__TARGET = DATA_LINK_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OUTPUT_PORT__NAME = DATA_LINK_TARGET_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OUTPUT_PORT__TYPE = DATA_LINK_TARGET_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Composite Output Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OUTPUT_PORT_FEATURE_COUNT = DATA_LINK_TARGET_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Composite Output Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OUTPUT_PORT_OPERATION_COUNT = DATA_LINK_TARGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.LeafInputPortImpl <em>Leaf Input Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.LeafInputPortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getLeafInputPort()
	 * @generated
	 */
	int LEAF_INPUT_PORT = 7;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_INPUT_PORT__SOURCE = DATA_LINK_TARGET__SOURCE;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_INPUT_PORT__NAME = DATA_LINK_TARGET_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_INPUT_PORT__INPUT = DATA_LINK_TARGET_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Leaf Input Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_INPUT_PORT_FEATURE_COUNT = DATA_LINK_TARGET_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Leaf Input Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_INPUT_PORT_OPERATION_COUNT = DATA_LINK_TARGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.LeafOutputPortImpl <em>Leaf Output Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.LeafOutputPortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getLeafOutputPort()
	 * @generated
	 */
	int LEAF_OUTPUT_PORT = 8;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_OUTPUT_PORT__TARGET = DATA_LINK_SOURCE__TARGET;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_OUTPUT_PORT__NAME = DATA_LINK_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Output</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_OUTPUT_PORT__OUTPUT = DATA_LINK_SOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Leaf Output Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_OUTPUT_PORT_FEATURE_COUNT = DATA_LINK_SOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Leaf Output Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_OUTPUT_PORT_OPERATION_COUNT = DATA_LINK_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.PortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.RequiredInterfacePortImpl <em>Required Interface Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.RequiredInterfacePortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getRequiredInterfacePort()
	 * @generated
	 */
	int REQUIRED_INTERFACE_PORT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_INTERFACE_PORT__NAME = PORT__NAME;

	/**
	 * The feature id for the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_INTERFACE_PORT__INTERFACE = PORT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Call Link</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_INTERFACE_PORT__CALL_LINK = PORT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Required Interface Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_INTERFACE_PORT_FEATURE_COUNT = PORT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Required Interface Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_INTERFACE_PORT_OPERATION_COUNT = PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.sys.impl.ProvidedInterfacePortImpl <em>Provided Interface Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.sys.impl.ProvidedInterfacePortImpl
	 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getProvidedInterfacePort()
	 * @generated
	 */
	int PROVIDED_INTERFACE_PORT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_INTERFACE_PORT__NAME = PORT__NAME;

	/**
	 * The feature id for the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_INTERFACE_PORT__INTERFACE = PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Provided Interface Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_INTERFACE_PORT_FEATURE_COUNT = PORT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Provided Interface Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_INTERFACE_PORT_OPERATION_COUNT = PORT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.CaresSystem <em>Cares System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cares System</em>'.
	 * @see fr.ubo.cares.sys.CaresSystem
	 * @generated
	 */
	EClass getCaresSystem();

	/**
	 * Returns the meta object for the reference list '{@link fr.ubo.cares.sys.CaresSystem#getDeclarations <em>Declarations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Declarations</em>'.
	 * @see fr.ubo.cares.sys.CaresSystem#getDeclarations()
	 * @see #getCaresSystem()
	 * @generated
	 */
	EReference getCaresSystem_Declarations();

	/**
	 * Returns the meta object for the containment reference '{@link fr.ubo.cares.sys.CaresSystem#getRootSystemComponent <em>Root System Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Root System Component</em>'.
	 * @see fr.ubo.cares.sys.CaresSystem#getRootSystemComponent()
	 * @see #getCaresSystem()
	 * @generated
	 */
	EReference getCaresSystem_RootSystemComponent();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.sys.CaresSystem#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see fr.ubo.cares.sys.CaresSystem#getStartTime()
	 * @see #getCaresSystem()
	 * @generated
	 */
	EAttribute getCaresSystem_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.sys.CaresSystem#getStepTime <em>Step Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step Time</em>'.
	 * @see fr.ubo.cares.sys.CaresSystem#getStepTime()
	 * @see #getCaresSystem()
	 * @generated
	 */
	EAttribute getCaresSystem_StepTime();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see fr.ubo.cares.sys.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.sys.Component#getSchedulingPriority <em>Scheduling Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scheduling Priority</em>'.
	 * @see fr.ubo.cares.sys.Component#getSchedulingPriority()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_SchedulingPriority();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.sys.Component#isInheritFrequency <em>Inherit Frequency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inherit Frequency</em>'.
	 * @see fr.ubo.cares.sys.Component#isInheritFrequency()
	 * @see #getComponent()
	 * @generated
	 */
	EAttribute getComponent_InheritFrequency();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.CompositeComponent <em>Composite Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Component</em>'.
	 * @see fr.ubo.cares.sys.CompositeComponent
	 * @generated
	 */
	EClass getCompositeComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.CompositeComponent#getComponents <em>Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Components</em>'.
	 * @see fr.ubo.cares.sys.CompositeComponent#getComponents()
	 * @see #getCompositeComponent()
	 * @generated
	 */
	EReference getCompositeComponent_Components();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.CompositeComponent#getLinks <em>Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Links</em>'.
	 * @see fr.ubo.cares.sys.CompositeComponent#getLinks()
	 * @see #getCompositeComponent()
	 * @generated
	 */
	EReference getCompositeComponent_Links();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.CompositeComponent#getInputPorts <em>Input Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Ports</em>'.
	 * @see fr.ubo.cares.sys.CompositeComponent#getInputPorts()
	 * @see #getCompositeComponent()
	 * @generated
	 */
	EReference getCompositeComponent_InputPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.CompositeComponent#getOutputPorts <em>Output Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output Ports</em>'.
	 * @see fr.ubo.cares.sys.CompositeComponent#getOutputPorts()
	 * @see #getCompositeComponent()
	 * @generated
	 */
	EReference getCompositeComponent_OutputPorts();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.LeafComponent <em>Leaf Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Component</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent
	 * @generated
	 */
	EClass getLeafComponent();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.LeafComponent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent#getType()
	 * @see #getLeafComponent()
	 * @generated
	 */
	EReference getLeafComponent_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.LeafComponent#getDeclarations <em>Declarations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Declarations</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent#getDeclarations()
	 * @see #getLeafComponent()
	 * @generated
	 */
	EReference getLeafComponent_Declarations();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.LeafComponent#getInputPorts <em>Input Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Input Ports</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent#getInputPorts()
	 * @see #getLeafComponent()
	 * @generated
	 */
	EReference getLeafComponent_InputPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.LeafComponent#getOutputPorts <em>Output Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Output Ports</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent#getOutputPorts()
	 * @see #getLeafComponent()
	 * @generated
	 */
	EReference getLeafComponent_OutputPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.LeafComponent#getProvidedPorts <em>Provided Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Ports</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent#getProvidedPorts()
	 * @see #getLeafComponent()
	 * @generated
	 */
	EReference getLeafComponent_ProvidedPorts();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.sys.LeafComponent#getRequiredPorts <em>Required Ports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Ports</em>'.
	 * @see fr.ubo.cares.sys.LeafComponent#getRequiredPorts()
	 * @see #getLeafComponent()
	 * @generated
	 */
	EReference getLeafComponent_RequiredPorts();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.DataLink <em>Data Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Link</em>'.
	 * @see fr.ubo.cares.sys.DataLink
	 * @generated
	 */
	EClass getDataLink();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.DataLink#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.ubo.cares.sys.DataLink#getSource()
	 * @see #getDataLink()
	 * @generated
	 */
	EReference getDataLink_Source();

	/**
	 * Returns the meta object for the reference list '{@link fr.ubo.cares.sys.DataLink#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Target</em>'.
	 * @see fr.ubo.cares.sys.DataLink#getTarget()
	 * @see #getDataLink()
	 * @generated
	 */
	EReference getDataLink_Target();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.CompositeInputPort <em>Composite Input Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Input Port</em>'.
	 * @see fr.ubo.cares.sys.CompositeInputPort
	 * @generated
	 */
	EClass getCompositeInputPort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.CompositeInputPort#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.ubo.cares.sys.CompositeInputPort#getType()
	 * @see #getCompositeInputPort()
	 * @generated
	 */
	EReference getCompositeInputPort_Type();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.CompositeOutputPort <em>Composite Output Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Output Port</em>'.
	 * @see fr.ubo.cares.sys.CompositeOutputPort
	 * @generated
	 */
	EClass getCompositeOutputPort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.CompositeOutputPort#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see fr.ubo.cares.sys.CompositeOutputPort#getType()
	 * @see #getCompositeOutputPort()
	 * @generated
	 */
	EReference getCompositeOutputPort_Type();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.LeafInputPort <em>Leaf Input Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Input Port</em>'.
	 * @see fr.ubo.cares.sys.LeafInputPort
	 * @generated
	 */
	EClass getLeafInputPort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.LeafInputPort#getInput <em>Input</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input</em>'.
	 * @see fr.ubo.cares.sys.LeafInputPort#getInput()
	 * @see #getLeafInputPort()
	 * @generated
	 */
	EReference getLeafInputPort_Input();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.LeafOutputPort <em>Leaf Output Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Output Port</em>'.
	 * @see fr.ubo.cares.sys.LeafOutputPort
	 * @generated
	 */
	EClass getLeafOutputPort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.LeafOutputPort#getOutput <em>Output</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Output</em>'.
	 * @see fr.ubo.cares.sys.LeafOutputPort#getOutput()
	 * @see #getLeafOutputPort()
	 * @generated
	 */
	EReference getLeafOutputPort_Output();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.DataLinkSource <em>Data Link Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Link Source</em>'.
	 * @see fr.ubo.cares.sys.DataLinkSource
	 * @generated
	 */
	EClass getDataLinkSource();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.DataLinkSource#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see fr.ubo.cares.sys.DataLinkSource#getTarget()
	 * @see #getDataLinkSource()
	 * @generated
	 */
	EReference getDataLinkSource_Target();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.DataLinkTarget <em>Data Link Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Link Target</em>'.
	 * @see fr.ubo.cares.sys.DataLinkTarget
	 * @generated
	 */
	EClass getDataLinkTarget();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.DataLinkTarget#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see fr.ubo.cares.sys.DataLinkTarget#getSource()
	 * @see #getDataLinkTarget()
	 * @generated
	 */
	EReference getDataLinkTarget_Source();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see fr.ubo.cares.sys.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.RequiredInterfacePort <em>Required Interface Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Interface Port</em>'.
	 * @see fr.ubo.cares.sys.RequiredInterfacePort
	 * @generated
	 */
	EClass getRequiredInterfacePort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.RequiredInterfacePort#getInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface</em>'.
	 * @see fr.ubo.cares.sys.RequiredInterfacePort#getInterface()
	 * @see #getRequiredInterfacePort()
	 * @generated
	 */
	EReference getRequiredInterfacePort_Interface();

	/**
	 * Returns the meta object for the reference list '{@link fr.ubo.cares.sys.RequiredInterfacePort#getCallLink <em>Call Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Call Link</em>'.
	 * @see fr.ubo.cares.sys.RequiredInterfacePort#getCallLink()
	 * @see #getRequiredInterfacePort()
	 * @generated
	 */
	EReference getRequiredInterfacePort_CallLink();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.sys.ProvidedInterfacePort <em>Provided Interface Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Interface Port</em>'.
	 * @see fr.ubo.cares.sys.ProvidedInterfacePort
	 * @generated
	 */
	EClass getProvidedInterfacePort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.sys.ProvidedInterfacePort#getInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface</em>'.
	 * @see fr.ubo.cares.sys.ProvidedInterfacePort#getInterface()
	 * @see #getProvidedInterfacePort()
	 * @generated
	 */
	EReference getProvidedInterfacePort_Interface();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SysFactory getSysFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.CaresSystemImpl <em>Cares System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.CaresSystemImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCaresSystem()
		 * @generated
		 */
		EClass CARES_SYSTEM = eINSTANCE.getCaresSystem();

		/**
		 * The meta object literal for the '<em><b>Declarations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CARES_SYSTEM__DECLARATIONS = eINSTANCE.getCaresSystem_Declarations();

		/**
		 * The meta object literal for the '<em><b>Root System Component</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CARES_SYSTEM__ROOT_SYSTEM_COMPONENT = eINSTANCE.getCaresSystem_RootSystemComponent();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CARES_SYSTEM__START_TIME = eINSTANCE.getCaresSystem_StartTime();

		/**
		 * The meta object literal for the '<em><b>Step Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CARES_SYSTEM__STEP_TIME = eINSTANCE.getCaresSystem_StepTime();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.ComponentImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Scheduling Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT__SCHEDULING_PRIORITY = eINSTANCE.getComponent_SchedulingPriority();

		/**
		 * The meta object literal for the '<em><b>Inherit Frequency</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT__INHERIT_FREQUENCY = eINSTANCE.getComponent_InheritFrequency();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.CompositeComponentImpl <em>Composite Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.CompositeComponentImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCompositeComponent()
		 * @generated
		 */
		EClass COMPOSITE_COMPONENT = eINSTANCE.getCompositeComponent();

		/**
		 * The meta object literal for the '<em><b>Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_COMPONENT__COMPONENTS = eINSTANCE.getCompositeComponent_Components();

		/**
		 * The meta object literal for the '<em><b>Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_COMPONENT__LINKS = eINSTANCE.getCompositeComponent_Links();

		/**
		 * The meta object literal for the '<em><b>Input Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_COMPONENT__INPUT_PORTS = eINSTANCE.getCompositeComponent_InputPorts();

		/**
		 * The meta object literal for the '<em><b>Output Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_COMPONENT__OUTPUT_PORTS = eINSTANCE.getCompositeComponent_OutputPorts();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.LeafComponentImpl <em>Leaf Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.LeafComponentImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getLeafComponent()
		 * @generated
		 */
		EClass LEAF_COMPONENT = eINSTANCE.getLeafComponent();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_COMPONENT__TYPE = eINSTANCE.getLeafComponent_Type();

		/**
		 * The meta object literal for the '<em><b>Declarations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_COMPONENT__DECLARATIONS = eINSTANCE.getLeafComponent_Declarations();

		/**
		 * The meta object literal for the '<em><b>Input Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_COMPONENT__INPUT_PORTS = eINSTANCE.getLeafComponent_InputPorts();

		/**
		 * The meta object literal for the '<em><b>Output Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_COMPONENT__OUTPUT_PORTS = eINSTANCE.getLeafComponent_OutputPorts();

		/**
		 * The meta object literal for the '<em><b>Provided Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_COMPONENT__PROVIDED_PORTS = eINSTANCE.getLeafComponent_ProvidedPorts();

		/**
		 * The meta object literal for the '<em><b>Required Ports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_COMPONENT__REQUIRED_PORTS = eINSTANCE.getLeafComponent_RequiredPorts();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.DataLinkImpl <em>Data Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.DataLinkImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getDataLink()
		 * @generated
		 */
		EClass DATA_LINK = eINSTANCE.getDataLink();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LINK__SOURCE = eINSTANCE.getDataLink_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LINK__TARGET = eINSTANCE.getDataLink_Target();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.CompositeInputPortImpl <em>Composite Input Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.CompositeInputPortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCompositeInputPort()
		 * @generated
		 */
		EClass COMPOSITE_INPUT_PORT = eINSTANCE.getCompositeInputPort();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_INPUT_PORT__TYPE = eINSTANCE.getCompositeInputPort_Type();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.CompositeOutputPortImpl <em>Composite Output Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.CompositeOutputPortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getCompositeOutputPort()
		 * @generated
		 */
		EClass COMPOSITE_OUTPUT_PORT = eINSTANCE.getCompositeOutputPort();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_OUTPUT_PORT__TYPE = eINSTANCE.getCompositeOutputPort_Type();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.LeafInputPortImpl <em>Leaf Input Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.LeafInputPortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getLeafInputPort()
		 * @generated
		 */
		EClass LEAF_INPUT_PORT = eINSTANCE.getLeafInputPort();

		/**
		 * The meta object literal for the '<em><b>Input</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_INPUT_PORT__INPUT = eINSTANCE.getLeafInputPort_Input();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.LeafOutputPortImpl <em>Leaf Output Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.LeafOutputPortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getLeafOutputPort()
		 * @generated
		 */
		EClass LEAF_OUTPUT_PORT = eINSTANCE.getLeafOutputPort();

		/**
		 * The meta object literal for the '<em><b>Output</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LEAF_OUTPUT_PORT__OUTPUT = eINSTANCE.getLeafOutputPort_Output();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.DataLinkSourceImpl <em>Data Link Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.DataLinkSourceImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getDataLinkSource()
		 * @generated
		 */
		EClass DATA_LINK_SOURCE = eINSTANCE.getDataLinkSource();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LINK_SOURCE__TARGET = eINSTANCE.getDataLinkSource_Target();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.DataLinkTargetImpl <em>Data Link Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.DataLinkTargetImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getDataLinkTarget()
		 * @generated
		 */
		EClass DATA_LINK_TARGET = eINSTANCE.getDataLinkTarget();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_LINK_TARGET__SOURCE = eINSTANCE.getDataLinkTarget_Source();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.PortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.RequiredInterfacePortImpl <em>Required Interface Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.RequiredInterfacePortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getRequiredInterfacePort()
		 * @generated
		 */
		EClass REQUIRED_INTERFACE_PORT = eINSTANCE.getRequiredInterfacePort();

		/**
		 * The meta object literal for the '<em><b>Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRED_INTERFACE_PORT__INTERFACE = eINSTANCE.getRequiredInterfacePort_Interface();

		/**
		 * The meta object literal for the '<em><b>Call Link</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIRED_INTERFACE_PORT__CALL_LINK = eINSTANCE.getRequiredInterfacePort_CallLink();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.sys.impl.ProvidedInterfacePortImpl <em>Provided Interface Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.sys.impl.ProvidedInterfacePortImpl
		 * @see fr.ubo.cares.sys.impl.SysPackageImpl#getProvidedInterfacePort()
		 * @generated
		 */
		EClass PROVIDED_INTERFACE_PORT = eINSTANCE.getProvidedInterfacePort();

		/**
		 * The meta object literal for the '<em><b>Interface</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROVIDED_INTERFACE_PORT__INTERFACE = eINSTANCE.getProvidedInterfacePort_Interface();

	}

} //SysPackage
