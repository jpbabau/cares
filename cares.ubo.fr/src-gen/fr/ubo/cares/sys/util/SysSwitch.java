/**
 */
package fr.ubo.cares.sys.util;

import fr.ubo.cares.dcl.NamedElement;
import fr.ubo.cares.dcl.TimeableObject;

import fr.ubo.cares.sys.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.sys.SysPackage
 * @generated
 */
public class SysSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SysPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SysSwitch() {
		if (modelPackage == null) {
			modelPackage = SysPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SysPackage.CARES_SYSTEM: {
				CaresSystem caresSystem = (CaresSystem)theEObject;
				T result = caseCaresSystem(caresSystem);
				if (result == null) result = caseNamedElement(caresSystem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.COMPONENT: {
				Component component = (Component)theEObject;
				T result = caseComponent(component);
				if (result == null) result = caseNamedElement(component);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.COMPOSITE_COMPONENT: {
				CompositeComponent compositeComponent = (CompositeComponent)theEObject;
				T result = caseCompositeComponent(compositeComponent);
				if (result == null) result = caseComponent(compositeComponent);
				if (result == null) result = caseTimeableObject(compositeComponent);
				if (result == null) result = caseNamedElement(compositeComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.LEAF_COMPONENT: {
				LeafComponent leafComponent = (LeafComponent)theEObject;
				T result = caseLeafComponent(leafComponent);
				if (result == null) result = caseComponent(leafComponent);
				if (result == null) result = caseTimeableObject(leafComponent);
				if (result == null) result = caseNamedElement(leafComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.DATA_LINK: {
				DataLink dataLink = (DataLink)theEObject;
				T result = caseDataLink(dataLink);
				if (result == null) result = caseNamedElement(dataLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.COMPOSITE_INPUT_PORT: {
				CompositeInputPort compositeInputPort = (CompositeInputPort)theEObject;
				T result = caseCompositeInputPort(compositeInputPort);
				if (result == null) result = caseDataLinkSource(compositeInputPort);
				if (result == null) result = caseDataLinkTarget(compositeInputPort);
				if (result == null) result = casePort(compositeInputPort);
				if (result == null) result = caseNamedElement(compositeInputPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.COMPOSITE_OUTPUT_PORT: {
				CompositeOutputPort compositeOutputPort = (CompositeOutputPort)theEObject;
				T result = caseCompositeOutputPort(compositeOutputPort);
				if (result == null) result = caseDataLinkTarget(compositeOutputPort);
				if (result == null) result = caseDataLinkSource(compositeOutputPort);
				if (result == null) result = casePort(compositeOutputPort);
				if (result == null) result = caseNamedElement(compositeOutputPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.LEAF_INPUT_PORT: {
				LeafInputPort leafInputPort = (LeafInputPort)theEObject;
				T result = caseLeafInputPort(leafInputPort);
				if (result == null) result = caseDataLinkTarget(leafInputPort);
				if (result == null) result = casePort(leafInputPort);
				if (result == null) result = caseNamedElement(leafInputPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.LEAF_OUTPUT_PORT: {
				LeafOutputPort leafOutputPort = (LeafOutputPort)theEObject;
				T result = caseLeafOutputPort(leafOutputPort);
				if (result == null) result = caseDataLinkSource(leafOutputPort);
				if (result == null) result = casePort(leafOutputPort);
				if (result == null) result = caseNamedElement(leafOutputPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.DATA_LINK_SOURCE: {
				DataLinkSource dataLinkSource = (DataLinkSource)theEObject;
				T result = caseDataLinkSource(dataLinkSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.DATA_LINK_TARGET: {
				DataLinkTarget dataLinkTarget = (DataLinkTarget)theEObject;
				T result = caseDataLinkTarget(dataLinkTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.PORT: {
				Port port = (Port)theEObject;
				T result = casePort(port);
				if (result == null) result = caseNamedElement(port);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.REQUIRED_INTERFACE_PORT: {
				RequiredInterfacePort requiredInterfacePort = (RequiredInterfacePort)theEObject;
				T result = caseRequiredInterfacePort(requiredInterfacePort);
				if (result == null) result = casePort(requiredInterfacePort);
				if (result == null) result = caseNamedElement(requiredInterfacePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SysPackage.PROVIDED_INTERFACE_PORT: {
				ProvidedInterfacePort providedInterfacePort = (ProvidedInterfacePort)theEObject;
				T result = caseProvidedInterfacePort(providedInterfacePort);
				if (result == null) result = casePort(providedInterfacePort);
				if (result == null) result = caseNamedElement(providedInterfacePort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cares System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cares System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCaresSystem(CaresSystem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeComponent(CompositeComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeafComponent(LeafComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataLink(DataLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Input Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Input Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeInputPort(CompositeInputPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Output Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Output Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeOutputPort(CompositeOutputPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf Input Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf Input Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeafInputPort(LeafInputPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf Output Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf Output Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeafOutputPort(LeafOutputPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Link Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Link Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataLinkSource(DataLinkSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Link Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Link Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataLinkTarget(DataLinkTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required Interface Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required Interface Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredInterfacePort(RequiredInterfacePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provided Interface Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provided Interface Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvidedInterfacePort(ProvidedInterfacePort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timeable Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timeable Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimeableObject(TimeableObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SysSwitch
