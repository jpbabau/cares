/**
 */
package fr.ubo.cares.sys;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Link Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.DataLinkSource#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getDataLinkSource()
 * @model abstract="true"
 * @generated
 */
public interface DataLinkSource extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.ubo.cares.sys.DataLink#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(DataLink)
	 * @see fr.ubo.cares.sys.SysPackage#getDataLinkSource_Target()
	 * @see fr.ubo.cares.sys.DataLink#getSource
	 * @model opposite="source"
	 * @generated
	 */
	DataLink getTarget();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.DataLinkSource#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(DataLink value);

} // DataLinkSource
