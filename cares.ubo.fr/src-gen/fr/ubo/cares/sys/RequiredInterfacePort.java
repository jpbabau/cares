/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.InterfacePort;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Interface Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.RequiredInterfacePort#getInterface <em>Interface</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.RequiredInterfacePort#getCallLink <em>Call Link</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getRequiredInterfacePort()
 * @model
 * @generated
 */
public interface RequiredInterfacePort extends Port {
	/**
	 * Returns the value of the '<em><b>Interface</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' reference.
	 * @see #setInterface(InterfacePort)
	 * @see fr.ubo.cares.sys.SysPackage#getRequiredInterfacePort_Interface()
	 * @model
	 * @generated
	 */
	InterfacePort getInterface();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.RequiredInterfacePort#getInterface <em>Interface</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' reference.
	 * @see #getInterface()
	 * @generated
	 */
	void setInterface(InterfacePort value);

	/**
	 * Returns the value of the '<em><b>Call Link</b></em>' reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.ProvidedInterfacePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call Link</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call Link</em>' reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getRequiredInterfacePort_CallLink()
	 * @model required="true"
	 * @generated
	 */
	EList<ProvidedInterfacePort> getCallLink();

} // RequiredInterfacePort
