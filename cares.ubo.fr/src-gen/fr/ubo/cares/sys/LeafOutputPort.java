/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.Output;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf Output Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.LeafOutputPort#getOutput <em>Output</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getLeafOutputPort()
 * @model
 * @generated
 */
public interface LeafOutputPort extends DataLinkSource, Port {
	/**
	 * Returns the value of the '<em><b>Output</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output</em>' reference.
	 * @see #setOutput(Output)
	 * @see fr.ubo.cares.sys.SysPackage#getLeafOutputPort_Output()
	 * @model
	 * @generated
	 */
	Output getOutput();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.LeafOutputPort#getOutput <em>Output</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output</em>' reference.
	 * @see #getOutput()
	 * @generated
	 */
	void setOutput(Output value);

} // LeafOutputPort
