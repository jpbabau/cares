/**
 */
package fr.ubo.cares.sys;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.sys.SysPackage
 * @generated
 */
public interface SysFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SysFactory eINSTANCE = fr.ubo.cares.sys.impl.SysFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Cares System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cares System</em>'.
	 * @generated
	 */
	CaresSystem createCaresSystem();

	/**
	 * Returns a new object of class '<em>Composite Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Component</em>'.
	 * @generated
	 */
	CompositeComponent createCompositeComponent();

	/**
	 * Returns a new object of class '<em>Leaf Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Leaf Component</em>'.
	 * @generated
	 */
	LeafComponent createLeafComponent();

	/**
	 * Returns a new object of class '<em>Data Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Link</em>'.
	 * @generated
	 */
	DataLink createDataLink();

	/**
	 * Returns a new object of class '<em>Composite Input Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Input Port</em>'.
	 * @generated
	 */
	CompositeInputPort createCompositeInputPort();

	/**
	 * Returns a new object of class '<em>Composite Output Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Output Port</em>'.
	 * @generated
	 */
	CompositeOutputPort createCompositeOutputPort();

	/**
	 * Returns a new object of class '<em>Leaf Input Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Leaf Input Port</em>'.
	 * @generated
	 */
	LeafInputPort createLeafInputPort();

	/**
	 * Returns a new object of class '<em>Leaf Output Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Leaf Output Port</em>'.
	 * @generated
	 */
	LeafOutputPort createLeafOutputPort();

	/**
	 * Returns a new object of class '<em>Required Interface Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required Interface Port</em>'.
	 * @generated
	 */
	RequiredInterfacePort createRequiredInterfacePort();

	/**
	 * Returns a new object of class '<em>Provided Interface Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provided Interface Port</em>'.
	 * @generated
	 */
	ProvidedInterfacePort createProvidedInterfacePort();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SysPackage getSysPackage();

} //SysFactory
