/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Input Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.CompositeInputPort#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getCompositeInputPort()
 * @model
 * @generated
 */
public interface CompositeInputPort extends DataLinkSource, DataLinkTarget, Port {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Type)
	 * @see fr.ubo.cares.sys.SysPackage#getCompositeInputPort_Type()
	 * @model
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.CompositeInputPort#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

} // CompositeInputPort
