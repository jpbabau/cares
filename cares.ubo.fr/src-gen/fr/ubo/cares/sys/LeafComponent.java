/**
 */
package fr.ubo.cares.sys;

import fr.ubo.cares.dcl.LeafComponentType;
import fr.ubo.cares.dcl.ParameterInstanciation;
import fr.ubo.cares.dcl.TimeableObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.sys.LeafComponent#getType <em>Type</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.LeafComponent#getDeclarations <em>Declarations</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.LeafComponent#getInputPorts <em>Input Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.LeafComponent#getOutputPorts <em>Output Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.LeafComponent#getProvidedPorts <em>Provided Ports</em>}</li>
 *   <li>{@link fr.ubo.cares.sys.LeafComponent#getRequiredPorts <em>Required Ports</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent()
 * @model
 * @generated
 */
public interface LeafComponent extends Component, TimeableObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(LeafComponentType)
	 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent_Type()
	 * @model
	 * @generated
	 */
	LeafComponentType getType();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.sys.LeafComponent#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(LeafComponentType value);

	/**
	 * Returns the value of the '<em><b>Declarations</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.ParameterInstanciation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declarations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declarations</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent_Declarations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterInstanciation> getDeclarations();

	/**
	 * Returns the value of the '<em><b>Input Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.LeafInputPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Ports</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent_InputPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<LeafInputPort> getInputPorts();

	/**
	 * Returns the value of the '<em><b>Output Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.LeafOutputPort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Ports</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent_OutputPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<LeafOutputPort> getOutputPorts();

	/**
	 * Returns the value of the '<em><b>Provided Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.ProvidedInterfacePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Ports</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent_ProvidedPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<ProvidedInterfacePort> getProvidedPorts();

	/**
	 * Returns the value of the '<em><b>Required Ports</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.sys.RequiredInterfacePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Ports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Ports</em>' containment reference list.
	 * @see fr.ubo.cares.sys.SysPackage#getLeafComponent_RequiredPorts()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredInterfacePort> getRequiredPorts();

} // LeafComponent
