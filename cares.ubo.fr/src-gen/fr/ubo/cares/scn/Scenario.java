/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.NamedElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scenario</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.Scenario#getBegin <em>Begin</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Scenario#getEvents <em>Events</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Scenario#getEnd <em>End</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Scenario#getNumber <em>Number</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Scenario#getLogs <em>Logs</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getScenario()
 * @model
 * @generated
 */
public interface Scenario extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Begin</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Begin</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Begin</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getScenario_Begin()
	 * @model containment="true"
	 * @generated
	 */
	EList<Action> getBegin();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getScenario_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getEvents();

	/**
	 * Returns the value of the '<em><b>End</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.ServiceCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getScenario_End()
	 * @model containment="true"
	 * @generated
	 */
	EList<ServiceCall> getEnd();

	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see fr.ubo.cares.scn.ScnPackage#getScenario_Number()
	 * @model default="1" required="true"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Scenario#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * Returns the value of the '<em><b>Logs</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.LogObservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logs</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getScenario_Logs()
	 * @model containment="true"
	 * @generated
	 */
	EList<LogObservation> getLogs();

} // Scenario
