/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.RequiredInterfacePort;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XOR</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.XOR#getRequiredInterfacePort <em>Required Interface Port</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.XOR#getProvidedInterfacePort <em>Provided Interface Port</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getXOR()
 * @model
 * @generated
 */
public interface XOR extends Action {
	/**
	 * Returns the value of the '<em><b>Required Interface Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Interface Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Interface Port</em>' reference.
	 * @see #setRequiredInterfacePort(RequiredInterfacePort)
	 * @see fr.ubo.cares.scn.ScnPackage#getXOR_RequiredInterfacePort()
	 * @model required="true"
	 * @generated
	 */
	RequiredInterfacePort getRequiredInterfacePort();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.XOR#getRequiredInterfacePort <em>Required Interface Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Interface Port</em>' reference.
	 * @see #getRequiredInterfacePort()
	 * @generated
	 */
	void setRequiredInterfacePort(RequiredInterfacePort value);

	/**
	 * Returns the value of the '<em><b>Provided Interface Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Interface Port</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Interface Port</em>' reference.
	 * @see #setProvidedInterfacePort(ProvidedInterfacePort)
	 * @see fr.ubo.cares.scn.ScnPackage#getXOR_ProvidedInterfacePort()
	 * @model required="true"
	 * @generated
	 */
	ProvidedInterfacePort getProvidedInterfacePort();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.XOR#getProvidedInterfacePort <em>Provided Interface Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Interface Port</em>' reference.
	 * @see #getProvidedInterfacePort()
	 * @generated
	 */
	void setProvidedInterfacePort(ProvidedInterfacePort value);

} // XOR
