/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.sys.LeafOutputPort;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Log Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.LogColumn#getData <em>Data</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getLogColumn()
 * @model
 * @generated
 */
public interface LogColumn extends EObject {
	/**
	 * Returns the value of the '<em><b>Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' reference.
	 * @see #setData(LeafOutputPort)
	 * @see fr.ubo.cares.scn.ScnPackage#getLogColumn_Data()
	 * @model required="true"
	 * @generated
	 */
	LeafOutputPort getData();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.LogColumn#getData <em>Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data</em>' reference.
	 * @see #getData()
	 * @generated
	 */
	void setData(LeafOutputPort value);

} // LogColumn
