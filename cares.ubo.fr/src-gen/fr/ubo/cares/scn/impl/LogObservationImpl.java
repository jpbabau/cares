/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.dcl.impl.NamedElementImpl;

import fr.ubo.cares.scn.LogColumn;
import fr.ubo.cares.scn.LogObservation;
import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.TimeStamp;
import fr.ubo.cares.scn.TypeFile;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Log Observation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.LogObservationImpl#getFreq <em>Freq</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.LogObservationImpl#getColumns <em>Columns</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.LogObservationImpl#getType <em>Type</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.LogObservationImpl#getTimeStamp <em>Time Stamp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LogObservationImpl extends NamedElementImpl implements LogObservation {
	/**
	 * The default value of the '{@link #getFreq() <em>Freq</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFreq()
	 * @generated
	 * @ordered
	 */
	protected static final double FREQ_EDEFAULT = 1.0;

	/**
	 * The cached value of the '{@link #getFreq() <em>Freq</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFreq()
	 * @generated
	 * @ordered
	 */
	protected double freq = FREQ_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColumns() <em>Columns</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumns()
	 * @generated
	 * @ordered
	 */
	protected EList<LogColumn> columns;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final TypeFile TYPE_EDEFAULT = TypeFile.CSV;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected TypeFile type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected static final TimeStamp TIME_STAMP_EDEFAULT = TimeStamp.TIMED;

	/**
	 * The cached value of the '{@link #getTimeStamp() <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeStamp()
	 * @generated
	 * @ordered
	 */
	protected TimeStamp timeStamp = TIME_STAMP_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogObservationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.LOG_OBSERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getFreq() {
		return freq;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFreq(double newFreq) {
		double oldFreq = freq;
		freq = newFreq;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.LOG_OBSERVATION__FREQ, oldFreq, freq));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LogColumn> getColumns() {
		if (columns == null) {
			columns = new EObjectContainmentEList<LogColumn>(LogColumn.class, this, ScnPackage.LOG_OBSERVATION__COLUMNS);
		}
		return columns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeFile getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(TypeFile newType) {
		TypeFile oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.LOG_OBSERVATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeStamp getTimeStamp() {
		return timeStamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeStamp(TimeStamp newTimeStamp) {
		TimeStamp oldTimeStamp = timeStamp;
		timeStamp = newTimeStamp == null ? TIME_STAMP_EDEFAULT : newTimeStamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.LOG_OBSERVATION__TIME_STAMP, oldTimeStamp, timeStamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScnPackage.LOG_OBSERVATION__COLUMNS:
				return ((InternalEList<?>)getColumns()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.LOG_OBSERVATION__FREQ:
				return getFreq();
			case ScnPackage.LOG_OBSERVATION__COLUMNS:
				return getColumns();
			case ScnPackage.LOG_OBSERVATION__TYPE:
				return getType();
			case ScnPackage.LOG_OBSERVATION__TIME_STAMP:
				return getTimeStamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.LOG_OBSERVATION__FREQ:
				setFreq((Double)newValue);
				return;
			case ScnPackage.LOG_OBSERVATION__COLUMNS:
				getColumns().clear();
				getColumns().addAll((Collection<? extends LogColumn>)newValue);
				return;
			case ScnPackage.LOG_OBSERVATION__TYPE:
				setType((TypeFile)newValue);
				return;
			case ScnPackage.LOG_OBSERVATION__TIME_STAMP:
				setTimeStamp((TimeStamp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.LOG_OBSERVATION__FREQ:
				setFreq(FREQ_EDEFAULT);
				return;
			case ScnPackage.LOG_OBSERVATION__COLUMNS:
				getColumns().clear();
				return;
			case ScnPackage.LOG_OBSERVATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case ScnPackage.LOG_OBSERVATION__TIME_STAMP:
				setTimeStamp(TIME_STAMP_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.LOG_OBSERVATION__FREQ:
				return freq != FREQ_EDEFAULT;
			case ScnPackage.LOG_OBSERVATION__COLUMNS:
				return columns != null && !columns.isEmpty();
			case ScnPackage.LOG_OBSERVATION__TYPE:
				return type != TYPE_EDEFAULT;
			case ScnPackage.LOG_OBSERVATION__TIME_STAMP:
				return timeStamp != TIME_STAMP_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (freq: ");
		result.append(freq);
		result.append(", type: ");
		result.append(type);
		result.append(", timeStamp: ");
		result.append(timeStamp);
		result.append(')');
		return result.toString();
	}

} //LogObservationImpl
