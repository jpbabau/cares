/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.Time;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Time</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.TimeImpl#getStartingTime <em>Starting Time</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.TimeImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.TimeImpl#getStepTime <em>Step Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimeImpl extends MinimalEObjectImpl.Container implements Time {
	/**
	 * The default value of the '{@link #getStartingTime() <em>Starting Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingTime()
	 * @generated
	 * @ordered
	 */
	protected static final long STARTING_TIME_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getStartingTime() <em>Starting Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartingTime()
	 * @generated
	 * @ordered
	 */
	protected long startingTime = STARTING_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected static final long END_TIME_EDEFAULT = 100L;

	/**
	 * The cached value of the '{@link #getEndTime() <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndTime()
	 * @generated
	 * @ordered
	 */
	protected long endTime = END_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStepTime() <em>Step Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepTime()
	 * @generated
	 * @ordered
	 */
	protected static final long STEP_TIME_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getStepTime() <em>Step Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepTime()
	 * @generated
	 * @ordered
	 */
	protected long stepTime = STEP_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TimeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.TIME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStartingTime() {
		return startingTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartingTime(long newStartingTime) {
		long oldStartingTime = startingTime;
		startingTime = newStartingTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.TIME__STARTING_TIME, oldStartingTime, startingTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getEndTime() {
		return endTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(long newEndTime) {
		long oldEndTime = endTime;
		endTime = newEndTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.TIME__END_TIME, oldEndTime, endTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getStepTime() {
		return stepTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStepTime(long newStepTime) {
		long oldStepTime = stepTime;
		stepTime = newStepTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.TIME__STEP_TIME, oldStepTime, stepTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.TIME__STARTING_TIME:
				return getStartingTime();
			case ScnPackage.TIME__END_TIME:
				return getEndTime();
			case ScnPackage.TIME__STEP_TIME:
				return getStepTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.TIME__STARTING_TIME:
				setStartingTime((Long)newValue);
				return;
			case ScnPackage.TIME__END_TIME:
				setEndTime((Long)newValue);
				return;
			case ScnPackage.TIME__STEP_TIME:
				setStepTime((Long)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.TIME__STARTING_TIME:
				setStartingTime(STARTING_TIME_EDEFAULT);
				return;
			case ScnPackage.TIME__END_TIME:
				setEndTime(END_TIME_EDEFAULT);
				return;
			case ScnPackage.TIME__STEP_TIME:
				setStepTime(STEP_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.TIME__STARTING_TIME:
				return startingTime != STARTING_TIME_EDEFAULT;
			case ScnPackage.TIME__END_TIME:
				return endTime != END_TIME_EDEFAULT;
			case ScnPackage.TIME__STEP_TIME:
				return stepTime != STEP_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (startingTime: ");
		result.append(startingTime);
		result.append(", endTime: ");
		result.append(endTime);
		result.append(", stepTime: ");
		result.append(stepTime);
		result.append(')');
		return result.toString();
	}

} //TimeImpl
