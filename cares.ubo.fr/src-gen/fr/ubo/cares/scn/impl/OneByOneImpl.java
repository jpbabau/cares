/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.OneByOne;
import fr.ubo.cares.scn.ScnPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>One By One</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class OneByOneImpl extends ExplorationPolicyImpl implements OneByOne {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OneByOneImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.ONE_BY_ONE;
	}

} //OneByOneImpl
