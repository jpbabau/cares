/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.BoolParameterSet;
import fr.ubo.cares.scn.ScnPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bool Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BoolParameterSetImpl extends MinimalEObjectImpl.Container implements BoolParameterSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BoolParameterSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.BOOL_PARAMETER_SET;
	}

} //BoolParameterSetImpl
