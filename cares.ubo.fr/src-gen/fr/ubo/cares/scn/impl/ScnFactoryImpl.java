/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScnFactoryImpl extends EFactoryImpl implements ScnFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ScnFactory init() {
		try {
			ScnFactory theScnFactory = (ScnFactory)EPackage.Registry.INSTANCE.getEFactory(ScnPackage.eNS_URI);
			if (theScnFactory != null) {
				return theScnFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ScnFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScnFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ScnPackage.SIMULATION: return createSimulation();
			case ScnPackage.TIME: return createTime();
			case ScnPackage.SCENARIO: return createScenario();
			case ScnPackage.BASIC_EVENT: return createBasicEvent();
			case ScnPackage.COMPONENT_INITIALIZATION: return createComponentInitialization();
			case ScnPackage.COMPONENT_START: return createComponentStart();
			case ScnPackage.COMPONENT_STOP: return createComponentStop();
			case ScnPackage.SERVICE_CALL: return createServiceCall();
			case ScnPackage.SIMPLE_DOUBLE_PARAMETER_SET: return createSimpleDoubleParameterSet();
			case ScnPackage.SIMPLE_INT_PARAMETER_SET: return createSimpleIntParameterSet();
			case ScnPackage.SIMPLE_BOOL_PARAMETER_SET: return createSimpleBoolParameterSet();
			case ScnPackage.SIMPLE_STRING_PARAMETER_SET: return createSimpleStringParameterSet();
			case ScnPackage.COMPLEX_EVENT: return createComplexEvent();
			case ScnPackage.VARIABLE_FUNCTION_CALL: return createVariableFunctionCall();
			case ScnPackage.DOUBLE_PARAMETER_SET: return createDoubleParameterSet();
			case ScnPackage.INT_PARAMETER_SET: return createIntParameterSet();
			case ScnPackage.BOOL_PARAMETER_SET: return createBoolParameterSet();
			case ScnPackage.EXHAUSTIVE: return createExhaustive();
			case ScnPackage.RANDOM: return createRandom();
			case ScnPackage.ONE_BY_ONE: return createOneByOne();
			case ScnPackage.DIFFERENTIAL: return createDifferential();
			case ScnPackage.USER_POLICY: return createUserPolicy();
			case ScnPackage.LOG_OBSERVATION: return createLogObservation();
			case ScnPackage.LOG_COLUMN: return createLogColumn();
			case ScnPackage.XOR: return createXOR();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ScnPackage.TYPE_FILE:
				return createTypeFileFromString(eDataType, initialValue);
			case ScnPackage.TIME_STAMP:
				return createTimeStampFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ScnPackage.TYPE_FILE:
				return convertTypeFileToString(eDataType, instanceValue);
			case ScnPackage.TIME_STAMP:
				return convertTimeStampToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation createSimulation() {
		SimulationImpl simulation = new SimulationImpl();
		return simulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time createTime() {
		TimeImpl time = new TimeImpl();
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scenario createScenario() {
		ScenarioImpl scenario = new ScenarioImpl();
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BasicEvent createBasicEvent() {
		BasicEventImpl basicEvent = new BasicEventImpl();
		return basicEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInitialization createComponentInitialization() {
		ComponentInitializationImpl componentInitialization = new ComponentInitializationImpl();
		return componentInitialization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentStart createComponentStart() {
		ComponentStartImpl componentStart = new ComponentStartImpl();
		return componentStart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentStop createComponentStop() {
		ComponentStopImpl componentStop = new ComponentStopImpl();
		return componentStop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceCall createServiceCall() {
		ServiceCallImpl serviceCall = new ServiceCallImpl();
		return serviceCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleDoubleParameterSet createSimpleDoubleParameterSet() {
		SimpleDoubleParameterSetImpl simpleDoubleParameterSet = new SimpleDoubleParameterSetImpl();
		return simpleDoubleParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleIntParameterSet createSimpleIntParameterSet() {
		SimpleIntParameterSetImpl simpleIntParameterSet = new SimpleIntParameterSetImpl();
		return simpleIntParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleBoolParameterSet createSimpleBoolParameterSet() {
		SimpleBoolParameterSetImpl simpleBoolParameterSet = new SimpleBoolParameterSetImpl();
		return simpleBoolParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleStringParameterSet createSimpleStringParameterSet() {
		SimpleStringParameterSetImpl simpleStringParameterSet = new SimpleStringParameterSetImpl();
		return simpleStringParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComplexEvent createComplexEvent() {
		ComplexEventImpl complexEvent = new ComplexEventImpl();
		return complexEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableFunctionCall createVariableFunctionCall() {
		VariableFunctionCallImpl variableFunctionCall = new VariableFunctionCallImpl();
		return variableFunctionCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DoubleParameterSet createDoubleParameterSet() {
		DoubleParameterSetImpl doubleParameterSet = new DoubleParameterSetImpl();
		return doubleParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IntParameterSet createIntParameterSet() {
		IntParameterSetImpl intParameterSet = new IntParameterSetImpl();
		return intParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BoolParameterSet createBoolParameterSet() {
		BoolParameterSetImpl boolParameterSet = new BoolParameterSetImpl();
		return boolParameterSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Exhaustive createExhaustive() {
		ExhaustiveImpl exhaustive = new ExhaustiveImpl();
		return exhaustive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Random createRandom() {
		RandomImpl random = new RandomImpl();
		return random;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OneByOne createOneByOne() {
		OneByOneImpl oneByOne = new OneByOneImpl();
		return oneByOne;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Differential createDifferential() {
		DifferentialImpl differential = new DifferentialImpl();
		return differential;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserPolicy createUserPolicy() {
		UserPolicyImpl userPolicy = new UserPolicyImpl();
		return userPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogObservation createLogObservation() {
		LogObservationImpl logObservation = new LogObservationImpl();
		return logObservation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogColumn createLogColumn() {
		LogColumnImpl logColumn = new LogColumnImpl();
		return logColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XOR createXOR() {
		XORImpl xor = new XORImpl();
		return xor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeFile createTypeFileFromString(EDataType eDataType, String initialValue) {
		TypeFile result = TypeFile.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTypeFileToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeStamp createTimeStampFromString(EDataType eDataType, String initialValue) {
		TimeStamp result = TimeStamp.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTimeStampToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScnPackage getScnPackage() {
		return (ScnPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ScnPackage getPackage() {
		return ScnPackage.eINSTANCE;
	}

} //ScnFactoryImpl
