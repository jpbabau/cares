/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.dcl.DclPackage;

import fr.ubo.cares.scn.Action;
import fr.ubo.cares.scn.BasicEvent;
import fr.ubo.cares.scn.BoolParameterSet;
import fr.ubo.cares.scn.ComplexEvent;
import fr.ubo.cares.scn.ComponentInitialization;
import fr.ubo.cares.scn.ComponentStart;
import fr.ubo.cares.scn.ComponentStop;
import fr.ubo.cares.scn.Differential;
import fr.ubo.cares.scn.DoubleParameterSet;
import fr.ubo.cares.scn.Event;
import fr.ubo.cares.scn.Exhaustive;
import fr.ubo.cares.scn.ExplorationPolicy;
import fr.ubo.cares.scn.IntParameterSet;
import fr.ubo.cares.scn.LogColumn;
import fr.ubo.cares.scn.LogObservation;
import fr.ubo.cares.scn.OneByOne;
import fr.ubo.cares.scn.ParameterChange;
import fr.ubo.cares.scn.Random;
import fr.ubo.cares.scn.Scenario;
import fr.ubo.cares.scn.ScnFactory;
import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.ServiceCall;
import fr.ubo.cares.scn.SimpleBoolParameterSet;
import fr.ubo.cares.scn.SimpleDoubleParameterSet;
import fr.ubo.cares.scn.SimpleIntParameterSet;
import fr.ubo.cares.scn.SimpleStringParameterSet;
import fr.ubo.cares.scn.Simulation;
import fr.ubo.cares.scn.Time;
import fr.ubo.cares.scn.TimeStamp;
import fr.ubo.cares.scn.TypeFile;
import fr.ubo.cares.scn.UserPolicy;
import fr.ubo.cares.scn.VariableFunctionCall;

import fr.ubo.cares.sys.SysPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScnPackageImpl extends EPackageImpl implements ScnPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scenarioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass basicEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInitializationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentStartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentStopEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serviceCallEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterChangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleDoubleParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleIntParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleBoolParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simpleStringParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass complexEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variableFunctionCallEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boolParameterSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass explorationPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exhaustiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass randomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oneByOneEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass differentialEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass userPolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logObservationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass logColumnEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum typeFileEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum timeStampEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.ubo.cares.scn.ScnPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScnPackageImpl() {
		super(eNS_URI, ScnFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link ScnPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScnPackage init() {
		if (isInited) return (ScnPackage)EPackage.Registry.INSTANCE.getEPackage(ScnPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredScnPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		ScnPackageImpl theScnPackage = registeredScnPackage instanceof ScnPackageImpl ? (ScnPackageImpl)registeredScnPackage : new ScnPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		SysPackage.eINSTANCE.eClass();
		DclPackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theScnPackage.createPackageContents();

		// Initialize created meta-data
		theScnPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theScnPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ScnPackage.eNS_URI, theScnPackage);
		return theScnPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulation() {
		return simulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_CaresSystem() {
		return (EReference)simulationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_TimeUnit() {
		return (EAttribute)simulationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_ImportantDates() {
		return (EReference)simulationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Begin() {
		return (EReference)simulationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Scenarios() {
		return (EReference)simulationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_End() {
		return (EReference)simulationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTime() {
		return timeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTime_StartingTime() {
		return (EAttribute)timeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTime_EndTime() {
		return (EAttribute)timeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTime_StepTime() {
		return (EAttribute)timeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScenario() {
		return scenarioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenario_Begin() {
		return (EReference)scenarioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenario_Events() {
		return (EReference)scenarioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenario_End() {
		return (EReference)scenarioEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScenario_Number() {
		return (EAttribute)scenarioEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScenario_Logs() {
		return (EReference)scenarioEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEvent_Time() {
		return (EAttribute)eventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBasicEvent() {
		return basicEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBasicEvent_Actions() {
		return (EReference)basicEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInitialization() {
		return componentInitializationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInitialization_Component() {
		return (EReference)componentInitializationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentStart() {
		return componentStartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentStart_Component() {
		return (EReference)componentStartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentStop() {
		return componentStopEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentStop_Component() {
		return (EReference)componentStopEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServiceCall() {
		return serviceCallEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceCall_FunctionCall() {
		return (EReference)serviceCallEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServiceCall_Component() {
		return (EReference)serviceCallEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameterChange() {
		return parameterChangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getParameterChange_Parameter() {
		return (EReference)parameterChangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleDoubleParameterSet() {
		return simpleDoubleParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimpleDoubleParameterSet_Value() {
		return (EAttribute)simpleDoubleParameterSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleIntParameterSet() {
		return simpleIntParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimpleIntParameterSet_Value() {
		return (EAttribute)simpleIntParameterSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleBoolParameterSet() {
		return simpleBoolParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimpleBoolParameterSet_Value() {
		return (EAttribute)simpleBoolParameterSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimpleStringParameterSet() {
		return simpleStringParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimpleStringParameterSet_Value() {
		return (EAttribute)simpleStringParameterSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComplexEvent() {
		return complexEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexEvent_Parameters() {
		return (EReference)complexEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexEvent_Policy() {
		return (EReference)complexEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComplexEvent_Functions() {
		return (EReference)complexEventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariableFunctionCall() {
		return variableFunctionCallEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableFunctionCall_Function() {
		return (EReference)variableFunctionCallEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariableFunctionCall_Parameters() {
		return (EReference)variableFunctionCallEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoubleParameterSet() {
		return doubleParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDoubleParameterSet_Minimum() {
		return (EAttribute)doubleParameterSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDoubleParameterSet_Maximum() {
		return (EAttribute)doubleParameterSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDoubleParameterSet_Step() {
		return (EAttribute)doubleParameterSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntParameterSet() {
		return intParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntParameterSet_Minimum() {
		return (EAttribute)intParameterSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntParameterSet_Maximum() {
		return (EAttribute)intParameterSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntParameterSet_Step() {
		return (EAttribute)intParameterSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBoolParameterSet() {
		return boolParameterSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExplorationPolicy() {
		return explorationPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExplorationPolicy_ConstraintFunctions() {
		return (EReference)explorationPolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExplorationPolicy_ObjectiveFunctions() {
		return (EReference)explorationPolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExhaustive() {
		return exhaustiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRandom() {
		return randomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRandom_Max() {
		return (EAttribute)randomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOneByOne() {
		return oneByOneEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDifferential() {
		return differentialEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUserPolicy() {
		return userPolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogObservation() {
		return logObservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogObservation_Freq() {
		return (EAttribute)logObservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogObservation_Columns() {
		return (EReference)logObservationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogObservation_Type() {
		return (EAttribute)logObservationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLogObservation_TimeStamp() {
		return (EAttribute)logObservationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLogColumn() {
		return logColumnEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLogColumn_Data() {
		return (EReference)logColumnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXOR() {
		return xorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXOR_RequiredInterfacePort() {
		return (EReference)xorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXOR_ProvidedInterfacePort() {
		return (EReference)xorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTypeFile() {
		return typeFileEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTimeStamp() {
		return timeStampEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScnFactory getScnFactory() {
		return (ScnFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		simulationEClass = createEClass(SIMULATION);
		createEReference(simulationEClass, SIMULATION__CARES_SYSTEM);
		createEAttribute(simulationEClass, SIMULATION__TIME_UNIT);
		createEReference(simulationEClass, SIMULATION__IMPORTANT_DATES);
		createEReference(simulationEClass, SIMULATION__BEGIN);
		createEReference(simulationEClass, SIMULATION__SCENARIOS);
		createEReference(simulationEClass, SIMULATION__END);

		timeEClass = createEClass(TIME);
		createEAttribute(timeEClass, TIME__STARTING_TIME);
		createEAttribute(timeEClass, TIME__END_TIME);
		createEAttribute(timeEClass, TIME__STEP_TIME);

		scenarioEClass = createEClass(SCENARIO);
		createEReference(scenarioEClass, SCENARIO__BEGIN);
		createEReference(scenarioEClass, SCENARIO__EVENTS);
		createEReference(scenarioEClass, SCENARIO__END);
		createEAttribute(scenarioEClass, SCENARIO__NUMBER);
		createEReference(scenarioEClass, SCENARIO__LOGS);

		eventEClass = createEClass(EVENT);
		createEAttribute(eventEClass, EVENT__TIME);

		basicEventEClass = createEClass(BASIC_EVENT);
		createEReference(basicEventEClass, BASIC_EVENT__ACTIONS);

		actionEClass = createEClass(ACTION);

		componentInitializationEClass = createEClass(COMPONENT_INITIALIZATION);
		createEReference(componentInitializationEClass, COMPONENT_INITIALIZATION__COMPONENT);

		componentStartEClass = createEClass(COMPONENT_START);
		createEReference(componentStartEClass, COMPONENT_START__COMPONENT);

		componentStopEClass = createEClass(COMPONENT_STOP);
		createEReference(componentStopEClass, COMPONENT_STOP__COMPONENT);

		serviceCallEClass = createEClass(SERVICE_CALL);
		createEReference(serviceCallEClass, SERVICE_CALL__FUNCTION_CALL);
		createEReference(serviceCallEClass, SERVICE_CALL__COMPONENT);

		parameterChangeEClass = createEClass(PARAMETER_CHANGE);
		createEReference(parameterChangeEClass, PARAMETER_CHANGE__PARAMETER);

		simpleDoubleParameterSetEClass = createEClass(SIMPLE_DOUBLE_PARAMETER_SET);
		createEAttribute(simpleDoubleParameterSetEClass, SIMPLE_DOUBLE_PARAMETER_SET__VALUE);

		simpleIntParameterSetEClass = createEClass(SIMPLE_INT_PARAMETER_SET);
		createEAttribute(simpleIntParameterSetEClass, SIMPLE_INT_PARAMETER_SET__VALUE);

		simpleBoolParameterSetEClass = createEClass(SIMPLE_BOOL_PARAMETER_SET);
		createEAttribute(simpleBoolParameterSetEClass, SIMPLE_BOOL_PARAMETER_SET__VALUE);

		simpleStringParameterSetEClass = createEClass(SIMPLE_STRING_PARAMETER_SET);
		createEAttribute(simpleStringParameterSetEClass, SIMPLE_STRING_PARAMETER_SET__VALUE);

		complexEventEClass = createEClass(COMPLEX_EVENT);
		createEReference(complexEventEClass, COMPLEX_EVENT__PARAMETERS);
		createEReference(complexEventEClass, COMPLEX_EVENT__POLICY);
		createEReference(complexEventEClass, COMPLEX_EVENT__FUNCTIONS);

		variableFunctionCallEClass = createEClass(VARIABLE_FUNCTION_CALL);
		createEReference(variableFunctionCallEClass, VARIABLE_FUNCTION_CALL__FUNCTION);
		createEReference(variableFunctionCallEClass, VARIABLE_FUNCTION_CALL__PARAMETERS);

		doubleParameterSetEClass = createEClass(DOUBLE_PARAMETER_SET);
		createEAttribute(doubleParameterSetEClass, DOUBLE_PARAMETER_SET__MINIMUM);
		createEAttribute(doubleParameterSetEClass, DOUBLE_PARAMETER_SET__MAXIMUM);
		createEAttribute(doubleParameterSetEClass, DOUBLE_PARAMETER_SET__STEP);

		intParameterSetEClass = createEClass(INT_PARAMETER_SET);
		createEAttribute(intParameterSetEClass, INT_PARAMETER_SET__MINIMUM);
		createEAttribute(intParameterSetEClass, INT_PARAMETER_SET__MAXIMUM);
		createEAttribute(intParameterSetEClass, INT_PARAMETER_SET__STEP);

		boolParameterSetEClass = createEClass(BOOL_PARAMETER_SET);

		explorationPolicyEClass = createEClass(EXPLORATION_POLICY);
		createEReference(explorationPolicyEClass, EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS);
		createEReference(explorationPolicyEClass, EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS);

		exhaustiveEClass = createEClass(EXHAUSTIVE);

		randomEClass = createEClass(RANDOM);
		createEAttribute(randomEClass, RANDOM__MAX);

		oneByOneEClass = createEClass(ONE_BY_ONE);

		differentialEClass = createEClass(DIFFERENTIAL);

		userPolicyEClass = createEClass(USER_POLICY);

		logObservationEClass = createEClass(LOG_OBSERVATION);
		createEAttribute(logObservationEClass, LOG_OBSERVATION__FREQ);
		createEReference(logObservationEClass, LOG_OBSERVATION__COLUMNS);
		createEAttribute(logObservationEClass, LOG_OBSERVATION__TYPE);
		createEAttribute(logObservationEClass, LOG_OBSERVATION__TIME_STAMP);

		logColumnEClass = createEClass(LOG_COLUMN);
		createEReference(logColumnEClass, LOG_COLUMN__DATA);

		xorEClass = createEClass(XOR);
		createEReference(xorEClass, XOR__REQUIRED_INTERFACE_PORT);
		createEReference(xorEClass, XOR__PROVIDED_INTERFACE_PORT);

		// Create enums
		typeFileEEnum = createEEnum(TYPE_FILE);
		timeStampEEnum = createEEnum(TIME_STAMP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		DclPackage theDclPackage = (DclPackage)EPackage.Registry.INSTANCE.getEPackage(DclPackage.eNS_URI);
		SysPackage theSysPackage = (SysPackage)EPackage.Registry.INSTANCE.getEPackage(SysPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		simulationEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		scenarioEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		basicEventEClass.getESuperTypes().add(this.getEvent());
		componentInitializationEClass.getESuperTypes().add(this.getAction());
		componentStartEClass.getESuperTypes().add(this.getAction());
		componentStopEClass.getESuperTypes().add(this.getAction());
		serviceCallEClass.getESuperTypes().add(this.getAction());
		parameterChangeEClass.getESuperTypes().add(this.getAction());
		simpleDoubleParameterSetEClass.getESuperTypes().add(this.getParameterChange());
		simpleIntParameterSetEClass.getESuperTypes().add(this.getParameterChange());
		simpleBoolParameterSetEClass.getESuperTypes().add(this.getParameterChange());
		simpleStringParameterSetEClass.getESuperTypes().add(this.getParameterChange());
		exhaustiveEClass.getESuperTypes().add(this.getExplorationPolicy());
		randomEClass.getESuperTypes().add(this.getExplorationPolicy());
		oneByOneEClass.getESuperTypes().add(this.getExplorationPolicy());
		differentialEClass.getESuperTypes().add(this.getExplorationPolicy());
		userPolicyEClass.getESuperTypes().add(this.getExplorationPolicy());
		userPolicyEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		logObservationEClass.getESuperTypes().add(theDclPackage.getNamedElement());
		xorEClass.getESuperTypes().add(this.getAction());

		// Initialize classes, features, and operations; add parameters
		initEClass(simulationEClass, Simulation.class, "Simulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimulation_CaresSystem(), theSysPackage.getCaresSystem(), null, "caresSystem", null, 0, 1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_TimeUnit(), theDclPackage.getTimeUnits(), "timeUnit", null, 1, 1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_ImportantDates(), this.getTime(), null, "importantDates", null, 1, 1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Begin(), this.getAction(), null, "begin", null, 0, -1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Scenarios(), this.getScenario(), null, "scenarios", null, 0, -1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_End(), this.getServiceCall(), null, "end", null, 0, -1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timeEClass, Time.class, "Time", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTime_StartingTime(), ecorePackage.getELong(), "startingTime", "0", 1, 1, Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTime_EndTime(), ecorePackage.getELong(), "endTime", "100", 1, 1, Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTime_StepTime(), ecorePackage.getELong(), "stepTime", "0", 1, 1, Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scenarioEClass, Scenario.class, "Scenario", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScenario_Begin(), this.getAction(), null, "begin", null, 0, -1, Scenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenario_Events(), this.getEvent(), null, "events", null, 0, -1, Scenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenario_End(), this.getServiceCall(), null, "end", null, 0, -1, Scenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScenario_Number(), ecorePackage.getEInt(), "number", "1", 1, 1, Scenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScenario_Logs(), this.getLogObservation(), null, "logs", null, 0, -1, Scenario.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEvent_Time(), ecorePackage.getEInt(), "time", null, 1, 1, Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(basicEventEClass, BasicEvent.class, "BasicEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBasicEvent_Actions(), this.getAction(), null, "actions", null, 0, -1, BasicEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionEClass, Action.class, "Action", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentInitializationEClass, ComponentInitialization.class, "ComponentInitialization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInitialization_Component(), theSysPackage.getComponent(), null, "component", null, 1, 1, ComponentInitialization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentStartEClass, ComponentStart.class, "ComponentStart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentStart_Component(), theSysPackage.getLeafComponent(), null, "component", null, 1, 1, ComponentStart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentStopEClass, ComponentStop.class, "ComponentStop", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentStop_Component(), theSysPackage.getLeafComponent(), null, "component", null, 1, 1, ComponentStop.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serviceCallEClass, ServiceCall.class, "ServiceCall", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getServiceCall_FunctionCall(), theDclPackage.getFunctionCall(), null, "functionCall", null, 1, 1, ServiceCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServiceCall_Component(), theSysPackage.getLeafComponent(), null, "component", null, 1, 1, ServiceCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(parameterChangeEClass, ParameterChange.class, "ParameterChange", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getParameterChange_Parameter(), theDclPackage.getParameterInstanciation(), null, "parameter", null, 1, 1, ParameterChange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simpleDoubleParameterSetEClass, SimpleDoubleParameterSet.class, "SimpleDoubleParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimpleDoubleParameterSet_Value(), ecorePackage.getEDouble(), "value", null, 1, 1, SimpleDoubleParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simpleIntParameterSetEClass, SimpleIntParameterSet.class, "SimpleIntParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimpleIntParameterSet_Value(), ecorePackage.getEInt(), "value", null, 1, 1, SimpleIntParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simpleBoolParameterSetEClass, SimpleBoolParameterSet.class, "SimpleBoolParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimpleBoolParameterSet_Value(), ecorePackage.getEBoolean(), "value", null, 1, 1, SimpleBoolParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simpleStringParameterSetEClass, SimpleStringParameterSet.class, "SimpleStringParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimpleStringParameterSet_Value(), theEcorePackage.getEString(), "value", null, 1, 1, SimpleStringParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(complexEventEClass, ComplexEvent.class, "ComplexEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComplexEvent_Parameters(), this.getParameterChange(), null, "parameters", null, 1, -1, ComplexEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexEvent_Policy(), this.getExplorationPolicy(), null, "policy", null, 1, 1, ComplexEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComplexEvent_Functions(), this.getVariableFunctionCall(), null, "functions", null, 0, -1, ComplexEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variableFunctionCallEClass, VariableFunctionCall.class, "VariableFunctionCall", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariableFunctionCall_Function(), theDclPackage.getFunction(), null, "function", null, 1, 1, VariableFunctionCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariableFunctionCall_Parameters(), this.getParameterChange(), null, "parameters", null, 0, -1, VariableFunctionCall.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(doubleParameterSetEClass, DoubleParameterSet.class, "DoubleParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDoubleParameterSet_Minimum(), ecorePackage.getEDouble(), "minimum", null, 1, 1, DoubleParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDoubleParameterSet_Maximum(), ecorePackage.getEDouble(), "maximum", null, 1, 1, DoubleParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDoubleParameterSet_Step(), theEcorePackage.getEDouble(), "step", null, 1, 1, DoubleParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(intParameterSetEClass, IntParameterSet.class, "IntParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntParameterSet_Minimum(), ecorePackage.getEInt(), "minimum", null, 1, 1, IntParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntParameterSet_Maximum(), ecorePackage.getEInt(), "maximum", null, 1, 1, IntParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIntParameterSet_Step(), ecorePackage.getEInt(), "step", null, 1, 1, IntParameterSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(boolParameterSetEClass, BoolParameterSet.class, "BoolParameterSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(explorationPolicyEClass, ExplorationPolicy.class, "ExplorationPolicy", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExplorationPolicy_ConstraintFunctions(), theDclPackage.getFunction(), null, "constraintFunctions", null, 0, -1, ExplorationPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExplorationPolicy_ObjectiveFunctions(), theDclPackage.getFunction(), null, "objectiveFunctions", null, 0, -1, ExplorationPolicy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exhaustiveEClass, Exhaustive.class, "Exhaustive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(randomEClass, Random.class, "Random", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRandom_Max(), theEcorePackage.getEInt(), "max", null, 1, 1, Random.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(oneByOneEClass, OneByOne.class, "OneByOne", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(differentialEClass, Differential.class, "Differential", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(userPolicyEClass, UserPolicy.class, "UserPolicy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(logObservationEClass, LogObservation.class, "LogObservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLogObservation_Freq(), ecorePackage.getEDouble(), "freq", "1.0", 1, 1, LogObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLogObservation_Columns(), this.getLogColumn(), null, "columns", null, 0, -1, LogObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLogObservation_Type(), this.getTypeFile(), "type", "csv", 1, 1, LogObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLogObservation_TimeStamp(), this.getTimeStamp(), "timeStamp", "timed", 1, 1, LogObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(logColumnEClass, LogColumn.class, "LogColumn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLogColumn_Data(), theSysPackage.getLeafOutputPort(), null, "data", null, 1, 1, LogColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xorEClass, fr.ubo.cares.scn.XOR.class, "XOR", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXOR_RequiredInterfacePort(), theSysPackage.getRequiredInterfacePort(), null, "requiredInterfacePort", null, 1, 1, fr.ubo.cares.scn.XOR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXOR_ProvidedInterfacePort(), theSysPackage.getProvidedInterfacePort(), null, "providedInterfacePort", null, 1, 1, fr.ubo.cares.scn.XOR.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(typeFileEEnum, TypeFile.class, "TypeFile");
		addEEnumLiteral(typeFileEEnum, TypeFile.CSV);
		addEEnumLiteral(typeFileEEnum, TypeFile.JSON);

		initEEnum(timeStampEEnum, TimeStamp.class, "TimeStamp");
		addEEnumLiteral(timeStampEEnum, TimeStamp.TIMED);
		addEEnumLiteral(timeStampEEnum, TimeStamp.UNTIMED);

		// Create resource
		createResource(eNS_URI);
	}

} //ScnPackageImpl
