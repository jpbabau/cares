/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.ComplexEvent;
import fr.ubo.cares.scn.ExplorationPolicy;
import fr.ubo.cares.scn.ParameterChange;
import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.VariableFunctionCall;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Complex Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.ComplexEventImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ComplexEventImpl#getPolicy <em>Policy</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ComplexEventImpl#getFunctions <em>Functions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComplexEventImpl extends MinimalEObjectImpl.Container implements ComplexEvent {
	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterChange> parameters;

	/**
	 * The cached value of the '{@link #getPolicy() <em>Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolicy()
	 * @generated
	 * @ordered
	 */
	protected ExplorationPolicy policy;

	/**
	 * The cached value of the '{@link #getFunctions() <em>Functions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<VariableFunctionCall> functions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComplexEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.COMPLEX_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterChange> getParameters() {
		if (parameters == null) {
			parameters = new EObjectResolvingEList<ParameterChange>(ParameterChange.class, this, ScnPackage.COMPLEX_EVENT__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExplorationPolicy getPolicy() {
		return policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPolicy(ExplorationPolicy newPolicy, NotificationChain msgs) {
		ExplorationPolicy oldPolicy = policy;
		policy = newPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScnPackage.COMPLEX_EVENT__POLICY, oldPolicy, newPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolicy(ExplorationPolicy newPolicy) {
		if (newPolicy != policy) {
			NotificationChain msgs = null;
			if (policy != null)
				msgs = ((InternalEObject)policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScnPackage.COMPLEX_EVENT__POLICY, null, msgs);
			if (newPolicy != null)
				msgs = ((InternalEObject)newPolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScnPackage.COMPLEX_EVENT__POLICY, null, msgs);
			msgs = basicSetPolicy(newPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.COMPLEX_EVENT__POLICY, newPolicy, newPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariableFunctionCall> getFunctions() {
		if (functions == null) {
			functions = new EObjectResolvingEList<VariableFunctionCall>(VariableFunctionCall.class, this, ScnPackage.COMPLEX_EVENT__FUNCTIONS);
		}
		return functions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScnPackage.COMPLEX_EVENT__POLICY:
				return basicSetPolicy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.COMPLEX_EVENT__PARAMETERS:
				return getParameters();
			case ScnPackage.COMPLEX_EVENT__POLICY:
				return getPolicy();
			case ScnPackage.COMPLEX_EVENT__FUNCTIONS:
				return getFunctions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.COMPLEX_EVENT__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ParameterChange>)newValue);
				return;
			case ScnPackage.COMPLEX_EVENT__POLICY:
				setPolicy((ExplorationPolicy)newValue);
				return;
			case ScnPackage.COMPLEX_EVENT__FUNCTIONS:
				getFunctions().clear();
				getFunctions().addAll((Collection<? extends VariableFunctionCall>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.COMPLEX_EVENT__PARAMETERS:
				getParameters().clear();
				return;
			case ScnPackage.COMPLEX_EVENT__POLICY:
				setPolicy((ExplorationPolicy)null);
				return;
			case ScnPackage.COMPLEX_EVENT__FUNCTIONS:
				getFunctions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.COMPLEX_EVENT__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case ScnPackage.COMPLEX_EVENT__POLICY:
				return policy != null;
			case ScnPackage.COMPLEX_EVENT__FUNCTIONS:
				return functions != null && !functions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ComplexEventImpl
