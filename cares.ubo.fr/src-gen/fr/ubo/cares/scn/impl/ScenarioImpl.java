/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.dcl.impl.NamedElementImpl;

import fr.ubo.cares.scn.Action;
import fr.ubo.cares.scn.Event;
import fr.ubo.cares.scn.LogObservation;
import fr.ubo.cares.scn.Scenario;
import fr.ubo.cares.scn.ScnPackage;

import fr.ubo.cares.scn.ServiceCall;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scenario</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.ScenarioImpl#getBegin <em>Begin</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ScenarioImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ScenarioImpl#getEnd <em>End</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ScenarioImpl#getNumber <em>Number</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ScenarioImpl#getLogs <em>Logs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScenarioImpl extends NamedElementImpl implements Scenario {
	/**
	 * The cached value of the '{@link #getBegin() <em>Begin</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBegin()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> begin;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceCall> end;

	/**
	 * The default value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getNumber() <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber()
	 * @generated
	 * @ordered
	 */
	protected int number = NUMBER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLogs() <em>Logs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogs()
	 * @generated
	 * @ordered
	 */
	protected EList<LogObservation> logs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScenarioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.SCENARIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getBegin() {
		if (begin == null) {
			begin = new EObjectContainmentEList<Action>(Action.class, this, ScnPackage.SCENARIO__BEGIN);
		}
		return begin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<Event>(Event.class, this, ScnPackage.SCENARIO__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceCall> getEnd() {
		if (end == null) {
			end = new EObjectContainmentEList<ServiceCall>(ServiceCall.class, this, ScnPackage.SCENARIO__END);
		}
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber(int newNumber) {
		int oldNumber = number;
		number = newNumber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.SCENARIO__NUMBER, oldNumber, number));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LogObservation> getLogs() {
		if (logs == null) {
			logs = new EObjectContainmentEList<LogObservation>(LogObservation.class, this, ScnPackage.SCENARIO__LOGS);
		}
		return logs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScnPackage.SCENARIO__BEGIN:
				return ((InternalEList<?>)getBegin()).basicRemove(otherEnd, msgs);
			case ScnPackage.SCENARIO__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
			case ScnPackage.SCENARIO__END:
				return ((InternalEList<?>)getEnd()).basicRemove(otherEnd, msgs);
			case ScnPackage.SCENARIO__LOGS:
				return ((InternalEList<?>)getLogs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.SCENARIO__BEGIN:
				return getBegin();
			case ScnPackage.SCENARIO__EVENTS:
				return getEvents();
			case ScnPackage.SCENARIO__END:
				return getEnd();
			case ScnPackage.SCENARIO__NUMBER:
				return getNumber();
			case ScnPackage.SCENARIO__LOGS:
				return getLogs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.SCENARIO__BEGIN:
				getBegin().clear();
				getBegin().addAll((Collection<? extends Action>)newValue);
				return;
			case ScnPackage.SCENARIO__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case ScnPackage.SCENARIO__END:
				getEnd().clear();
				getEnd().addAll((Collection<? extends ServiceCall>)newValue);
				return;
			case ScnPackage.SCENARIO__NUMBER:
				setNumber((Integer)newValue);
				return;
			case ScnPackage.SCENARIO__LOGS:
				getLogs().clear();
				getLogs().addAll((Collection<? extends LogObservation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.SCENARIO__BEGIN:
				getBegin().clear();
				return;
			case ScnPackage.SCENARIO__EVENTS:
				getEvents().clear();
				return;
			case ScnPackage.SCENARIO__END:
				getEnd().clear();
				return;
			case ScnPackage.SCENARIO__NUMBER:
				setNumber(NUMBER_EDEFAULT);
				return;
			case ScnPackage.SCENARIO__LOGS:
				getLogs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.SCENARIO__BEGIN:
				return begin != null && !begin.isEmpty();
			case ScnPackage.SCENARIO__EVENTS:
				return events != null && !events.isEmpty();
			case ScnPackage.SCENARIO__END:
				return end != null && !end.isEmpty();
			case ScnPackage.SCENARIO__NUMBER:
				return number != NUMBER_EDEFAULT;
			case ScnPackage.SCENARIO__LOGS:
				return logs != null && !logs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (number: ");
		result.append(number);
		result.append(')');
		return result.toString();
	}

} //ScenarioImpl
