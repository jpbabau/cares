/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.dcl.FunctionCall;

import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.ServiceCall;

import fr.ubo.cares.sys.LeafComponent;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.ServiceCallImpl#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ServiceCallImpl#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServiceCallImpl extends ActionImpl implements ServiceCall {
	/**
	 * The cached value of the '{@link #getFunctionCall() <em>Function Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctionCall()
	 * @generated
	 * @ordered
	 */
	protected FunctionCall functionCall;

	/**
	 * The cached value of the '{@link #getComponent() <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponent()
	 * @generated
	 * @ordered
	 */
	protected LeafComponent component;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceCallImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.SERVICE_CALL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionCall getFunctionCall() {
		return functionCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFunctionCall(FunctionCall newFunctionCall, NotificationChain msgs) {
		FunctionCall oldFunctionCall = functionCall;
		functionCall = newFunctionCall;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScnPackage.SERVICE_CALL__FUNCTION_CALL, oldFunctionCall, newFunctionCall);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionCall(FunctionCall newFunctionCall) {
		if (newFunctionCall != functionCall) {
			NotificationChain msgs = null;
			if (functionCall != null)
				msgs = ((InternalEObject)functionCall).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScnPackage.SERVICE_CALL__FUNCTION_CALL, null, msgs);
			if (newFunctionCall != null)
				msgs = ((InternalEObject)newFunctionCall).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScnPackage.SERVICE_CALL__FUNCTION_CALL, null, msgs);
			msgs = basicSetFunctionCall(newFunctionCall, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.SERVICE_CALL__FUNCTION_CALL, newFunctionCall, newFunctionCall));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponent getComponent() {
		if (component != null && component.eIsProxy()) {
			InternalEObject oldComponent = (InternalEObject)component;
			component = (LeafComponent)eResolveProxy(oldComponent);
			if (component != oldComponent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScnPackage.SERVICE_CALL__COMPONENT, oldComponent, component));
			}
		}
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponent basicGetComponent() {
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(LeafComponent newComponent) {
		LeafComponent oldComponent = component;
		component = newComponent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.SERVICE_CALL__COMPONENT, oldComponent, component));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScnPackage.SERVICE_CALL__FUNCTION_CALL:
				return basicSetFunctionCall(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.SERVICE_CALL__FUNCTION_CALL:
				return getFunctionCall();
			case ScnPackage.SERVICE_CALL__COMPONENT:
				if (resolve) return getComponent();
				return basicGetComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.SERVICE_CALL__FUNCTION_CALL:
				setFunctionCall((FunctionCall)newValue);
				return;
			case ScnPackage.SERVICE_CALL__COMPONENT:
				setComponent((LeafComponent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.SERVICE_CALL__FUNCTION_CALL:
				setFunctionCall((FunctionCall)null);
				return;
			case ScnPackage.SERVICE_CALL__COMPONENT:
				setComponent((LeafComponent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.SERVICE_CALL__FUNCTION_CALL:
				return functionCall != null;
			case ScnPackage.SERVICE_CALL__COMPONENT:
				return component != null;
		}
		return super.eIsSet(featureID);
	}

} //ServiceCallImpl
