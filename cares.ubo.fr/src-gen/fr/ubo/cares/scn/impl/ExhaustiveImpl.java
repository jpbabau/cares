/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.Exhaustive;
import fr.ubo.cares.scn.ScnPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exhaustive</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExhaustiveImpl extends ExplorationPolicyImpl implements Exhaustive {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExhaustiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.EXHAUSTIVE;
	}

} //ExhaustiveImpl
