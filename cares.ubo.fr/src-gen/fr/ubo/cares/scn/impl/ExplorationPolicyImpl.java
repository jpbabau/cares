/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.dcl.Function;

import fr.ubo.cares.scn.ExplorationPolicy;
import fr.ubo.cares.scn.ScnPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exploration Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.ExplorationPolicyImpl#getConstraintFunctions <em>Constraint Functions</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.ExplorationPolicyImpl#getObjectiveFunctions <em>Objective Functions</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ExplorationPolicyImpl extends MinimalEObjectImpl.Container implements ExplorationPolicy {
	/**
	 * The cached value of the '{@link #getConstraintFunctions() <em>Constraint Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Function> constraintFunctions;

	/**
	 * The cached value of the '{@link #getObjectiveFunctions() <em>Objective Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectiveFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Function> objectiveFunctions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExplorationPolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.EXPLORATION_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function> getConstraintFunctions() {
		if (constraintFunctions == null) {
			constraintFunctions = new EObjectContainmentEList<Function>(Function.class, this, ScnPackage.EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS);
		}
		return constraintFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Function> getObjectiveFunctions() {
		if (objectiveFunctions == null) {
			objectiveFunctions = new EObjectContainmentEList<Function>(Function.class, this, ScnPackage.EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS);
		}
		return objectiveFunctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScnPackage.EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS:
				return ((InternalEList<?>)getConstraintFunctions()).basicRemove(otherEnd, msgs);
			case ScnPackage.EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS:
				return ((InternalEList<?>)getObjectiveFunctions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS:
				return getConstraintFunctions();
			case ScnPackage.EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS:
				return getObjectiveFunctions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS:
				getConstraintFunctions().clear();
				getConstraintFunctions().addAll((Collection<? extends Function>)newValue);
				return;
			case ScnPackage.EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS:
				getObjectiveFunctions().clear();
				getObjectiveFunctions().addAll((Collection<? extends Function>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS:
				getConstraintFunctions().clear();
				return;
			case ScnPackage.EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS:
				getObjectiveFunctions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS:
				return constraintFunctions != null && !constraintFunctions.isEmpty();
			case ScnPackage.EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS:
				return objectiveFunctions != null && !objectiveFunctions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ExplorationPolicyImpl
