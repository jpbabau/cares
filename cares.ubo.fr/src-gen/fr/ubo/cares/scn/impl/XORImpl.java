/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.XOR;

import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.RequiredInterfacePort;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XOR</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.XORImpl#getRequiredInterfacePort <em>Required Interface Port</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.XORImpl#getProvidedInterfacePort <em>Provided Interface Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class XORImpl extends ActionImpl implements XOR {
	/**
	 * The cached value of the '{@link #getRequiredInterfacePort() <em>Required Interface Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredInterfacePort()
	 * @generated
	 * @ordered
	 */
	protected RequiredInterfacePort requiredInterfacePort;

	/**
	 * The cached value of the '{@link #getProvidedInterfacePort() <em>Provided Interface Port</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvidedInterfacePort()
	 * @generated
	 * @ordered
	 */
	protected ProvidedInterfacePort providedInterfacePort;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XORImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.XOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredInterfacePort getRequiredInterfacePort() {
		if (requiredInterfacePort != null && requiredInterfacePort.eIsProxy()) {
			InternalEObject oldRequiredInterfacePort = (InternalEObject)requiredInterfacePort;
			requiredInterfacePort = (RequiredInterfacePort)eResolveProxy(oldRequiredInterfacePort);
			if (requiredInterfacePort != oldRequiredInterfacePort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScnPackage.XOR__REQUIRED_INTERFACE_PORT, oldRequiredInterfacePort, requiredInterfacePort));
			}
		}
		return requiredInterfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredInterfacePort basicGetRequiredInterfacePort() {
		return requiredInterfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredInterfacePort(RequiredInterfacePort newRequiredInterfacePort) {
		RequiredInterfacePort oldRequiredInterfacePort = requiredInterfacePort;
		requiredInterfacePort = newRequiredInterfacePort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.XOR__REQUIRED_INTERFACE_PORT, oldRequiredInterfacePort, requiredInterfacePort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedInterfacePort getProvidedInterfacePort() {
		if (providedInterfacePort != null && providedInterfacePort.eIsProxy()) {
			InternalEObject oldProvidedInterfacePort = (InternalEObject)providedInterfacePort;
			providedInterfacePort = (ProvidedInterfacePort)eResolveProxy(oldProvidedInterfacePort);
			if (providedInterfacePort != oldProvidedInterfacePort) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScnPackage.XOR__PROVIDED_INTERFACE_PORT, oldProvidedInterfacePort, providedInterfacePort));
			}
		}
		return providedInterfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedInterfacePort basicGetProvidedInterfacePort() {
		return providedInterfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedInterfacePort(ProvidedInterfacePort newProvidedInterfacePort) {
		ProvidedInterfacePort oldProvidedInterfacePort = providedInterfacePort;
		providedInterfacePort = newProvidedInterfacePort;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.XOR__PROVIDED_INTERFACE_PORT, oldProvidedInterfacePort, providedInterfacePort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.XOR__REQUIRED_INTERFACE_PORT:
				if (resolve) return getRequiredInterfacePort();
				return basicGetRequiredInterfacePort();
			case ScnPackage.XOR__PROVIDED_INTERFACE_PORT:
				if (resolve) return getProvidedInterfacePort();
				return basicGetProvidedInterfacePort();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.XOR__REQUIRED_INTERFACE_PORT:
				setRequiredInterfacePort((RequiredInterfacePort)newValue);
				return;
			case ScnPackage.XOR__PROVIDED_INTERFACE_PORT:
				setProvidedInterfacePort((ProvidedInterfacePort)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.XOR__REQUIRED_INTERFACE_PORT:
				setRequiredInterfacePort((RequiredInterfacePort)null);
				return;
			case ScnPackage.XOR__PROVIDED_INTERFACE_PORT:
				setProvidedInterfacePort((ProvidedInterfacePort)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.XOR__REQUIRED_INTERFACE_PORT:
				return requiredInterfacePort != null;
			case ScnPackage.XOR__PROVIDED_INTERFACE_PORT:
				return providedInterfacePort != null;
		}
		return super.eIsSet(featureID);
	}

} //XORImpl
