/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.scn.Differential;
import fr.ubo.cares.scn.ScnPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Differential</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DifferentialImpl extends ExplorationPolicyImpl implements Differential {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DifferentialImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.DIFFERENTIAL;
	}

} //DifferentialImpl
