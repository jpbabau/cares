/**
 */
package fr.ubo.cares.scn.impl;

import fr.ubo.cares.dcl.TimeUnits;

import fr.ubo.cares.dcl.impl.NamedElementImpl;

import fr.ubo.cares.scn.Action;
import fr.ubo.cares.scn.Scenario;
import fr.ubo.cares.scn.ScnPackage;
import fr.ubo.cares.scn.ServiceCall;
import fr.ubo.cares.scn.Simulation;
import fr.ubo.cares.scn.Time;

import fr.ubo.cares.sys.CaresSystem;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.impl.SimulationImpl#getCaresSystem <em>Cares System</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.SimulationImpl#getTimeUnit <em>Time Unit</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.SimulationImpl#getImportantDates <em>Important Dates</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.SimulationImpl#getBegin <em>Begin</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.SimulationImpl#getScenarios <em>Scenarios</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.impl.SimulationImpl#getEnd <em>End</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimulationImpl extends NamedElementImpl implements Simulation {
	/**
	 * The cached value of the '{@link #getCaresSystem() <em>Cares System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCaresSystem()
	 * @generated
	 * @ordered
	 */
	protected CaresSystem caresSystem;

	/**
	 * The default value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnits TIME_UNIT_EDEFAULT = TimeUnits.MMS;

	/**
	 * The cached value of the '{@link #getTimeUnit() <em>Time Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeUnit()
	 * @generated
	 * @ordered
	 */
	protected TimeUnits timeUnit = TIME_UNIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImportantDates() <em>Important Dates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportantDates()
	 * @generated
	 * @ordered
	 */
	protected Time importantDates;

	/**
	 * The cached value of the '{@link #getBegin() <em>Begin</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBegin()
	 * @generated
	 * @ordered
	 */
	protected EList<Action> begin;

	/**
	 * The cached value of the '{@link #getScenarios() <em>Scenarios</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenarios()
	 * @generated
	 * @ordered
	 */
	protected EList<Scenario> scenarios;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected EList<ServiceCall> end;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScnPackage.Literals.SIMULATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaresSystem getCaresSystem() {
		if (caresSystem != null && caresSystem.eIsProxy()) {
			InternalEObject oldCaresSystem = (InternalEObject)caresSystem;
			caresSystem = (CaresSystem)eResolveProxy(oldCaresSystem);
			if (caresSystem != oldCaresSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ScnPackage.SIMULATION__CARES_SYSTEM, oldCaresSystem, caresSystem));
			}
		}
		return caresSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CaresSystem basicGetCaresSystem() {
		return caresSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCaresSystem(CaresSystem newCaresSystem) {
		CaresSystem oldCaresSystem = caresSystem;
		caresSystem = newCaresSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.SIMULATION__CARES_SYSTEM, oldCaresSystem, caresSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnits getTimeUnit() {
		return timeUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeUnit(TimeUnits newTimeUnit) {
		TimeUnits oldTimeUnit = timeUnit;
		timeUnit = newTimeUnit == null ? TIME_UNIT_EDEFAULT : newTimeUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.SIMULATION__TIME_UNIT, oldTimeUnit, timeUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Time getImportantDates() {
		return importantDates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetImportantDates(Time newImportantDates, NotificationChain msgs) {
		Time oldImportantDates = importantDates;
		importantDates = newImportantDates;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ScnPackage.SIMULATION__IMPORTANT_DATES, oldImportantDates, newImportantDates);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImportantDates(Time newImportantDates) {
		if (newImportantDates != importantDates) {
			NotificationChain msgs = null;
			if (importantDates != null)
				msgs = ((InternalEObject)importantDates).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ScnPackage.SIMULATION__IMPORTANT_DATES, null, msgs);
			if (newImportantDates != null)
				msgs = ((InternalEObject)newImportantDates).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ScnPackage.SIMULATION__IMPORTANT_DATES, null, msgs);
			msgs = basicSetImportantDates(newImportantDates, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ScnPackage.SIMULATION__IMPORTANT_DATES, newImportantDates, newImportantDates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Action> getBegin() {
		if (begin == null) {
			begin = new EObjectContainmentEList<Action>(Action.class, this, ScnPackage.SIMULATION__BEGIN);
		}
		return begin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scenario> getScenarios() {
		if (scenarios == null) {
			scenarios = new EObjectContainmentEList<Scenario>(Scenario.class, this, ScnPackage.SIMULATION__SCENARIOS);
		}
		return scenarios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServiceCall> getEnd() {
		if (end == null) {
			end = new EObjectContainmentEList<ServiceCall>(ServiceCall.class, this, ScnPackage.SIMULATION__END);
		}
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ScnPackage.SIMULATION__IMPORTANT_DATES:
				return basicSetImportantDates(null, msgs);
			case ScnPackage.SIMULATION__BEGIN:
				return ((InternalEList<?>)getBegin()).basicRemove(otherEnd, msgs);
			case ScnPackage.SIMULATION__SCENARIOS:
				return ((InternalEList<?>)getScenarios()).basicRemove(otherEnd, msgs);
			case ScnPackage.SIMULATION__END:
				return ((InternalEList<?>)getEnd()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ScnPackage.SIMULATION__CARES_SYSTEM:
				if (resolve) return getCaresSystem();
				return basicGetCaresSystem();
			case ScnPackage.SIMULATION__TIME_UNIT:
				return getTimeUnit();
			case ScnPackage.SIMULATION__IMPORTANT_DATES:
				return getImportantDates();
			case ScnPackage.SIMULATION__BEGIN:
				return getBegin();
			case ScnPackage.SIMULATION__SCENARIOS:
				return getScenarios();
			case ScnPackage.SIMULATION__END:
				return getEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ScnPackage.SIMULATION__CARES_SYSTEM:
				setCaresSystem((CaresSystem)newValue);
				return;
			case ScnPackage.SIMULATION__TIME_UNIT:
				setTimeUnit((TimeUnits)newValue);
				return;
			case ScnPackage.SIMULATION__IMPORTANT_DATES:
				setImportantDates((Time)newValue);
				return;
			case ScnPackage.SIMULATION__BEGIN:
				getBegin().clear();
				getBegin().addAll((Collection<? extends Action>)newValue);
				return;
			case ScnPackage.SIMULATION__SCENARIOS:
				getScenarios().clear();
				getScenarios().addAll((Collection<? extends Scenario>)newValue);
				return;
			case ScnPackage.SIMULATION__END:
				getEnd().clear();
				getEnd().addAll((Collection<? extends ServiceCall>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ScnPackage.SIMULATION__CARES_SYSTEM:
				setCaresSystem((CaresSystem)null);
				return;
			case ScnPackage.SIMULATION__TIME_UNIT:
				setTimeUnit(TIME_UNIT_EDEFAULT);
				return;
			case ScnPackage.SIMULATION__IMPORTANT_DATES:
				setImportantDates((Time)null);
				return;
			case ScnPackage.SIMULATION__BEGIN:
				getBegin().clear();
				return;
			case ScnPackage.SIMULATION__SCENARIOS:
				getScenarios().clear();
				return;
			case ScnPackage.SIMULATION__END:
				getEnd().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ScnPackage.SIMULATION__CARES_SYSTEM:
				return caresSystem != null;
			case ScnPackage.SIMULATION__TIME_UNIT:
				return timeUnit != TIME_UNIT_EDEFAULT;
			case ScnPackage.SIMULATION__IMPORTANT_DATES:
				return importantDates != null;
			case ScnPackage.SIMULATION__BEGIN:
				return begin != null && !begin.isEmpty();
			case ScnPackage.SIMULATION__SCENARIOS:
				return scenarios != null && !scenarios.isEmpty();
			case ScnPackage.SIMULATION__END:
				return end != null && !end.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (timeUnit: ");
		result.append(timeUnit);
		result.append(')');
		return result.toString();
	}

} //SimulationImpl
