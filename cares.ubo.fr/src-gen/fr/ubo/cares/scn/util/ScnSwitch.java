/**
 */
package fr.ubo.cares.scn.util;

import fr.ubo.cares.dcl.NamedElement;

import fr.ubo.cares.scn.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.scn.ScnPackage
 * @generated
 */
public class ScnSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScnPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScnSwitch() {
		if (modelPackage == null) {
			modelPackage = ScnPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ScnPackage.SIMULATION: {
				Simulation simulation = (Simulation)theEObject;
				T result = caseSimulation(simulation);
				if (result == null) result = caseNamedElement(simulation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.TIME: {
				Time time = (Time)theEObject;
				T result = caseTime(time);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.SCENARIO: {
				Scenario scenario = (Scenario)theEObject;
				T result = caseScenario(scenario);
				if (result == null) result = caseNamedElement(scenario);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.EVENT: {
				Event event = (Event)theEObject;
				T result = caseEvent(event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.BASIC_EVENT: {
				BasicEvent basicEvent = (BasicEvent)theEObject;
				T result = caseBasicEvent(basicEvent);
				if (result == null) result = caseEvent(basicEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.ACTION: {
				Action action = (Action)theEObject;
				T result = caseAction(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.COMPONENT_INITIALIZATION: {
				ComponentInitialization componentInitialization = (ComponentInitialization)theEObject;
				T result = caseComponentInitialization(componentInitialization);
				if (result == null) result = caseAction(componentInitialization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.COMPONENT_START: {
				ComponentStart componentStart = (ComponentStart)theEObject;
				T result = caseComponentStart(componentStart);
				if (result == null) result = caseAction(componentStart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.COMPONENT_STOP: {
				ComponentStop componentStop = (ComponentStop)theEObject;
				T result = caseComponentStop(componentStop);
				if (result == null) result = caseAction(componentStop);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.SERVICE_CALL: {
				ServiceCall serviceCall = (ServiceCall)theEObject;
				T result = caseServiceCall(serviceCall);
				if (result == null) result = caseAction(serviceCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.PARAMETER_CHANGE: {
				ParameterChange parameterChange = (ParameterChange)theEObject;
				T result = caseParameterChange(parameterChange);
				if (result == null) result = caseAction(parameterChange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.SIMPLE_DOUBLE_PARAMETER_SET: {
				SimpleDoubleParameterSet simpleDoubleParameterSet = (SimpleDoubleParameterSet)theEObject;
				T result = caseSimpleDoubleParameterSet(simpleDoubleParameterSet);
				if (result == null) result = caseParameterChange(simpleDoubleParameterSet);
				if (result == null) result = caseAction(simpleDoubleParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.SIMPLE_INT_PARAMETER_SET: {
				SimpleIntParameterSet simpleIntParameterSet = (SimpleIntParameterSet)theEObject;
				T result = caseSimpleIntParameterSet(simpleIntParameterSet);
				if (result == null) result = caseParameterChange(simpleIntParameterSet);
				if (result == null) result = caseAction(simpleIntParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.SIMPLE_BOOL_PARAMETER_SET: {
				SimpleBoolParameterSet simpleBoolParameterSet = (SimpleBoolParameterSet)theEObject;
				T result = caseSimpleBoolParameterSet(simpleBoolParameterSet);
				if (result == null) result = caseParameterChange(simpleBoolParameterSet);
				if (result == null) result = caseAction(simpleBoolParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.SIMPLE_STRING_PARAMETER_SET: {
				SimpleStringParameterSet simpleStringParameterSet = (SimpleStringParameterSet)theEObject;
				T result = caseSimpleStringParameterSet(simpleStringParameterSet);
				if (result == null) result = caseParameterChange(simpleStringParameterSet);
				if (result == null) result = caseAction(simpleStringParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.COMPLEX_EVENT: {
				ComplexEvent complexEvent = (ComplexEvent)theEObject;
				T result = caseComplexEvent(complexEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.VARIABLE_FUNCTION_CALL: {
				VariableFunctionCall variableFunctionCall = (VariableFunctionCall)theEObject;
				T result = caseVariableFunctionCall(variableFunctionCall);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.DOUBLE_PARAMETER_SET: {
				DoubleParameterSet doubleParameterSet = (DoubleParameterSet)theEObject;
				T result = caseDoubleParameterSet(doubleParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.INT_PARAMETER_SET: {
				IntParameterSet intParameterSet = (IntParameterSet)theEObject;
				T result = caseIntParameterSet(intParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.BOOL_PARAMETER_SET: {
				BoolParameterSet boolParameterSet = (BoolParameterSet)theEObject;
				T result = caseBoolParameterSet(boolParameterSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.EXPLORATION_POLICY: {
				ExplorationPolicy explorationPolicy = (ExplorationPolicy)theEObject;
				T result = caseExplorationPolicy(explorationPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.EXHAUSTIVE: {
				Exhaustive exhaustive = (Exhaustive)theEObject;
				T result = caseExhaustive(exhaustive);
				if (result == null) result = caseExplorationPolicy(exhaustive);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.RANDOM: {
				Random random = (Random)theEObject;
				T result = caseRandom(random);
				if (result == null) result = caseExplorationPolicy(random);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.ONE_BY_ONE: {
				OneByOne oneByOne = (OneByOne)theEObject;
				T result = caseOneByOne(oneByOne);
				if (result == null) result = caseExplorationPolicy(oneByOne);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.DIFFERENTIAL: {
				Differential differential = (Differential)theEObject;
				T result = caseDifferential(differential);
				if (result == null) result = caseExplorationPolicy(differential);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.USER_POLICY: {
				UserPolicy userPolicy = (UserPolicy)theEObject;
				T result = caseUserPolicy(userPolicy);
				if (result == null) result = caseExplorationPolicy(userPolicy);
				if (result == null) result = caseNamedElement(userPolicy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.LOG_OBSERVATION: {
				LogObservation logObservation = (LogObservation)theEObject;
				T result = caseLogObservation(logObservation);
				if (result == null) result = caseNamedElement(logObservation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.LOG_COLUMN: {
				LogColumn logColumn = (LogColumn)theEObject;
				T result = caseLogColumn(logColumn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ScnPackage.XOR: {
				XOR xor = (XOR)theEObject;
				T result = caseXOR(xor);
				if (result == null) result = caseAction(xor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulation(Simulation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Time</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTime(Time object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scenario</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScenario(Scenario object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Basic Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Basic Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBasicEvent(BasicEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Initialization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInitialization(ComponentInitialization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Start</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Start</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentStart(ComponentStart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Stop</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Stop</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentStop(ComponentStop object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceCall(ServiceCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Change</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Change</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterChange(ParameterChange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Double Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Double Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleDoubleParameterSet(SimpleDoubleParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Int Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Int Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleIntParameterSet(SimpleIntParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Bool Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Bool Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleBoolParameterSet(SimpleBoolParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple String Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple String Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleStringParameterSet(SimpleStringParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Complex Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Complex Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComplexEvent(ComplexEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable Function Call</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable Function Call</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVariableFunctionCall(VariableFunctionCall object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Double Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Double Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDoubleParameterSet(DoubleParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntParameterSet(IntParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bool Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bool Parameter Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoolParameterSet(BoolParameterSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exploration Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exploration Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExplorationPolicy(ExplorationPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exhaustive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exhaustive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExhaustive(Exhaustive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Random</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Random</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRandom(Random object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>One By One</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>One By One</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOneByOne(OneByOne object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Differential</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Differential</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDifferential(Differential object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserPolicy(UserPolicy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Log Observation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Log Observation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLogObservation(LogObservation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Log Column</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Log Column</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLogColumn(LogColumn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XOR</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XOR</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXOR(XOR object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ScnSwitch
