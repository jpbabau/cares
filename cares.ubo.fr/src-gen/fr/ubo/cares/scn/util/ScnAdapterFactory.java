/**
 */
package fr.ubo.cares.scn.util;

import fr.ubo.cares.dcl.NamedElement;

import fr.ubo.cares.scn.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.scn.ScnPackage
 * @generated
 */
public class ScnAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ScnPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScnAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ScnPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScnSwitch<Adapter> modelSwitch =
		new ScnSwitch<Adapter>() {
			@Override
			public Adapter caseSimulation(Simulation object) {
				return createSimulationAdapter();
			}
			@Override
			public Adapter caseTime(Time object) {
				return createTimeAdapter();
			}
			@Override
			public Adapter caseScenario(Scenario object) {
				return createScenarioAdapter();
			}
			@Override
			public Adapter caseEvent(Event object) {
				return createEventAdapter();
			}
			@Override
			public Adapter caseBasicEvent(BasicEvent object) {
				return createBasicEventAdapter();
			}
			@Override
			public Adapter caseAction(Action object) {
				return createActionAdapter();
			}
			@Override
			public Adapter caseComponentInitialization(ComponentInitialization object) {
				return createComponentInitializationAdapter();
			}
			@Override
			public Adapter caseComponentStart(ComponentStart object) {
				return createComponentStartAdapter();
			}
			@Override
			public Adapter caseComponentStop(ComponentStop object) {
				return createComponentStopAdapter();
			}
			@Override
			public Adapter caseServiceCall(ServiceCall object) {
				return createServiceCallAdapter();
			}
			@Override
			public Adapter caseParameterChange(ParameterChange object) {
				return createParameterChangeAdapter();
			}
			@Override
			public Adapter caseSimpleDoubleParameterSet(SimpleDoubleParameterSet object) {
				return createSimpleDoubleParameterSetAdapter();
			}
			@Override
			public Adapter caseSimpleIntParameterSet(SimpleIntParameterSet object) {
				return createSimpleIntParameterSetAdapter();
			}
			@Override
			public Adapter caseSimpleBoolParameterSet(SimpleBoolParameterSet object) {
				return createSimpleBoolParameterSetAdapter();
			}
			@Override
			public Adapter caseSimpleStringParameterSet(SimpleStringParameterSet object) {
				return createSimpleStringParameterSetAdapter();
			}
			@Override
			public Adapter caseComplexEvent(ComplexEvent object) {
				return createComplexEventAdapter();
			}
			@Override
			public Adapter caseVariableFunctionCall(VariableFunctionCall object) {
				return createVariableFunctionCallAdapter();
			}
			@Override
			public Adapter caseDoubleParameterSet(DoubleParameterSet object) {
				return createDoubleParameterSetAdapter();
			}
			@Override
			public Adapter caseIntParameterSet(IntParameterSet object) {
				return createIntParameterSetAdapter();
			}
			@Override
			public Adapter caseBoolParameterSet(BoolParameterSet object) {
				return createBoolParameterSetAdapter();
			}
			@Override
			public Adapter caseExplorationPolicy(ExplorationPolicy object) {
				return createExplorationPolicyAdapter();
			}
			@Override
			public Adapter caseExhaustive(Exhaustive object) {
				return createExhaustiveAdapter();
			}
			@Override
			public Adapter caseRandom(Random object) {
				return createRandomAdapter();
			}
			@Override
			public Adapter caseOneByOne(OneByOne object) {
				return createOneByOneAdapter();
			}
			@Override
			public Adapter caseDifferential(Differential object) {
				return createDifferentialAdapter();
			}
			@Override
			public Adapter caseUserPolicy(UserPolicy object) {
				return createUserPolicyAdapter();
			}
			@Override
			public Adapter caseLogObservation(LogObservation object) {
				return createLogObservationAdapter();
			}
			@Override
			public Adapter caseLogColumn(LogColumn object) {
				return createLogColumnAdapter();
			}
			@Override
			public Adapter caseXOR(XOR object) {
				return createXORAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Simulation
	 * @generated
	 */
	public Adapter createSimulationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Time
	 * @generated
	 */
	public Adapter createTimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Scenario
	 * @generated
	 */
	public Adapter createScenarioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Event
	 * @generated
	 */
	public Adapter createEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.BasicEvent <em>Basic Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.BasicEvent
	 * @generated
	 */
	public Adapter createBasicEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ComponentInitialization <em>Component Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ComponentInitialization
	 * @generated
	 */
	public Adapter createComponentInitializationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ComponentStart <em>Component Start</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ComponentStart
	 * @generated
	 */
	public Adapter createComponentStartAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ComponentStop <em>Component Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ComponentStop
	 * @generated
	 */
	public Adapter createComponentStopAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ServiceCall <em>Service Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ServiceCall
	 * @generated
	 */
	public Adapter createServiceCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ParameterChange <em>Parameter Change</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ParameterChange
	 * @generated
	 */
	public Adapter createParameterChangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.SimpleDoubleParameterSet <em>Simple Double Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.SimpleDoubleParameterSet
	 * @generated
	 */
	public Adapter createSimpleDoubleParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.SimpleIntParameterSet <em>Simple Int Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.SimpleIntParameterSet
	 * @generated
	 */
	public Adapter createSimpleIntParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.SimpleBoolParameterSet <em>Simple Bool Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.SimpleBoolParameterSet
	 * @generated
	 */
	public Adapter createSimpleBoolParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.SimpleStringParameterSet <em>Simple String Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.SimpleStringParameterSet
	 * @generated
	 */
	public Adapter createSimpleStringParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ComplexEvent <em>Complex Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ComplexEvent
	 * @generated
	 */
	public Adapter createComplexEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.VariableFunctionCall <em>Variable Function Call</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.VariableFunctionCall
	 * @generated
	 */
	public Adapter createVariableFunctionCallAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.DoubleParameterSet <em>Double Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.DoubleParameterSet
	 * @generated
	 */
	public Adapter createDoubleParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.IntParameterSet <em>Int Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.IntParameterSet
	 * @generated
	 */
	public Adapter createIntParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.BoolParameterSet <em>Bool Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.BoolParameterSet
	 * @generated
	 */
	public Adapter createBoolParameterSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.ExplorationPolicy <em>Exploration Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.ExplorationPolicy
	 * @generated
	 */
	public Adapter createExplorationPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Exhaustive <em>Exhaustive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Exhaustive
	 * @generated
	 */
	public Adapter createExhaustiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Random <em>Random</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Random
	 * @generated
	 */
	public Adapter createRandomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.OneByOne <em>One By One</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.OneByOne
	 * @generated
	 */
	public Adapter createOneByOneAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.Differential <em>Differential</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.Differential
	 * @generated
	 */
	public Adapter createDifferentialAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.UserPolicy <em>User Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.UserPolicy
	 * @generated
	 */
	public Adapter createUserPolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.LogObservation <em>Log Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.LogObservation
	 * @generated
	 */
	public Adapter createLogObservationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.LogColumn <em>Log Column</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.LogColumn
	 * @generated
	 */
	public Adapter createLogColumnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.scn.XOR <em>XOR</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.scn.XOR
	 * @generated
	 */
	public Adapter createXORAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link fr.ubo.cares.dcl.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see fr.ubo.cares.dcl.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ScnAdapterFactory
