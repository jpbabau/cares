/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.NamedElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Log Observation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.LogObservation#getFreq <em>Freq</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.LogObservation#getColumns <em>Columns</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.LogObservation#getType <em>Type</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.LogObservation#getTimeStamp <em>Time Stamp</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getLogObservation()
 * @model
 * @generated
 */
public interface LogObservation extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Freq</b></em>' attribute.
	 * The default value is <code>"1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Freq</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Freq</em>' attribute.
	 * @see #setFreq(double)
	 * @see fr.ubo.cares.scn.ScnPackage#getLogObservation_Freq()
	 * @model default="1.0" required="true"
	 * @generated
	 */
	double getFreq();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.LogObservation#getFreq <em>Freq</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Freq</em>' attribute.
	 * @see #getFreq()
	 * @generated
	 */
	void setFreq(double value);

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.LogColumn}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getLogObservation_Columns()
	 * @model containment="true"
	 * @generated
	 */
	EList<LogColumn> getColumns();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"csv"</code>.
	 * The literals are from the enumeration {@link fr.ubo.cares.scn.TypeFile}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see fr.ubo.cares.scn.TypeFile
	 * @see #setType(TypeFile)
	 * @see fr.ubo.cares.scn.ScnPackage#getLogObservation_Type()
	 * @model default="csv" required="true"
	 * @generated
	 */
	TypeFile getType();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.LogObservation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see fr.ubo.cares.scn.TypeFile
	 * @see #getType()
	 * @generated
	 */
	void setType(TypeFile value);

	/**
	 * Returns the value of the '<em><b>Time Stamp</b></em>' attribute.
	 * The default value is <code>"timed"</code>.
	 * The literals are from the enumeration {@link fr.ubo.cares.scn.TimeStamp}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Stamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Stamp</em>' attribute.
	 * @see fr.ubo.cares.scn.TimeStamp
	 * @see #setTimeStamp(TimeStamp)
	 * @see fr.ubo.cares.scn.ScnPackage#getLogObservation_TimeStamp()
	 * @model default="timed" required="true"
	 * @generated
	 */
	TimeStamp getTimeStamp();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.LogObservation#getTimeStamp <em>Time Stamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Stamp</em>' attribute.
	 * @see fr.ubo.cares.scn.TimeStamp
	 * @see #getTimeStamp()
	 * @generated
	 */
	void setTimeStamp(TimeStamp value);

} // LogObservation
