/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.ParameterInstanciation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Change</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.ParameterChange#getParameter <em>Parameter</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getParameterChange()
 * @model abstract="true"
 * @generated
 */
public interface ParameterChange extends Action {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #setParameter(ParameterInstanciation)
	 * @see fr.ubo.cares.scn.ScnPackage#getParameterChange_Parameter()
	 * @model required="true"
	 * @generated
	 */
	ParameterInstanciation getParameter();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.ParameterChange#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(ParameterInstanciation value);

} // ParameterChange
