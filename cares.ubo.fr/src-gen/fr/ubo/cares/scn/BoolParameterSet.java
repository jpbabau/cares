/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bool Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.scn.ScnPackage#getBoolParameterSet()
 * @model
 * @generated
 */
public interface BoolParameterSet extends EObject {
} // BoolParameterSet
