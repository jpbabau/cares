/**
 */
package fr.ubo.cares.scn;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Bool Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.SimpleBoolParameterSet#isValue <em>Value</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getSimpleBoolParameterSet()
 * @model
 * @generated
 */
public interface SimpleBoolParameterSet extends ParameterChange {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(boolean)
	 * @see fr.ubo.cares.scn.ScnPackage#getSimpleBoolParameterSet_Value()
	 * @model required="true"
	 * @generated
	 */
	boolean isValue();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.SimpleBoolParameterSet#isValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isValue()
	 * @generated
	 */
	void setValue(boolean value);

} // SimpleBoolParameterSet
