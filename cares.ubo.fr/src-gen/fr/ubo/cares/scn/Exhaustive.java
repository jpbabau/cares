/**
 */
package fr.ubo.cares.scn;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exhaustive</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.scn.ScnPackage#getExhaustive()
 * @model
 * @generated
 */
public interface Exhaustive extends ExplorationPolicy {
} // Exhaustive
