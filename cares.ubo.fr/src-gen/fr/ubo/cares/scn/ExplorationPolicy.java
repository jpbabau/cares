/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.Function;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exploration Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.ExplorationPolicy#getConstraintFunctions <em>Constraint Functions</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.ExplorationPolicy#getObjectiveFunctions <em>Objective Functions</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getExplorationPolicy()
 * @model abstract="true"
 * @generated
 */
public interface ExplorationPolicy extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Functions</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getExplorationPolicy_ConstraintFunctions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Function> getConstraintFunctions();

	/**
	 * Returns the value of the '<em><b>Objective Functions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Objective Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objective Functions</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getExplorationPolicy_ObjectiveFunctions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Function> getObjectiveFunctions();

} // ExplorationPolicy
