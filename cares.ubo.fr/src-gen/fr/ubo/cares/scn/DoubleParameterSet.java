/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.DoubleParameterSet#getMinimum <em>Minimum</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.DoubleParameterSet#getMaximum <em>Maximum</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.DoubleParameterSet#getStep <em>Step</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getDoubleParameterSet()
 * @model
 * @generated
 */
public interface DoubleParameterSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Minimum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minimum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minimum</em>' attribute.
	 * @see #setMinimum(double)
	 * @see fr.ubo.cares.scn.ScnPackage#getDoubleParameterSet_Minimum()
	 * @model required="true"
	 * @generated
	 */
	double getMinimum();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.DoubleParameterSet#getMinimum <em>Minimum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum</em>' attribute.
	 * @see #getMinimum()
	 * @generated
	 */
	void setMinimum(double value);

	/**
	 * Returns the value of the '<em><b>Maximum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maximum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maximum</em>' attribute.
	 * @see #setMaximum(double)
	 * @see fr.ubo.cares.scn.ScnPackage#getDoubleParameterSet_Maximum()
	 * @model required="true"
	 * @generated
	 */
	double getMaximum();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.DoubleParameterSet#getMaximum <em>Maximum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum</em>' attribute.
	 * @see #getMaximum()
	 * @generated
	 */
	void setMaximum(double value);

	/**
	 * Returns the value of the '<em><b>Step</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step</em>' attribute.
	 * @see #setStep(double)
	 * @see fr.ubo.cares.scn.ScnPackage#getDoubleParameterSet_Step()
	 * @model required="true"
	 * @generated
	 */
	double getStep();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.DoubleParameterSet#getStep <em>Step</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step</em>' attribute.
	 * @see #getStep()
	 * @generated
	 */
	void setStep(double value);

} // DoubleParameterSet
