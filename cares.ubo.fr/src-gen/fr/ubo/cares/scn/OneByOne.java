/**
 */
package fr.ubo.cares.scn;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>One By One</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.scn.ScnPackage#getOneByOne()
 * @model
 * @generated
 */
public interface OneByOne extends ExplorationPolicy {
} // OneByOne
