/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.FunctionCall;

import fr.ubo.cares.sys.LeafComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.ServiceCall#getFunctionCall <em>Function Call</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.ServiceCall#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getServiceCall()
 * @model
 * @generated
 */
public interface ServiceCall extends Action {
	/**
	 * Returns the value of the '<em><b>Function Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function Call</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function Call</em>' containment reference.
	 * @see #setFunctionCall(FunctionCall)
	 * @see fr.ubo.cares.scn.ScnPackage#getServiceCall_FunctionCall()
	 * @model containment="true" required="true"
	 * @generated
	 */
	FunctionCall getFunctionCall();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.ServiceCall#getFunctionCall <em>Function Call</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function Call</em>' containment reference.
	 * @see #getFunctionCall()
	 * @generated
	 */
	void setFunctionCall(FunctionCall value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(LeafComponent)
	 * @see fr.ubo.cares.scn.ScnPackage#getServiceCall_Component()
	 * @model required="true"
	 * @generated
	 */
	LeafComponent getComponent();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.ServiceCall#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(LeafComponent value);

} // ServiceCall
