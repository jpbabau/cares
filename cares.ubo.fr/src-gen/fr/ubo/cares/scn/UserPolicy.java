/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.NamedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.scn.ScnPackage#getUserPolicy()
 * @model
 * @generated
 */
public interface UserPolicy extends ExplorationPolicy, NamedElement {
} // UserPolicy
