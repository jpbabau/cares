/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.scn.ScnPackage
 * @generated
 */
public interface ScnFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScnFactory eINSTANCE = fr.ubo.cares.scn.impl.ScnFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation</em>'.
	 * @generated
	 */
	Simulation createSimulation();

	/**
	 * Returns a new object of class '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Time</em>'.
	 * @generated
	 */
	Time createTime();

	/**
	 * Returns a new object of class '<em>Scenario</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scenario</em>'.
	 * @generated
	 */
	Scenario createScenario();

	/**
	 * Returns a new object of class '<em>Basic Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Basic Event</em>'.
	 * @generated
	 */
	BasicEvent createBasicEvent();

	/**
	 * Returns a new object of class '<em>Component Initialization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Initialization</em>'.
	 * @generated
	 */
	ComponentInitialization createComponentInitialization();

	/**
	 * Returns a new object of class '<em>Component Start</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Start</em>'.
	 * @generated
	 */
	ComponentStart createComponentStart();

	/**
	 * Returns a new object of class '<em>Component Stop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Component Stop</em>'.
	 * @generated
	 */
	ComponentStop createComponentStop();

	/**
	 * Returns a new object of class '<em>Service Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Service Call</em>'.
	 * @generated
	 */
	ServiceCall createServiceCall();

	/**
	 * Returns a new object of class '<em>Simple Double Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Double Parameter Set</em>'.
	 * @generated
	 */
	SimpleDoubleParameterSet createSimpleDoubleParameterSet();

	/**
	 * Returns a new object of class '<em>Simple Int Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Int Parameter Set</em>'.
	 * @generated
	 */
	SimpleIntParameterSet createSimpleIntParameterSet();

	/**
	 * Returns a new object of class '<em>Simple Bool Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Bool Parameter Set</em>'.
	 * @generated
	 */
	SimpleBoolParameterSet createSimpleBoolParameterSet();

	/**
	 * Returns a new object of class '<em>Simple String Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple String Parameter Set</em>'.
	 * @generated
	 */
	SimpleStringParameterSet createSimpleStringParameterSet();

	/**
	 * Returns a new object of class '<em>Complex Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Complex Event</em>'.
	 * @generated
	 */
	ComplexEvent createComplexEvent();

	/**
	 * Returns a new object of class '<em>Variable Function Call</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variable Function Call</em>'.
	 * @generated
	 */
	VariableFunctionCall createVariableFunctionCall();

	/**
	 * Returns a new object of class '<em>Double Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Double Parameter Set</em>'.
	 * @generated
	 */
	DoubleParameterSet createDoubleParameterSet();

	/**
	 * Returns a new object of class '<em>Int Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Int Parameter Set</em>'.
	 * @generated
	 */
	IntParameterSet createIntParameterSet();

	/**
	 * Returns a new object of class '<em>Bool Parameter Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bool Parameter Set</em>'.
	 * @generated
	 */
	BoolParameterSet createBoolParameterSet();

	/**
	 * Returns a new object of class '<em>Exhaustive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exhaustive</em>'.
	 * @generated
	 */
	Exhaustive createExhaustive();

	/**
	 * Returns a new object of class '<em>Random</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Random</em>'.
	 * @generated
	 */
	Random createRandom();

	/**
	 * Returns a new object of class '<em>One By One</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>One By One</em>'.
	 * @generated
	 */
	OneByOne createOneByOne();

	/**
	 * Returns a new object of class '<em>Differential</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Differential</em>'.
	 * @generated
	 */
	Differential createDifferential();

	/**
	 * Returns a new object of class '<em>User Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Policy</em>'.
	 * @generated
	 */
	UserPolicy createUserPolicy();

	/**
	 * Returns a new object of class '<em>Log Observation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Log Observation</em>'.
	 * @generated
	 */
	LogObservation createLogObservation();

	/**
	 * Returns a new object of class '<em>Log Column</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Log Column</em>'.
	 * @generated
	 */
	LogColumn createLogColumn();

	/**
	 * Returns a new object of class '<em>XOR</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XOR</em>'.
	 * @generated
	 */
	XOR createXOR();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ScnPackage getScnPackage();

} //ScnFactory
