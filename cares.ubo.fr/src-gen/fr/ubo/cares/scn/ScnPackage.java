/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.DclPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.ubo.cares.scn.ScnFactory
 * @model kind="package"
 * @generated
 */
public interface ScnPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scn";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.ubo.mde.cares/scn";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "scn";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScnPackage eINSTANCE = fr.ubo.cares.scn.impl.ScnPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.SimulationImpl <em>Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.SimulationImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimulation()
	 * @generated
	 */
	int SIMULATION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Cares System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__CARES_SYSTEM = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__TIME_UNIT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Important Dates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__IMPORTANT_DATES = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Begin</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__BEGIN = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Scenarios</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__SCENARIOS = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>End</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__END = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.TimeImpl <em>Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.TimeImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getTime()
	 * @generated
	 */
	int TIME = 1;

	/**
	 * The feature id for the '<em><b>Starting Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__STARTING_TIME = 0;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__END_TIME = 1;

	/**
	 * The feature id for the '<em><b>Step Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME__STEP_TIME = 2;

	/**
	 * The number of structural features of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIME_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ScenarioImpl <em>Scenario</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ScenarioImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getScenario()
	 * @generated
	 */
	int SCENARIO = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Begin</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__BEGIN = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__EVENTS = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__END = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__NUMBER = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Logs</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO__LOGS = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Scenario</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCENARIO_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.EventImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 3;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TIME = 0;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.BasicEventImpl <em>Basic Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.BasicEventImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getBasicEvent()
	 * @generated
	 */
	int BASIC_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__TIME = EVENT__TIME;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT__ACTIONS = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Basic Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Basic Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASIC_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ActionImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 5;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ComponentInitializationImpl <em>Component Initialization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ComponentInitializationImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComponentInitialization()
	 * @generated
	 */
	int COMPONENT_INITIALIZATION = 6;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INITIALIZATION__COMPONENT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Initialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INITIALIZATION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Component Initialization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INITIALIZATION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ComponentStartImpl <em>Component Start</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ComponentStartImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComponentStart()
	 * @generated
	 */
	int COMPONENT_START = 7;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_START__COMPONENT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Start</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_START_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Component Start</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_START_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ComponentStopImpl <em>Component Stop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ComponentStopImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComponentStop()
	 * @generated
	 */
	int COMPONENT_STOP = 8;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_STOP__COMPONENT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Component Stop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_STOP_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Component Stop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_STOP_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ServiceCallImpl <em>Service Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ServiceCallImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getServiceCall()
	 * @generated
	 */
	int SERVICE_CALL = 9;

	/**
	 * The feature id for the '<em><b>Function Call</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CALL__FUNCTION_CALL = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CALL__COMPONENT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CALL_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Service Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_CALL_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ParameterChangeImpl <em>Parameter Change</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ParameterChangeImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getParameterChange()
	 * @generated
	 */
	int PARAMETER_CHANGE = 10;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CHANGE__PARAMETER = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CHANGE_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Parameter Change</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CHANGE_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.SimpleDoubleParameterSetImpl <em>Simple Double Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.SimpleDoubleParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleDoubleParameterSet()
	 * @generated
	 */
	int SIMPLE_DOUBLE_PARAMETER_SET = 11;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_DOUBLE_PARAMETER_SET__PARAMETER = PARAMETER_CHANGE__PARAMETER;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_DOUBLE_PARAMETER_SET__VALUE = PARAMETER_CHANGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple Double Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_DOUBLE_PARAMETER_SET_FEATURE_COUNT = PARAMETER_CHANGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple Double Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_DOUBLE_PARAMETER_SET_OPERATION_COUNT = PARAMETER_CHANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.SimpleIntParameterSetImpl <em>Simple Int Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.SimpleIntParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleIntParameterSet()
	 * @generated
	 */
	int SIMPLE_INT_PARAMETER_SET = 12;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INT_PARAMETER_SET__PARAMETER = PARAMETER_CHANGE__PARAMETER;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INT_PARAMETER_SET__VALUE = PARAMETER_CHANGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple Int Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INT_PARAMETER_SET_FEATURE_COUNT = PARAMETER_CHANGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple Int Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_INT_PARAMETER_SET_OPERATION_COUNT = PARAMETER_CHANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.SimpleBoolParameterSetImpl <em>Simple Bool Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.SimpleBoolParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleBoolParameterSet()
	 * @generated
	 */
	int SIMPLE_BOOL_PARAMETER_SET = 13;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_BOOL_PARAMETER_SET__PARAMETER = PARAMETER_CHANGE__PARAMETER;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_BOOL_PARAMETER_SET__VALUE = PARAMETER_CHANGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple Bool Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_BOOL_PARAMETER_SET_FEATURE_COUNT = PARAMETER_CHANGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple Bool Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_BOOL_PARAMETER_SET_OPERATION_COUNT = PARAMETER_CHANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.SimpleStringParameterSetImpl <em>Simple String Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.SimpleStringParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleStringParameterSet()
	 * @generated
	 */
	int SIMPLE_STRING_PARAMETER_SET = 14;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STRING_PARAMETER_SET__PARAMETER = PARAMETER_CHANGE__PARAMETER;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STRING_PARAMETER_SET__VALUE = PARAMETER_CHANGE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Simple String Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STRING_PARAMETER_SET_FEATURE_COUNT = PARAMETER_CHANGE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Simple String Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_STRING_PARAMETER_SET_OPERATION_COUNT = PARAMETER_CHANGE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ComplexEventImpl <em>Complex Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ComplexEventImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComplexEvent()
	 * @generated
	 */
	int COMPLEX_EVENT = 15;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_EVENT__PARAMETERS = 0;

	/**
	 * The feature id for the '<em><b>Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_EVENT__POLICY = 1;

	/**
	 * The feature id for the '<em><b>Functions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_EVENT__FUNCTIONS = 2;

	/**
	 * The number of structural features of the '<em>Complex Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_EVENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Complex Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPLEX_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.VariableFunctionCallImpl <em>Variable Function Call</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.VariableFunctionCallImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getVariableFunctionCall()
	 * @generated
	 */
	int VARIABLE_FUNCTION_CALL = 16;

	/**
	 * The feature id for the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FUNCTION_CALL__FUNCTION = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FUNCTION_CALL__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Variable Function Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FUNCTION_CALL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variable Function Call</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FUNCTION_CALL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.DoubleParameterSetImpl <em>Double Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.DoubleParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getDoubleParameterSet()
	 * @generated
	 */
	int DOUBLE_PARAMETER_SET = 17;

	/**
	 * The feature id for the '<em><b>Minimum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_PARAMETER_SET__MINIMUM = 0;

	/**
	 * The feature id for the '<em><b>Maximum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_PARAMETER_SET__MAXIMUM = 1;

	/**
	 * The feature id for the '<em><b>Step</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_PARAMETER_SET__STEP = 2;

	/**
	 * The number of structural features of the '<em>Double Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_PARAMETER_SET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Double Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOUBLE_PARAMETER_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.IntParameterSetImpl <em>Int Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.IntParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getIntParameterSet()
	 * @generated
	 */
	int INT_PARAMETER_SET = 18;

	/**
	 * The feature id for the '<em><b>Minimum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_PARAMETER_SET__MINIMUM = 0;

	/**
	 * The feature id for the '<em><b>Maximum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_PARAMETER_SET__MAXIMUM = 1;

	/**
	 * The feature id for the '<em><b>Step</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_PARAMETER_SET__STEP = 2;

	/**
	 * The number of structural features of the '<em>Int Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_PARAMETER_SET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Int Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_PARAMETER_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.BoolParameterSetImpl <em>Bool Parameter Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.BoolParameterSetImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getBoolParameterSet()
	 * @generated
	 */
	int BOOL_PARAMETER_SET = 19;

	/**
	 * The number of structural features of the '<em>Bool Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_PARAMETER_SET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Bool Parameter Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_PARAMETER_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ExplorationPolicyImpl <em>Exploration Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ExplorationPolicyImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getExplorationPolicy()
	 * @generated
	 */
	int EXPLORATION_POLICY = 20;

	/**
	 * The feature id for the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS = 0;

	/**
	 * The feature id for the '<em><b>Objective Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS = 1;

	/**
	 * The number of structural features of the '<em>Exploration Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLORATION_POLICY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Exploration Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPLORATION_POLICY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.ExhaustiveImpl <em>Exhaustive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.ExhaustiveImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getExhaustive()
	 * @generated
	 */
	int EXHAUSTIVE = 21;

	/**
	 * The feature id for the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXHAUSTIVE__CONSTRAINT_FUNCTIONS = EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Objective Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXHAUSTIVE__OBJECTIVE_FUNCTIONS = EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS;

	/**
	 * The number of structural features of the '<em>Exhaustive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXHAUSTIVE_FEATURE_COUNT = EXPLORATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Exhaustive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXHAUSTIVE_OPERATION_COUNT = EXPLORATION_POLICY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.RandomImpl <em>Random</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.RandomImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getRandom()
	 * @generated
	 */
	int RANDOM = 22;

	/**
	 * The feature id for the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM__CONSTRAINT_FUNCTIONS = EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Objective Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM__OBJECTIVE_FUNCTIONS = EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM__MAX = EXPLORATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Random</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_FEATURE_COUNT = EXPLORATION_POLICY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Random</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RANDOM_OPERATION_COUNT = EXPLORATION_POLICY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.OneByOneImpl <em>One By One</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.OneByOneImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getOneByOne()
	 * @generated
	 */
	int ONE_BY_ONE = 23;

	/**
	 * The feature id for the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONE_BY_ONE__CONSTRAINT_FUNCTIONS = EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Objective Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONE_BY_ONE__OBJECTIVE_FUNCTIONS = EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS;

	/**
	 * The number of structural features of the '<em>One By One</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONE_BY_ONE_FEATURE_COUNT = EXPLORATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>One By One</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ONE_BY_ONE_OPERATION_COUNT = EXPLORATION_POLICY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.DifferentialImpl <em>Differential</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.DifferentialImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getDifferential()
	 * @generated
	 */
	int DIFFERENTIAL = 24;

	/**
	 * The feature id for the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENTIAL__CONSTRAINT_FUNCTIONS = EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Objective Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENTIAL__OBJECTIVE_FUNCTIONS = EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS;

	/**
	 * The number of structural features of the '<em>Differential</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENTIAL_FEATURE_COUNT = EXPLORATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Differential</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIFFERENTIAL_OPERATION_COUNT = EXPLORATION_POLICY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.UserPolicyImpl <em>User Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.UserPolicyImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getUserPolicy()
	 * @generated
	 */
	int USER_POLICY = 25;

	/**
	 * The feature id for the '<em><b>Constraint Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_POLICY__CONSTRAINT_FUNCTIONS = EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Objective Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_POLICY__OBJECTIVE_FUNCTIONS = EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_POLICY__NAME = EXPLORATION_POLICY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>User Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_POLICY_FEATURE_COUNT = EXPLORATION_POLICY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>User Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_POLICY_OPERATION_COUNT = EXPLORATION_POLICY_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.LogObservationImpl <em>Log Observation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.LogObservationImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getLogObservation()
	 * @generated
	 */
	int LOG_OBSERVATION = 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION__NAME = DclPackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Freq</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION__FREQ = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION__COLUMNS = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION__TYPE = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Time Stamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION__TIME_STAMP = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Log Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION_FEATURE_COUNT = DclPackage.NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Log Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_OBSERVATION_OPERATION_COUNT = DclPackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.LogColumnImpl <em>Log Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.LogColumnImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getLogColumn()
	 * @generated
	 */
	int LOG_COLUMN = 27;

	/**
	 * The feature id for the '<em><b>Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_COLUMN__DATA = 0;

	/**
	 * The number of structural features of the '<em>Log Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_COLUMN_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Log Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOG_COLUMN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.impl.XORImpl <em>XOR</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.impl.XORImpl
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getXOR()
	 * @generated
	 */
	int XOR = 28;

	/**
	 * The feature id for the '<em><b>Required Interface Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR__REQUIRED_INTERFACE_PORT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Interface Port</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR__PROVIDED_INTERFACE_PORT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>XOR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>XOR</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XOR_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.TypeFile <em>Type File</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.TypeFile
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getTypeFile()
	 * @generated
	 */
	int TYPE_FILE = 29;

	/**
	 * The meta object id for the '{@link fr.ubo.cares.scn.TimeStamp <em>Time Stamp</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.ubo.cares.scn.TimeStamp
	 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getTimeStamp()
	 * @generated
	 */
	int TIME_STAMP = 30;


	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation</em>'.
	 * @see fr.ubo.cares.scn.Simulation
	 * @generated
	 */
	EClass getSimulation();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.Simulation#getCaresSystem <em>Cares System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Cares System</em>'.
	 * @see fr.ubo.cares.scn.Simulation#getCaresSystem()
	 * @see #getSimulation()
	 * @generated
	 */
	EReference getSimulation_CaresSystem();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Simulation#getTimeUnit <em>Time Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Unit</em>'.
	 * @see fr.ubo.cares.scn.Simulation#getTimeUnit()
	 * @see #getSimulation()
	 * @generated
	 */
	EAttribute getSimulation_TimeUnit();

	/**
	 * Returns the meta object for the containment reference '{@link fr.ubo.cares.scn.Simulation#getImportantDates <em>Important Dates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Important Dates</em>'.
	 * @see fr.ubo.cares.scn.Simulation#getImportantDates()
	 * @see #getSimulation()
	 * @generated
	 */
	EReference getSimulation_ImportantDates();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Simulation#getBegin <em>Begin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Begin</em>'.
	 * @see fr.ubo.cares.scn.Simulation#getBegin()
	 * @see #getSimulation()
	 * @generated
	 */
	EReference getSimulation_Begin();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Simulation#getScenarios <em>Scenarios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scenarios</em>'.
	 * @see fr.ubo.cares.scn.Simulation#getScenarios()
	 * @see #getSimulation()
	 * @generated
	 */
	EReference getSimulation_Scenarios();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Simulation#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>End</em>'.
	 * @see fr.ubo.cares.scn.Simulation#getEnd()
	 * @see #getSimulation()
	 * @generated
	 */
	EReference getSimulation_End();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Time <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Time</em>'.
	 * @see fr.ubo.cares.scn.Time
	 * @generated
	 */
	EClass getTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Time#getStartingTime <em>Starting Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Starting Time</em>'.
	 * @see fr.ubo.cares.scn.Time#getStartingTime()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_StartingTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Time#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see fr.ubo.cares.scn.Time#getEndTime()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Time#getStepTime <em>Step Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step Time</em>'.
	 * @see fr.ubo.cares.scn.Time#getStepTime()
	 * @see #getTime()
	 * @generated
	 */
	EAttribute getTime_StepTime();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Scenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scenario</em>'.
	 * @see fr.ubo.cares.scn.Scenario
	 * @generated
	 */
	EClass getScenario();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Scenario#getBegin <em>Begin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Begin</em>'.
	 * @see fr.ubo.cares.scn.Scenario#getBegin()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_Begin();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Scenario#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see fr.ubo.cares.scn.Scenario#getEvents()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_Events();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Scenario#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>End</em>'.
	 * @see fr.ubo.cares.scn.Scenario#getEnd()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_End();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Scenario#getNumber <em>Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number</em>'.
	 * @see fr.ubo.cares.scn.Scenario#getNumber()
	 * @see #getScenario()
	 * @generated
	 */
	EAttribute getScenario_Number();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.Scenario#getLogs <em>Logs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Logs</em>'.
	 * @see fr.ubo.cares.scn.Scenario#getLogs()
	 * @see #getScenario()
	 * @generated
	 */
	EReference getScenario_Logs();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see fr.ubo.cares.scn.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Event#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see fr.ubo.cares.scn.Event#getTime()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Time();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.BasicEvent <em>Basic Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Basic Event</em>'.
	 * @see fr.ubo.cares.scn.BasicEvent
	 * @generated
	 */
	EClass getBasicEvent();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.BasicEvent#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see fr.ubo.cares.scn.BasicEvent#getActions()
	 * @see #getBasicEvent()
	 * @generated
	 */
	EReference getBasicEvent_Actions();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see fr.ubo.cares.scn.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ComponentInitialization <em>Component Initialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Initialization</em>'.
	 * @see fr.ubo.cares.scn.ComponentInitialization
	 * @generated
	 */
	EClass getComponentInitialization();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.ComponentInitialization#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see fr.ubo.cares.scn.ComponentInitialization#getComponent()
	 * @see #getComponentInitialization()
	 * @generated
	 */
	EReference getComponentInitialization_Component();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ComponentStart <em>Component Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Start</em>'.
	 * @see fr.ubo.cares.scn.ComponentStart
	 * @generated
	 */
	EClass getComponentStart();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.ComponentStart#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see fr.ubo.cares.scn.ComponentStart#getComponent()
	 * @see #getComponentStart()
	 * @generated
	 */
	EReference getComponentStart_Component();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ComponentStop <em>Component Stop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Stop</em>'.
	 * @see fr.ubo.cares.scn.ComponentStop
	 * @generated
	 */
	EClass getComponentStop();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.ComponentStop#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see fr.ubo.cares.scn.ComponentStop#getComponent()
	 * @see #getComponentStop()
	 * @generated
	 */
	EReference getComponentStop_Component();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ServiceCall <em>Service Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Call</em>'.
	 * @see fr.ubo.cares.scn.ServiceCall
	 * @generated
	 */
	EClass getServiceCall();

	/**
	 * Returns the meta object for the containment reference '{@link fr.ubo.cares.scn.ServiceCall#getFunctionCall <em>Function Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Function Call</em>'.
	 * @see fr.ubo.cares.scn.ServiceCall#getFunctionCall()
	 * @see #getServiceCall()
	 * @generated
	 */
	EReference getServiceCall_FunctionCall();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.ServiceCall#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see fr.ubo.cares.scn.ServiceCall#getComponent()
	 * @see #getServiceCall()
	 * @generated
	 */
	EReference getServiceCall_Component();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ParameterChange <em>Parameter Change</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Change</em>'.
	 * @see fr.ubo.cares.scn.ParameterChange
	 * @generated
	 */
	EClass getParameterChange();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.ParameterChange#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see fr.ubo.cares.scn.ParameterChange#getParameter()
	 * @see #getParameterChange()
	 * @generated
	 */
	EReference getParameterChange_Parameter();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.SimpleDoubleParameterSet <em>Simple Double Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Double Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.SimpleDoubleParameterSet
	 * @generated
	 */
	EClass getSimpleDoubleParameterSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.SimpleDoubleParameterSet#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.ubo.cares.scn.SimpleDoubleParameterSet#getValue()
	 * @see #getSimpleDoubleParameterSet()
	 * @generated
	 */
	EAttribute getSimpleDoubleParameterSet_Value();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.SimpleIntParameterSet <em>Simple Int Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Int Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.SimpleIntParameterSet
	 * @generated
	 */
	EClass getSimpleIntParameterSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.SimpleIntParameterSet#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.ubo.cares.scn.SimpleIntParameterSet#getValue()
	 * @see #getSimpleIntParameterSet()
	 * @generated
	 */
	EAttribute getSimpleIntParameterSet_Value();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.SimpleBoolParameterSet <em>Simple Bool Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Bool Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.SimpleBoolParameterSet
	 * @generated
	 */
	EClass getSimpleBoolParameterSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.SimpleBoolParameterSet#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.ubo.cares.scn.SimpleBoolParameterSet#isValue()
	 * @see #getSimpleBoolParameterSet()
	 * @generated
	 */
	EAttribute getSimpleBoolParameterSet_Value();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.SimpleStringParameterSet <em>Simple String Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple String Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.SimpleStringParameterSet
	 * @generated
	 */
	EClass getSimpleStringParameterSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.SimpleStringParameterSet#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see fr.ubo.cares.scn.SimpleStringParameterSet#getValue()
	 * @see #getSimpleStringParameterSet()
	 * @generated
	 */
	EAttribute getSimpleStringParameterSet_Value();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ComplexEvent <em>Complex Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Complex Event</em>'.
	 * @see fr.ubo.cares.scn.ComplexEvent
	 * @generated
	 */
	EClass getComplexEvent();

	/**
	 * Returns the meta object for the reference list '{@link fr.ubo.cares.scn.ComplexEvent#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Parameters</em>'.
	 * @see fr.ubo.cares.scn.ComplexEvent#getParameters()
	 * @see #getComplexEvent()
	 * @generated
	 */
	EReference getComplexEvent_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link fr.ubo.cares.scn.ComplexEvent#getPolicy <em>Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Policy</em>'.
	 * @see fr.ubo.cares.scn.ComplexEvent#getPolicy()
	 * @see #getComplexEvent()
	 * @generated
	 */
	EReference getComplexEvent_Policy();

	/**
	 * Returns the meta object for the reference list '{@link fr.ubo.cares.scn.ComplexEvent#getFunctions <em>Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Functions</em>'.
	 * @see fr.ubo.cares.scn.ComplexEvent#getFunctions()
	 * @see #getComplexEvent()
	 * @generated
	 */
	EReference getComplexEvent_Functions();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.VariableFunctionCall <em>Variable Function Call</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Function Call</em>'.
	 * @see fr.ubo.cares.scn.VariableFunctionCall
	 * @generated
	 */
	EClass getVariableFunctionCall();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.VariableFunctionCall#getFunction <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Function</em>'.
	 * @see fr.ubo.cares.scn.VariableFunctionCall#getFunction()
	 * @see #getVariableFunctionCall()
	 * @generated
	 */
	EReference getVariableFunctionCall_Function();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.VariableFunctionCall#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see fr.ubo.cares.scn.VariableFunctionCall#getParameters()
	 * @see #getVariableFunctionCall()
	 * @generated
	 */
	EReference getVariableFunctionCall_Parameters();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.DoubleParameterSet <em>Double Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Double Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.DoubleParameterSet
	 * @generated
	 */
	EClass getDoubleParameterSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.DoubleParameterSet#getMinimum <em>Minimum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum</em>'.
	 * @see fr.ubo.cares.scn.DoubleParameterSet#getMinimum()
	 * @see #getDoubleParameterSet()
	 * @generated
	 */
	EAttribute getDoubleParameterSet_Minimum();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.DoubleParameterSet#getMaximum <em>Maximum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum</em>'.
	 * @see fr.ubo.cares.scn.DoubleParameterSet#getMaximum()
	 * @see #getDoubleParameterSet()
	 * @generated
	 */
	EAttribute getDoubleParameterSet_Maximum();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.DoubleParameterSet#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step</em>'.
	 * @see fr.ubo.cares.scn.DoubleParameterSet#getStep()
	 * @see #getDoubleParameterSet()
	 * @generated
	 */
	EAttribute getDoubleParameterSet_Step();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.IntParameterSet <em>Int Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.IntParameterSet
	 * @generated
	 */
	EClass getIntParameterSet();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.IntParameterSet#getMinimum <em>Minimum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum</em>'.
	 * @see fr.ubo.cares.scn.IntParameterSet#getMinimum()
	 * @see #getIntParameterSet()
	 * @generated
	 */
	EAttribute getIntParameterSet_Minimum();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.IntParameterSet#getMaximum <em>Maximum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum</em>'.
	 * @see fr.ubo.cares.scn.IntParameterSet#getMaximum()
	 * @see #getIntParameterSet()
	 * @generated
	 */
	EAttribute getIntParameterSet_Maximum();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.IntParameterSet#getStep <em>Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step</em>'.
	 * @see fr.ubo.cares.scn.IntParameterSet#getStep()
	 * @see #getIntParameterSet()
	 * @generated
	 */
	EAttribute getIntParameterSet_Step();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.BoolParameterSet <em>Bool Parameter Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bool Parameter Set</em>'.
	 * @see fr.ubo.cares.scn.BoolParameterSet
	 * @generated
	 */
	EClass getBoolParameterSet();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.ExplorationPolicy <em>Exploration Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exploration Policy</em>'.
	 * @see fr.ubo.cares.scn.ExplorationPolicy
	 * @generated
	 */
	EClass getExplorationPolicy();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.ExplorationPolicy#getConstraintFunctions <em>Constraint Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraint Functions</em>'.
	 * @see fr.ubo.cares.scn.ExplorationPolicy#getConstraintFunctions()
	 * @see #getExplorationPolicy()
	 * @generated
	 */
	EReference getExplorationPolicy_ConstraintFunctions();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.ExplorationPolicy#getObjectiveFunctions <em>Objective Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Objective Functions</em>'.
	 * @see fr.ubo.cares.scn.ExplorationPolicy#getObjectiveFunctions()
	 * @see #getExplorationPolicy()
	 * @generated
	 */
	EReference getExplorationPolicy_ObjectiveFunctions();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Exhaustive <em>Exhaustive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exhaustive</em>'.
	 * @see fr.ubo.cares.scn.Exhaustive
	 * @generated
	 */
	EClass getExhaustive();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Random <em>Random</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Random</em>'.
	 * @see fr.ubo.cares.scn.Random
	 * @generated
	 */
	EClass getRandom();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.Random#getMax <em>Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max</em>'.
	 * @see fr.ubo.cares.scn.Random#getMax()
	 * @see #getRandom()
	 * @generated
	 */
	EAttribute getRandom_Max();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.OneByOne <em>One By One</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>One By One</em>'.
	 * @see fr.ubo.cares.scn.OneByOne
	 * @generated
	 */
	EClass getOneByOne();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.Differential <em>Differential</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Differential</em>'.
	 * @see fr.ubo.cares.scn.Differential
	 * @generated
	 */
	EClass getDifferential();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.UserPolicy <em>User Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Policy</em>'.
	 * @see fr.ubo.cares.scn.UserPolicy
	 * @generated
	 */
	EClass getUserPolicy();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.LogObservation <em>Log Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Log Observation</em>'.
	 * @see fr.ubo.cares.scn.LogObservation
	 * @generated
	 */
	EClass getLogObservation();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.LogObservation#getFreq <em>Freq</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Freq</em>'.
	 * @see fr.ubo.cares.scn.LogObservation#getFreq()
	 * @see #getLogObservation()
	 * @generated
	 */
	EAttribute getLogObservation_Freq();

	/**
	 * Returns the meta object for the containment reference list '{@link fr.ubo.cares.scn.LogObservation#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Columns</em>'.
	 * @see fr.ubo.cares.scn.LogObservation#getColumns()
	 * @see #getLogObservation()
	 * @generated
	 */
	EReference getLogObservation_Columns();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.LogObservation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see fr.ubo.cares.scn.LogObservation#getType()
	 * @see #getLogObservation()
	 * @generated
	 */
	EAttribute getLogObservation_Type();

	/**
	 * Returns the meta object for the attribute '{@link fr.ubo.cares.scn.LogObservation#getTimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Stamp</em>'.
	 * @see fr.ubo.cares.scn.LogObservation#getTimeStamp()
	 * @see #getLogObservation()
	 * @generated
	 */
	EAttribute getLogObservation_TimeStamp();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.LogColumn <em>Log Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Log Column</em>'.
	 * @see fr.ubo.cares.scn.LogColumn
	 * @generated
	 */
	EClass getLogColumn();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.LogColumn#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data</em>'.
	 * @see fr.ubo.cares.scn.LogColumn#getData()
	 * @see #getLogColumn()
	 * @generated
	 */
	EReference getLogColumn_Data();

	/**
	 * Returns the meta object for class '{@link fr.ubo.cares.scn.XOR <em>XOR</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XOR</em>'.
	 * @see fr.ubo.cares.scn.XOR
	 * @generated
	 */
	EClass getXOR();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.XOR#getRequiredInterfacePort <em>Required Interface Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Interface Port</em>'.
	 * @see fr.ubo.cares.scn.XOR#getRequiredInterfacePort()
	 * @see #getXOR()
	 * @generated
	 */
	EReference getXOR_RequiredInterfacePort();

	/**
	 * Returns the meta object for the reference '{@link fr.ubo.cares.scn.XOR#getProvidedInterfacePort <em>Provided Interface Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Interface Port</em>'.
	 * @see fr.ubo.cares.scn.XOR#getProvidedInterfacePort()
	 * @see #getXOR()
	 * @generated
	 */
	EReference getXOR_ProvidedInterfacePort();

	/**
	 * Returns the meta object for enum '{@link fr.ubo.cares.scn.TypeFile <em>Type File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type File</em>'.
	 * @see fr.ubo.cares.scn.TypeFile
	 * @generated
	 */
	EEnum getTypeFile();

	/**
	 * Returns the meta object for enum '{@link fr.ubo.cares.scn.TimeStamp <em>Time Stamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Time Stamp</em>'.
	 * @see fr.ubo.cares.scn.TimeStamp
	 * @generated
	 */
	EEnum getTimeStamp();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScnFactory getScnFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.SimulationImpl <em>Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.SimulationImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimulation()
		 * @generated
		 */
		EClass SIMULATION = eINSTANCE.getSimulation();

		/**
		 * The meta object literal for the '<em><b>Cares System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION__CARES_SYSTEM = eINSTANCE.getSimulation_CaresSystem();

		/**
		 * The meta object literal for the '<em><b>Time Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION__TIME_UNIT = eINSTANCE.getSimulation_TimeUnit();

		/**
		 * The meta object literal for the '<em><b>Important Dates</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION__IMPORTANT_DATES = eINSTANCE.getSimulation_ImportantDates();

		/**
		 * The meta object literal for the '<em><b>Begin</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION__BEGIN = eINSTANCE.getSimulation_Begin();

		/**
		 * The meta object literal for the '<em><b>Scenarios</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION__SCENARIOS = eINSTANCE.getSimulation_Scenarios();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION__END = eINSTANCE.getSimulation_End();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.TimeImpl <em>Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.TimeImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getTime()
		 * @generated
		 */
		EClass TIME = eINSTANCE.getTime();

		/**
		 * The meta object literal for the '<em><b>Starting Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__STARTING_TIME = eINSTANCE.getTime_StartingTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__END_TIME = eINSTANCE.getTime_EndTime();

		/**
		 * The meta object literal for the '<em><b>Step Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIME__STEP_TIME = eINSTANCE.getTime_StepTime();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ScenarioImpl <em>Scenario</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ScenarioImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getScenario()
		 * @generated
		 */
		EClass SCENARIO = eINSTANCE.getScenario();

		/**
		 * The meta object literal for the '<em><b>Begin</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__BEGIN = eINSTANCE.getScenario_Begin();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__EVENTS = eINSTANCE.getScenario_Events();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__END = eINSTANCE.getScenario_End();

		/**
		 * The meta object literal for the '<em><b>Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCENARIO__NUMBER = eINSTANCE.getScenario_Number();

		/**
		 * The meta object literal for the '<em><b>Logs</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCENARIO__LOGS = eINSTANCE.getScenario_Logs();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.EventImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__TIME = eINSTANCE.getEvent_Time();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.BasicEventImpl <em>Basic Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.BasicEventImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getBasicEvent()
		 * @generated
		 */
		EClass BASIC_EVENT = eINSTANCE.getBasicEvent();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASIC_EVENT__ACTIONS = eINSTANCE.getBasicEvent_Actions();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ActionImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ComponentInitializationImpl <em>Component Initialization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ComponentInitializationImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComponentInitialization()
		 * @generated
		 */
		EClass COMPONENT_INITIALIZATION = eINSTANCE.getComponentInitialization();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INITIALIZATION__COMPONENT = eINSTANCE.getComponentInitialization_Component();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ComponentStartImpl <em>Component Start</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ComponentStartImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComponentStart()
		 * @generated
		 */
		EClass COMPONENT_START = eINSTANCE.getComponentStart();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_START__COMPONENT = eINSTANCE.getComponentStart_Component();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ComponentStopImpl <em>Component Stop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ComponentStopImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComponentStop()
		 * @generated
		 */
		EClass COMPONENT_STOP = eINSTANCE.getComponentStop();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_STOP__COMPONENT = eINSTANCE.getComponentStop_Component();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ServiceCallImpl <em>Service Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ServiceCallImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getServiceCall()
		 * @generated
		 */
		EClass SERVICE_CALL = eINSTANCE.getServiceCall();

		/**
		 * The meta object literal for the '<em><b>Function Call</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CALL__FUNCTION_CALL = eINSTANCE.getServiceCall_FunctionCall();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_CALL__COMPONENT = eINSTANCE.getServiceCall_Component();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ParameterChangeImpl <em>Parameter Change</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ParameterChangeImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getParameterChange()
		 * @generated
		 */
		EClass PARAMETER_CHANGE = eINSTANCE.getParameterChange();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_CHANGE__PARAMETER = eINSTANCE.getParameterChange_Parameter();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.SimpleDoubleParameterSetImpl <em>Simple Double Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.SimpleDoubleParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleDoubleParameterSet()
		 * @generated
		 */
		EClass SIMPLE_DOUBLE_PARAMETER_SET = eINSTANCE.getSimpleDoubleParameterSet();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_DOUBLE_PARAMETER_SET__VALUE = eINSTANCE.getSimpleDoubleParameterSet_Value();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.SimpleIntParameterSetImpl <em>Simple Int Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.SimpleIntParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleIntParameterSet()
		 * @generated
		 */
		EClass SIMPLE_INT_PARAMETER_SET = eINSTANCE.getSimpleIntParameterSet();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_INT_PARAMETER_SET__VALUE = eINSTANCE.getSimpleIntParameterSet_Value();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.SimpleBoolParameterSetImpl <em>Simple Bool Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.SimpleBoolParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleBoolParameterSet()
		 * @generated
		 */
		EClass SIMPLE_BOOL_PARAMETER_SET = eINSTANCE.getSimpleBoolParameterSet();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_BOOL_PARAMETER_SET__VALUE = eINSTANCE.getSimpleBoolParameterSet_Value();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.SimpleStringParameterSetImpl <em>Simple String Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.SimpleStringParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getSimpleStringParameterSet()
		 * @generated
		 */
		EClass SIMPLE_STRING_PARAMETER_SET = eINSTANCE.getSimpleStringParameterSet();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_STRING_PARAMETER_SET__VALUE = eINSTANCE.getSimpleStringParameterSet_Value();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ComplexEventImpl <em>Complex Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ComplexEventImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getComplexEvent()
		 * @generated
		 */
		EClass COMPLEX_EVENT = eINSTANCE.getComplexEvent();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_EVENT__PARAMETERS = eINSTANCE.getComplexEvent_Parameters();

		/**
		 * The meta object literal for the '<em><b>Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_EVENT__POLICY = eINSTANCE.getComplexEvent_Policy();

		/**
		 * The meta object literal for the '<em><b>Functions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPLEX_EVENT__FUNCTIONS = eINSTANCE.getComplexEvent_Functions();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.VariableFunctionCallImpl <em>Variable Function Call</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.VariableFunctionCallImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getVariableFunctionCall()
		 * @generated
		 */
		EClass VARIABLE_FUNCTION_CALL = eINSTANCE.getVariableFunctionCall();

		/**
		 * The meta object literal for the '<em><b>Function</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_FUNCTION_CALL__FUNCTION = eINSTANCE.getVariableFunctionCall_Function();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIABLE_FUNCTION_CALL__PARAMETERS = eINSTANCE.getVariableFunctionCall_Parameters();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.DoubleParameterSetImpl <em>Double Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.DoubleParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getDoubleParameterSet()
		 * @generated
		 */
		EClass DOUBLE_PARAMETER_SET = eINSTANCE.getDoubleParameterSet();

		/**
		 * The meta object literal for the '<em><b>Minimum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOUBLE_PARAMETER_SET__MINIMUM = eINSTANCE.getDoubleParameterSet_Minimum();

		/**
		 * The meta object literal for the '<em><b>Maximum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOUBLE_PARAMETER_SET__MAXIMUM = eINSTANCE.getDoubleParameterSet_Maximum();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOUBLE_PARAMETER_SET__STEP = eINSTANCE.getDoubleParameterSet_Step();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.IntParameterSetImpl <em>Int Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.IntParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getIntParameterSet()
		 * @generated
		 */
		EClass INT_PARAMETER_SET = eINSTANCE.getIntParameterSet();

		/**
		 * The meta object literal for the '<em><b>Minimum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_PARAMETER_SET__MINIMUM = eINSTANCE.getIntParameterSet_Minimum();

		/**
		 * The meta object literal for the '<em><b>Maximum</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_PARAMETER_SET__MAXIMUM = eINSTANCE.getIntParameterSet_Maximum();

		/**
		 * The meta object literal for the '<em><b>Step</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_PARAMETER_SET__STEP = eINSTANCE.getIntParameterSet_Step();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.BoolParameterSetImpl <em>Bool Parameter Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.BoolParameterSetImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getBoolParameterSet()
		 * @generated
		 */
		EClass BOOL_PARAMETER_SET = eINSTANCE.getBoolParameterSet();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ExplorationPolicyImpl <em>Exploration Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ExplorationPolicyImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getExplorationPolicy()
		 * @generated
		 */
		EClass EXPLORATION_POLICY = eINSTANCE.getExplorationPolicy();

		/**
		 * The meta object literal for the '<em><b>Constraint Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPLORATION_POLICY__CONSTRAINT_FUNCTIONS = eINSTANCE.getExplorationPolicy_ConstraintFunctions();

		/**
		 * The meta object literal for the '<em><b>Objective Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXPLORATION_POLICY__OBJECTIVE_FUNCTIONS = eINSTANCE.getExplorationPolicy_ObjectiveFunctions();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.ExhaustiveImpl <em>Exhaustive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.ExhaustiveImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getExhaustive()
		 * @generated
		 */
		EClass EXHAUSTIVE = eINSTANCE.getExhaustive();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.RandomImpl <em>Random</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.RandomImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getRandom()
		 * @generated
		 */
		EClass RANDOM = eINSTANCE.getRandom();

		/**
		 * The meta object literal for the '<em><b>Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RANDOM__MAX = eINSTANCE.getRandom_Max();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.OneByOneImpl <em>One By One</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.OneByOneImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getOneByOne()
		 * @generated
		 */
		EClass ONE_BY_ONE = eINSTANCE.getOneByOne();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.DifferentialImpl <em>Differential</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.DifferentialImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getDifferential()
		 * @generated
		 */
		EClass DIFFERENTIAL = eINSTANCE.getDifferential();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.UserPolicyImpl <em>User Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.UserPolicyImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getUserPolicy()
		 * @generated
		 */
		EClass USER_POLICY = eINSTANCE.getUserPolicy();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.LogObservationImpl <em>Log Observation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.LogObservationImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getLogObservation()
		 * @generated
		 */
		EClass LOG_OBSERVATION = eINSTANCE.getLogObservation();

		/**
		 * The meta object literal for the '<em><b>Freq</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOG_OBSERVATION__FREQ = eINSTANCE.getLogObservation_Freq();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG_OBSERVATION__COLUMNS = eINSTANCE.getLogObservation_Columns();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOG_OBSERVATION__TYPE = eINSTANCE.getLogObservation_Type();

		/**
		 * The meta object literal for the '<em><b>Time Stamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOG_OBSERVATION__TIME_STAMP = eINSTANCE.getLogObservation_TimeStamp();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.LogColumnImpl <em>Log Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.LogColumnImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getLogColumn()
		 * @generated
		 */
		EClass LOG_COLUMN = eINSTANCE.getLogColumn();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOG_COLUMN__DATA = eINSTANCE.getLogColumn_Data();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.impl.XORImpl <em>XOR</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.impl.XORImpl
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getXOR()
		 * @generated
		 */
		EClass XOR = eINSTANCE.getXOR();

		/**
		 * The meta object literal for the '<em><b>Required Interface Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XOR__REQUIRED_INTERFACE_PORT = eINSTANCE.getXOR_RequiredInterfacePort();

		/**
		 * The meta object literal for the '<em><b>Provided Interface Port</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XOR__PROVIDED_INTERFACE_PORT = eINSTANCE.getXOR_ProvidedInterfacePort();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.TypeFile <em>Type File</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.TypeFile
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getTypeFile()
		 * @generated
		 */
		EEnum TYPE_FILE = eINSTANCE.getTypeFile();

		/**
		 * The meta object literal for the '{@link fr.ubo.cares.scn.TimeStamp <em>Time Stamp</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.ubo.cares.scn.TimeStamp
		 * @see fr.ubo.cares.scn.impl.ScnPackageImpl#getTimeStamp()
		 * @generated
		 */
		EEnum TIME_STAMP = eINSTANCE.getTimeStamp();

	}

} //ScnPackage
