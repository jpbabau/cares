/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Time</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.Time#getStartingTime <em>Starting Time</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Time#getEndTime <em>End Time</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Time#getStepTime <em>Step Time</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getTime()
 * @model
 * @generated
 */
public interface Time extends EObject {
	/**
	 * Returns the value of the '<em><b>Starting Time</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Starting Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Starting Time</em>' attribute.
	 * @see #setStartingTime(long)
	 * @see fr.ubo.cares.scn.ScnPackage#getTime_StartingTime()
	 * @model default="0" required="true"
	 * @generated
	 */
	long getStartingTime();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Time#getStartingTime <em>Starting Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Starting Time</em>' attribute.
	 * @see #getStartingTime()
	 * @generated
	 */
	void setStartingTime(long value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * The default value is <code>"100"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(long)
	 * @see fr.ubo.cares.scn.ScnPackage#getTime_EndTime()
	 * @model default="100" required="true"
	 * @generated
	 */
	long getEndTime();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Time#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(long value);

	/**
	 * Returns the value of the '<em><b>Step Time</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Time</em>' attribute.
	 * @see #setStepTime(long)
	 * @see fr.ubo.cares.scn.ScnPackage#getTime_StepTime()
	 * @model default="0" required="true"
	 * @generated
	 */
	long getStepTime();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Time#getStepTime <em>Step Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Time</em>' attribute.
	 * @see #getStepTime()
	 * @generated
	 */
	void setStepTime(long value);

} // Time
