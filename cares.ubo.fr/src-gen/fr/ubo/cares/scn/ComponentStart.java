/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.sys.LeafComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Start</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.ComponentStart#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getComponentStart()
 * @model
 * @generated
 */
public interface ComponentStart extends Action {
	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(LeafComponent)
	 * @see fr.ubo.cares.scn.ScnPackage#getComponentStart_Component()
	 * @model required="true"
	 * @generated
	 */
	LeafComponent getComponent();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.ComponentStart#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(LeafComponent value);

} // ComponentStart
