/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.ComplexEvent#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.ComplexEvent#getPolicy <em>Policy</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.ComplexEvent#getFunctions <em>Functions</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getComplexEvent()
 * @model
 * @generated
 */
public interface ComplexEvent extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.ParameterChange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getComplexEvent_Parameters()
	 * @model required="true"
	 * @generated
	 */
	EList<ParameterChange> getParameters();

	/**
	 * Returns the value of the '<em><b>Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Policy</em>' containment reference.
	 * @see #setPolicy(ExplorationPolicy)
	 * @see fr.ubo.cares.scn.ScnPackage#getComplexEvent_Policy()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ExplorationPolicy getPolicy();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.ComplexEvent#getPolicy <em>Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Policy</em>' containment reference.
	 * @see #getPolicy()
	 * @generated
	 */
	void setPolicy(ExplorationPolicy value);

	/**
	 * Returns the value of the '<em><b>Functions</b></em>' reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.VariableFunctionCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functions</em>' reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getComplexEvent_Functions()
	 * @model
	 * @generated
	 */
	EList<VariableFunctionCall> getFunctions();

} // ComplexEvent
