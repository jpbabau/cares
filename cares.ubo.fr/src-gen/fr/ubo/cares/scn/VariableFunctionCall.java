/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.Function;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Function Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.VariableFunctionCall#getFunction <em>Function</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.VariableFunctionCall#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getVariableFunctionCall()
 * @model
 * @generated
 */
public interface VariableFunctionCall extends EObject {
	/**
	 * Returns the value of the '<em><b>Function</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Function</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Function</em>' reference.
	 * @see #setFunction(Function)
	 * @see fr.ubo.cares.scn.ScnPackage#getVariableFunctionCall_Function()
	 * @model required="true"
	 * @generated
	 */
	Function getFunction();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.VariableFunctionCall#getFunction <em>Function</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Function</em>' reference.
	 * @see #getFunction()
	 * @generated
	 */
	void setFunction(Function value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.ParameterChange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getVariableFunctionCall_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterChange> getParameters();

} // VariableFunctionCall
