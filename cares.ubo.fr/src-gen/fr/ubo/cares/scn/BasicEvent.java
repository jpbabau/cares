/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Basic Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.BasicEvent#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getBasicEvent()
 * @model
 * @generated
 */
public interface BasicEvent extends Event {
	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getBasicEvent_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Action> getActions();

} // BasicEvent
