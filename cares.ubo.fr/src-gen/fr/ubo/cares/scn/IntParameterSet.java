/**
 */
package fr.ubo.cares.scn;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Parameter Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.IntParameterSet#getMinimum <em>Minimum</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.IntParameterSet#getMaximum <em>Maximum</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.IntParameterSet#getStep <em>Step</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getIntParameterSet()
 * @model
 * @generated
 */
public interface IntParameterSet extends EObject {
	/**
	 * Returns the value of the '<em><b>Minimum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minimum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minimum</em>' attribute.
	 * @see #setMinimum(int)
	 * @see fr.ubo.cares.scn.ScnPackage#getIntParameterSet_Minimum()
	 * @model required="true"
	 * @generated
	 */
	int getMinimum();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.IntParameterSet#getMinimum <em>Minimum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimum</em>' attribute.
	 * @see #getMinimum()
	 * @generated
	 */
	void setMinimum(int value);

	/**
	 * Returns the value of the '<em><b>Maximum</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maximum</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maximum</em>' attribute.
	 * @see #setMaximum(int)
	 * @see fr.ubo.cares.scn.ScnPackage#getIntParameterSet_Maximum()
	 * @model required="true"
	 * @generated
	 */
	int getMaximum();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.IntParameterSet#getMaximum <em>Maximum</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Maximum</em>' attribute.
	 * @see #getMaximum()
	 * @generated
	 */
	void setMaximum(int value);

	/**
	 * Returns the value of the '<em><b>Step</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step</em>' attribute.
	 * @see #setStep(int)
	 * @see fr.ubo.cares.scn.ScnPackage#getIntParameterSet_Step()
	 * @model required="true"
	 * @generated
	 */
	int getStep();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.IntParameterSet#getStep <em>Step</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step</em>' attribute.
	 * @see #getStep()
	 * @generated
	 */
	void setStep(int value);

} // IntParameterSet
