/**
 */
package fr.ubo.cares.scn;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Differential</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.scn.ScnPackage#getDifferential()
 * @model
 * @generated
 */
public interface Differential extends ExplorationPolicy {
} // Differential
