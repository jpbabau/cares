/**
 */
package fr.ubo.cares.scn;

import fr.ubo.cares.dcl.NamedElement;
import fr.ubo.cares.dcl.TimeUnits;

import fr.ubo.cares.sys.CaresSystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.scn.Simulation#getCaresSystem <em>Cares System</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Simulation#getTimeUnit <em>Time Unit</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Simulation#getImportantDates <em>Important Dates</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Simulation#getBegin <em>Begin</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Simulation#getScenarios <em>Scenarios</em>}</li>
 *   <li>{@link fr.ubo.cares.scn.Simulation#getEnd <em>End</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.scn.ScnPackage#getSimulation()
 * @model
 * @generated
 */
public interface Simulation extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Cares System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cares System</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cares System</em>' reference.
	 * @see #setCaresSystem(CaresSystem)
	 * @see fr.ubo.cares.scn.ScnPackage#getSimulation_CaresSystem()
	 * @model
	 * @generated
	 */
	CaresSystem getCaresSystem();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Simulation#getCaresSystem <em>Cares System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cares System</em>' reference.
	 * @see #getCaresSystem()
	 * @generated
	 */
	void setCaresSystem(CaresSystem value);

	/**
	 * Returns the value of the '<em><b>Time Unit</b></em>' attribute.
	 * The literals are from the enumeration {@link fr.ubo.cares.dcl.TimeUnits}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Unit</em>' attribute.
	 * @see fr.ubo.cares.dcl.TimeUnits
	 * @see #setTimeUnit(TimeUnits)
	 * @see fr.ubo.cares.scn.ScnPackage#getSimulation_TimeUnit()
	 * @model required="true"
	 * @generated
	 */
	TimeUnits getTimeUnit();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Simulation#getTimeUnit <em>Time Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Unit</em>' attribute.
	 * @see fr.ubo.cares.dcl.TimeUnits
	 * @see #getTimeUnit()
	 * @generated
	 */
	void setTimeUnit(TimeUnits value);

	/**
	 * Returns the value of the '<em><b>Important Dates</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Important Dates</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Important Dates</em>' containment reference.
	 * @see #setImportantDates(Time)
	 * @see fr.ubo.cares.scn.ScnPackage#getSimulation_ImportantDates()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Time getImportantDates();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.scn.Simulation#getImportantDates <em>Important Dates</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Important Dates</em>' containment reference.
	 * @see #getImportantDates()
	 * @generated
	 */
	void setImportantDates(Time value);

	/**
	 * Returns the value of the '<em><b>Begin</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Begin</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Begin</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getSimulation_Begin()
	 * @model containment="true"
	 * @generated
	 */
	EList<Action> getBegin();

	/**
	 * Returns the value of the '<em><b>Scenarios</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.Scenario}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenarios</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenarios</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getSimulation_Scenarios()
	 * @model containment="true"
	 * @generated
	 */
	EList<Scenario> getScenarios();

	/**
	 * Returns the value of the '<em><b>End</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.scn.ServiceCall}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' containment reference list.
	 * @see fr.ubo.cares.scn.ScnPackage#getSimulation_End()
	 * @model containment="true"
	 * @generated
	 */
	EList<ServiceCall> getEnd();

} // Simulation
