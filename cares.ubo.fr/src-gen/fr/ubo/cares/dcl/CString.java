/**
 */
package fr.ubo.cares.dcl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CString</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.dcl.DclPackage#getCString()
 * @model
 * @generated
 */
public interface CString extends SimpleType {
} // CString
