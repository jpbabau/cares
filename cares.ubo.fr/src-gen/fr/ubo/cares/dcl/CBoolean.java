/**
 */
package fr.ubo.cares.dcl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CBoolean</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.dcl.DclPackage#getCBoolean()
 * @model
 * @generated
 */
public interface CBoolean extends SimpleType {
} // CBoolean
