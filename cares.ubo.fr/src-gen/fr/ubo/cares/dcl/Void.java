/**
 */
package fr.ubo.cares.dcl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Void</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.dcl.DclPackage#getVoid()
 * @model
 * @generated
 */
public interface Void extends SimpleType {
} // Void
