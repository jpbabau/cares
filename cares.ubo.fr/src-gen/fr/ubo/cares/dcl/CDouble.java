/**
 */
package fr.ubo.cares.dcl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CDouble</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.dcl.DclPackage#getCDouble()
 * @model
 * @generated
 */
public interface CDouble extends SimpleType {
} // CDouble
