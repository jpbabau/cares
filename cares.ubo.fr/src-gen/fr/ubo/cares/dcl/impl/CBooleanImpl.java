/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.CBoolean;
import fr.ubo.cares.dcl.DclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CBoolean</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CBooleanImpl extends SimpleTypeImpl implements CBoolean {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CBooleanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.CBOOLEAN;
	}

} //CBooleanImpl
