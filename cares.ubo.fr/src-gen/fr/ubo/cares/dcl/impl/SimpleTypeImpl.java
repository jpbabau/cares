/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.DclPackage;
import fr.ubo.cares.dcl.SimpleType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SimpleTypeImpl extends TypeImpl implements SimpleType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimpleTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.SIMPLE_TYPE;
	}

} //SimpleTypeImpl
