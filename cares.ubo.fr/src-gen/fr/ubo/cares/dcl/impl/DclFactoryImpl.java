/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.CBoolean;
import fr.ubo.cares.dcl.CDouble;
import fr.ubo.cares.dcl.CInt;
import fr.ubo.cares.dcl.CString;
import fr.ubo.cares.dcl.DclFactory;
import fr.ubo.cares.dcl.DclPackage;
import fr.ubo.cares.dcl.Declaration;
import fr.ubo.cares.dcl.Function;
import fr.ubo.cares.dcl.FunctionCall;
import fr.ubo.cares.dcl.Input;
import fr.ubo.cares.dcl.Interface;
import fr.ubo.cares.dcl.InterfacePort;
import fr.ubo.cares.dcl.JavaType;
import fr.ubo.cares.dcl.LeafComponentType;
import fr.ubo.cares.dcl.Output;
import fr.ubo.cares.dcl.ParameterDeclaration;
import fr.ubo.cares.dcl.ParameterInstanciation;
import fr.ubo.cares.dcl.ReturnType;
import fr.ubo.cares.dcl.StructType;
import fr.ubo.cares.dcl.TimeUnits;
import fr.ubo.cares.dcl.TimeableObject;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DclFactoryImpl extends EFactoryImpl implements DclFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DclFactory init() {
		try {
			DclFactory theDclFactory = (DclFactory)EPackage.Registry.INSTANCE.getEFactory(DclPackage.eNS_URI);
			if (theDclFactory != null) {
				return theDclFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DclFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DclFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DclPackage.DECLARATION: return createDeclaration();
			case DclPackage.STRUCT_TYPE: return createStructType();
			case DclPackage.PARAMETER_DECLARATION: return createParameterDeclaration();
			case DclPackage.TIMEABLE_OBJECT: return createTimeableObject();
			case DclPackage.INTERFACE: return createInterface();
			case DclPackage.FUNCTION: return createFunction();
			case DclPackage.RETURN_TYPE: return createReturnType();
			case DclPackage.PARAMETER_INSTANCIATION: return createParameterInstanciation();
			case DclPackage.FUNCTION_CALL: return createFunctionCall();
			case DclPackage.INTERFACE_PORT: return createInterfacePort();
			case DclPackage.LEAF_COMPONENT_TYPE: return createLeafComponentType();
			case DclPackage.INPUT: return createInput();
			case DclPackage.OUTPUT: return createOutput();
			case DclPackage.CINT: return createCInt();
			case DclPackage.CDOUBLE: return createCDouble();
			case DclPackage.CBOOLEAN: return createCBoolean();
			case DclPackage.CSTRING: return createCString();
			case DclPackage.JAVA_TYPE: return createJavaType();
			case DclPackage.VOID: return createVoid();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DclPackage.TIME_UNITS:
				return createTimeUnitsFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DclPackage.TIME_UNITS:
				return convertTimeUnitsToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Declaration createDeclaration() {
		DeclarationImpl declaration = new DeclarationImpl();
		return declaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StructType createStructType() {
		StructTypeImpl structType = new StructTypeImpl();
		return structType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterDeclaration createParameterDeclaration() {
		ParameterDeclarationImpl parameterDeclaration = new ParameterDeclarationImpl();
		return parameterDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeableObject createTimeableObject() {
		TimeableObjectImpl timeableObject = new TimeableObjectImpl();
		return timeableObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createInterface() {
		InterfaceImpl interface_ = new InterfaceImpl();
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function createFunction() {
		FunctionImpl function = new FunctionImpl();
		return function;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReturnType createReturnType() {
		ReturnTypeImpl returnType = new ReturnTypeImpl();
		return returnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ParameterInstanciation createParameterInstanciation() {
		ParameterInstanciationImpl parameterInstanciation = new ParameterInstanciationImpl();
		return parameterInstanciation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FunctionCall createFunctionCall() {
		FunctionCallImpl functionCall = new FunctionCallImpl();
		return functionCall;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InterfacePort createInterfacePort() {
		InterfacePortImpl interfacePort = new InterfacePortImpl();
		return interfacePort;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafComponentType createLeafComponentType() {
		LeafComponentTypeImpl leafComponentType = new LeafComponentTypeImpl();
		return leafComponentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Input createInput() {
		InputImpl input = new InputImpl();
		return input;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Output createOutput() {
		OutputImpl output = new OutputImpl();
		return output;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CInt createCInt() {
		CIntImpl cInt = new CIntImpl();
		return cInt;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CDouble createCDouble() {
		CDoubleImpl cDouble = new CDoubleImpl();
		return cDouble;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CBoolean createCBoolean() {
		CBooleanImpl cBoolean = new CBooleanImpl();
		return cBoolean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CString createCString() {
		CStringImpl cString = new CStringImpl();
		return cString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaType createJavaType() {
		JavaTypeImpl javaType = new JavaTypeImpl();
		return javaType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public fr.ubo.cares.dcl.Void createVoid() {
		VoidImpl void_ = new VoidImpl();
		return void_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnits createTimeUnitsFromString(EDataType eDataType, String initialValue) {
		TimeUnits result = TimeUnits.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTimeUnitsToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DclPackage getDclPackage() {
		return (DclPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DclPackage getPackage() {
		return DclPackage.eINSTANCE;
	}

} //DclFactoryImpl
