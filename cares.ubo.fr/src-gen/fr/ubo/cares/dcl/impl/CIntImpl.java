/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.CInt;
import fr.ubo.cares.dcl.DclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CInt</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CIntImpl extends SimpleTypeImpl implements CInt {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CIntImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.CINT;
	}

} //CIntImpl
