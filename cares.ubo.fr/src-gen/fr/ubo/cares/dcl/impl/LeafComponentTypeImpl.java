/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.DclPackage;
import fr.ubo.cares.dcl.Input;
import fr.ubo.cares.dcl.InterfacePort;
import fr.ubo.cares.dcl.LeafComponentType;
import fr.ubo.cares.dcl.Output;
import fr.ubo.cares.dcl.ParameterDeclaration;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Leaf Component Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.dcl.impl.LeafComponentTypeImpl#getRequiredInterface <em>Required Interface</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.impl.LeafComponentTypeImpl#getProvidedInterface <em>Provided Interface</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.impl.LeafComponentTypeImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.impl.LeafComponentTypeImpl#getInputs <em>Inputs</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.impl.LeafComponentTypeImpl#getOutputs <em>Outputs</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LeafComponentTypeImpl extends NamedElementImpl implements LeafComponentType {
	/**
	 * The cached value of the '{@link #getRequiredInterface() <em>Required Interface</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredInterface()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfacePort> requiredInterface;

	/**
	 * The cached value of the '{@link #getProvidedInterface() <em>Provided Interface</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProvidedInterface()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfacePort> providedInterface;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<ParameterDeclaration> parameters;

	/**
	 * The cached value of the '{@link #getInputs() <em>Inputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInputs()
	 * @generated
	 * @ordered
	 */
	protected EList<Input> inputs;

	/**
	 * The cached value of the '{@link #getOutputs() <em>Outputs</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutputs()
	 * @generated
	 * @ordered
	 */
	protected EList<Output> outputs;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafComponentTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.LEAF_COMPONENT_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfacePort> getRequiredInterface() {
		if (requiredInterface == null) {
			requiredInterface = new EObjectContainmentEList<InterfacePort>(InterfacePort.class, this, DclPackage.LEAF_COMPONENT_TYPE__REQUIRED_INTERFACE);
		}
		return requiredInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfacePort> getProvidedInterface() {
		if (providedInterface == null) {
			providedInterface = new EObjectContainmentEList<InterfacePort>(InterfacePort.class, this, DclPackage.LEAF_COMPONENT_TYPE__PROVIDED_INTERFACE);
		}
		return providedInterface;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ParameterDeclaration> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<ParameterDeclaration>(ParameterDeclaration.class, this, DclPackage.LEAF_COMPONENT_TYPE__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Input> getInputs() {
		if (inputs == null) {
			inputs = new EObjectContainmentEList<Input>(Input.class, this, DclPackage.LEAF_COMPONENT_TYPE__INPUTS);
		}
		return inputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Output> getOutputs() {
		if (outputs == null) {
			outputs = new EObjectContainmentEList<Output>(Output.class, this, DclPackage.LEAF_COMPONENT_TYPE__OUTPUTS);
		}
		return outputs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DclPackage.LEAF_COMPONENT_TYPE__REQUIRED_INTERFACE:
				return ((InternalEList<?>)getRequiredInterface()).basicRemove(otherEnd, msgs);
			case DclPackage.LEAF_COMPONENT_TYPE__PROVIDED_INTERFACE:
				return ((InternalEList<?>)getProvidedInterface()).basicRemove(otherEnd, msgs);
			case DclPackage.LEAF_COMPONENT_TYPE__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case DclPackage.LEAF_COMPONENT_TYPE__INPUTS:
				return ((InternalEList<?>)getInputs()).basicRemove(otherEnd, msgs);
			case DclPackage.LEAF_COMPONENT_TYPE__OUTPUTS:
				return ((InternalEList<?>)getOutputs()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DclPackage.LEAF_COMPONENT_TYPE__REQUIRED_INTERFACE:
				return getRequiredInterface();
			case DclPackage.LEAF_COMPONENT_TYPE__PROVIDED_INTERFACE:
				return getProvidedInterface();
			case DclPackage.LEAF_COMPONENT_TYPE__PARAMETERS:
				return getParameters();
			case DclPackage.LEAF_COMPONENT_TYPE__INPUTS:
				return getInputs();
			case DclPackage.LEAF_COMPONENT_TYPE__OUTPUTS:
				return getOutputs();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DclPackage.LEAF_COMPONENT_TYPE__REQUIRED_INTERFACE:
				getRequiredInterface().clear();
				getRequiredInterface().addAll((Collection<? extends InterfacePort>)newValue);
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__PROVIDED_INTERFACE:
				getProvidedInterface().clear();
				getProvidedInterface().addAll((Collection<? extends InterfacePort>)newValue);
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends ParameterDeclaration>)newValue);
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__INPUTS:
				getInputs().clear();
				getInputs().addAll((Collection<? extends Input>)newValue);
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__OUTPUTS:
				getOutputs().clear();
				getOutputs().addAll((Collection<? extends Output>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DclPackage.LEAF_COMPONENT_TYPE__REQUIRED_INTERFACE:
				getRequiredInterface().clear();
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__PROVIDED_INTERFACE:
				getProvidedInterface().clear();
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__PARAMETERS:
				getParameters().clear();
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__INPUTS:
				getInputs().clear();
				return;
			case DclPackage.LEAF_COMPONENT_TYPE__OUTPUTS:
				getOutputs().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DclPackage.LEAF_COMPONENT_TYPE__REQUIRED_INTERFACE:
				return requiredInterface != null && !requiredInterface.isEmpty();
			case DclPackage.LEAF_COMPONENT_TYPE__PROVIDED_INTERFACE:
				return providedInterface != null && !providedInterface.isEmpty();
			case DclPackage.LEAF_COMPONENT_TYPE__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case DclPackage.LEAF_COMPONENT_TYPE__INPUTS:
				return inputs != null && !inputs.isEmpty();
			case DclPackage.LEAF_COMPONENT_TYPE__OUTPUTS:
				return outputs != null && !outputs.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LeafComponentTypeImpl
