/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.CString;
import fr.ubo.cares.dcl.DclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CString</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CStringImpl extends SimpleTypeImpl implements CString {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CStringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.CSTRING;
	}

} //CStringImpl
