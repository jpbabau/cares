/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.CDouble;
import fr.ubo.cares.dcl.DclPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CDouble</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDoubleImpl extends SimpleTypeImpl implements CDouble {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CDoubleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.CDOUBLE;
	}

} //CDoubleImpl
