/**
 */
package fr.ubo.cares.dcl.impl;

import fr.ubo.cares.dcl.DclPackage;
import fr.ubo.cares.dcl.Declaration;
import fr.ubo.cares.dcl.Interface;
import fr.ubo.cares.dcl.LeafComponentType;
import fr.ubo.cares.dcl.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.dcl.impl.DeclarationImpl#getTypes <em>Types</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.impl.DeclarationImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.impl.DeclarationImpl#getLeafComponentTypes <em>Leaf Component Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeclarationImpl extends NamedElementImpl implements Declaration {
	/**
	 * The cached value of the '{@link #getTypes() <em>Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<Type> types;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Interface> interfaces;

	/**
	 * The cached value of the '{@link #getLeafComponentTypes() <em>Leaf Component Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeafComponentTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<LeafComponentType> leafComponentTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DclPackage.Literals.DECLARATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Type> getTypes() {
		if (types == null) {
			types = new EObjectContainmentEList<Type>(Type.class, this, DclPackage.DECLARATION__TYPES);
		}
		return types;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interface> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectContainmentEList<Interface>(Interface.class, this, DclPackage.DECLARATION__INTERFACES);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LeafComponentType> getLeafComponentTypes() {
		if (leafComponentTypes == null) {
			leafComponentTypes = new EObjectContainmentEList<LeafComponentType>(LeafComponentType.class, this, DclPackage.DECLARATION__LEAF_COMPONENT_TYPES);
		}
		return leafComponentTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DclPackage.DECLARATION__TYPES:
				return ((InternalEList<?>)getTypes()).basicRemove(otherEnd, msgs);
			case DclPackage.DECLARATION__INTERFACES:
				return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
			case DclPackage.DECLARATION__LEAF_COMPONENT_TYPES:
				return ((InternalEList<?>)getLeafComponentTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DclPackage.DECLARATION__TYPES:
				return getTypes();
			case DclPackage.DECLARATION__INTERFACES:
				return getInterfaces();
			case DclPackage.DECLARATION__LEAF_COMPONENT_TYPES:
				return getLeafComponentTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DclPackage.DECLARATION__TYPES:
				getTypes().clear();
				getTypes().addAll((Collection<? extends Type>)newValue);
				return;
			case DclPackage.DECLARATION__INTERFACES:
				getInterfaces().clear();
				getInterfaces().addAll((Collection<? extends Interface>)newValue);
				return;
			case DclPackage.DECLARATION__LEAF_COMPONENT_TYPES:
				getLeafComponentTypes().clear();
				getLeafComponentTypes().addAll((Collection<? extends LeafComponentType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DclPackage.DECLARATION__TYPES:
				getTypes().clear();
				return;
			case DclPackage.DECLARATION__INTERFACES:
				getInterfaces().clear();
				return;
			case DclPackage.DECLARATION__LEAF_COMPONENT_TYPES:
				getLeafComponentTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DclPackage.DECLARATION__TYPES:
				return types != null && !types.isEmpty();
			case DclPackage.DECLARATION__INTERFACES:
				return interfaces != null && !interfaces.isEmpty();
			case DclPackage.DECLARATION__LEAF_COMPONENT_TYPES:
				return leafComponentTypes != null && !leafComponentTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DeclarationImpl
