/**
 */
package fr.ubo.cares.dcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf Component Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.dcl.LeafComponentType#getRequiredInterface <em>Required Interface</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.LeafComponentType#getProvidedInterface <em>Provided Interface</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.LeafComponentType#getParameters <em>Parameters</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.LeafComponentType#getInputs <em>Inputs</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.LeafComponentType#getOutputs <em>Outputs</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.dcl.DclPackage#getLeafComponentType()
 * @model
 * @generated
 */
public interface LeafComponentType extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Required Interface</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.InterfacePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Interface</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Interface</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getLeafComponentType_RequiredInterface()
	 * @model containment="true"
	 * @generated
	 */
	EList<InterfacePort> getRequiredInterface();

	/**
	 * Returns the value of the '<em><b>Provided Interface</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.InterfacePort}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Interface</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Interface</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getLeafComponentType_ProvidedInterface()
	 * @model containment="true"
	 * @generated
	 */
	EList<InterfacePort> getProvidedInterface();

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.ParameterDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getLeafComponentType_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterDeclaration> getParameters();

	/**
	 * Returns the value of the '<em><b>Inputs</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Input}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inputs</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getLeafComponentType_Inputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Input> getInputs();

	/**
	 * Returns the value of the '<em><b>Outputs</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Output}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Outputs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Outputs</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getLeafComponentType_Outputs()
	 * @model containment="true"
	 * @generated
	 */
	EList<Output> getOutputs();

} // LeafComponentType
