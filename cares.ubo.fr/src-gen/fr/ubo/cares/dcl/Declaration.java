/**
 */
package fr.ubo.cares.dcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.dcl.Declaration#getTypes <em>Types</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.Declaration#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link fr.ubo.cares.dcl.Declaration#getLeafComponentTypes <em>Leaf Component Types</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.dcl.DclPackage#getDeclaration()
 * @model
 * @generated
 */
public interface Declaration extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Types</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Types</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getDeclaration_Types()
	 * @model containment="true"
	 * @generated
	 */
	EList<Type> getTypes();

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interfaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getDeclaration_Interfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<Interface> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Leaf Component Types</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.LeafComponentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leaf Component Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leaf Component Types</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getDeclaration_LeafComponentTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<LeafComponentType> getLeafComponentTypes();

} // Declaration
