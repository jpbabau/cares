/**
 */
package fr.ubo.cares.dcl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Struct Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.dcl.StructType#getFields <em>Fields</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.dcl.DclPackage#getStructType()
 * @model
 * @generated
 */
public interface StructType extends Type {
	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link fr.ubo.cares.dcl.ParameterDeclaration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fields</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see fr.ubo.cares.dcl.DclPackage#getStructType_Fields()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<ParameterDeclaration> getFields();

} // StructType
