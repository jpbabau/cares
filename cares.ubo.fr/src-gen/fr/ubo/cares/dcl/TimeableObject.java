/**
 */
package fr.ubo.cares.dcl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timeable Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.ubo.cares.dcl.TimeableObject#getFrequency <em>Frequency</em>}</li>
 * </ul>
 *
 * @see fr.ubo.cares.dcl.DclPackage#getTimeableObject()
 * @model
 * @generated
 */
public interface TimeableObject extends EObject {
	/**
	 * Returns the value of the '<em><b>Frequency</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Frequency</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Frequency</em>' attribute.
	 * @see #setFrequency(double)
	 * @see fr.ubo.cares.dcl.DclPackage#getTimeableObject_Frequency()
	 * @model required="true"
	 * @generated
	 */
	double getFrequency();

	/**
	 * Sets the value of the '{@link fr.ubo.cares.dcl.TimeableObject#getFrequency <em>Frequency</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Frequency</em>' attribute.
	 * @see #getFrequency()
	 * @generated
	 */
	void setFrequency(double value);

} // TimeableObject
