/**
 */
package fr.ubo.cares.dcl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CInt</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see fr.ubo.cares.dcl.DclPackage#getCInt()
 * @model
 * @generated
 */
public interface CInt extends SimpleType {
} // CInt
