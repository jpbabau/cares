package xtext.dcl.cares.ubo.fr.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import xtext.dcl.cares.ubo.fr.services.DeclarationGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDeclarationParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_STRING", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Declaration'", "'{'", "'types'", "'}'", "'interfaces'", "'leafComponentTypes'", "'('", "'Void'", "')'", "';'", "'CInt'", "'CDouble'", "'CBoolean'", "'CString'", "'.'", "'Record'", "'['", "','", "']'", "'-'", "'Interface'", "'LeafComponentType'", "'requiredInterfaces'", "'providedInterfaces'", "'inputs'", "'outputs'", "'parameters'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__37=37;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=6;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDeclarationParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDeclarationParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDeclarationParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDeclaration.g"; }



     	private DeclarationGrammarAccess grammarAccess;

        public InternalDeclarationParser(TokenStream input, DeclarationGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Declaration";
       	}

       	@Override
       	protected DeclarationGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDeclaration"
    // InternalDeclaration.g:64:1: entryRuleDeclaration returns [EObject current=null] : iv_ruleDeclaration= ruleDeclaration EOF ;
    public final EObject entryRuleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaration = null;


        try {
            // InternalDeclaration.g:64:52: (iv_ruleDeclaration= ruleDeclaration EOF )
            // InternalDeclaration.g:65:2: iv_ruleDeclaration= ruleDeclaration EOF
            {
             newCompositeNode(grammarAccess.getDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclaration=ruleDeclaration();

            state._fsp--;

             current =iv_ruleDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // InternalDeclaration.g:71:1: ruleDeclaration returns [EObject current=null] : ( () otherlv_1= 'Declaration' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}' )? (otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}' )? (otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}' )? otherlv_19= '}' ) ;
    public final EObject ruleDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_types_6_0 = null;

        EObject lv_types_7_0 = null;

        EObject lv_interfaces_11_0 = null;

        EObject lv_interfaces_12_0 = null;

        EObject lv_leafComponentTypes_16_0 = null;

        EObject lv_leafComponentTypes_17_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:77:2: ( ( () otherlv_1= 'Declaration' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}' )? (otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}' )? (otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}' )? otherlv_19= '}' ) )
            // InternalDeclaration.g:78:2: ( () otherlv_1= 'Declaration' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}' )? (otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}' )? (otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}' )? otherlv_19= '}' )
            {
            // InternalDeclaration.g:78:2: ( () otherlv_1= 'Declaration' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}' )? (otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}' )? (otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}' )? otherlv_19= '}' )
            // InternalDeclaration.g:79:3: () otherlv_1= 'Declaration' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}' )? (otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}' )? (otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}' )? otherlv_19= '}'
            {
            // InternalDeclaration.g:79:3: ()
            // InternalDeclaration.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDeclarationAccess().getDeclarationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getDeclarationAccess().getDeclarationKeyword_1());
            		
            // InternalDeclaration.g:90:3: ( (lv_name_2_0= ruleEString ) )
            // InternalDeclaration.g:91:4: (lv_name_2_0= ruleEString )
            {
            // InternalDeclaration.g:91:4: (lv_name_2_0= ruleEString )
            // InternalDeclaration.g:92:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getDeclarationAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDeclarationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_3, grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalDeclaration.g:113:3: (otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDeclaration.g:114:4: otherlv_4= 'types' otherlv_5= '{' ( (lv_types_6_0= ruleType ) ) ( (lv_types_7_0= ruleType ) )* otherlv_8= '}'
                    {
                    otherlv_4=(Token)match(input,13,FOLLOW_4); 

                    				newLeafNode(otherlv_4, grammarAccess.getDeclarationAccess().getTypesKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,12,FOLLOW_6); 

                    				newLeafNode(otherlv_5, grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalDeclaration.g:122:4: ( (lv_types_6_0= ruleType ) )
                    // InternalDeclaration.g:123:5: (lv_types_6_0= ruleType )
                    {
                    // InternalDeclaration.g:123:5: (lv_types_6_0= ruleType )
                    // InternalDeclaration.g:124:6: lv_types_6_0= ruleType
                    {

                    						newCompositeNode(grammarAccess.getDeclarationAccess().getTypesTypeParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_7);
                    lv_types_6_0=ruleType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeclarationRule());
                    						}
                    						add(
                    							current,
                    							"types",
                    							lv_types_6_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.Type");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:141:4: ( (lv_types_7_0= ruleType ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( ((LA1_0>=RULE_STRING && LA1_0<=RULE_ID)||LA1_0==26) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalDeclaration.g:142:5: (lv_types_7_0= ruleType )
                    	    {
                    	    // InternalDeclaration.g:142:5: (lv_types_7_0= ruleType )
                    	    // InternalDeclaration.g:143:6: lv_types_7_0= ruleType
                    	    {

                    	    						newCompositeNode(grammarAccess.getDeclarationAccess().getTypesTypeParserRuleCall_4_3_0());
                    	    					
                    	    pushFollow(FOLLOW_7);
                    	    lv_types_7_0=ruleType();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDeclarationRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"types",
                    	    							lv_types_7_0,
                    	    							"xtext.dcl.cares.ubo.fr.Declaration.Type");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,14,FOLLOW_8); 

                    				newLeafNode(otherlv_8, grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:165:3: (otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==15) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalDeclaration.g:166:4: otherlv_9= 'interfaces' otherlv_10= '{' ( (lv_interfaces_11_0= ruleInterface ) ) ( (lv_interfaces_12_0= ruleInterface ) )* otherlv_13= '}'
                    {
                    otherlv_9=(Token)match(input,15,FOLLOW_4); 

                    				newLeafNode(otherlv_9, grammarAccess.getDeclarationAccess().getInterfacesKeyword_5_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_9); 

                    				newLeafNode(otherlv_10, grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalDeclaration.g:174:4: ( (lv_interfaces_11_0= ruleInterface ) )
                    // InternalDeclaration.g:175:5: (lv_interfaces_11_0= ruleInterface )
                    {
                    // InternalDeclaration.g:175:5: (lv_interfaces_11_0= ruleInterface )
                    // InternalDeclaration.g:176:6: lv_interfaces_11_0= ruleInterface
                    {

                    						newCompositeNode(grammarAccess.getDeclarationAccess().getInterfacesInterfaceParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_10);
                    lv_interfaces_11_0=ruleInterface();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeclarationRule());
                    						}
                    						add(
                    							current,
                    							"interfaces",
                    							lv_interfaces_11_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.Interface");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:193:4: ( (lv_interfaces_12_0= ruleInterface ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==31) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalDeclaration.g:194:5: (lv_interfaces_12_0= ruleInterface )
                    	    {
                    	    // InternalDeclaration.g:194:5: (lv_interfaces_12_0= ruleInterface )
                    	    // InternalDeclaration.g:195:6: lv_interfaces_12_0= ruleInterface
                    	    {

                    	    						newCompositeNode(grammarAccess.getDeclarationAccess().getInterfacesInterfaceParserRuleCall_5_3_0());
                    	    					
                    	    pushFollow(FOLLOW_10);
                    	    lv_interfaces_12_0=ruleInterface();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDeclarationRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"interfaces",
                    	    							lv_interfaces_12_0,
                    	    							"xtext.dcl.cares.ubo.fr.Declaration.Interface");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,14,FOLLOW_11); 

                    				newLeafNode(otherlv_13, grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:217:3: (otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalDeclaration.g:218:4: otherlv_14= 'leafComponentTypes' otherlv_15= '{' ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) ) ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )* otherlv_18= '}'
                    {
                    otherlv_14=(Token)match(input,16,FOLLOW_4); 

                    				newLeafNode(otherlv_14, grammarAccess.getDeclarationAccess().getLeafComponentTypesKeyword_6_0());
                    			
                    otherlv_15=(Token)match(input,12,FOLLOW_12); 

                    				newLeafNode(otherlv_15, grammarAccess.getDeclarationAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalDeclaration.g:226:4: ( (lv_leafComponentTypes_16_0= ruleLeafComponentType ) )
                    // InternalDeclaration.g:227:5: (lv_leafComponentTypes_16_0= ruleLeafComponentType )
                    {
                    // InternalDeclaration.g:227:5: (lv_leafComponentTypes_16_0= ruleLeafComponentType )
                    // InternalDeclaration.g:228:6: lv_leafComponentTypes_16_0= ruleLeafComponentType
                    {

                    						newCompositeNode(grammarAccess.getDeclarationAccess().getLeafComponentTypesLeafComponentTypeParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_leafComponentTypes_16_0=ruleLeafComponentType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeclarationRule());
                    						}
                    						add(
                    							current,
                    							"leafComponentTypes",
                    							lv_leafComponentTypes_16_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.LeafComponentType");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:245:4: ( (lv_leafComponentTypes_17_0= ruleLeafComponentType ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==32) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalDeclaration.g:246:5: (lv_leafComponentTypes_17_0= ruleLeafComponentType )
                    	    {
                    	    // InternalDeclaration.g:246:5: (lv_leafComponentTypes_17_0= ruleLeafComponentType )
                    	    // InternalDeclaration.g:247:6: lv_leafComponentTypes_17_0= ruleLeafComponentType
                    	    {

                    	    						newCompositeNode(grammarAccess.getDeclarationAccess().getLeafComponentTypesLeafComponentTypeParserRuleCall_6_3_0());
                    	    					
                    	    pushFollow(FOLLOW_13);
                    	    lv_leafComponentTypes_17_0=ruleLeafComponentType();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDeclarationRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"leafComponentTypes",
                    	    							lv_leafComponentTypes_17_0,
                    	    							"xtext.dcl.cares.ubo.fr.Declaration.LeafComponentType");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_18=(Token)match(input,14,FOLLOW_14); 

                    				newLeafNode(otherlv_18, grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            otherlv_19=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_19, grammarAccess.getDeclarationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleType"
    // InternalDeclaration.g:277:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalDeclaration.g:277:45: (iv_ruleType= ruleType EOF )
            // InternalDeclaration.g:278:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalDeclaration.g:284:1: ruleType returns [EObject current=null] : (this_StructType_0= ruleStructType | this_CInt_1= ruleCInt | this_CDouble_2= ruleCDouble | this_CBoolean_3= ruleCBoolean | this_CString_4= ruleCString | this_JavaType_5= ruleJavaType | this_Void_6= ruleVoid ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_StructType_0 = null;

        EObject this_CInt_1 = null;

        EObject this_CDouble_2 = null;

        EObject this_CBoolean_3 = null;

        EObject this_CString_4 = null;

        EObject this_JavaType_5 = null;

        EObject this_Void_6 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:290:2: ( (this_StructType_0= ruleStructType | this_CInt_1= ruleCInt | this_CDouble_2= ruleCDouble | this_CBoolean_3= ruleCBoolean | this_CString_4= ruleCString | this_JavaType_5= ruleJavaType | this_Void_6= ruleVoid ) )
            // InternalDeclaration.g:291:2: (this_StructType_0= ruleStructType | this_CInt_1= ruleCInt | this_CDouble_2= ruleCDouble | this_CBoolean_3= ruleCBoolean | this_CString_4= ruleCString | this_JavaType_5= ruleJavaType | this_Void_6= ruleVoid )
            {
            // InternalDeclaration.g:291:2: (this_StructType_0= ruleStructType | this_CInt_1= ruleCInt | this_CDouble_2= ruleCDouble | this_CBoolean_3= ruleCBoolean | this_CString_4= ruleCString | this_JavaType_5= ruleJavaType | this_Void_6= ruleVoid )
            int alt7=7;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // InternalDeclaration.g:292:3: this_StructType_0= ruleStructType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getStructTypeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_StructType_0=ruleStructType();

                    state._fsp--;


                    			current = this_StructType_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDeclaration.g:301:3: this_CInt_1= ruleCInt
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getCIntParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_CInt_1=ruleCInt();

                    state._fsp--;


                    			current = this_CInt_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDeclaration.g:310:3: this_CDouble_2= ruleCDouble
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getCDoubleParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_CDouble_2=ruleCDouble();

                    state._fsp--;


                    			current = this_CDouble_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalDeclaration.g:319:3: this_CBoolean_3= ruleCBoolean
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getCBooleanParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_CBoolean_3=ruleCBoolean();

                    state._fsp--;


                    			current = this_CBoolean_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalDeclaration.g:328:3: this_CString_4= ruleCString
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getCStringParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_CString_4=ruleCString();

                    state._fsp--;


                    			current = this_CString_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalDeclaration.g:337:3: this_JavaType_5= ruleJavaType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getJavaTypeParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_JavaType_5=ruleJavaType();

                    state._fsp--;


                    			current = this_JavaType_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalDeclaration.g:346:3: this_Void_6= ruleVoid
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getVoidParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_Void_6=ruleVoid();

                    state._fsp--;


                    			current = this_Void_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleVoid"
    // InternalDeclaration.g:358:1: entryRuleVoid returns [EObject current=null] : iv_ruleVoid= ruleVoid EOF ;
    public final EObject entryRuleVoid() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVoid = null;


        try {
            // InternalDeclaration.g:358:45: (iv_ruleVoid= ruleVoid EOF )
            // InternalDeclaration.g:359:2: iv_ruleVoid= ruleVoid EOF
            {
             newCompositeNode(grammarAccess.getVoidRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVoid=ruleVoid();

            state._fsp--;

             current =iv_ruleVoid; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVoid"


    // $ANTLR start "ruleVoid"
    // InternalDeclaration.g:365:1: ruleVoid returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'Void' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleVoid() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:371:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'Void' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalDeclaration.g:372:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'Void' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalDeclaration.g:372:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'Void' otherlv_4= ')' otherlv_5= ';' )
            // InternalDeclaration.g:373:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'Void' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalDeclaration.g:373:3: ()
            // InternalDeclaration.g:374:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getVoidAccess().getVoidAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:380:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:381:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:381:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:382:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getVoidAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVoidRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getVoidAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,18,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getVoidAccess().getVoidKeyword_3());
            		
            otherlv_4=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_4, grammarAccess.getVoidAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getVoidAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVoid"


    // $ANTLR start "entryRuleCInt"
    // InternalDeclaration.g:419:1: entryRuleCInt returns [EObject current=null] : iv_ruleCInt= ruleCInt EOF ;
    public final EObject entryRuleCInt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCInt = null;


        try {
            // InternalDeclaration.g:419:45: (iv_ruleCInt= ruleCInt EOF )
            // InternalDeclaration.g:420:2: iv_ruleCInt= ruleCInt EOF
            {
             newCompositeNode(grammarAccess.getCIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCInt=ruleCInt();

            state._fsp--;

             current =iv_ruleCInt; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCInt"


    // $ANTLR start "ruleCInt"
    // InternalDeclaration.g:426:1: ruleCInt returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCInt() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:432:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalDeclaration.g:433:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalDeclaration.g:433:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' )
            // InternalDeclaration.g:434:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalDeclaration.g:434:3: ()
            // InternalDeclaration.g:435:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCIntAccess().getCIntAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:441:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:442:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:442:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:443:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCIntAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCIntRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_19); 

            			newLeafNode(otherlv_2, grammarAccess.getCIntAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,21,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getCIntAccess().getCIntKeyword_3());
            		
            otherlv_4=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_4, grammarAccess.getCIntAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCIntAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCInt"


    // $ANTLR start "entryRuleCDouble"
    // InternalDeclaration.g:480:1: entryRuleCDouble returns [EObject current=null] : iv_ruleCDouble= ruleCDouble EOF ;
    public final EObject entryRuleCDouble() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCDouble = null;


        try {
            // InternalDeclaration.g:480:48: (iv_ruleCDouble= ruleCDouble EOF )
            // InternalDeclaration.g:481:2: iv_ruleCDouble= ruleCDouble EOF
            {
             newCompositeNode(grammarAccess.getCDoubleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCDouble=ruleCDouble();

            state._fsp--;

             current =iv_ruleCDouble; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCDouble"


    // $ANTLR start "ruleCDouble"
    // InternalDeclaration.g:487:1: ruleCDouble returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCDouble() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:493:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalDeclaration.g:494:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalDeclaration.g:494:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' )
            // InternalDeclaration.g:495:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalDeclaration.g:495:3: ()
            // InternalDeclaration.g:496:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCDoubleAccess().getCDoubleAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:502:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:503:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:503:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:504:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCDoubleAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCDoubleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getCDoubleAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,22,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getCDoubleAccess().getCDoubleKeyword_3());
            		
            otherlv_4=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_4, grammarAccess.getCDoubleAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCDoubleAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCDouble"


    // $ANTLR start "entryRuleCBoolean"
    // InternalDeclaration.g:541:1: entryRuleCBoolean returns [EObject current=null] : iv_ruleCBoolean= ruleCBoolean EOF ;
    public final EObject entryRuleCBoolean() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCBoolean = null;


        try {
            // InternalDeclaration.g:541:49: (iv_ruleCBoolean= ruleCBoolean EOF )
            // InternalDeclaration.g:542:2: iv_ruleCBoolean= ruleCBoolean EOF
            {
             newCompositeNode(grammarAccess.getCBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCBoolean=ruleCBoolean();

            state._fsp--;

             current =iv_ruleCBoolean; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCBoolean"


    // $ANTLR start "ruleCBoolean"
    // InternalDeclaration.g:548:1: ruleCBoolean returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCBoolean() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:554:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalDeclaration.g:555:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalDeclaration.g:555:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' )
            // InternalDeclaration.g:556:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalDeclaration.g:556:3: ()
            // InternalDeclaration.g:557:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCBooleanAccess().getCBooleanAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:563:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:564:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:564:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:565:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCBooleanAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCBooleanRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_21); 

            			newLeafNode(otherlv_2, grammarAccess.getCBooleanAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,23,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getCBooleanAccess().getCBooleanKeyword_3());
            		
            otherlv_4=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_4, grammarAccess.getCBooleanAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCBooleanAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCBoolean"


    // $ANTLR start "entryRuleCString"
    // InternalDeclaration.g:602:1: entryRuleCString returns [EObject current=null] : iv_ruleCString= ruleCString EOF ;
    public final EObject entryRuleCString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCString = null;


        try {
            // InternalDeclaration.g:602:48: (iv_ruleCString= ruleCString EOF )
            // InternalDeclaration.g:603:2: iv_ruleCString= ruleCString EOF
            {
             newCompositeNode(grammarAccess.getCStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCString=ruleCString();

            state._fsp--;

             current =iv_ruleCString; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCString"


    // $ANTLR start "ruleCString"
    // InternalDeclaration.g:609:1: ruleCString returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCString() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:615:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalDeclaration.g:616:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalDeclaration.g:616:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' )
            // InternalDeclaration.g:617:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalDeclaration.g:617:3: ()
            // InternalDeclaration.g:618:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCStringAccess().getCStringAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:624:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:625:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:625:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:626:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCStringAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCStringRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_22); 

            			newLeafNode(otherlv_2, grammarAccess.getCStringAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,24,FOLLOW_17); 

            			newLeafNode(otherlv_3, grammarAccess.getCStringAccess().getCStringKeyword_3());
            		
            otherlv_4=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_4, grammarAccess.getCStringAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCStringAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCString"


    // $ANTLR start "entryRuleJavaType"
    // InternalDeclaration.g:663:1: entryRuleJavaType returns [EObject current=null] : iv_ruleJavaType= ruleJavaType EOF ;
    public final EObject entryRuleJavaType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJavaType = null;


        try {
            // InternalDeclaration.g:663:49: (iv_ruleJavaType= ruleJavaType EOF )
            // InternalDeclaration.g:664:2: iv_ruleJavaType= ruleJavaType EOF
            {
             newCompositeNode(grammarAccess.getJavaTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleJavaType=ruleJavaType();

            state._fsp--;

             current =iv_ruleJavaType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJavaType"


    // $ANTLR start "ruleJavaType"
    // InternalDeclaration.g:670:1: ruleJavaType returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_packageName_3_0= ruleEString ) ) otherlv_4= '.' ( (lv_className_5_0= ruleEString ) ) otherlv_6= ')' otherlv_7= ';' ) ;
    public final EObject ruleJavaType() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_packageName_3_0 = null;

        AntlrDatatypeRuleToken lv_className_5_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:676:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_packageName_3_0= ruleEString ) ) otherlv_4= '.' ( (lv_className_5_0= ruleEString ) ) otherlv_6= ')' otherlv_7= ';' ) )
            // InternalDeclaration.g:677:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_packageName_3_0= ruleEString ) ) otherlv_4= '.' ( (lv_className_5_0= ruleEString ) ) otherlv_6= ')' otherlv_7= ';' )
            {
            // InternalDeclaration.g:677:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_packageName_3_0= ruleEString ) ) otherlv_4= '.' ( (lv_className_5_0= ruleEString ) ) otherlv_6= ')' otherlv_7= ';' )
            // InternalDeclaration.g:678:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_packageName_3_0= ruleEString ) ) otherlv_4= '.' ( (lv_className_5_0= ruleEString ) ) otherlv_6= ')' otherlv_7= ';'
            {
            // InternalDeclaration.g:678:3: ()
            // InternalDeclaration.g:679:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getJavaTypeAccess().getJavaTypeAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:685:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:686:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:686:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:687:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getJavaTypeAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJavaTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getJavaTypeAccess().getLeftParenthesisKeyword_2());
            		
            // InternalDeclaration.g:708:3: ( (lv_packageName_3_0= ruleEString ) )
            // InternalDeclaration.g:709:4: (lv_packageName_3_0= ruleEString )
            {
            // InternalDeclaration.g:709:4: (lv_packageName_3_0= ruleEString )
            // InternalDeclaration.g:710:5: lv_packageName_3_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getJavaTypeAccess().getPackageNameEStringParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_23);
            lv_packageName_3_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJavaTypeRule());
            					}
            					set(
            						current,
            						"packageName",
            						lv_packageName_3_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,25,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getJavaTypeAccess().getFullStopKeyword_4());
            		
            // InternalDeclaration.g:731:3: ( (lv_className_5_0= ruleEString ) )
            // InternalDeclaration.g:732:4: (lv_className_5_0= ruleEString )
            {
            // InternalDeclaration.g:732:4: (lv_className_5_0= ruleEString )
            // InternalDeclaration.g:733:5: lv_className_5_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getJavaTypeAccess().getClassNameEStringParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_17);
            lv_className_5_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getJavaTypeRule());
            					}
            					set(
            						current,
            						"className",
            						lv_className_5_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_6, grammarAccess.getJavaTypeAccess().getRightParenthesisKeyword_6());
            		
            otherlv_7=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getJavaTypeAccess().getSemicolonKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJavaType"


    // $ANTLR start "entryRuleStructType"
    // InternalDeclaration.g:762:1: entryRuleStructType returns [EObject current=null] : iv_ruleStructType= ruleStructType EOF ;
    public final EObject entryRuleStructType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructType = null;


        try {
            // InternalDeclaration.g:762:51: (iv_ruleStructType= ruleStructType EOF )
            // InternalDeclaration.g:763:2: iv_ruleStructType= ruleStructType EOF
            {
             newCompositeNode(grammarAccess.getStructTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStructType=ruleStructType();

            state._fsp--;

             current =iv_ruleStructType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructType"


    // $ANTLR start "ruleStructType"
    // InternalDeclaration.g:769:1: ruleStructType returns [EObject current=null] : (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) (otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) ) )* otherlv_6= ';' otherlv_7= '}' otherlv_8= ';' ) ;
    public final EObject ruleStructType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_fields_3_0 = null;

        EObject lv_fields_5_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:775:2: ( (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) (otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) ) )* otherlv_6= ';' otherlv_7= '}' otherlv_8= ';' ) )
            // InternalDeclaration.g:776:2: (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) (otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) ) )* otherlv_6= ';' otherlv_7= '}' otherlv_8= ';' )
            {
            // InternalDeclaration.g:776:2: (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) (otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) ) )* otherlv_6= ';' otherlv_7= '}' otherlv_8= ';' )
            // InternalDeclaration.g:777:3: otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) (otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) ) )* otherlv_6= ';' otherlv_7= '}' otherlv_8= ';'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStructTypeAccess().getRecordKeyword_0());
            		
            // InternalDeclaration.g:781:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:782:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:782:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:783:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStructTypeAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStructTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getStructTypeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalDeclaration.g:804:3: ( (lv_fields_3_0= ruleParameterDeclaration ) )
            // InternalDeclaration.g:805:4: (lv_fields_3_0= ruleParameterDeclaration )
            {
            // InternalDeclaration.g:805:4: (lv_fields_3_0= ruleParameterDeclaration )
            // InternalDeclaration.g:806:5: lv_fields_3_0= ruleParameterDeclaration
            {

            					newCompositeNode(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_18);
            lv_fields_3_0=ruleParameterDeclaration();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStructTypeRule());
            					}
            					add(
            						current,
            						"fields",
            						lv_fields_3_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.ParameterDeclaration");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDeclaration.g:823:3: (otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==20) ) {
                    int LA8_1 = input.LA(2);

                    if ( ((LA8_1>=RULE_STRING && LA8_1<=RULE_ID)) ) {
                        alt8=1;
                    }


                }


                switch (alt8) {
            	case 1 :
            	    // InternalDeclaration.g:824:4: otherlv_4= ';' ( (lv_fields_5_0= ruleParameterDeclaration ) )
            	    {
            	    otherlv_4=(Token)match(input,20,FOLLOW_3); 

            	    				newLeafNode(otherlv_4, grammarAccess.getStructTypeAccess().getSemicolonKeyword_4_0());
            	    			
            	    // InternalDeclaration.g:828:4: ( (lv_fields_5_0= ruleParameterDeclaration ) )
            	    // InternalDeclaration.g:829:5: (lv_fields_5_0= ruleParameterDeclaration )
            	    {
            	    // InternalDeclaration.g:829:5: (lv_fields_5_0= ruleParameterDeclaration )
            	    // InternalDeclaration.g:830:6: lv_fields_5_0= ruleParameterDeclaration
            	    {

            	    						newCompositeNode(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_4_1_0());
            	    					
            	    pushFollow(FOLLOW_18);
            	    lv_fields_5_0=ruleParameterDeclaration();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStructTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fields",
            	    							lv_fields_5_0,
            	    							"xtext.dcl.cares.ubo.fr.Declaration.ParameterDeclaration");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_6=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_6, grammarAccess.getStructTypeAccess().getSemicolonKeyword_5());
            		
            otherlv_7=(Token)match(input,14,FOLLOW_18); 

            			newLeafNode(otherlv_7, grammarAccess.getStructTypeAccess().getRightCurlyBracketKeyword_6());
            		
            otherlv_8=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getStructTypeAccess().getSemicolonKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructType"


    // $ANTLR start "entryRuleParameterDeclaration"
    // InternalDeclaration.g:864:1: entryRuleParameterDeclaration returns [EObject current=null] : iv_ruleParameterDeclaration= ruleParameterDeclaration EOF ;
    public final EObject entryRuleParameterDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterDeclaration = null;


        try {
            // InternalDeclaration.g:864:61: (iv_ruleParameterDeclaration= ruleParameterDeclaration EOF )
            // InternalDeclaration.g:865:2: iv_ruleParameterDeclaration= ruleParameterDeclaration EOF
            {
             newCompositeNode(grammarAccess.getParameterDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameterDeclaration=ruleParameterDeclaration();

            state._fsp--;

             current =iv_ruleParameterDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterDeclaration"


    // $ANTLR start "ruleParameterDeclaration"
    // InternalDeclaration.g:871:1: ruleParameterDeclaration returns [EObject current=null] : ( ( ( ruleEString ) ) (otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']' )? ( (lv_name_6_0= ruleEString ) ) ) ;
    public final EObject ruleParameterDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_lowerBound_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_upperBound_4_0 = null;

        AntlrDatatypeRuleToken lv_name_6_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:877:2: ( ( ( ( ruleEString ) ) (otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']' )? ( (lv_name_6_0= ruleEString ) ) ) )
            // InternalDeclaration.g:878:2: ( ( ( ruleEString ) ) (otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']' )? ( (lv_name_6_0= ruleEString ) ) )
            {
            // InternalDeclaration.g:878:2: ( ( ( ruleEString ) ) (otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']' )? ( (lv_name_6_0= ruleEString ) ) )
            // InternalDeclaration.g:879:3: ( ( ruleEString ) ) (otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']' )? ( (lv_name_6_0= ruleEString ) )
            {
            // InternalDeclaration.g:879:3: ( ( ruleEString ) )
            // InternalDeclaration.g:880:4: ( ruleEString )
            {
            // InternalDeclaration.g:880:4: ( ruleEString )
            // InternalDeclaration.g:881:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterDeclarationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getParameterDeclarationAccess().getTypeTypeCrossReference_0_0());
            				
            pushFollow(FOLLOW_24);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDeclaration.g:895:3: (otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==27) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDeclaration.g:896:4: otherlv_1= '[' ( (lv_lowerBound_2_0= RULE_INT ) ) otherlv_3= ',' ( (lv_upperBound_4_0= ruleNUMBER ) ) otherlv_5= ']'
                    {
                    otherlv_1=(Token)match(input,27,FOLLOW_25); 

                    				newLeafNode(otherlv_1, grammarAccess.getParameterDeclarationAccess().getLeftSquareBracketKeyword_1_0());
                    			
                    // InternalDeclaration.g:900:4: ( (lv_lowerBound_2_0= RULE_INT ) )
                    // InternalDeclaration.g:901:5: (lv_lowerBound_2_0= RULE_INT )
                    {
                    // InternalDeclaration.g:901:5: (lv_lowerBound_2_0= RULE_INT )
                    // InternalDeclaration.g:902:6: lv_lowerBound_2_0= RULE_INT
                    {
                    lv_lowerBound_2_0=(Token)match(input,RULE_INT,FOLLOW_26); 

                    						newLeafNode(lv_lowerBound_2_0, grammarAccess.getParameterDeclarationAccess().getLowerBoundINTTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getParameterDeclarationRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"lowerBound",
                    							lv_lowerBound_2_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }

                    otherlv_3=(Token)match(input,28,FOLLOW_27); 

                    				newLeafNode(otherlv_3, grammarAccess.getParameterDeclarationAccess().getCommaKeyword_1_2());
                    			
                    // InternalDeclaration.g:922:4: ( (lv_upperBound_4_0= ruleNUMBER ) )
                    // InternalDeclaration.g:923:5: (lv_upperBound_4_0= ruleNUMBER )
                    {
                    // InternalDeclaration.g:923:5: (lv_upperBound_4_0= ruleNUMBER )
                    // InternalDeclaration.g:924:6: lv_upperBound_4_0= ruleNUMBER
                    {

                    						newCompositeNode(grammarAccess.getParameterDeclarationAccess().getUpperBoundNUMBERParserRuleCall_1_3_0());
                    					
                    pushFollow(FOLLOW_28);
                    lv_upperBound_4_0=ruleNUMBER();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getParameterDeclarationRule());
                    						}
                    						set(
                    							current,
                    							"upperBound",
                    							lv_upperBound_4_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.NUMBER");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,29,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getParameterDeclarationAccess().getRightSquareBracketKeyword_1_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:946:3: ( (lv_name_6_0= ruleEString ) )
            // InternalDeclaration.g:947:4: (lv_name_6_0= ruleEString )
            {
            // InternalDeclaration.g:947:4: (lv_name_6_0= ruleEString )
            // InternalDeclaration.g:948:5: lv_name_6_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getParameterDeclarationAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_6_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParameterDeclarationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_6_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterDeclaration"


    // $ANTLR start "entryRuleNUMBER"
    // InternalDeclaration.g:969:1: entryRuleNUMBER returns [String current=null] : iv_ruleNUMBER= ruleNUMBER EOF ;
    public final String entryRuleNUMBER() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNUMBER = null;


        try {
            // InternalDeclaration.g:969:46: (iv_ruleNUMBER= ruleNUMBER EOF )
            // InternalDeclaration.g:970:2: iv_ruleNUMBER= ruleNUMBER EOF
            {
             newCompositeNode(grammarAccess.getNUMBERRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNUMBER=ruleNUMBER();

            state._fsp--;

             current =iv_ruleNUMBER.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNUMBER"


    // $ANTLR start "ruleNUMBER"
    // InternalDeclaration.g:976:1: ruleNUMBER returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleNUMBER() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalDeclaration.g:982:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalDeclaration.g:983:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalDeclaration.g:983:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalDeclaration.g:984:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalDeclaration.g:984:3: (kw= '-' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==30) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDeclaration.g:985:4: kw= '-'
                    {
                    kw=(Token)match(input,30,FOLLOW_25); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getNUMBERAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getNUMBERAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNUMBER"


    // $ANTLR start "entryRuleInterface"
    // InternalDeclaration.g:1002:1: entryRuleInterface returns [EObject current=null] : iv_ruleInterface= ruleInterface EOF ;
    public final EObject entryRuleInterface() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterface = null;


        try {
            // InternalDeclaration.g:1002:50: (iv_ruleInterface= ruleInterface EOF )
            // InternalDeclaration.g:1003:2: iv_ruleInterface= ruleInterface EOF
            {
             newCompositeNode(grammarAccess.getInterfaceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInterface=ruleInterface();

            state._fsp--;

             current =iv_ruleInterface; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterface"


    // $ANTLR start "ruleInterface"
    // InternalDeclaration.g:1009:1: ruleInterface returns [EObject current=null] : ( () otherlv_1= 'Interface' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )* )? otherlv_6= '}' ) ;
    public final EObject ruleInterface() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_functions_4_0 = null;

        EObject lv_functions_5_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:1015:2: ( ( () otherlv_1= 'Interface' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )* )? otherlv_6= '}' ) )
            // InternalDeclaration.g:1016:2: ( () otherlv_1= 'Interface' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )* )? otherlv_6= '}' )
            {
            // InternalDeclaration.g:1016:2: ( () otherlv_1= 'Interface' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )* )? otherlv_6= '}' )
            // InternalDeclaration.g:1017:3: () otherlv_1= 'Interface' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' ( ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )* )? otherlv_6= '}'
            {
            // InternalDeclaration.g:1017:3: ()
            // InternalDeclaration.g:1018:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInterfaceAccess().getInterfaceAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getInterfaceAccess().getInterfaceKeyword_1());
            		
            // InternalDeclaration.g:1028:3: ( (lv_name_2_0= ruleEString ) )
            // InternalDeclaration.g:1029:4: (lv_name_2_0= ruleEString )
            {
            // InternalDeclaration.g:1029:4: (lv_name_2_0= ruleEString )
            // InternalDeclaration.g:1030:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getInterfaceAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInterfaceRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_29); 

            			newLeafNode(otherlv_3, grammarAccess.getInterfaceAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalDeclaration.g:1051:3: ( ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )* )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( ((LA12_0>=RULE_STRING && LA12_0<=RULE_ID)) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalDeclaration.g:1052:4: ( (lv_functions_4_0= ruleFunction ) ) ( (lv_functions_5_0= ruleFunction ) )*
                    {
                    // InternalDeclaration.g:1052:4: ( (lv_functions_4_0= ruleFunction ) )
                    // InternalDeclaration.g:1053:5: (lv_functions_4_0= ruleFunction )
                    {
                    // InternalDeclaration.g:1053:5: (lv_functions_4_0= ruleFunction )
                    // InternalDeclaration.g:1054:6: lv_functions_4_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getInterfaceAccess().getFunctionsFunctionParserRuleCall_4_0_0());
                    					
                    pushFollow(FOLLOW_29);
                    lv_functions_4_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInterfaceRule());
                    						}
                    						add(
                    							current,
                    							"functions",
                    							lv_functions_4_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1071:4: ( (lv_functions_5_0= ruleFunction ) )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( ((LA11_0>=RULE_STRING && LA11_0<=RULE_ID)) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalDeclaration.g:1072:5: (lv_functions_5_0= ruleFunction )
                    	    {
                    	    // InternalDeclaration.g:1072:5: (lv_functions_5_0= ruleFunction )
                    	    // InternalDeclaration.g:1073:6: lv_functions_5_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getInterfaceAccess().getFunctionsFunctionParserRuleCall_4_1_0());
                    	    					
                    	    pushFollow(FOLLOW_29);
                    	    lv_functions_5_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getInterfaceRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"functions",
                    	    							lv_functions_5_0,
                    	    							"xtext.dcl.cares.ubo.fr.Declaration.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getInterfaceAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterface"


    // $ANTLR start "entryRuleFunction"
    // InternalDeclaration.g:1099:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalDeclaration.g:1099:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalDeclaration.g:1100:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalDeclaration.g:1106:1: ruleFunction returns [EObject current=null] : ( ( (lv_returnType_0_0= ruleReturnType ) )? ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )* )? otherlv_6= ')' otherlv_7= ';' ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        EObject lv_returnType_0_0 = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_parameters_3_0 = null;

        EObject lv_parameters_5_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:1112:2: ( ( ( (lv_returnType_0_0= ruleReturnType ) )? ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )* )? otherlv_6= ')' otherlv_7= ';' ) )
            // InternalDeclaration.g:1113:2: ( ( (lv_returnType_0_0= ruleReturnType ) )? ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )* )? otherlv_6= ')' otherlv_7= ';' )
            {
            // InternalDeclaration.g:1113:2: ( ( (lv_returnType_0_0= ruleReturnType ) )? ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )* )? otherlv_6= ')' otherlv_7= ';' )
            // InternalDeclaration.g:1114:3: ( (lv_returnType_0_0= ruleReturnType ) )? ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )* )? otherlv_6= ')' otherlv_7= ';'
            {
            // InternalDeclaration.g:1114:3: ( (lv_returnType_0_0= ruleReturnType ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_STRING) ) {
                int LA13_1 = input.LA(2);

                if ( ((LA13_1>=RULE_STRING && LA13_1<=RULE_ID)) ) {
                    alt13=1;
                }
            }
            else if ( (LA13_0==RULE_ID) ) {
                int LA13_2 = input.LA(2);

                if ( ((LA13_2>=RULE_STRING && LA13_2<=RULE_ID)) ) {
                    alt13=1;
                }
            }
            switch (alt13) {
                case 1 :
                    // InternalDeclaration.g:1115:4: (lv_returnType_0_0= ruleReturnType )
                    {
                    // InternalDeclaration.g:1115:4: (lv_returnType_0_0= ruleReturnType )
                    // InternalDeclaration.g:1116:5: lv_returnType_0_0= ruleReturnType
                    {

                    					newCompositeNode(grammarAccess.getFunctionAccess().getReturnTypeReturnTypeParserRuleCall_0_0());
                    				
                    pushFollow(FOLLOW_3);
                    lv_returnType_0_0=ruleReturnType();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFunctionRule());
                    					}
                    					set(
                    						current,
                    						"returnType",
                    						lv_returnType_0_0,
                    						"xtext.dcl.cares.ubo.fr.Declaration.ReturnType");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalDeclaration.g:1133:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:1134:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:1134:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:1135:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getFunctionAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_15);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFunctionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_30); 

            			newLeafNode(otherlv_2, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_2());
            		
            // InternalDeclaration.g:1156:3: ( ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )* )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( ((LA15_0>=RULE_STRING && LA15_0<=RULE_ID)) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalDeclaration.g:1157:4: ( (lv_parameters_3_0= ruleParameterDeclaration ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )*
                    {
                    // InternalDeclaration.g:1157:4: ( (lv_parameters_3_0= ruleParameterDeclaration ) )
                    // InternalDeclaration.g:1158:5: (lv_parameters_3_0= ruleParameterDeclaration )
                    {
                    // InternalDeclaration.g:1158:5: (lv_parameters_3_0= ruleParameterDeclaration )
                    // InternalDeclaration.g:1159:6: lv_parameters_3_0= ruleParameterDeclaration
                    {

                    						newCompositeNode(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_31);
                    lv_parameters_3_0=ruleParameterDeclaration();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFunctionRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_3_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.ParameterDeclaration");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1176:4: (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==28) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalDeclaration.g:1177:5: otherlv_4= ',' ( (lv_parameters_5_0= ruleParameterDeclaration ) )
                    	    {
                    	    otherlv_4=(Token)match(input,28,FOLLOW_3); 

                    	    					newLeafNode(otherlv_4, grammarAccess.getFunctionAccess().getCommaKeyword_3_1_0());
                    	    				
                    	    // InternalDeclaration.g:1181:5: ( (lv_parameters_5_0= ruleParameterDeclaration ) )
                    	    // InternalDeclaration.g:1182:6: (lv_parameters_5_0= ruleParameterDeclaration )
                    	    {
                    	    // InternalDeclaration.g:1182:6: (lv_parameters_5_0= ruleParameterDeclaration )
                    	    // InternalDeclaration.g:1183:7: lv_parameters_5_0= ruleParameterDeclaration
                    	    {

                    	    							newCompositeNode(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_3_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_31);
                    	    lv_parameters_5_0=ruleParameterDeclaration();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFunctionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_5_0,
                    	    								"xtext.dcl.cares.ubo.fr.Declaration.ParameterDeclaration");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,19,FOLLOW_18); 

            			newLeafNode(otherlv_6, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_4());
            		
            otherlv_7=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getFunctionAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleReturnType"
    // InternalDeclaration.g:1214:1: entryRuleReturnType returns [EObject current=null] : iv_ruleReturnType= ruleReturnType EOF ;
    public final EObject entryRuleReturnType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturnType = null;


        try {
            // InternalDeclaration.g:1214:51: (iv_ruleReturnType= ruleReturnType EOF )
            // InternalDeclaration.g:1215:2: iv_ruleReturnType= ruleReturnType EOF
            {
             newCompositeNode(grammarAccess.getReturnTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReturnType=ruleReturnType();

            state._fsp--;

             current =iv_ruleReturnType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnType"


    // $ANTLR start "ruleReturnType"
    // InternalDeclaration.g:1221:1: ruleReturnType returns [EObject current=null] : ( () ( ( ruleEString ) ) ) ;
    public final EObject ruleReturnType() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalDeclaration.g:1227:2: ( ( () ( ( ruleEString ) ) ) )
            // InternalDeclaration.g:1228:2: ( () ( ( ruleEString ) ) )
            {
            // InternalDeclaration.g:1228:2: ( () ( ( ruleEString ) ) )
            // InternalDeclaration.g:1229:3: () ( ( ruleEString ) )
            {
            // InternalDeclaration.g:1229:3: ()
            // InternalDeclaration.g:1230:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getReturnTypeAccess().getReturnTypeAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:1236:3: ( ( ruleEString ) )
            // InternalDeclaration.g:1237:4: ( ruleEString )
            {
            // InternalDeclaration.g:1237:4: ( ruleEString )
            // InternalDeclaration.g:1238:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReturnTypeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getReturnTypeAccess().getTypeTypeCrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnType"


    // $ANTLR start "entryRuleInterfacePort"
    // InternalDeclaration.g:1256:1: entryRuleInterfacePort returns [EObject current=null] : iv_ruleInterfacePort= ruleInterfacePort EOF ;
    public final EObject entryRuleInterfacePort() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInterfacePort = null;


        try {
            // InternalDeclaration.g:1256:54: (iv_ruleInterfacePort= ruleInterfacePort EOF )
            // InternalDeclaration.g:1257:2: iv_ruleInterfacePort= ruleInterfacePort EOF
            {
             newCompositeNode(grammarAccess.getInterfacePortRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInterfacePort=ruleInterfacePort();

            state._fsp--;

             current =iv_ruleInterfacePort; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInterfacePort"


    // $ANTLR start "ruleInterfacePort"
    // InternalDeclaration.g:1263:1: ruleInterfacePort returns [EObject current=null] : ( () ( ( ruleEString ) ) ( (lv_name_2_0= ruleEString ) ) ) ;
    public final EObject ruleInterfacePort() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_2_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:1269:2: ( ( () ( ( ruleEString ) ) ( (lv_name_2_0= ruleEString ) ) ) )
            // InternalDeclaration.g:1270:2: ( () ( ( ruleEString ) ) ( (lv_name_2_0= ruleEString ) ) )
            {
            // InternalDeclaration.g:1270:2: ( () ( ( ruleEString ) ) ( (lv_name_2_0= ruleEString ) ) )
            // InternalDeclaration.g:1271:3: () ( ( ruleEString ) ) ( (lv_name_2_0= ruleEString ) )
            {
            // InternalDeclaration.g:1271:3: ()
            // InternalDeclaration.g:1272:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInterfacePortAccess().getInterfacePortAction_0(),
            					current);
            			

            }

            // InternalDeclaration.g:1278:3: ( ( ruleEString ) )
            // InternalDeclaration.g:1279:4: ( ruleEString )
            {
            // InternalDeclaration.g:1279:4: ( ruleEString )
            // InternalDeclaration.g:1280:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInterfacePortRule());
            					}
            				

            					newCompositeNode(grammarAccess.getInterfacePortAccess().getInterfaceInterfaceCrossReference_1_0());
            				
            pushFollow(FOLLOW_3);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDeclaration.g:1294:3: ( (lv_name_2_0= ruleEString ) )
            // InternalDeclaration.g:1295:4: (lv_name_2_0= ruleEString )
            {
            // InternalDeclaration.g:1295:4: (lv_name_2_0= ruleEString )
            // InternalDeclaration.g:1296:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getInterfacePortAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInterfacePortRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInterfacePort"


    // $ANTLR start "entryRuleLeafComponentType"
    // InternalDeclaration.g:1317:1: entryRuleLeafComponentType returns [EObject current=null] : iv_ruleLeafComponentType= ruleLeafComponentType EOF ;
    public final EObject entryRuleLeafComponentType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLeafComponentType = null;


        try {
            // InternalDeclaration.g:1317:58: (iv_ruleLeafComponentType= ruleLeafComponentType EOF )
            // InternalDeclaration.g:1318:2: iv_ruleLeafComponentType= ruleLeafComponentType EOF
            {
             newCompositeNode(grammarAccess.getLeafComponentTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLeafComponentType=ruleLeafComponentType();

            state._fsp--;

             current =iv_ruleLeafComponentType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLeafComponentType"


    // $ANTLR start "ruleLeafComponentType"
    // InternalDeclaration.g:1324:1: ruleLeafComponentType returns [EObject current=null] : ( () otherlv_1= 'LeafComponentType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')' )? (otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')' )? (otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}' )? (otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}' )? (otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}' )? otherlv_33= '}' ) ;
    public final EObject ruleLeafComponentType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_25=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_32=null;
        Token otherlv_33=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        EObject lv_requiredInterface_6_0 = null;

        EObject lv_requiredInterface_8_0 = null;

        EObject lv_providedInterface_12_0 = null;

        EObject lv_providedInterface_14_0 = null;

        EObject lv_inputs_18_0 = null;

        EObject lv_inputs_19_0 = null;

        EObject lv_outputs_23_0 = null;

        EObject lv_outputs_24_0 = null;

        EObject lv_parameters_28_0 = null;

        EObject lv_parameters_30_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:1330:2: ( ( () otherlv_1= 'LeafComponentType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')' )? (otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')' )? (otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}' )? (otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}' )? (otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}' )? otherlv_33= '}' ) )
            // InternalDeclaration.g:1331:2: ( () otherlv_1= 'LeafComponentType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')' )? (otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')' )? (otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}' )? (otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}' )? (otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}' )? otherlv_33= '}' )
            {
            // InternalDeclaration.g:1331:2: ( () otherlv_1= 'LeafComponentType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')' )? (otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')' )? (otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}' )? (otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}' )? (otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}' )? otherlv_33= '}' )
            // InternalDeclaration.g:1332:3: () otherlv_1= 'LeafComponentType' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')' )? (otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')' )? (otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}' )? (otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}' )? (otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}' )? otherlv_33= '}'
            {
            // InternalDeclaration.g:1332:3: ()
            // InternalDeclaration.g:1333:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLeafComponentTypeAccess().getLeafComponentTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getLeafComponentTypeAccess().getLeafComponentTypeKeyword_1());
            		
            // InternalDeclaration.g:1343:3: ( (lv_name_2_0= ruleEString ) )
            // InternalDeclaration.g:1344:4: (lv_name_2_0= ruleEString )
            {
            // InternalDeclaration.g:1344:4: (lv_name_2_0= ruleEString )
            // InternalDeclaration.g:1345:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_32); 

            			newLeafNode(otherlv_3, grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalDeclaration.g:1366:3: (otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==33) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalDeclaration.g:1367:4: otherlv_4= 'requiredInterfaces' otherlv_5= '(' ( (lv_requiredInterface_6_0= ruleInterfacePort ) ) (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )* otherlv_9= ')'
                    {
                    otherlv_4=(Token)match(input,33,FOLLOW_15); 

                    				newLeafNode(otherlv_4, grammarAccess.getLeafComponentTypeAccess().getRequiredInterfacesKeyword_4_0());
                    			
                    otherlv_5=(Token)match(input,17,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getLeafComponentTypeAccess().getLeftParenthesisKeyword_4_1());
                    			
                    // InternalDeclaration.g:1375:4: ( (lv_requiredInterface_6_0= ruleInterfacePort ) )
                    // InternalDeclaration.g:1376:5: (lv_requiredInterface_6_0= ruleInterfacePort )
                    {
                    // InternalDeclaration.g:1376:5: (lv_requiredInterface_6_0= ruleInterfacePort )
                    // InternalDeclaration.g:1377:6: lv_requiredInterface_6_0= ruleInterfacePort
                    {

                    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceInterfacePortParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_31);
                    lv_requiredInterface_6_0=ruleInterfacePort();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    						}
                    						add(
                    							current,
                    							"requiredInterface",
                    							lv_requiredInterface_6_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.InterfacePort");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1394:4: (otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==28) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalDeclaration.g:1395:5: otherlv_7= ',' ( (lv_requiredInterface_8_0= ruleInterfacePort ) )
                    	    {
                    	    otherlv_7=(Token)match(input,28,FOLLOW_3); 

                    	    					newLeafNode(otherlv_7, grammarAccess.getLeafComponentTypeAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalDeclaration.g:1399:5: ( (lv_requiredInterface_8_0= ruleInterfacePort ) )
                    	    // InternalDeclaration.g:1400:6: (lv_requiredInterface_8_0= ruleInterfacePort )
                    	    {
                    	    // InternalDeclaration.g:1400:6: (lv_requiredInterface_8_0= ruleInterfacePort )
                    	    // InternalDeclaration.g:1401:7: lv_requiredInterface_8_0= ruleInterfacePort
                    	    {

                    	    							newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getRequiredInterfaceInterfacePortParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_31);
                    	    lv_requiredInterface_8_0=ruleInterfacePort();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"requiredInterface",
                    	    								lv_requiredInterface_8_0,
                    	    								"xtext.dcl.cares.ubo.fr.Declaration.InterfacePort");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    otherlv_9=(Token)match(input,19,FOLLOW_33); 

                    				newLeafNode(otherlv_9, grammarAccess.getLeafComponentTypeAccess().getRightParenthesisKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:1424:3: (otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==34) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalDeclaration.g:1425:4: otherlv_10= 'providedInterfaces' otherlv_11= '(' ( (lv_providedInterface_12_0= ruleInterfacePort ) ) (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )* otherlv_15= ')'
                    {
                    otherlv_10=(Token)match(input,34,FOLLOW_15); 

                    				newLeafNode(otherlv_10, grammarAccess.getLeafComponentTypeAccess().getProvidedInterfacesKeyword_5_0());
                    			
                    otherlv_11=(Token)match(input,17,FOLLOW_3); 

                    				newLeafNode(otherlv_11, grammarAccess.getLeafComponentTypeAccess().getLeftParenthesisKeyword_5_1());
                    			
                    // InternalDeclaration.g:1433:4: ( (lv_providedInterface_12_0= ruleInterfacePort ) )
                    // InternalDeclaration.g:1434:5: (lv_providedInterface_12_0= ruleInterfacePort )
                    {
                    // InternalDeclaration.g:1434:5: (lv_providedInterface_12_0= ruleInterfacePort )
                    // InternalDeclaration.g:1435:6: lv_providedInterface_12_0= ruleInterfacePort
                    {

                    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceInterfacePortParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_31);
                    lv_providedInterface_12_0=ruleInterfacePort();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    						}
                    						add(
                    							current,
                    							"providedInterface",
                    							lv_providedInterface_12_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.InterfacePort");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1452:4: (otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==28) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalDeclaration.g:1453:5: otherlv_13= ',' ( (lv_providedInterface_14_0= ruleInterfacePort ) )
                    	    {
                    	    otherlv_13=(Token)match(input,28,FOLLOW_3); 

                    	    					newLeafNode(otherlv_13, grammarAccess.getLeafComponentTypeAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalDeclaration.g:1457:5: ( (lv_providedInterface_14_0= ruleInterfacePort ) )
                    	    // InternalDeclaration.g:1458:6: (lv_providedInterface_14_0= ruleInterfacePort )
                    	    {
                    	    // InternalDeclaration.g:1458:6: (lv_providedInterface_14_0= ruleInterfacePort )
                    	    // InternalDeclaration.g:1459:7: lv_providedInterface_14_0= ruleInterfacePort
                    	    {

                    	    							newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getProvidedInterfaceInterfacePortParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_31);
                    	    lv_providedInterface_14_0=ruleInterfacePort();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"providedInterface",
                    	    								lv_providedInterface_14_0,
                    	    								"xtext.dcl.cares.ubo.fr.Declaration.InterfacePort");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    otherlv_15=(Token)match(input,19,FOLLOW_34); 

                    				newLeafNode(otherlv_15, grammarAccess.getLeafComponentTypeAccess().getRightParenthesisKeyword_5_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:1482:3: (otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==35) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalDeclaration.g:1483:4: otherlv_16= 'inputs' otherlv_17= '{' ( (lv_inputs_18_0= ruleInput ) ) ( (lv_inputs_19_0= ruleInput ) )* otherlv_20= '}'
                    {
                    otherlv_16=(Token)match(input,35,FOLLOW_4); 

                    				newLeafNode(otherlv_16, grammarAccess.getLeafComponentTypeAccess().getInputsKeyword_6_0());
                    			
                    otherlv_17=(Token)match(input,12,FOLLOW_3); 

                    				newLeafNode(otherlv_17, grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalDeclaration.g:1491:4: ( (lv_inputs_18_0= ruleInput ) )
                    // InternalDeclaration.g:1492:5: (lv_inputs_18_0= ruleInput )
                    {
                    // InternalDeclaration.g:1492:5: (lv_inputs_18_0= ruleInput )
                    // InternalDeclaration.g:1493:6: lv_inputs_18_0= ruleInput
                    {

                    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getInputsInputParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_29);
                    lv_inputs_18_0=ruleInput();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    						}
                    						add(
                    							current,
                    							"inputs",
                    							lv_inputs_18_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.Input");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1510:4: ( (lv_inputs_19_0= ruleInput ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( ((LA20_0>=RULE_STRING && LA20_0<=RULE_ID)) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalDeclaration.g:1511:5: (lv_inputs_19_0= ruleInput )
                    	    {
                    	    // InternalDeclaration.g:1511:5: (lv_inputs_19_0= ruleInput )
                    	    // InternalDeclaration.g:1512:6: lv_inputs_19_0= ruleInput
                    	    {

                    	    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getInputsInputParserRuleCall_6_3_0());
                    	    					
                    	    pushFollow(FOLLOW_29);
                    	    lv_inputs_19_0=ruleInput();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"inputs",
                    	    							lv_inputs_19_0,
                    	    							"xtext.dcl.cares.ubo.fr.Declaration.Input");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,14,FOLLOW_35); 

                    				newLeafNode(otherlv_20, grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:1534:3: (otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}' )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==36) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalDeclaration.g:1535:4: otherlv_21= 'outputs' otherlv_22= '{' ( (lv_outputs_23_0= ruleOutput ) ) ( (lv_outputs_24_0= ruleOutput ) )* otherlv_25= '}'
                    {
                    otherlv_21=(Token)match(input,36,FOLLOW_4); 

                    				newLeafNode(otherlv_21, grammarAccess.getLeafComponentTypeAccess().getOutputsKeyword_7_0());
                    			
                    otherlv_22=(Token)match(input,12,FOLLOW_3); 

                    				newLeafNode(otherlv_22, grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalDeclaration.g:1543:4: ( (lv_outputs_23_0= ruleOutput ) )
                    // InternalDeclaration.g:1544:5: (lv_outputs_23_0= ruleOutput )
                    {
                    // InternalDeclaration.g:1544:5: (lv_outputs_23_0= ruleOutput )
                    // InternalDeclaration.g:1545:6: lv_outputs_23_0= ruleOutput
                    {

                    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getOutputsOutputParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_29);
                    lv_outputs_23_0=ruleOutput();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    						}
                    						add(
                    							current,
                    							"outputs",
                    							lv_outputs_23_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.Output");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1562:4: ( (lv_outputs_24_0= ruleOutput ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( ((LA22_0>=RULE_STRING && LA22_0<=RULE_ID)) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalDeclaration.g:1563:5: (lv_outputs_24_0= ruleOutput )
                    	    {
                    	    // InternalDeclaration.g:1563:5: (lv_outputs_24_0= ruleOutput )
                    	    // InternalDeclaration.g:1564:6: lv_outputs_24_0= ruleOutput
                    	    {

                    	    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getOutputsOutputParserRuleCall_7_3_0());
                    	    					
                    	    pushFollow(FOLLOW_29);
                    	    lv_outputs_24_0=ruleOutput();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"outputs",
                    	    							lv_outputs_24_0,
                    	    							"xtext.dcl.cares.ubo.fr.Declaration.Output");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    otherlv_25=(Token)match(input,14,FOLLOW_36); 

                    				newLeafNode(otherlv_25, grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalDeclaration.g:1586:3: (otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==37) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalDeclaration.g:1587:4: otherlv_26= 'parameters' otherlv_27= '{' ( (lv_parameters_28_0= ruleParameterDeclaration ) ) (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )* otherlv_31= ';' otherlv_32= '}'
                    {
                    otherlv_26=(Token)match(input,37,FOLLOW_4); 

                    				newLeafNode(otherlv_26, grammarAccess.getLeafComponentTypeAccess().getParametersKeyword_8_0());
                    			
                    otherlv_27=(Token)match(input,12,FOLLOW_3); 

                    				newLeafNode(otherlv_27, grammarAccess.getLeafComponentTypeAccess().getLeftCurlyBracketKeyword_8_1());
                    			
                    // InternalDeclaration.g:1595:4: ( (lv_parameters_28_0= ruleParameterDeclaration ) )
                    // InternalDeclaration.g:1596:5: (lv_parameters_28_0= ruleParameterDeclaration )
                    {
                    // InternalDeclaration.g:1596:5: (lv_parameters_28_0= ruleParameterDeclaration )
                    // InternalDeclaration.g:1597:6: lv_parameters_28_0= ruleParameterDeclaration
                    {

                    						newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getParametersParameterDeclarationParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_parameters_28_0=ruleParameterDeclaration();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_28_0,
                    							"xtext.dcl.cares.ubo.fr.Declaration.ParameterDeclaration");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalDeclaration.g:1614:4: (otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) ) )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==20) ) {
                            int LA24_1 = input.LA(2);

                            if ( ((LA24_1>=RULE_STRING && LA24_1<=RULE_ID)) ) {
                                alt24=1;
                            }


                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalDeclaration.g:1615:5: otherlv_29= ';' ( (lv_parameters_30_0= ruleParameterDeclaration ) )
                    	    {
                    	    otherlv_29=(Token)match(input,20,FOLLOW_3); 

                    	    					newLeafNode(otherlv_29, grammarAccess.getLeafComponentTypeAccess().getSemicolonKeyword_8_3_0());
                    	    				
                    	    // InternalDeclaration.g:1619:5: ( (lv_parameters_30_0= ruleParameterDeclaration ) )
                    	    // InternalDeclaration.g:1620:6: (lv_parameters_30_0= ruleParameterDeclaration )
                    	    {
                    	    // InternalDeclaration.g:1620:6: (lv_parameters_30_0= ruleParameterDeclaration )
                    	    // InternalDeclaration.g:1621:7: lv_parameters_30_0= ruleParameterDeclaration
                    	    {

                    	    							newCompositeNode(grammarAccess.getLeafComponentTypeAccess().getParametersParameterDeclarationParserRuleCall_8_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_18);
                    	    lv_parameters_30_0=ruleParameterDeclaration();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getLeafComponentTypeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_30_0,
                    	    								"xtext.dcl.cares.ubo.fr.Declaration.ParameterDeclaration");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    otherlv_31=(Token)match(input,20,FOLLOW_14); 

                    				newLeafNode(otherlv_31, grammarAccess.getLeafComponentTypeAccess().getSemicolonKeyword_8_4());
                    			
                    otherlv_32=(Token)match(input,14,FOLLOW_14); 

                    				newLeafNode(otherlv_32, grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_8_5());
                    			

                    }
                    break;

            }

            otherlv_33=(Token)match(input,14,FOLLOW_2); 

            			newLeafNode(otherlv_33, grammarAccess.getLeafComponentTypeAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLeafComponentType"


    // $ANTLR start "entryRuleInput"
    // InternalDeclaration.g:1656:1: entryRuleInput returns [EObject current=null] : iv_ruleInput= ruleInput EOF ;
    public final EObject entryRuleInput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInput = null;


        try {
            // InternalDeclaration.g:1656:46: (iv_ruleInput= ruleInput EOF )
            // InternalDeclaration.g:1657:2: iv_ruleInput= ruleInput EOF
            {
             newCompositeNode(grammarAccess.getInputRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInput=ruleInput();

            state._fsp--;

             current =iv_ruleInput; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // InternalDeclaration.g:1663:1: ruleInput returns [EObject current=null] : ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' ) ;
    public final EObject ruleInput() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:1669:2: ( ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' ) )
            // InternalDeclaration.g:1670:2: ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' )
            {
            // InternalDeclaration.g:1670:2: ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' )
            // InternalDeclaration.g:1671:3: ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';'
            {
            // InternalDeclaration.g:1671:3: ( ( ruleEString ) )
            // InternalDeclaration.g:1672:4: ( ruleEString )
            {
            // InternalDeclaration.g:1672:4: ( ruleEString )
            // InternalDeclaration.g:1673:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getInputRule());
            					}
            				

            					newCompositeNode(grammarAccess.getInputAccess().getTypeTypeCrossReference_0_0());
            				
            pushFollow(FOLLOW_3);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDeclaration.g:1687:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:1688:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:1688:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:1689:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getInputAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_18);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getInputRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getInputAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "entryRuleOutput"
    // InternalDeclaration.g:1714:1: entryRuleOutput returns [EObject current=null] : iv_ruleOutput= ruleOutput EOF ;
    public final EObject entryRuleOutput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutput = null;


        try {
            // InternalDeclaration.g:1714:47: (iv_ruleOutput= ruleOutput EOF )
            // InternalDeclaration.g:1715:2: iv_ruleOutput= ruleOutput EOF
            {
             newCompositeNode(grammarAccess.getOutputRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutput=ruleOutput();

            state._fsp--;

             current =iv_ruleOutput; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutput"


    // $ANTLR start "ruleOutput"
    // InternalDeclaration.g:1721:1: ruleOutput returns [EObject current=null] : ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' ) ;
    public final EObject ruleOutput() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalDeclaration.g:1727:2: ( ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' ) )
            // InternalDeclaration.g:1728:2: ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' )
            {
            // InternalDeclaration.g:1728:2: ( ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';' )
            // InternalDeclaration.g:1729:3: ( ( ruleEString ) ) ( (lv_name_1_0= ruleEString ) ) otherlv_2= ';'
            {
            // InternalDeclaration.g:1729:3: ( ( ruleEString ) )
            // InternalDeclaration.g:1730:4: ( ruleEString )
            {
            // InternalDeclaration.g:1730:4: ( ruleEString )
            // InternalDeclaration.g:1731:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getOutputRule());
            					}
            				

            					newCompositeNode(grammarAccess.getOutputAccess().getTypeTypeCrossReference_0_0());
            				
            pushFollow(FOLLOW_3);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDeclaration.g:1745:3: ( (lv_name_1_0= ruleEString ) )
            // InternalDeclaration.g:1746:4: (lv_name_1_0= ruleEString )
            {
            // InternalDeclaration.g:1746:4: (lv_name_1_0= ruleEString )
            // InternalDeclaration.g:1747:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getOutputAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_18);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOutputRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.dcl.cares.ubo.fr.Declaration.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_2, grammarAccess.getOutputAccess().getSemicolonKeyword_2());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutput"


    // $ANTLR start "entryRuleEString"
    // InternalDeclaration.g:1772:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalDeclaration.g:1772:47: (iv_ruleEString= ruleEString EOF )
            // InternalDeclaration.g:1773:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDeclaration.g:1779:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalDeclaration.g:1785:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalDeclaration.g:1786:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalDeclaration.g:1786:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==RULE_STRING) ) {
                alt26=1;
            }
            else if ( (LA26_0==RULE_ID) ) {
                alt26=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }
            switch (alt26) {
                case 1 :
                    // InternalDeclaration.g:1787:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalDeclaration.g:1795:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    static final String dfa_1s = "\13\uffff";
    static final String dfa_2s = "\1\5\1\uffff\2\21\1\5\6\uffff";
    static final String dfa_3s = "\1\32\1\uffff\2\21\1\30\6\uffff";
    static final String dfa_4s = "\1\uffff\1\1\3\uffff\1\7\1\6\1\5\1\4\1\3\1\2";
    static final String dfa_5s = "\13\uffff}>";
    static final String[] dfa_6s = {
            "\1\2\1\3\23\uffff\1\1",
            "",
            "\1\4",
            "\1\4",
            "\2\6\13\uffff\1\5\2\uffff\1\12\1\11\1\10\1\7",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "291:2: (this_StructType_0= ruleStructType | this_CInt_1= ruleCInt | this_CDouble_2= ruleCDouble | this_CBoolean_3= ruleCBoolean | this_CString_4= ruleCString | this_JavaType_5= ruleJavaType | this_Void_6= ruleVoid )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000001E000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000004000060L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000004004060L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000001C000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000080004000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000014000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000100004000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000008000060L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000040000010L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000000004060L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000080060L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000010080000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000003E00004000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000003C00004000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000003800004000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000003000004000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000002000004000L});

}