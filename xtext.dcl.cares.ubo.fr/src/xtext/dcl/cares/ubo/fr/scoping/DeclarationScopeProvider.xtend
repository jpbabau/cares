/*
 * generated by Xtext 2.16.0-SNAPSHOT
 */
package xtext.dcl.cares.ubo.fr.scoping


/**
 * This class contains custom scoping description.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#scoping
 * on how and when to use it.
 */
class DeclarationScopeProvider extends AbstractDeclarationScopeProvider {

}
