package xtext.dcl.cares.ubo.fr.ressource

import com.google.inject.Inject
import java.util.Map
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.naming.IQualifiedNameConverter
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.eclipse.xtext.resource.IFragmentProvider
import org.eclipse.xtext.util.IResourceScopeCache


class CaresCsysFragmentProvider implements IFragmentProvider {
    
    @Inject IQualifiedNameConverter qualifiedNameConverter
    @Inject IQualifiedNameProvider qualifiedNameProvider
    @Inject IResourceScopeCache cache
    
    static val CACHE_KEY = 'name2element'
    
    override getEObject(Resource resource, String fragment, Fallback fallback) {
        try {
            val qualifiedName = qualifiedNameConverter.toQualifiedName(fragment)
            return getName2ElementMap(resource, fallback).get(qualifiedNameConverter.toString(qualifiedName))
        } catch (Exception exc) {
            return fallback.getEObject(fragment)
        }
    }
    
    /**
     * Returns a map qualifiedName -> EObject that is calculated on demand and cached.
     */
    def private Map<String, EObject> getName2ElementMap(Resource resource, Fallback fallback) {
        cache.get(CACHE_KEY, resource, [
            val name2element = <String, EObject>newHashMap
            resource.allContents.forEach [
                name2element.put(getFragment(it, fallback), it)
            ]
            return name2element
        ])
    }
    
    override getFragment(EObject obj, Fallback fallback) {
        try {
            return qualifiedNameConverter.toString(qualifiedNameProvider.getFullyQualifiedName(obj))
        }
        catch (Exception exc) {
            return fallback.getFragment(obj)
        }
    }
}
