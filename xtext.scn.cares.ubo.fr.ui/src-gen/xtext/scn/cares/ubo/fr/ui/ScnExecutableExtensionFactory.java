/*
 * generated by Xtext 2.16.0-SNAPSHOT
 */
package xtext.scn.cares.ubo.fr.ui;

import com.google.inject.Injector;
import org.eclipse.core.runtime.Platform;
import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;
import xtext.scn.cares.ubo.fr.ui.internal.FrActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class ScnExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return Platform.getBundle(FrActivator.PLUGIN_ID);
	}
	
	@Override
	protected Injector getInjector() {
		FrActivator activator = FrActivator.getInstance();
		return activator != null ? activator.getInjector(FrActivator.XTEXT_SCN_CARES_UBO_FR_SCN) : null;
	}

}
