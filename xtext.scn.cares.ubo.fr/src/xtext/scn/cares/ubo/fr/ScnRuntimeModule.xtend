/*
 * generated by Xtext 2.16.0-SNAPSHOT
 */
package xtext.scn.cares.ubo.fr


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class ScnRuntimeModule extends AbstractScnRuntimeModule {
}
