package xtext.scn.cares.ubo.fr.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import xtext.scn.cares.ubo.fr.services.ScnGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalScnParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_STRING", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Simulation'", "'('", "')'", "'system'", "';'", "'begin'", "'{'", "'}'", "'scenarios'", "'end'", "'simulationTime'", "'['", "','", "']'", "':'", "'.Initialize()'", "'.Start()'", "'.Stop()'", "'bind'", "'to'", "'.'", "'='", "'::'", "'Scenario'", "'events'", "'logs'", "'instant'", "'CInt'", "'CDouble'", "'CBoolean'", "'CString'", "'Record'", "'-'", "'Exhaustive exploration with'", "'constraints'", "'objectives'", "'Random exploration with'", "'OneByOne exploration with'", "'Differential exploration with'", "'UserPolicy exploration'", "'with'", "'true'", "'false'", "'E'", "'e'", "'mms'", "'ms'", "'s'", "'h'", "'csv'", "'json'", "'timed'", "'untimed'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=6;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalScnParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalScnParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalScnParser.tokenNames; }
    public String getGrammarFileName() { return "InternalScn.g"; }



     	private ScnGrammarAccess grammarAccess;

        public InternalScnParser(TokenStream input, ScnGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Simulation";
       	}

       	@Override
       	protected ScnGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleSimulation"
    // InternalScn.g:65:1: entryRuleSimulation returns [EObject current=null] : iv_ruleSimulation= ruleSimulation EOF ;
    public final EObject entryRuleSimulation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimulation = null;


        try {
            // InternalScn.g:65:51: (iv_ruleSimulation= ruleSimulation EOF )
            // InternalScn.g:66:2: iv_ruleSimulation= ruleSimulation EOF
            {
             newCompositeNode(grammarAccess.getSimulationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimulation=ruleSimulation();

            state._fsp--;

             current =iv_ruleSimulation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimulation"


    // $ANTLR start "ruleSimulation"
    // InternalScn.g:72:1: ruleSimulation returns [EObject current=null] : (otherlv_0= 'Simulation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_timeUnit_3_0= ruleTimeUnits ) ) otherlv_4= ')' (otherlv_5= 'system' ( ( ruleEString ) ) )? otherlv_7= ';' ( (lv_importantDates_8_0= ruleTime ) ) otherlv_9= 'begin' otherlv_10= '{' ( ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )* )? otherlv_15= '}' otherlv_16= 'scenarios' otherlv_17= '{' ( (lv_scenarios_18_0= ruleScenario ) ) otherlv_19= ';' ( ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';' )* otherlv_22= '}' otherlv_23= 'end' otherlv_24= '{' ( ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )* )? otherlv_29= '}' ) ;
    public final EObject ruleSimulation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        Enumerator lv_timeUnit_3_0 = null;

        EObject lv_importantDates_8_0 = null;

        EObject lv_begin_11_0 = null;

        EObject lv_begin_13_0 = null;

        EObject lv_scenarios_18_0 = null;

        EObject lv_scenarios_20_0 = null;

        EObject lv_end_25_0 = null;

        EObject lv_end_27_0 = null;



        	enterRule();

        try {
            // InternalScn.g:78:2: ( (otherlv_0= 'Simulation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_timeUnit_3_0= ruleTimeUnits ) ) otherlv_4= ')' (otherlv_5= 'system' ( ( ruleEString ) ) )? otherlv_7= ';' ( (lv_importantDates_8_0= ruleTime ) ) otherlv_9= 'begin' otherlv_10= '{' ( ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )* )? otherlv_15= '}' otherlv_16= 'scenarios' otherlv_17= '{' ( (lv_scenarios_18_0= ruleScenario ) ) otherlv_19= ';' ( ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';' )* otherlv_22= '}' otherlv_23= 'end' otherlv_24= '{' ( ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )* )? otherlv_29= '}' ) )
            // InternalScn.g:79:2: (otherlv_0= 'Simulation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_timeUnit_3_0= ruleTimeUnits ) ) otherlv_4= ')' (otherlv_5= 'system' ( ( ruleEString ) ) )? otherlv_7= ';' ( (lv_importantDates_8_0= ruleTime ) ) otherlv_9= 'begin' otherlv_10= '{' ( ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )* )? otherlv_15= '}' otherlv_16= 'scenarios' otherlv_17= '{' ( (lv_scenarios_18_0= ruleScenario ) ) otherlv_19= ';' ( ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';' )* otherlv_22= '}' otherlv_23= 'end' otherlv_24= '{' ( ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )* )? otherlv_29= '}' )
            {
            // InternalScn.g:79:2: (otherlv_0= 'Simulation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_timeUnit_3_0= ruleTimeUnits ) ) otherlv_4= ')' (otherlv_5= 'system' ( ( ruleEString ) ) )? otherlv_7= ';' ( (lv_importantDates_8_0= ruleTime ) ) otherlv_9= 'begin' otherlv_10= '{' ( ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )* )? otherlv_15= '}' otherlv_16= 'scenarios' otherlv_17= '{' ( (lv_scenarios_18_0= ruleScenario ) ) otherlv_19= ';' ( ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';' )* otherlv_22= '}' otherlv_23= 'end' otherlv_24= '{' ( ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )* )? otherlv_29= '}' )
            // InternalScn.g:80:3: otherlv_0= 'Simulation' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' ( (lv_timeUnit_3_0= ruleTimeUnits ) ) otherlv_4= ')' (otherlv_5= 'system' ( ( ruleEString ) ) )? otherlv_7= ';' ( (lv_importantDates_8_0= ruleTime ) ) otherlv_9= 'begin' otherlv_10= '{' ( ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )* )? otherlv_15= '}' otherlv_16= 'scenarios' otherlv_17= '{' ( (lv_scenarios_18_0= ruleScenario ) ) otherlv_19= ';' ( ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';' )* otherlv_22= '}' otherlv_23= 'end' otherlv_24= '{' ( ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )* )? otherlv_29= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSimulationAccess().getSimulationKeyword_0());
            		
            // InternalScn.g:84:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:85:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:85:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:86:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSimulationAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimulationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_2, grammarAccess.getSimulationAccess().getLeftParenthesisKeyword_2());
            		
            // InternalScn.g:107:3: ( (lv_timeUnit_3_0= ruleTimeUnits ) )
            // InternalScn.g:108:4: (lv_timeUnit_3_0= ruleTimeUnits )
            {
            // InternalScn.g:108:4: (lv_timeUnit_3_0= ruleTimeUnits )
            // InternalScn.g:109:5: lv_timeUnit_3_0= ruleTimeUnits
            {

            					newCompositeNode(grammarAccess.getSimulationAccess().getTimeUnitTimeUnitsEnumRuleCall_3_0());
            				
            pushFollow(FOLLOW_6);
            lv_timeUnit_3_0=ruleTimeUnits();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimulationRule());
            					}
            					set(
            						current,
            						"timeUnit",
            						lv_timeUnit_3_0,
            						"xtext.scn.cares.ubo.fr.Scn.TimeUnits");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_7); 

            			newLeafNode(otherlv_4, grammarAccess.getSimulationAccess().getRightParenthesisKeyword_4());
            		
            // InternalScn.g:130:3: (otherlv_5= 'system' ( ( ruleEString ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==14) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalScn.g:131:4: otherlv_5= 'system' ( ( ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,14,FOLLOW_3); 

                    				newLeafNode(otherlv_5, grammarAccess.getSimulationAccess().getSystemKeyword_5_0());
                    			
                    // InternalScn.g:135:4: ( ( ruleEString ) )
                    // InternalScn.g:136:5: ( ruleEString )
                    {
                    // InternalScn.g:136:5: ( ruleEString )
                    // InternalScn.g:137:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getSimulationRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getSimulationAccess().getCaresSystemCaresSystemCrossReference_5_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FOLLOW_9); 

            			newLeafNode(otherlv_7, grammarAccess.getSimulationAccess().getSemicolonKeyword_6());
            		
            // InternalScn.g:156:3: ( (lv_importantDates_8_0= ruleTime ) )
            // InternalScn.g:157:4: (lv_importantDates_8_0= ruleTime )
            {
            // InternalScn.g:157:4: (lv_importantDates_8_0= ruleTime )
            // InternalScn.g:158:5: lv_importantDates_8_0= ruleTime
            {

            					newCompositeNode(grammarAccess.getSimulationAccess().getImportantDatesTimeParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_10);
            lv_importantDates_8_0=ruleTime();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimulationRule());
            					}
            					set(
            						current,
            						"importantDates",
            						lv_importantDates_8_0,
            						"xtext.scn.cares.ubo.fr.Scn.Time");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_9=(Token)match(input,16,FOLLOW_11); 

            			newLeafNode(otherlv_9, grammarAccess.getSimulationAccess().getBeginKeyword_8());
            		
            otherlv_10=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_10, grammarAccess.getSimulationAccess().getLeftCurlyBracketKeyword_9());
            		
            // InternalScn.g:183:3: ( ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )* )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( ((LA3_0>=RULE_STRING && LA3_0<=RULE_ID)||LA3_0==29) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalScn.g:184:4: ( (lv_begin_11_0= ruleAction ) ) otherlv_12= ';' ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )*
                    {
                    // InternalScn.g:184:4: ( (lv_begin_11_0= ruleAction ) )
                    // InternalScn.g:185:5: (lv_begin_11_0= ruleAction )
                    {
                    // InternalScn.g:185:5: (lv_begin_11_0= ruleAction )
                    // InternalScn.g:186:6: lv_begin_11_0= ruleAction
                    {

                    						newCompositeNode(grammarAccess.getSimulationAccess().getBeginActionParserRuleCall_10_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_begin_11_0=ruleAction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSimulationRule());
                    						}
                    						add(
                    							current,
                    							"begin",
                    							lv_begin_11_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Action");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_12=(Token)match(input,15,FOLLOW_12); 

                    				newLeafNode(otherlv_12, grammarAccess.getSimulationAccess().getSemicolonKeyword_10_1());
                    			
                    // InternalScn.g:207:4: ( ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';' )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>=RULE_STRING && LA2_0<=RULE_ID)||LA2_0==29) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalScn.g:208:5: ( (lv_begin_13_0= ruleAction ) ) otherlv_14= ';'
                    	    {
                    	    // InternalScn.g:208:5: ( (lv_begin_13_0= ruleAction ) )
                    	    // InternalScn.g:209:6: (lv_begin_13_0= ruleAction )
                    	    {
                    	    // InternalScn.g:209:6: (lv_begin_13_0= ruleAction )
                    	    // InternalScn.g:210:7: lv_begin_13_0= ruleAction
                    	    {

                    	    							newCompositeNode(grammarAccess.getSimulationAccess().getBeginActionParserRuleCall_10_2_0_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_begin_13_0=ruleAction();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSimulationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"begin",
                    	    								lv_begin_13_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.Action");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    otherlv_14=(Token)match(input,15,FOLLOW_12); 

                    	    					newLeafNode(otherlv_14, grammarAccess.getSimulationAccess().getSemicolonKeyword_10_2_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_15=(Token)match(input,18,FOLLOW_13); 

            			newLeafNode(otherlv_15, grammarAccess.getSimulationAccess().getRightCurlyBracketKeyword_11());
            		
            otherlv_16=(Token)match(input,19,FOLLOW_11); 

            			newLeafNode(otherlv_16, grammarAccess.getSimulationAccess().getScenariosKeyword_12());
            		
            otherlv_17=(Token)match(input,17,FOLLOW_14); 

            			newLeafNode(otherlv_17, grammarAccess.getSimulationAccess().getLeftCurlyBracketKeyword_13());
            		
            // InternalScn.g:245:3: ( (lv_scenarios_18_0= ruleScenario ) )
            // InternalScn.g:246:4: (lv_scenarios_18_0= ruleScenario )
            {
            // InternalScn.g:246:4: (lv_scenarios_18_0= ruleScenario )
            // InternalScn.g:247:5: lv_scenarios_18_0= ruleScenario
            {

            					newCompositeNode(grammarAccess.getSimulationAccess().getScenariosScenarioParserRuleCall_14_0());
            				
            pushFollow(FOLLOW_8);
            lv_scenarios_18_0=ruleScenario();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimulationRule());
            					}
            					add(
            						current,
            						"scenarios",
            						lv_scenarios_18_0,
            						"xtext.scn.cares.ubo.fr.Scn.Scenario");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_19=(Token)match(input,15,FOLLOW_15); 

            			newLeafNode(otherlv_19, grammarAccess.getSimulationAccess().getSemicolonKeyword_15());
            		
            // InternalScn.g:268:3: ( ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==34) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalScn.g:269:4: ( (lv_scenarios_20_0= ruleScenario ) ) otherlv_21= ';'
            	    {
            	    // InternalScn.g:269:4: ( (lv_scenarios_20_0= ruleScenario ) )
            	    // InternalScn.g:270:5: (lv_scenarios_20_0= ruleScenario )
            	    {
            	    // InternalScn.g:270:5: (lv_scenarios_20_0= ruleScenario )
            	    // InternalScn.g:271:6: lv_scenarios_20_0= ruleScenario
            	    {

            	    						newCompositeNode(grammarAccess.getSimulationAccess().getScenariosScenarioParserRuleCall_16_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_scenarios_20_0=ruleScenario();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getSimulationRule());
            	    						}
            	    						add(
            	    							current,
            	    							"scenarios",
            	    							lv_scenarios_20_0,
            	    							"xtext.scn.cares.ubo.fr.Scn.Scenario");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_21=(Token)match(input,15,FOLLOW_15); 

            	    				newLeafNode(otherlv_21, grammarAccess.getSimulationAccess().getSemicolonKeyword_16_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_22=(Token)match(input,18,FOLLOW_16); 

            			newLeafNode(otherlv_22, grammarAccess.getSimulationAccess().getRightCurlyBracketKeyword_17());
            		
            otherlv_23=(Token)match(input,20,FOLLOW_11); 

            			newLeafNode(otherlv_23, grammarAccess.getSimulationAccess().getEndKeyword_18());
            		
            otherlv_24=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_24, grammarAccess.getSimulationAccess().getLeftCurlyBracketKeyword_19());
            		
            // InternalScn.g:305:3: ( ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )* )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( ((LA6_0>=RULE_STRING && LA6_0<=RULE_ID)) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalScn.g:306:4: ( (lv_end_25_0= ruleServiceCall ) ) otherlv_26= ';' ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )*
                    {
                    // InternalScn.g:306:4: ( (lv_end_25_0= ruleServiceCall ) )
                    // InternalScn.g:307:5: (lv_end_25_0= ruleServiceCall )
                    {
                    // InternalScn.g:307:5: (lv_end_25_0= ruleServiceCall )
                    // InternalScn.g:308:6: lv_end_25_0= ruleServiceCall
                    {

                    						newCompositeNode(grammarAccess.getSimulationAccess().getEndServiceCallParserRuleCall_20_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_end_25_0=ruleServiceCall();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSimulationRule());
                    						}
                    						add(
                    							current,
                    							"end",
                    							lv_end_25_0,
                    							"xtext.scn.cares.ubo.fr.Scn.ServiceCall");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_26=(Token)match(input,15,FOLLOW_17); 

                    				newLeafNode(otherlv_26, grammarAccess.getSimulationAccess().getSemicolonKeyword_20_1());
                    			
                    // InternalScn.g:329:4: ( ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';' )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( ((LA5_0>=RULE_STRING && LA5_0<=RULE_ID)) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalScn.g:330:5: ( (lv_end_27_0= ruleServiceCall ) ) otherlv_28= ';'
                    	    {
                    	    // InternalScn.g:330:5: ( (lv_end_27_0= ruleServiceCall ) )
                    	    // InternalScn.g:331:6: (lv_end_27_0= ruleServiceCall )
                    	    {
                    	    // InternalScn.g:331:6: (lv_end_27_0= ruleServiceCall )
                    	    // InternalScn.g:332:7: lv_end_27_0= ruleServiceCall
                    	    {

                    	    							newCompositeNode(grammarAccess.getSimulationAccess().getEndServiceCallParserRuleCall_20_2_0_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_end_27_0=ruleServiceCall();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getSimulationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"end",
                    	    								lv_end_27_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.ServiceCall");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    otherlv_28=(Token)match(input,15,FOLLOW_17); 

                    	    					newLeafNode(otherlv_28, grammarAccess.getSimulationAccess().getSemicolonKeyword_20_2_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_29=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_29, grammarAccess.getSimulationAccess().getRightCurlyBracketKeyword_21());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimulation"


    // $ANTLR start "entryRuleTime"
    // InternalScn.g:363:1: entryRuleTime returns [EObject current=null] : iv_ruleTime= ruleTime EOF ;
    public final EObject entryRuleTime() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTime = null;


        try {
            // InternalScn.g:363:45: (iv_ruleTime= ruleTime EOF )
            // InternalScn.g:364:2: iv_ruleTime= ruleTime EOF
            {
             newCompositeNode(grammarAccess.getTimeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTime=ruleTime();

            state._fsp--;

             current =iv_ruleTime; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTime"


    // $ANTLR start "ruleTime"
    // InternalScn.g:370:1: ruleTime returns [EObject current=null] : (otherlv_0= 'simulationTime' otherlv_1= '[' ( (lv_startingTime_2_0= ruleELong ) ) otherlv_3= ',' ( (lv_endTime_4_0= ruleELong ) ) otherlv_5= ']' otherlv_6= ':' ( (lv_stepTime_7_0= ruleELong ) ) otherlv_8= ';' ) ;
    public final EObject ruleTime() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_startingTime_2_0 = null;

        AntlrDatatypeRuleToken lv_endTime_4_0 = null;

        AntlrDatatypeRuleToken lv_stepTime_7_0 = null;



        	enterRule();

        try {
            // InternalScn.g:376:2: ( (otherlv_0= 'simulationTime' otherlv_1= '[' ( (lv_startingTime_2_0= ruleELong ) ) otherlv_3= ',' ( (lv_endTime_4_0= ruleELong ) ) otherlv_5= ']' otherlv_6= ':' ( (lv_stepTime_7_0= ruleELong ) ) otherlv_8= ';' ) )
            // InternalScn.g:377:2: (otherlv_0= 'simulationTime' otherlv_1= '[' ( (lv_startingTime_2_0= ruleELong ) ) otherlv_3= ',' ( (lv_endTime_4_0= ruleELong ) ) otherlv_5= ']' otherlv_6= ':' ( (lv_stepTime_7_0= ruleELong ) ) otherlv_8= ';' )
            {
            // InternalScn.g:377:2: (otherlv_0= 'simulationTime' otherlv_1= '[' ( (lv_startingTime_2_0= ruleELong ) ) otherlv_3= ',' ( (lv_endTime_4_0= ruleELong ) ) otherlv_5= ']' otherlv_6= ':' ( (lv_stepTime_7_0= ruleELong ) ) otherlv_8= ';' )
            // InternalScn.g:378:3: otherlv_0= 'simulationTime' otherlv_1= '[' ( (lv_startingTime_2_0= ruleELong ) ) otherlv_3= ',' ( (lv_endTime_4_0= ruleELong ) ) otherlv_5= ']' otherlv_6= ':' ( (lv_stepTime_7_0= ruleELong ) ) otherlv_8= ';'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_18); 

            			newLeafNode(otherlv_0, grammarAccess.getTimeAccess().getSimulationTimeKeyword_0());
            		
            otherlv_1=(Token)match(input,22,FOLLOW_19); 

            			newLeafNode(otherlv_1, grammarAccess.getTimeAccess().getLeftSquareBracketKeyword_1());
            		
            // InternalScn.g:386:3: ( (lv_startingTime_2_0= ruleELong ) )
            // InternalScn.g:387:4: (lv_startingTime_2_0= ruleELong )
            {
            // InternalScn.g:387:4: (lv_startingTime_2_0= ruleELong )
            // InternalScn.g:388:5: lv_startingTime_2_0= ruleELong
            {

            					newCompositeNode(grammarAccess.getTimeAccess().getStartingTimeELongParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_20);
            lv_startingTime_2_0=ruleELong();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTimeRule());
            					}
            					set(
            						current,
            						"startingTime",
            						lv_startingTime_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.ELong");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_19); 

            			newLeafNode(otherlv_3, grammarAccess.getTimeAccess().getCommaKeyword_3());
            		
            // InternalScn.g:409:3: ( (lv_endTime_4_0= ruleELong ) )
            // InternalScn.g:410:4: (lv_endTime_4_0= ruleELong )
            {
            // InternalScn.g:410:4: (lv_endTime_4_0= ruleELong )
            // InternalScn.g:411:5: lv_endTime_4_0= ruleELong
            {

            					newCompositeNode(grammarAccess.getTimeAccess().getEndTimeELongParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_21);
            lv_endTime_4_0=ruleELong();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTimeRule());
            					}
            					set(
            						current,
            						"endTime",
            						lv_endTime_4_0,
            						"xtext.scn.cares.ubo.fr.Scn.ELong");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,24,FOLLOW_22); 

            			newLeafNode(otherlv_5, grammarAccess.getTimeAccess().getRightSquareBracketKeyword_5());
            		
            otherlv_6=(Token)match(input,25,FOLLOW_19); 

            			newLeafNode(otherlv_6, grammarAccess.getTimeAccess().getColonKeyword_6());
            		
            // InternalScn.g:436:3: ( (lv_stepTime_7_0= ruleELong ) )
            // InternalScn.g:437:4: (lv_stepTime_7_0= ruleELong )
            {
            // InternalScn.g:437:4: (lv_stepTime_7_0= ruleELong )
            // InternalScn.g:438:5: lv_stepTime_7_0= ruleELong
            {

            					newCompositeNode(grammarAccess.getTimeAccess().getStepTimeELongParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_8);
            lv_stepTime_7_0=ruleELong();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTimeRule());
            					}
            					set(
            						current,
            						"stepTime",
            						lv_stepTime_7_0,
            						"xtext.scn.cares.ubo.fr.Scn.ELong");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getTimeAccess().getSemicolonKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTime"


    // $ANTLR start "entryRuleAction"
    // InternalScn.g:463:1: entryRuleAction returns [EObject current=null] : iv_ruleAction= ruleAction EOF ;
    public final EObject entryRuleAction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAction = null;


        try {
            // InternalScn.g:463:47: (iv_ruleAction= ruleAction EOF )
            // InternalScn.g:464:2: iv_ruleAction= ruleAction EOF
            {
             newCompositeNode(grammarAccess.getActionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAction=ruleAction();

            state._fsp--;

             current =iv_ruleAction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAction"


    // $ANTLR start "ruleAction"
    // InternalScn.g:470:1: ruleAction returns [EObject current=null] : (this_ComponentInitialization_0= ruleComponentInitialization | this_ServiceCall_1= ruleServiceCall | this_ParameterChange_2= ruleParameterChange | this_ComponentStart_3= ruleComponentStart | this_ComponentStop_4= ruleComponentStop | this_Switch_5= ruleSwitch ) ;
    public final EObject ruleAction() throws RecognitionException {
        EObject current = null;

        EObject this_ComponentInitialization_0 = null;

        EObject this_ServiceCall_1 = null;

        EObject this_ParameterChange_2 = null;

        EObject this_ComponentStart_3 = null;

        EObject this_ComponentStop_4 = null;

        EObject this_Switch_5 = null;



        	enterRule();

        try {
            // InternalScn.g:476:2: ( (this_ComponentInitialization_0= ruleComponentInitialization | this_ServiceCall_1= ruleServiceCall | this_ParameterChange_2= ruleParameterChange | this_ComponentStart_3= ruleComponentStart | this_ComponentStop_4= ruleComponentStop | this_Switch_5= ruleSwitch ) )
            // InternalScn.g:477:2: (this_ComponentInitialization_0= ruleComponentInitialization | this_ServiceCall_1= ruleServiceCall | this_ParameterChange_2= ruleParameterChange | this_ComponentStart_3= ruleComponentStart | this_ComponentStop_4= ruleComponentStop | this_Switch_5= ruleSwitch )
            {
            // InternalScn.g:477:2: (this_ComponentInitialization_0= ruleComponentInitialization | this_ServiceCall_1= ruleServiceCall | this_ParameterChange_2= ruleParameterChange | this_ComponentStart_3= ruleComponentStart | this_ComponentStop_4= ruleComponentStop | this_Switch_5= ruleSwitch )
            int alt7=6;
            switch ( input.LA(1) ) {
            case RULE_STRING:
                {
                switch ( input.LA(2) ) {
                case 32:
                    {
                    alt7=3;
                    }
                    break;
                case 26:
                    {
                    alt7=1;
                    }
                    break;
                case 27:
                    {
                    alt7=4;
                    }
                    break;
                case 31:
                    {
                    alt7=2;
                    }
                    break;
                case 28:
                    {
                    alt7=5;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }

                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case 28:
                    {
                    alt7=5;
                    }
                    break;
                case 32:
                    {
                    alt7=3;
                    }
                    break;
                case 26:
                    {
                    alt7=1;
                    }
                    break;
                case 27:
                    {
                    alt7=4;
                    }
                    break;
                case 31:
                    {
                    alt7=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 2, input);

                    throw nvae;
                }

                }
                break;
            case 29:
                {
                alt7=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalScn.g:478:3: this_ComponentInitialization_0= ruleComponentInitialization
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getComponentInitializationParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComponentInitialization_0=ruleComponentInitialization();

                    state._fsp--;


                    			current = this_ComponentInitialization_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalScn.g:487:3: this_ServiceCall_1= ruleServiceCall
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getServiceCallParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ServiceCall_1=ruleServiceCall();

                    state._fsp--;


                    			current = this_ServiceCall_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalScn.g:496:3: this_ParameterChange_2= ruleParameterChange
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getParameterChangeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ParameterChange_2=ruleParameterChange();

                    state._fsp--;


                    			current = this_ParameterChange_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalScn.g:505:3: this_ComponentStart_3= ruleComponentStart
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getComponentStartParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComponentStart_3=ruleComponentStart();

                    state._fsp--;


                    			current = this_ComponentStart_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalScn.g:514:3: this_ComponentStop_4= ruleComponentStop
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getComponentStopParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComponentStop_4=ruleComponentStop();

                    state._fsp--;


                    			current = this_ComponentStop_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalScn.g:523:3: this_Switch_5= ruleSwitch
                    {

                    			newCompositeNode(grammarAccess.getActionAccess().getSwitchParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_Switch_5=ruleSwitch();

                    state._fsp--;


                    			current = this_Switch_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAction"


    // $ANTLR start "entryRuleComponentInitialization"
    // InternalScn.g:535:1: entryRuleComponentInitialization returns [EObject current=null] : iv_ruleComponentInitialization= ruleComponentInitialization EOF ;
    public final EObject entryRuleComponentInitialization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponentInitialization = null;


        try {
            // InternalScn.g:535:64: (iv_ruleComponentInitialization= ruleComponentInitialization EOF )
            // InternalScn.g:536:2: iv_ruleComponentInitialization= ruleComponentInitialization EOF
            {
             newCompositeNode(grammarAccess.getComponentInitializationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComponentInitialization=ruleComponentInitialization();

            state._fsp--;

             current =iv_ruleComponentInitialization; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponentInitialization"


    // $ANTLR start "ruleComponentInitialization"
    // InternalScn.g:542:1: ruleComponentInitialization returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '.Initialize()' ) ;
    public final EObject ruleComponentInitialization() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalScn.g:548:2: ( ( ( ( ruleEString ) ) otherlv_1= '.Initialize()' ) )
            // InternalScn.g:549:2: ( ( ( ruleEString ) ) otherlv_1= '.Initialize()' )
            {
            // InternalScn.g:549:2: ( ( ( ruleEString ) ) otherlv_1= '.Initialize()' )
            // InternalScn.g:550:3: ( ( ruleEString ) ) otherlv_1= '.Initialize()'
            {
            // InternalScn.g:550:3: ( ( ruleEString ) )
            // InternalScn.g:551:4: ( ruleEString )
            {
            // InternalScn.g:551:4: ( ruleEString )
            // InternalScn.g:552:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComponentInitializationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getComponentInitializationAccess().getComponentComponentCrossReference_0_0());
            				
            pushFollow(FOLLOW_23);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,26,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getComponentInitializationAccess().getInitializeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponentInitialization"


    // $ANTLR start "entryRuleComponentStart"
    // InternalScn.g:574:1: entryRuleComponentStart returns [EObject current=null] : iv_ruleComponentStart= ruleComponentStart EOF ;
    public final EObject entryRuleComponentStart() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponentStart = null;


        try {
            // InternalScn.g:574:55: (iv_ruleComponentStart= ruleComponentStart EOF )
            // InternalScn.g:575:2: iv_ruleComponentStart= ruleComponentStart EOF
            {
             newCompositeNode(grammarAccess.getComponentStartRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComponentStart=ruleComponentStart();

            state._fsp--;

             current =iv_ruleComponentStart; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponentStart"


    // $ANTLR start "ruleComponentStart"
    // InternalScn.g:581:1: ruleComponentStart returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '.Start()' ) ;
    public final EObject ruleComponentStart() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalScn.g:587:2: ( ( ( ( ruleEString ) ) otherlv_1= '.Start()' ) )
            // InternalScn.g:588:2: ( ( ( ruleEString ) ) otherlv_1= '.Start()' )
            {
            // InternalScn.g:588:2: ( ( ( ruleEString ) ) otherlv_1= '.Start()' )
            // InternalScn.g:589:3: ( ( ruleEString ) ) otherlv_1= '.Start()'
            {
            // InternalScn.g:589:3: ( ( ruleEString ) )
            // InternalScn.g:590:4: ( ruleEString )
            {
            // InternalScn.g:590:4: ( ruleEString )
            // InternalScn.g:591:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComponentStartRule());
            					}
            				

            					newCompositeNode(grammarAccess.getComponentStartAccess().getComponentLeafComponentCrossReference_0_0());
            				
            pushFollow(FOLLOW_24);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,27,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getComponentStartAccess().getStartKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponentStart"


    // $ANTLR start "entryRuleComponentStop"
    // InternalScn.g:613:1: entryRuleComponentStop returns [EObject current=null] : iv_ruleComponentStop= ruleComponentStop EOF ;
    public final EObject entryRuleComponentStop() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComponentStop = null;


        try {
            // InternalScn.g:613:54: (iv_ruleComponentStop= ruleComponentStop EOF )
            // InternalScn.g:614:2: iv_ruleComponentStop= ruleComponentStop EOF
            {
             newCompositeNode(grammarAccess.getComponentStopRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComponentStop=ruleComponentStop();

            state._fsp--;

             current =iv_ruleComponentStop; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComponentStop"


    // $ANTLR start "ruleComponentStop"
    // InternalScn.g:620:1: ruleComponentStop returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '.Stop()' ) ;
    public final EObject ruleComponentStop() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalScn.g:626:2: ( ( ( ( ruleEString ) ) otherlv_1= '.Stop()' ) )
            // InternalScn.g:627:2: ( ( ( ruleEString ) ) otherlv_1= '.Stop()' )
            {
            // InternalScn.g:627:2: ( ( ( ruleEString ) ) otherlv_1= '.Stop()' )
            // InternalScn.g:628:3: ( ( ruleEString ) ) otherlv_1= '.Stop()'
            {
            // InternalScn.g:628:3: ( ( ruleEString ) )
            // InternalScn.g:629:4: ( ruleEString )
            {
            // InternalScn.g:629:4: ( ruleEString )
            // InternalScn.g:630:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getComponentStopRule());
            					}
            				

            					newCompositeNode(grammarAccess.getComponentStopAccess().getComponentLeafComponentCrossReference_0_0());
            				
            pushFollow(FOLLOW_25);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,28,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getComponentStopAccess().getStopKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComponentStop"


    // $ANTLR start "entryRuleSwitch"
    // InternalScn.g:652:1: entryRuleSwitch returns [EObject current=null] : iv_ruleSwitch= ruleSwitch EOF ;
    public final EObject entryRuleSwitch() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSwitch = null;


        try {
            // InternalScn.g:652:47: (iv_ruleSwitch= ruleSwitch EOF )
            // InternalScn.g:653:2: iv_ruleSwitch= ruleSwitch EOF
            {
             newCompositeNode(grammarAccess.getSwitchRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSwitch=ruleSwitch();

            state._fsp--;

             current =iv_ruleSwitch; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSwitch"


    // $ANTLR start "ruleSwitch"
    // InternalScn.g:659:1: ruleSwitch returns [EObject current=null] : (otherlv_0= 'bind' ( ( ruleEString ) ) otherlv_2= 'to' ( ( ruleEString ) ) ) ;
    public final EObject ruleSwitch() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalScn.g:665:2: ( (otherlv_0= 'bind' ( ( ruleEString ) ) otherlv_2= 'to' ( ( ruleEString ) ) ) )
            // InternalScn.g:666:2: (otherlv_0= 'bind' ( ( ruleEString ) ) otherlv_2= 'to' ( ( ruleEString ) ) )
            {
            // InternalScn.g:666:2: (otherlv_0= 'bind' ( ( ruleEString ) ) otherlv_2= 'to' ( ( ruleEString ) ) )
            // InternalScn.g:667:3: otherlv_0= 'bind' ( ( ruleEString ) ) otherlv_2= 'to' ( ( ruleEString ) )
            {
            otherlv_0=(Token)match(input,29,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getSwitchAccess().getBindKeyword_0());
            		
            // InternalScn.g:671:3: ( ( ruleEString ) )
            // InternalScn.g:672:4: ( ruleEString )
            {
            // InternalScn.g:672:4: ( ruleEString )
            // InternalScn.g:673:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSwitchRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSwitchAccess().getProvidedInterfacePortProvidedInterfacePortCrossReference_1_0());
            				
            pushFollow(FOLLOW_26);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getSwitchAccess().getToKeyword_2());
            		
            // InternalScn.g:691:3: ( ( ruleEString ) )
            // InternalScn.g:692:4: ( ruleEString )
            {
            // InternalScn.g:692:4: ( ruleEString )
            // InternalScn.g:693:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSwitchRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSwitchAccess().getRequiredInterfacePortRequiredInterfacePortCrossReference_3_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSwitch"


    // $ANTLR start "entryRuleServiceCall"
    // InternalScn.g:711:1: entryRuleServiceCall returns [EObject current=null] : iv_ruleServiceCall= ruleServiceCall EOF ;
    public final EObject entryRuleServiceCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleServiceCall = null;


        try {
            // InternalScn.g:711:52: (iv_ruleServiceCall= ruleServiceCall EOF )
            // InternalScn.g:712:2: iv_ruleServiceCall= ruleServiceCall EOF
            {
             newCompositeNode(grammarAccess.getServiceCallRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleServiceCall=ruleServiceCall();

            state._fsp--;

             current =iv_ruleServiceCall; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleServiceCall"


    // $ANTLR start "ruleServiceCall"
    // InternalScn.g:718:1: ruleServiceCall returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '.' ( (lv_functionCall_2_0= ruleFunctionCall ) ) ) ;
    public final EObject ruleServiceCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_functionCall_2_0 = null;



        	enterRule();

        try {
            // InternalScn.g:724:2: ( ( ( ( ruleEString ) ) otherlv_1= '.' ( (lv_functionCall_2_0= ruleFunctionCall ) ) ) )
            // InternalScn.g:725:2: ( ( ( ruleEString ) ) otherlv_1= '.' ( (lv_functionCall_2_0= ruleFunctionCall ) ) )
            {
            // InternalScn.g:725:2: ( ( ( ruleEString ) ) otherlv_1= '.' ( (lv_functionCall_2_0= ruleFunctionCall ) ) )
            // InternalScn.g:726:3: ( ( ruleEString ) ) otherlv_1= '.' ( (lv_functionCall_2_0= ruleFunctionCall ) )
            {
            // InternalScn.g:726:3: ( ( ruleEString ) )
            // InternalScn.g:727:4: ( ruleEString )
            {
            // InternalScn.g:727:4: ( ruleEString )
            // InternalScn.g:728:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getServiceCallRule());
            					}
            				

            					newCompositeNode(grammarAccess.getServiceCallAccess().getComponentLeafComponentCrossReference_0_0());
            				
            pushFollow(FOLLOW_27);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,31,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getServiceCallAccess().getFullStopKeyword_1());
            		
            // InternalScn.g:746:3: ( (lv_functionCall_2_0= ruleFunctionCall ) )
            // InternalScn.g:747:4: (lv_functionCall_2_0= ruleFunctionCall )
            {
            // InternalScn.g:747:4: (lv_functionCall_2_0= ruleFunctionCall )
            // InternalScn.g:748:5: lv_functionCall_2_0= ruleFunctionCall
            {

            					newCompositeNode(grammarAccess.getServiceCallAccess().getFunctionCallFunctionCallParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_functionCall_2_0=ruleFunctionCall();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getServiceCallRule());
            					}
            					set(
            						current,
            						"functionCall",
            						lv_functionCall_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.FunctionCall");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleServiceCall"


    // $ANTLR start "entryRuleParameterChange"
    // InternalScn.g:769:1: entryRuleParameterChange returns [EObject current=null] : iv_ruleParameterChange= ruleParameterChange EOF ;
    public final EObject entryRuleParameterChange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterChange = null;


        try {
            // InternalScn.g:769:56: (iv_ruleParameterChange= ruleParameterChange EOF )
            // InternalScn.g:770:2: iv_ruleParameterChange= ruleParameterChange EOF
            {
             newCompositeNode(grammarAccess.getParameterChangeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameterChange=ruleParameterChange();

            state._fsp--;

             current =iv_ruleParameterChange; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterChange"


    // $ANTLR start "ruleParameterChange"
    // InternalScn.g:776:1: ruleParameterChange returns [EObject current=null] : (this_SimpleDoubleParameterSet_0= ruleSimpleDoubleParameterSet | this_SimpleIntParameterSet_1= ruleSimpleIntParameterSet | this_SimpleBoolParameterSet_2= ruleSimpleBoolParameterSet | this_SimpleStringParameterSet_3= ruleSimpleStringParameterSet ) ;
    public final EObject ruleParameterChange() throws RecognitionException {
        EObject current = null;

        EObject this_SimpleDoubleParameterSet_0 = null;

        EObject this_SimpleIntParameterSet_1 = null;

        EObject this_SimpleBoolParameterSet_2 = null;

        EObject this_SimpleStringParameterSet_3 = null;



        	enterRule();

        try {
            // InternalScn.g:782:2: ( (this_SimpleDoubleParameterSet_0= ruleSimpleDoubleParameterSet | this_SimpleIntParameterSet_1= ruleSimpleIntParameterSet | this_SimpleBoolParameterSet_2= ruleSimpleBoolParameterSet | this_SimpleStringParameterSet_3= ruleSimpleStringParameterSet ) )
            // InternalScn.g:783:2: (this_SimpleDoubleParameterSet_0= ruleSimpleDoubleParameterSet | this_SimpleIntParameterSet_1= ruleSimpleIntParameterSet | this_SimpleBoolParameterSet_2= ruleSimpleBoolParameterSet | this_SimpleStringParameterSet_3= ruleSimpleStringParameterSet )
            {
            // InternalScn.g:783:2: (this_SimpleDoubleParameterSet_0= ruleSimpleDoubleParameterSet | this_SimpleIntParameterSet_1= ruleSimpleIntParameterSet | this_SimpleBoolParameterSet_2= ruleSimpleBoolParameterSet | this_SimpleStringParameterSet_3= ruleSimpleStringParameterSet )
            int alt8=4;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // InternalScn.g:784:3: this_SimpleDoubleParameterSet_0= ruleSimpleDoubleParameterSet
                    {

                    			newCompositeNode(grammarAccess.getParameterChangeAccess().getSimpleDoubleParameterSetParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleDoubleParameterSet_0=ruleSimpleDoubleParameterSet();

                    state._fsp--;


                    			current = this_SimpleDoubleParameterSet_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalScn.g:793:3: this_SimpleIntParameterSet_1= ruleSimpleIntParameterSet
                    {

                    			newCompositeNode(grammarAccess.getParameterChangeAccess().getSimpleIntParameterSetParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleIntParameterSet_1=ruleSimpleIntParameterSet();

                    state._fsp--;


                    			current = this_SimpleIntParameterSet_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalScn.g:802:3: this_SimpleBoolParameterSet_2= ruleSimpleBoolParameterSet
                    {

                    			newCompositeNode(grammarAccess.getParameterChangeAccess().getSimpleBoolParameterSetParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleBoolParameterSet_2=ruleSimpleBoolParameterSet();

                    state._fsp--;


                    			current = this_SimpleBoolParameterSet_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalScn.g:811:3: this_SimpleStringParameterSet_3= ruleSimpleStringParameterSet
                    {

                    			newCompositeNode(grammarAccess.getParameterChangeAccess().getSimpleStringParameterSetParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleStringParameterSet_3=ruleSimpleStringParameterSet();

                    state._fsp--;


                    			current = this_SimpleStringParameterSet_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterChange"


    // $ANTLR start "entryRuleSimpleStringParameterSet"
    // InternalScn.g:823:1: entryRuleSimpleStringParameterSet returns [EObject current=null] : iv_ruleSimpleStringParameterSet= ruleSimpleStringParameterSet EOF ;
    public final EObject entryRuleSimpleStringParameterSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleStringParameterSet = null;


        try {
            // InternalScn.g:823:65: (iv_ruleSimpleStringParameterSet= ruleSimpleStringParameterSet EOF )
            // InternalScn.g:824:2: iv_ruleSimpleStringParameterSet= ruleSimpleStringParameterSet EOF
            {
             newCompositeNode(grammarAccess.getSimpleStringParameterSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleStringParameterSet=ruleSimpleStringParameterSet();

            state._fsp--;

             current =iv_ruleSimpleStringParameterSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleStringParameterSet"


    // $ANTLR start "ruleSimpleStringParameterSet"
    // InternalScn.g:830:1: ruleSimpleStringParameterSet returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEString ) ) ) ;
    public final EObject ruleSimpleStringParameterSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalScn.g:836:2: ( ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEString ) ) ) )
            // InternalScn.g:837:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEString ) ) )
            {
            // InternalScn.g:837:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEString ) ) )
            // InternalScn.g:838:3: ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEString ) )
            {
            // InternalScn.g:838:3: ( ( ruleEString ) )
            // InternalScn.g:839:4: ( ruleEString )
            {
            // InternalScn.g:839:4: ( ruleEString )
            // InternalScn.g:840:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleStringParameterSetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSimpleStringParameterSetAccess().getParameterParameterInstanciationCrossReference_0_0());
            				
            pushFollow(FOLLOW_28);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleStringParameterSetAccess().getEqualsSignKeyword_1());
            		
            // InternalScn.g:858:3: ( (lv_value_2_0= ruleEString ) )
            // InternalScn.g:859:4: (lv_value_2_0= ruleEString )
            {
            // InternalScn.g:859:4: (lv_value_2_0= ruleEString )
            // InternalScn.g:860:5: lv_value_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSimpleStringParameterSetAccess().getValueEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleStringParameterSetRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleStringParameterSet"


    // $ANTLR start "entryRuleSimpleDoubleParameterSet"
    // InternalScn.g:881:1: entryRuleSimpleDoubleParameterSet returns [EObject current=null] : iv_ruleSimpleDoubleParameterSet= ruleSimpleDoubleParameterSet EOF ;
    public final EObject entryRuleSimpleDoubleParameterSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleDoubleParameterSet = null;


        try {
            // InternalScn.g:881:65: (iv_ruleSimpleDoubleParameterSet= ruleSimpleDoubleParameterSet EOF )
            // InternalScn.g:882:2: iv_ruleSimpleDoubleParameterSet= ruleSimpleDoubleParameterSet EOF
            {
             newCompositeNode(grammarAccess.getSimpleDoubleParameterSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleDoubleParameterSet=ruleSimpleDoubleParameterSet();

            state._fsp--;

             current =iv_ruleSimpleDoubleParameterSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleDoubleParameterSet"


    // $ANTLR start "ruleSimpleDoubleParameterSet"
    // InternalScn.g:888:1: ruleSimpleDoubleParameterSet returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEDouble ) ) ) ;
    public final EObject ruleSimpleDoubleParameterSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalScn.g:894:2: ( ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEDouble ) ) ) )
            // InternalScn.g:895:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEDouble ) ) )
            {
            // InternalScn.g:895:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEDouble ) ) )
            // InternalScn.g:896:3: ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEDouble ) )
            {
            // InternalScn.g:896:3: ( ( ruleEString ) )
            // InternalScn.g:897:4: ( ruleEString )
            {
            // InternalScn.g:897:4: ( ruleEString )
            // InternalScn.g:898:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleDoubleParameterSetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSimpleDoubleParameterSetAccess().getParameterParameterInstanciationCrossReference_0_0());
            				
            pushFollow(FOLLOW_28);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,32,FOLLOW_29); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleDoubleParameterSetAccess().getEqualsSignKeyword_1());
            		
            // InternalScn.g:916:3: ( (lv_value_2_0= ruleEDouble ) )
            // InternalScn.g:917:4: (lv_value_2_0= ruleEDouble )
            {
            // InternalScn.g:917:4: (lv_value_2_0= ruleEDouble )
            // InternalScn.g:918:5: lv_value_2_0= ruleEDouble
            {

            					newCompositeNode(grammarAccess.getSimpleDoubleParameterSetAccess().getValueEDoubleParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleEDouble();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleDoubleParameterSetRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.EDouble");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleDoubleParameterSet"


    // $ANTLR start "entryRuleSimpleIntParameterSet"
    // InternalScn.g:939:1: entryRuleSimpleIntParameterSet returns [EObject current=null] : iv_ruleSimpleIntParameterSet= ruleSimpleIntParameterSet EOF ;
    public final EObject entryRuleSimpleIntParameterSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleIntParameterSet = null;


        try {
            // InternalScn.g:939:62: (iv_ruleSimpleIntParameterSet= ruleSimpleIntParameterSet EOF )
            // InternalScn.g:940:2: iv_ruleSimpleIntParameterSet= ruleSimpleIntParameterSet EOF
            {
             newCompositeNode(grammarAccess.getSimpleIntParameterSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleIntParameterSet=ruleSimpleIntParameterSet();

            state._fsp--;

             current =iv_ruleSimpleIntParameterSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleIntParameterSet"


    // $ANTLR start "ruleSimpleIntParameterSet"
    // InternalScn.g:946:1: ruleSimpleIntParameterSet returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEInt ) ) ) ;
    public final EObject ruleSimpleIntParameterSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalScn.g:952:2: ( ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEInt ) ) ) )
            // InternalScn.g:953:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEInt ) ) )
            {
            // InternalScn.g:953:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEInt ) ) )
            // InternalScn.g:954:3: ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEInt ) )
            {
            // InternalScn.g:954:3: ( ( ruleEString ) )
            // InternalScn.g:955:4: ( ruleEString )
            {
            // InternalScn.g:955:4: ( ruleEString )
            // InternalScn.g:956:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleIntParameterSetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSimpleIntParameterSetAccess().getParameterParameterInstanciationCrossReference_0_0());
            				
            pushFollow(FOLLOW_28);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,32,FOLLOW_19); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleIntParameterSetAccess().getEqualsSignKeyword_1());
            		
            // InternalScn.g:974:3: ( (lv_value_2_0= ruleEInt ) )
            // InternalScn.g:975:4: (lv_value_2_0= ruleEInt )
            {
            // InternalScn.g:975:4: (lv_value_2_0= ruleEInt )
            // InternalScn.g:976:5: lv_value_2_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getSimpleIntParameterSetAccess().getValueEIntParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleIntParameterSetRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleIntParameterSet"


    // $ANTLR start "entryRuleSimpleBoolParameterSet"
    // InternalScn.g:997:1: entryRuleSimpleBoolParameterSet returns [EObject current=null] : iv_ruleSimpleBoolParameterSet= ruleSimpleBoolParameterSet EOF ;
    public final EObject entryRuleSimpleBoolParameterSet() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleBoolParameterSet = null;


        try {
            // InternalScn.g:997:63: (iv_ruleSimpleBoolParameterSet= ruleSimpleBoolParameterSet EOF )
            // InternalScn.g:998:2: iv_ruleSimpleBoolParameterSet= ruleSimpleBoolParameterSet EOF
            {
             newCompositeNode(grammarAccess.getSimpleBoolParameterSetRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleBoolParameterSet=ruleSimpleBoolParameterSet();

            state._fsp--;

             current =iv_ruleSimpleBoolParameterSet; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleBoolParameterSet"


    // $ANTLR start "ruleSimpleBoolParameterSet"
    // InternalScn.g:1004:1: ruleSimpleBoolParameterSet returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEBoolean ) ) ) ;
    public final EObject ruleSimpleBoolParameterSet() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        AntlrDatatypeRuleToken lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1010:2: ( ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEBoolean ) ) ) )
            // InternalScn.g:1011:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEBoolean ) ) )
            {
            // InternalScn.g:1011:2: ( ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEBoolean ) ) )
            // InternalScn.g:1012:3: ( ( ruleEString ) ) otherlv_1= '=' ( (lv_value_2_0= ruleEBoolean ) )
            {
            // InternalScn.g:1012:3: ( ( ruleEString ) )
            // InternalScn.g:1013:4: ( ruleEString )
            {
            // InternalScn.g:1013:4: ( ruleEString )
            // InternalScn.g:1014:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getSimpleBoolParameterSetRule());
            					}
            				

            					newCompositeNode(grammarAccess.getSimpleBoolParameterSetAccess().getParameterParameterInstanciationCrossReference_0_0());
            				
            pushFollow(FOLLOW_28);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,32,FOLLOW_30); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleBoolParameterSetAccess().getEqualsSignKeyword_1());
            		
            // InternalScn.g:1032:3: ( (lv_value_2_0= ruleEBoolean ) )
            // InternalScn.g:1033:4: (lv_value_2_0= ruleEBoolean )
            {
            // InternalScn.g:1033:4: (lv_value_2_0= ruleEBoolean )
            // InternalScn.g:1034:5: lv_value_2_0= ruleEBoolean
            {

            					newCompositeNode(grammarAccess.getSimpleBoolParameterSetAccess().getValueEBooleanParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleEBoolean();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSimpleBoolParameterSetRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.EBoolean");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleBoolParameterSet"


    // $ANTLR start "entryRuleFunctionCall"
    // InternalScn.g:1055:1: entryRuleFunctionCall returns [EObject current=null] : iv_ruleFunctionCall= ruleFunctionCall EOF ;
    public final EObject entryRuleFunctionCall() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionCall = null;


        try {
            // InternalScn.g:1055:53: (iv_ruleFunctionCall= ruleFunctionCall EOF )
            // InternalScn.g:1056:2: iv_ruleFunctionCall= ruleFunctionCall EOF
            {
             newCompositeNode(grammarAccess.getFunctionCallRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionCall=ruleFunctionCall();

            state._fsp--;

             current =iv_ruleFunctionCall; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionCall"


    // $ANTLR start "ruleFunctionCall"
    // InternalScn.g:1062:1: ruleFunctionCall returns [EObject current=null] : ( ( ( ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )* )? otherlv_5= ')' ) ;
    public final EObject ruleFunctionCall() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_parameters_2_0 = null;

        EObject lv_parameters_4_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1068:2: ( ( ( ( ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )* )? otherlv_5= ')' ) )
            // InternalScn.g:1069:2: ( ( ( ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )* )? otherlv_5= ')' )
            {
            // InternalScn.g:1069:2: ( ( ( ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )* )? otherlv_5= ')' )
            // InternalScn.g:1070:3: ( ( ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )* )? otherlv_5= ')'
            {
            // InternalScn.g:1070:3: ( ( ruleEString ) )
            // InternalScn.g:1071:4: ( ruleEString )
            {
            // InternalScn.g:1071:4: ( ruleEString )
            // InternalScn.g:1072:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionCallRule());
            					}
            				

            					newCompositeNode(grammarAccess.getFunctionCallAccess().getFunctionFunctionCrossReference_0_0());
            				
            pushFollow(FOLLOW_4);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_31); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionCallAccess().getLeftParenthesisKeyword_1());
            		
            // InternalScn.g:1090:3: ( ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )* )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( ((LA10_0>=RULE_STRING && LA10_0<=RULE_ID)) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalScn.g:1091:4: ( (lv_parameters_2_0= ruleParameterInstanciation ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )*
                    {
                    // InternalScn.g:1091:4: ( (lv_parameters_2_0= ruleParameterInstanciation ) )
                    // InternalScn.g:1092:5: (lv_parameters_2_0= ruleParameterInstanciation )
                    {
                    // InternalScn.g:1092:5: (lv_parameters_2_0= ruleParameterInstanciation )
                    // InternalScn.g:1093:6: lv_parameters_2_0= ruleParameterInstanciation
                    {

                    						newCompositeNode(grammarAccess.getFunctionCallAccess().getParametersParameterInstanciationParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_32);
                    lv_parameters_2_0=ruleParameterInstanciation();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFunctionCallRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_2_0,
                    							"xtext.scn.cares.ubo.fr.Scn.ParameterInstanciation");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:1110:4: (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) ) )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==23) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalScn.g:1111:5: otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterInstanciation ) )
                    	    {
                    	    otherlv_3=(Token)match(input,23,FOLLOW_3); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getFunctionCallAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalScn.g:1115:5: ( (lv_parameters_4_0= ruleParameterInstanciation ) )
                    	    // InternalScn.g:1116:6: (lv_parameters_4_0= ruleParameterInstanciation )
                    	    {
                    	    // InternalScn.g:1116:6: (lv_parameters_4_0= ruleParameterInstanciation )
                    	    // InternalScn.g:1117:7: lv_parameters_4_0= ruleParameterInstanciation
                    	    {

                    	    							newCompositeNode(grammarAccess.getFunctionCallAccess().getParametersParameterInstanciationParserRuleCall_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_32);
                    	    lv_parameters_4_0=ruleParameterInstanciation();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFunctionCallRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_4_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.ParameterInstanciation");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getFunctionCallAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionCall"


    // $ANTLR start "entryRuleParameterInstanciation"
    // InternalScn.g:1144:1: entryRuleParameterInstanciation returns [EObject current=null] : iv_ruleParameterInstanciation= ruleParameterInstanciation EOF ;
    public final EObject entryRuleParameterInstanciation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterInstanciation = null;


        try {
            // InternalScn.g:1144:63: (iv_ruleParameterInstanciation= ruleParameterInstanciation EOF )
            // InternalScn.g:1145:2: iv_ruleParameterInstanciation= ruleParameterInstanciation EOF
            {
             newCompositeNode(grammarAccess.getParameterInstanciationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameterInstanciation=ruleParameterInstanciation();

            state._fsp--;

             current =iv_ruleParameterInstanciation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterInstanciation"


    // $ANTLR start "ruleParameterInstanciation"
    // InternalScn.g:1151:1: ruleParameterInstanciation returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '::' ( ( ruleEString ) ) otherlv_3= '=' ( (lv_value_4_0= ruleEString ) ) otherlv_5= ';' ) ;
    public final EObject ruleParameterInstanciation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_value_4_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1157:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '::' ( ( ruleEString ) ) otherlv_3= '=' ( (lv_value_4_0= ruleEString ) ) otherlv_5= ';' ) )
            // InternalScn.g:1158:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '::' ( ( ruleEString ) ) otherlv_3= '=' ( (lv_value_4_0= ruleEString ) ) otherlv_5= ';' )
            {
            // InternalScn.g:1158:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '::' ( ( ruleEString ) ) otherlv_3= '=' ( (lv_value_4_0= ruleEString ) ) otherlv_5= ';' )
            // InternalScn.g:1159:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= '::' ( ( ruleEString ) ) otherlv_3= '=' ( (lv_value_4_0= ruleEString ) ) otherlv_5= ';'
            {
            // InternalScn.g:1159:3: ( (lv_name_0_0= ruleEString ) )
            // InternalScn.g:1160:4: (lv_name_0_0= ruleEString )
            {
            // InternalScn.g:1160:4: (lv_name_0_0= ruleEString )
            // InternalScn.g:1161:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getParameterInstanciationAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_33);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParameterInstanciationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,33,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getParameterInstanciationAccess().getColonColonKeyword_1());
            		
            // InternalScn.g:1182:3: ( ( ruleEString ) )
            // InternalScn.g:1183:4: ( ruleEString )
            {
            // InternalScn.g:1183:4: ( ruleEString )
            // InternalScn.g:1184:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterInstanciationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getParameterInstanciationAccess().getParameterParameterDeclarationCrossReference_2_0());
            				
            pushFollow(FOLLOW_28);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,32,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getParameterInstanciationAccess().getEqualsSignKeyword_3());
            		
            // InternalScn.g:1202:3: ( (lv_value_4_0= ruleEString ) )
            // InternalScn.g:1203:4: (lv_value_4_0= ruleEString )
            {
            // InternalScn.g:1203:4: (lv_value_4_0= ruleEString )
            // InternalScn.g:1204:5: lv_value_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getParameterInstanciationAccess().getValueEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_8);
            lv_value_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParameterInstanciationRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_4_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getParameterInstanciationAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterInstanciation"


    // $ANTLR start "entryRuleScenario"
    // InternalScn.g:1229:1: entryRuleScenario returns [EObject current=null] : iv_ruleScenario= ruleScenario EOF ;
    public final EObject entryRuleScenario() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleScenario = null;


        try {
            // InternalScn.g:1229:49: (iv_ruleScenario= ruleScenario EOF )
            // InternalScn.g:1230:2: iv_ruleScenario= ruleScenario EOF
            {
             newCompositeNode(grammarAccess.getScenarioRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleScenario=ruleScenario();

            state._fsp--;

             current =iv_ruleScenario; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleScenario"


    // $ANTLR start "ruleScenario"
    // InternalScn.g:1236:1: ruleScenario returns [EObject current=null] : (otherlv_0= 'Scenario' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']' )? otherlv_5= 'begin' otherlv_6= '{' ( ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )* )? otherlv_11= '}' otherlv_12= 'events' otherlv_13= '{' ( ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )* )? otherlv_16= '}' otherlv_17= 'end' otherlv_18= '{' ( ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )* )? otherlv_23= '}' otherlv_24= 'logs' otherlv_25= '{' ( ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )* )? otherlv_28= '}' ) ;
    public final EObject ruleScenario() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        Token otherlv_28=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_number_3_0 = null;

        EObject lv_begin_7_0 = null;

        EObject lv_begin_9_0 = null;

        EObject lv_events_14_0 = null;

        EObject lv_events_15_0 = null;

        EObject lv_end_19_0 = null;

        EObject lv_end_21_0 = null;

        EObject lv_logs_26_0 = null;

        EObject lv_logs_27_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1242:2: ( (otherlv_0= 'Scenario' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']' )? otherlv_5= 'begin' otherlv_6= '{' ( ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )* )? otherlv_11= '}' otherlv_12= 'events' otherlv_13= '{' ( ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )* )? otherlv_16= '}' otherlv_17= 'end' otherlv_18= '{' ( ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )* )? otherlv_23= '}' otherlv_24= 'logs' otherlv_25= '{' ( ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )* )? otherlv_28= '}' ) )
            // InternalScn.g:1243:2: (otherlv_0= 'Scenario' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']' )? otherlv_5= 'begin' otherlv_6= '{' ( ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )* )? otherlv_11= '}' otherlv_12= 'events' otherlv_13= '{' ( ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )* )? otherlv_16= '}' otherlv_17= 'end' otherlv_18= '{' ( ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )* )? otherlv_23= '}' otherlv_24= 'logs' otherlv_25= '{' ( ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )* )? otherlv_28= '}' )
            {
            // InternalScn.g:1243:2: (otherlv_0= 'Scenario' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']' )? otherlv_5= 'begin' otherlv_6= '{' ( ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )* )? otherlv_11= '}' otherlv_12= 'events' otherlv_13= '{' ( ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )* )? otherlv_16= '}' otherlv_17= 'end' otherlv_18= '{' ( ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )* )? otherlv_23= '}' otherlv_24= 'logs' otherlv_25= '{' ( ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )* )? otherlv_28= '}' )
            // InternalScn.g:1244:3: otherlv_0= 'Scenario' ( (lv_name_1_0= ruleEString ) ) (otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']' )? otherlv_5= 'begin' otherlv_6= '{' ( ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )* )? otherlv_11= '}' otherlv_12= 'events' otherlv_13= '{' ( ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )* )? otherlv_16= '}' otherlv_17= 'end' otherlv_18= '{' ( ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )* )? otherlv_23= '}' otherlv_24= 'logs' otherlv_25= '{' ( ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )* )? otherlv_28= '}'
            {
            otherlv_0=(Token)match(input,34,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getScenarioAccess().getScenarioKeyword_0());
            		
            // InternalScn.g:1248:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:1249:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:1249:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:1250:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getScenarioAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_34);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getScenarioRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalScn.g:1267:3: (otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==22) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalScn.g:1268:4: otherlv_2= '[' ( (lv_number_3_0= ruleEInt ) ) otherlv_4= ']'
                    {
                    otherlv_2=(Token)match(input,22,FOLLOW_19); 

                    				newLeafNode(otherlv_2, grammarAccess.getScenarioAccess().getLeftSquareBracketKeyword_2_0());
                    			
                    // InternalScn.g:1272:4: ( (lv_number_3_0= ruleEInt ) )
                    // InternalScn.g:1273:5: (lv_number_3_0= ruleEInt )
                    {
                    // InternalScn.g:1273:5: (lv_number_3_0= ruleEInt )
                    // InternalScn.g:1274:6: lv_number_3_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getScenarioAccess().getNumberEIntParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_number_3_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    						}
                    						set(
                    							current,
                    							"number",
                    							lv_number_3_0,
                    							"xtext.scn.cares.ubo.fr.Scn.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_4=(Token)match(input,24,FOLLOW_10); 

                    				newLeafNode(otherlv_4, grammarAccess.getScenarioAccess().getRightSquareBracketKeyword_2_2());
                    			

                    }
                    break;

            }

            otherlv_5=(Token)match(input,16,FOLLOW_11); 

            			newLeafNode(otherlv_5, grammarAccess.getScenarioAccess().getBeginKeyword_3());
            		
            otherlv_6=(Token)match(input,17,FOLLOW_12); 

            			newLeafNode(otherlv_6, grammarAccess.getScenarioAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalScn.g:1304:3: ( ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )* )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0>=RULE_STRING && LA13_0<=RULE_ID)||LA13_0==29) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalScn.g:1305:4: ( (lv_begin_7_0= ruleAction ) ) otherlv_8= ';' ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )*
                    {
                    // InternalScn.g:1305:4: ( (lv_begin_7_0= ruleAction ) )
                    // InternalScn.g:1306:5: (lv_begin_7_0= ruleAction )
                    {
                    // InternalScn.g:1306:5: (lv_begin_7_0= ruleAction )
                    // InternalScn.g:1307:6: lv_begin_7_0= ruleAction
                    {

                    						newCompositeNode(grammarAccess.getScenarioAccess().getBeginActionParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_begin_7_0=ruleAction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    						}
                    						add(
                    							current,
                    							"begin",
                    							lv_begin_7_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Action");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_8=(Token)match(input,15,FOLLOW_12); 

                    				newLeafNode(otherlv_8, grammarAccess.getScenarioAccess().getSemicolonKeyword_5_1());
                    			
                    // InternalScn.g:1328:4: ( ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';' )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( ((LA12_0>=RULE_STRING && LA12_0<=RULE_ID)||LA12_0==29) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // InternalScn.g:1329:5: ( (lv_begin_9_0= ruleAction ) ) otherlv_10= ';'
                    	    {
                    	    // InternalScn.g:1329:5: ( (lv_begin_9_0= ruleAction ) )
                    	    // InternalScn.g:1330:6: (lv_begin_9_0= ruleAction )
                    	    {
                    	    // InternalScn.g:1330:6: (lv_begin_9_0= ruleAction )
                    	    // InternalScn.g:1331:7: lv_begin_9_0= ruleAction
                    	    {

                    	    							newCompositeNode(grammarAccess.getScenarioAccess().getBeginActionParserRuleCall_5_2_0_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_begin_9_0=ruleAction();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"begin",
                    	    								lv_begin_9_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.Action");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    otherlv_10=(Token)match(input,15,FOLLOW_12); 

                    	    					newLeafNode(otherlv_10, grammarAccess.getScenarioAccess().getSemicolonKeyword_5_2_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_11=(Token)match(input,18,FOLLOW_35); 

            			newLeafNode(otherlv_11, grammarAccess.getScenarioAccess().getRightCurlyBracketKeyword_6());
            		
            otherlv_12=(Token)match(input,35,FOLLOW_11); 

            			newLeafNode(otherlv_12, grammarAccess.getScenarioAccess().getEventsKeyword_7());
            		
            otherlv_13=(Token)match(input,17,FOLLOW_36); 

            			newLeafNode(otherlv_13, grammarAccess.getScenarioAccess().getLeftCurlyBracketKeyword_8());
            		
            // InternalScn.g:1366:3: ( ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )* )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==37) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalScn.g:1367:4: ( (lv_events_14_0= ruleEvent ) ) ( (lv_events_15_0= ruleEvent ) )*
                    {
                    // InternalScn.g:1367:4: ( (lv_events_14_0= ruleEvent ) )
                    // InternalScn.g:1368:5: (lv_events_14_0= ruleEvent )
                    {
                    // InternalScn.g:1368:5: (lv_events_14_0= ruleEvent )
                    // InternalScn.g:1369:6: lv_events_14_0= ruleEvent
                    {

                    						newCompositeNode(grammarAccess.getScenarioAccess().getEventsEventParserRuleCall_9_0_0());
                    					
                    pushFollow(FOLLOW_36);
                    lv_events_14_0=ruleEvent();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    						}
                    						add(
                    							current,
                    							"events",
                    							lv_events_14_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Event");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:1386:4: ( (lv_events_15_0= ruleEvent ) )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==37) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalScn.g:1387:5: (lv_events_15_0= ruleEvent )
                    	    {
                    	    // InternalScn.g:1387:5: (lv_events_15_0= ruleEvent )
                    	    // InternalScn.g:1388:6: lv_events_15_0= ruleEvent
                    	    {

                    	    						newCompositeNode(grammarAccess.getScenarioAccess().getEventsEventParserRuleCall_9_1_0());
                    	    					
                    	    pushFollow(FOLLOW_36);
                    	    lv_events_15_0=ruleEvent();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"events",
                    	    							lv_events_15_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Event");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_16=(Token)match(input,18,FOLLOW_16); 

            			newLeafNode(otherlv_16, grammarAccess.getScenarioAccess().getRightCurlyBracketKeyword_10());
            		
            otherlv_17=(Token)match(input,20,FOLLOW_11); 

            			newLeafNode(otherlv_17, grammarAccess.getScenarioAccess().getEndKeyword_11());
            		
            otherlv_18=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_18, grammarAccess.getScenarioAccess().getLeftCurlyBracketKeyword_12());
            		
            // InternalScn.g:1418:3: ( ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )* )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( ((LA17_0>=RULE_STRING && LA17_0<=RULE_ID)) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalScn.g:1419:4: ( (lv_end_19_0= ruleServiceCall ) ) otherlv_20= ';' ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )*
                    {
                    // InternalScn.g:1419:4: ( (lv_end_19_0= ruleServiceCall ) )
                    // InternalScn.g:1420:5: (lv_end_19_0= ruleServiceCall )
                    {
                    // InternalScn.g:1420:5: (lv_end_19_0= ruleServiceCall )
                    // InternalScn.g:1421:6: lv_end_19_0= ruleServiceCall
                    {

                    						newCompositeNode(grammarAccess.getScenarioAccess().getEndServiceCallParserRuleCall_13_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_end_19_0=ruleServiceCall();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    						}
                    						add(
                    							current,
                    							"end",
                    							lv_end_19_0,
                    							"xtext.scn.cares.ubo.fr.Scn.ServiceCall");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_20=(Token)match(input,15,FOLLOW_17); 

                    				newLeafNode(otherlv_20, grammarAccess.getScenarioAccess().getSemicolonKeyword_13_1());
                    			
                    // InternalScn.g:1442:4: ( ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';' )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( ((LA16_0>=RULE_STRING && LA16_0<=RULE_ID)) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalScn.g:1443:5: ( (lv_end_21_0= ruleServiceCall ) ) otherlv_22= ';'
                    	    {
                    	    // InternalScn.g:1443:5: ( (lv_end_21_0= ruleServiceCall ) )
                    	    // InternalScn.g:1444:6: (lv_end_21_0= ruleServiceCall )
                    	    {
                    	    // InternalScn.g:1444:6: (lv_end_21_0= ruleServiceCall )
                    	    // InternalScn.g:1445:7: lv_end_21_0= ruleServiceCall
                    	    {

                    	    							newCompositeNode(grammarAccess.getScenarioAccess().getEndServiceCallParserRuleCall_13_2_0_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_end_21_0=ruleServiceCall();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"end",
                    	    								lv_end_21_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.ServiceCall");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    otherlv_22=(Token)match(input,15,FOLLOW_17); 

                    	    					newLeafNode(otherlv_22, grammarAccess.getScenarioAccess().getSemicolonKeyword_13_2_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_23=(Token)match(input,18,FOLLOW_37); 

            			newLeafNode(otherlv_23, grammarAccess.getScenarioAccess().getRightCurlyBracketKeyword_14());
            		
            otherlv_24=(Token)match(input,36,FOLLOW_11); 

            			newLeafNode(otherlv_24, grammarAccess.getScenarioAccess().getLogsKeyword_15());
            		
            otherlv_25=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_25, grammarAccess.getScenarioAccess().getLeftCurlyBracketKeyword_16());
            		
            // InternalScn.g:1480:3: ( ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )* )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( ((LA19_0>=RULE_STRING && LA19_0<=RULE_ID)) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalScn.g:1481:4: ( (lv_logs_26_0= ruleLogObservation ) ) ( (lv_logs_27_0= ruleLogObservation ) )*
                    {
                    // InternalScn.g:1481:4: ( (lv_logs_26_0= ruleLogObservation ) )
                    // InternalScn.g:1482:5: (lv_logs_26_0= ruleLogObservation )
                    {
                    // InternalScn.g:1482:5: (lv_logs_26_0= ruleLogObservation )
                    // InternalScn.g:1483:6: lv_logs_26_0= ruleLogObservation
                    {

                    						newCompositeNode(grammarAccess.getScenarioAccess().getLogsLogObservationParserRuleCall_17_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_logs_26_0=ruleLogObservation();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    						}
                    						add(
                    							current,
                    							"logs",
                    							lv_logs_26_0,
                    							"xtext.scn.cares.ubo.fr.Scn.LogObservation");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:1500:4: ( (lv_logs_27_0= ruleLogObservation ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( ((LA18_0>=RULE_STRING && LA18_0<=RULE_ID)) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalScn.g:1501:5: (lv_logs_27_0= ruleLogObservation )
                    	    {
                    	    // InternalScn.g:1501:5: (lv_logs_27_0= ruleLogObservation )
                    	    // InternalScn.g:1502:6: lv_logs_27_0= ruleLogObservation
                    	    {

                    	    						newCompositeNode(grammarAccess.getScenarioAccess().getLogsLogObservationParserRuleCall_17_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_logs_27_0=ruleLogObservation();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getScenarioRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"logs",
                    	    							lv_logs_27_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.LogObservation");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_28=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_28, grammarAccess.getScenarioAccess().getRightCurlyBracketKeyword_18());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleScenario"


    // $ANTLR start "entryRuleLogObservation"
    // InternalScn.g:1528:1: entryRuleLogObservation returns [EObject current=null] : iv_ruleLogObservation= ruleLogObservation EOF ;
    public final EObject entryRuleLogObservation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogObservation = null;


        try {
            // InternalScn.g:1528:55: (iv_ruleLogObservation= ruleLogObservation EOF )
            // InternalScn.g:1529:2: iv_ruleLogObservation= ruleLogObservation EOF
            {
             newCompositeNode(grammarAccess.getLogObservationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogObservation=ruleLogObservation();

            state._fsp--;

             current =iv_ruleLogObservation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogObservation"


    // $ANTLR start "ruleLogObservation"
    // InternalScn.g:1535:1: ruleLogObservation returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '.' ( (lv_type_2_0= ruleTypeFile ) ) ( (lv_timeStamp_3_0= ruleTimeStamp ) )? (otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')' )? otherlv_7= '{' ( ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )* )? otherlv_12= '}' ) ;
    public final EObject ruleLogObservation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        Enumerator lv_type_2_0 = null;

        Enumerator lv_timeStamp_3_0 = null;

        AntlrDatatypeRuleToken lv_freq_5_0 = null;

        EObject lv_columns_8_0 = null;

        EObject lv_columns_10_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1541:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '.' ( (lv_type_2_0= ruleTypeFile ) ) ( (lv_timeStamp_3_0= ruleTimeStamp ) )? (otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')' )? otherlv_7= '{' ( ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )* )? otherlv_12= '}' ) )
            // InternalScn.g:1542:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '.' ( (lv_type_2_0= ruleTypeFile ) ) ( (lv_timeStamp_3_0= ruleTimeStamp ) )? (otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')' )? otherlv_7= '{' ( ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )* )? otherlv_12= '}' )
            {
            // InternalScn.g:1542:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '.' ( (lv_type_2_0= ruleTypeFile ) ) ( (lv_timeStamp_3_0= ruleTimeStamp ) )? (otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')' )? otherlv_7= '{' ( ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )* )? otherlv_12= '}' )
            // InternalScn.g:1543:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= '.' ( (lv_type_2_0= ruleTypeFile ) ) ( (lv_timeStamp_3_0= ruleTimeStamp ) )? (otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')' )? otherlv_7= '{' ( ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )* )? otherlv_12= '}'
            {
            // InternalScn.g:1543:3: ( (lv_name_0_0= ruleEString ) )
            // InternalScn.g:1544:4: (lv_name_0_0= ruleEString )
            {
            // InternalScn.g:1544:4: (lv_name_0_0= ruleEString )
            // InternalScn.g:1545:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getLogObservationAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_27);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLogObservationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,31,FOLLOW_38); 

            			newLeafNode(otherlv_1, grammarAccess.getLogObservationAccess().getFullStopKeyword_1());
            		
            // InternalScn.g:1566:3: ( (lv_type_2_0= ruleTypeFile ) )
            // InternalScn.g:1567:4: (lv_type_2_0= ruleTypeFile )
            {
            // InternalScn.g:1567:4: (lv_type_2_0= ruleTypeFile )
            // InternalScn.g:1568:5: lv_type_2_0= ruleTypeFile
            {

            					newCompositeNode(grammarAccess.getLogObservationAccess().getTypeTypeFileEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_39);
            lv_type_2_0=ruleTypeFile();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLogObservationRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_2_0,
            						"xtext.scn.cares.ubo.fr.Scn.TypeFile");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalScn.g:1585:3: ( (lv_timeStamp_3_0= ruleTimeStamp ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ((LA20_0>=62 && LA20_0<=63)) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalScn.g:1586:4: (lv_timeStamp_3_0= ruleTimeStamp )
                    {
                    // InternalScn.g:1586:4: (lv_timeStamp_3_0= ruleTimeStamp )
                    // InternalScn.g:1587:5: lv_timeStamp_3_0= ruleTimeStamp
                    {

                    					newCompositeNode(grammarAccess.getLogObservationAccess().getTimeStampTimeStampEnumRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_40);
                    lv_timeStamp_3_0=ruleTimeStamp();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getLogObservationRule());
                    					}
                    					set(
                    						current,
                    						"timeStamp",
                    						lv_timeStamp_3_0,
                    						"xtext.scn.cares.ubo.fr.Scn.TimeStamp");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalScn.g:1604:3: (otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==12) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalScn.g:1605:4: otherlv_4= '(' ( (lv_freq_5_0= ruleEDouble ) ) otherlv_6= ')'
                    {
                    otherlv_4=(Token)match(input,12,FOLLOW_29); 

                    				newLeafNode(otherlv_4, grammarAccess.getLogObservationAccess().getLeftParenthesisKeyword_4_0());
                    			
                    // InternalScn.g:1609:4: ( (lv_freq_5_0= ruleEDouble ) )
                    // InternalScn.g:1610:5: (lv_freq_5_0= ruleEDouble )
                    {
                    // InternalScn.g:1610:5: (lv_freq_5_0= ruleEDouble )
                    // InternalScn.g:1611:6: lv_freq_5_0= ruleEDouble
                    {

                    						newCompositeNode(grammarAccess.getLogObservationAccess().getFreqEDoubleParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_freq_5_0=ruleEDouble();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogObservationRule());
                    						}
                    						set(
                    							current,
                    							"freq",
                    							lv_freq_5_0,
                    							"xtext.scn.cares.ubo.fr.Scn.EDouble");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_6=(Token)match(input,13,FOLLOW_11); 

                    				newLeafNode(otherlv_6, grammarAccess.getLogObservationAccess().getRightParenthesisKeyword_4_2());
                    			

                    }
                    break;

            }

            otherlv_7=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_7, grammarAccess.getLogObservationAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalScn.g:1637:3: ( ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )* )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=RULE_STRING && LA23_0<=RULE_ID)) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalScn.g:1638:4: ( (lv_columns_8_0= ruleLogColumn ) ) otherlv_9= ';' ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )*
                    {
                    // InternalScn.g:1638:4: ( (lv_columns_8_0= ruleLogColumn ) )
                    // InternalScn.g:1639:5: (lv_columns_8_0= ruleLogColumn )
                    {
                    // InternalScn.g:1639:5: (lv_columns_8_0= ruleLogColumn )
                    // InternalScn.g:1640:6: lv_columns_8_0= ruleLogColumn
                    {

                    						newCompositeNode(grammarAccess.getLogObservationAccess().getColumnsLogColumnParserRuleCall_6_0_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_columns_8_0=ruleLogColumn();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getLogObservationRule());
                    						}
                    						add(
                    							current,
                    							"columns",
                    							lv_columns_8_0,
                    							"xtext.scn.cares.ubo.fr.Scn.LogColumn");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_9=(Token)match(input,15,FOLLOW_17); 

                    				newLeafNode(otherlv_9, grammarAccess.getLogObservationAccess().getSemicolonKeyword_6_1());
                    			
                    // InternalScn.g:1661:4: ( ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';' )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( ((LA22_0>=RULE_STRING && LA22_0<=RULE_ID)) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalScn.g:1662:5: ( (lv_columns_10_0= ruleLogColumn ) ) otherlv_11= ';'
                    	    {
                    	    // InternalScn.g:1662:5: ( (lv_columns_10_0= ruleLogColumn ) )
                    	    // InternalScn.g:1663:6: (lv_columns_10_0= ruleLogColumn )
                    	    {
                    	    // InternalScn.g:1663:6: (lv_columns_10_0= ruleLogColumn )
                    	    // InternalScn.g:1664:7: lv_columns_10_0= ruleLogColumn
                    	    {

                    	    							newCompositeNode(grammarAccess.getLogObservationAccess().getColumnsLogColumnParserRuleCall_6_2_0_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_columns_10_0=ruleLogColumn();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getLogObservationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"columns",
                    	    								lv_columns_10_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.LogColumn");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    otherlv_11=(Token)match(input,15,FOLLOW_17); 

                    	    					newLeafNode(otherlv_11, grammarAccess.getLogObservationAccess().getSemicolonKeyword_6_2_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_12=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getLogObservationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogObservation"


    // $ANTLR start "entryRuleLogColumn"
    // InternalScn.g:1695:1: entryRuleLogColumn returns [EObject current=null] : iv_ruleLogColumn= ruleLogColumn EOF ;
    public final EObject entryRuleLogColumn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogColumn = null;


        try {
            // InternalScn.g:1695:50: (iv_ruleLogColumn= ruleLogColumn EOF )
            // InternalScn.g:1696:2: iv_ruleLogColumn= ruleLogColumn EOF
            {
             newCompositeNode(grammarAccess.getLogColumnRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogColumn=ruleLogColumn();

            state._fsp--;

             current =iv_ruleLogColumn; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogColumn"


    // $ANTLR start "ruleLogColumn"
    // InternalScn.g:1702:1: ruleLogColumn returns [EObject current=null] : ( ( ruleEString ) ) ;
    public final EObject ruleLogColumn() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalScn.g:1708:2: ( ( ( ruleEString ) ) )
            // InternalScn.g:1709:2: ( ( ruleEString ) )
            {
            // InternalScn.g:1709:2: ( ( ruleEString ) )
            // InternalScn.g:1710:3: ( ruleEString )
            {
            // InternalScn.g:1710:3: ( ruleEString )
            // InternalScn.g:1711:4: ruleEString
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getLogColumnRule());
            				}
            			

            				newCompositeNode(grammarAccess.getLogColumnAccess().getDataLeafOutputPortCrossReference_0());
            			
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogColumn"


    // $ANTLR start "entryRuleEvent"
    // InternalScn.g:1728:1: entryRuleEvent returns [EObject current=null] : iv_ruleEvent= ruleEvent EOF ;
    public final EObject entryRuleEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEvent = null;


        try {
            // InternalScn.g:1728:46: (iv_ruleEvent= ruleEvent EOF )
            // InternalScn.g:1729:2: iv_ruleEvent= ruleEvent EOF
            {
             newCompositeNode(grammarAccess.getEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEvent=ruleEvent();

            state._fsp--;

             current =iv_ruleEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEvent"


    // $ANTLR start "ruleEvent"
    // InternalScn.g:1735:1: ruleEvent returns [EObject current=null] : this_BasicEvent_0= ruleBasicEvent ;
    public final EObject ruleEvent() throws RecognitionException {
        EObject current = null;

        EObject this_BasicEvent_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1741:2: (this_BasicEvent_0= ruleBasicEvent )
            // InternalScn.g:1742:2: this_BasicEvent_0= ruleBasicEvent
            {

            		newCompositeNode(grammarAccess.getEventAccess().getBasicEventParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_BasicEvent_0=ruleBasicEvent();

            state._fsp--;


            		current = this_BasicEvent_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEvent"


    // $ANTLR start "entryRuleBasicEvent"
    // InternalScn.g:1753:1: entryRuleBasicEvent returns [EObject current=null] : iv_ruleBasicEvent= ruleBasicEvent EOF ;
    public final EObject entryRuleBasicEvent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBasicEvent = null;


        try {
            // InternalScn.g:1753:51: (iv_ruleBasicEvent= ruleBasicEvent EOF )
            // InternalScn.g:1754:2: iv_ruleBasicEvent= ruleBasicEvent EOF
            {
             newCompositeNode(grammarAccess.getBasicEventRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBasicEvent=ruleBasicEvent();

            state._fsp--;

             current =iv_ruleBasicEvent; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBasicEvent"


    // $ANTLR start "ruleBasicEvent"
    // InternalScn.g:1760:1: ruleBasicEvent returns [EObject current=null] : (otherlv_0= 'instant' ( (lv_time_1_0= ruleEInt ) ) (otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}' )? ) ;
    public final EObject ruleBasicEvent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_time_1_0 = null;

        EObject lv_actions_3_0 = null;

        EObject lv_actions_5_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1766:2: ( (otherlv_0= 'instant' ( (lv_time_1_0= ruleEInt ) ) (otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}' )? ) )
            // InternalScn.g:1767:2: (otherlv_0= 'instant' ( (lv_time_1_0= ruleEInt ) ) (otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}' )? )
            {
            // InternalScn.g:1767:2: (otherlv_0= 'instant' ( (lv_time_1_0= ruleEInt ) ) (otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}' )? )
            // InternalScn.g:1768:3: otherlv_0= 'instant' ( (lv_time_1_0= ruleEInt ) ) (otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}' )?
            {
            otherlv_0=(Token)match(input,37,FOLLOW_19); 

            			newLeafNode(otherlv_0, grammarAccess.getBasicEventAccess().getInstantKeyword_0());
            		
            // InternalScn.g:1772:3: ( (lv_time_1_0= ruleEInt ) )
            // InternalScn.g:1773:4: (lv_time_1_0= ruleEInt )
            {
            // InternalScn.g:1773:4: (lv_time_1_0= ruleEInt )
            // InternalScn.g:1774:5: lv_time_1_0= ruleEInt
            {

            					newCompositeNode(grammarAccess.getBasicEventAccess().getTimeEIntParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_41);
            lv_time_1_0=ruleEInt();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBasicEventRule());
            					}
            					set(
            						current,
            						"time",
            						lv_time_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EInt");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalScn.g:1791:3: (otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==17) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalScn.g:1792:4: otherlv_2= '{' ( (lv_actions_3_0= ruleAction ) ) otherlv_4= ';' ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )* otherlv_7= '}'
                    {
                    otherlv_2=(Token)match(input,17,FOLLOW_42); 

                    				newLeafNode(otherlv_2, grammarAccess.getBasicEventAccess().getLeftCurlyBracketKeyword_2_0());
                    			
                    // InternalScn.g:1796:4: ( (lv_actions_3_0= ruleAction ) )
                    // InternalScn.g:1797:5: (lv_actions_3_0= ruleAction )
                    {
                    // InternalScn.g:1797:5: (lv_actions_3_0= ruleAction )
                    // InternalScn.g:1798:6: lv_actions_3_0= ruleAction
                    {

                    						newCompositeNode(grammarAccess.getBasicEventAccess().getActionsActionParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_actions_3_0=ruleAction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getBasicEventRule());
                    						}
                    						add(
                    							current,
                    							"actions",
                    							lv_actions_3_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Action");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_4=(Token)match(input,15,FOLLOW_12); 

                    				newLeafNode(otherlv_4, grammarAccess.getBasicEventAccess().getSemicolonKeyword_2_2());
                    			
                    // InternalScn.g:1819:4: ( ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';' )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( ((LA24_0>=RULE_STRING && LA24_0<=RULE_ID)||LA24_0==29) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalScn.g:1820:5: ( (lv_actions_5_0= ruleAction ) ) otherlv_6= ';'
                    	    {
                    	    // InternalScn.g:1820:5: ( (lv_actions_5_0= ruleAction ) )
                    	    // InternalScn.g:1821:6: (lv_actions_5_0= ruleAction )
                    	    {
                    	    // InternalScn.g:1821:6: (lv_actions_5_0= ruleAction )
                    	    // InternalScn.g:1822:7: lv_actions_5_0= ruleAction
                    	    {

                    	    							newCompositeNode(grammarAccess.getBasicEventAccess().getActionsActionParserRuleCall_2_3_0_0());
                    	    						
                    	    pushFollow(FOLLOW_8);
                    	    lv_actions_5_0=ruleAction();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getBasicEventRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"actions",
                    	    								lv_actions_5_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.Action");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }

                    	    otherlv_6=(Token)match(input,15,FOLLOW_12); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getBasicEventAccess().getSemicolonKeyword_2_3_1());
                    	    				

                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    otherlv_7=(Token)match(input,18,FOLLOW_2); 

                    				newLeafNode(otherlv_7, grammarAccess.getBasicEventAccess().getRightCurlyBracketKeyword_2_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBasicEvent"


    // $ANTLR start "entryRuleCInt"
    // InternalScn.g:1853:1: entryRuleCInt returns [EObject current=null] : iv_ruleCInt= ruleCInt EOF ;
    public final EObject entryRuleCInt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCInt = null;


        try {
            // InternalScn.g:1853:45: (iv_ruleCInt= ruleCInt EOF )
            // InternalScn.g:1854:2: iv_ruleCInt= ruleCInt EOF
            {
             newCompositeNode(grammarAccess.getCIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCInt=ruleCInt();

            state._fsp--;

             current =iv_ruleCInt; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCInt"


    // $ANTLR start "ruleCInt"
    // InternalScn.g:1860:1: ruleCInt returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCInt() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1866:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalScn.g:1867:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalScn.g:1867:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';' )
            // InternalScn.g:1868:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CInt' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalScn.g:1868:3: ()
            // InternalScn.g:1869:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCIntAccess().getCIntAction_0(),
            					current);
            			

            }

            // InternalScn.g:1875:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:1876:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:1876:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:1877:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCIntAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCIntRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_43); 

            			newLeafNode(otherlv_2, grammarAccess.getCIntAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,38,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getCIntAccess().getCIntKeyword_3());
            		
            otherlv_4=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_4, grammarAccess.getCIntAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCIntAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCInt"


    // $ANTLR start "entryRuleCDouble"
    // InternalScn.g:1914:1: entryRuleCDouble returns [EObject current=null] : iv_ruleCDouble= ruleCDouble EOF ;
    public final EObject entryRuleCDouble() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCDouble = null;


        try {
            // InternalScn.g:1914:48: (iv_ruleCDouble= ruleCDouble EOF )
            // InternalScn.g:1915:2: iv_ruleCDouble= ruleCDouble EOF
            {
             newCompositeNode(grammarAccess.getCDoubleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCDouble=ruleCDouble();

            state._fsp--;

             current =iv_ruleCDouble; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCDouble"


    // $ANTLR start "ruleCDouble"
    // InternalScn.g:1921:1: ruleCDouble returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCDouble() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1927:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalScn.g:1928:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalScn.g:1928:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';' )
            // InternalScn.g:1929:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CDouble' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalScn.g:1929:3: ()
            // InternalScn.g:1930:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCDoubleAccess().getCDoubleAction_0(),
            					current);
            			

            }

            // InternalScn.g:1936:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:1937:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:1937:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:1938:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCDoubleAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCDoubleRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_44); 

            			newLeafNode(otherlv_2, grammarAccess.getCDoubleAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,39,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getCDoubleAccess().getCDoubleKeyword_3());
            		
            otherlv_4=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_4, grammarAccess.getCDoubleAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCDoubleAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCDouble"


    // $ANTLR start "entryRuleCBoolean"
    // InternalScn.g:1975:1: entryRuleCBoolean returns [EObject current=null] : iv_ruleCBoolean= ruleCBoolean EOF ;
    public final EObject entryRuleCBoolean() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCBoolean = null;


        try {
            // InternalScn.g:1975:49: (iv_ruleCBoolean= ruleCBoolean EOF )
            // InternalScn.g:1976:2: iv_ruleCBoolean= ruleCBoolean EOF
            {
             newCompositeNode(grammarAccess.getCBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCBoolean=ruleCBoolean();

            state._fsp--;

             current =iv_ruleCBoolean; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCBoolean"


    // $ANTLR start "ruleCBoolean"
    // InternalScn.g:1982:1: ruleCBoolean returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCBoolean() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalScn.g:1988:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalScn.g:1989:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalScn.g:1989:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';' )
            // InternalScn.g:1990:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CBoolean' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalScn.g:1990:3: ()
            // InternalScn.g:1991:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCBooleanAccess().getCBooleanAction_0(),
            					current);
            			

            }

            // InternalScn.g:1997:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:1998:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:1998:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:1999:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCBooleanAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCBooleanRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_45); 

            			newLeafNode(otherlv_2, grammarAccess.getCBooleanAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,40,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getCBooleanAccess().getCBooleanKeyword_3());
            		
            otherlv_4=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_4, grammarAccess.getCBooleanAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCBooleanAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCBoolean"


    // $ANTLR start "entryRuleCString"
    // InternalScn.g:2036:1: entryRuleCString returns [EObject current=null] : iv_ruleCString= ruleCString EOF ;
    public final EObject entryRuleCString() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCString = null;


        try {
            // InternalScn.g:2036:48: (iv_ruleCString= ruleCString EOF )
            // InternalScn.g:2037:2: iv_ruleCString= ruleCString EOF
            {
             newCompositeNode(grammarAccess.getCStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCString=ruleCString();

            state._fsp--;

             current =iv_ruleCString; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCString"


    // $ANTLR start "ruleCString"
    // InternalScn.g:2043:1: ruleCString returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' ) ;
    public final EObject ruleCString() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2049:2: ( ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' ) )
            // InternalScn.g:2050:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' )
            {
            // InternalScn.g:2050:2: ( () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';' )
            // InternalScn.g:2051:3: () ( (lv_name_1_0= ruleEString ) ) otherlv_2= '(' otherlv_3= 'CString' otherlv_4= ')' otherlv_5= ';'
            {
            // InternalScn.g:2051:3: ()
            // InternalScn.g:2052:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCStringAccess().getCStringAction_0(),
            					current);
            			

            }

            // InternalScn.g:2058:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:2059:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:2059:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:2060:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCStringAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCStringRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_46); 

            			newLeafNode(otherlv_2, grammarAccess.getCStringAccess().getLeftParenthesisKeyword_2());
            		
            otherlv_3=(Token)match(input,41,FOLLOW_6); 

            			newLeafNode(otherlv_3, grammarAccess.getCStringAccess().getCStringKeyword_3());
            		
            otherlv_4=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_4, grammarAccess.getCStringAccess().getRightParenthesisKeyword_4());
            		
            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCStringAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCString"


    // $ANTLR start "entryRuleStructType"
    // InternalScn.g:2097:1: entryRuleStructType returns [EObject current=null] : iv_ruleStructType= ruleStructType EOF ;
    public final EObject entryRuleStructType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStructType = null;


        try {
            // InternalScn.g:2097:51: (iv_ruleStructType= ruleStructType EOF )
            // InternalScn.g:2098:2: iv_ruleStructType= ruleStructType EOF
            {
             newCompositeNode(grammarAccess.getStructTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStructType=ruleStructType();

            state._fsp--;

             current =iv_ruleStructType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStructType"


    // $ANTLR start "ruleStructType"
    // InternalScn.g:2104:1: ruleStructType returns [EObject current=null] : (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) otherlv_4= ';' ( ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';' )* otherlv_7= ';' otherlv_8= '}' otherlv_9= ';' ) ;
    public final EObject ruleStructType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_fields_3_0 = null;

        EObject lv_fields_5_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2110:2: ( (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) otherlv_4= ';' ( ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';' )* otherlv_7= ';' otherlv_8= '}' otherlv_9= ';' ) )
            // InternalScn.g:2111:2: (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) otherlv_4= ';' ( ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';' )* otherlv_7= ';' otherlv_8= '}' otherlv_9= ';' )
            {
            // InternalScn.g:2111:2: (otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) otherlv_4= ';' ( ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';' )* otherlv_7= ';' otherlv_8= '}' otherlv_9= ';' )
            // InternalScn.g:2112:3: otherlv_0= 'Record' ( (lv_name_1_0= ruleEString ) ) otherlv_2= '{' ( (lv_fields_3_0= ruleParameterDeclaration ) ) otherlv_4= ';' ( ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';' )* otherlv_7= ';' otherlv_8= '}' otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,42,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getStructTypeAccess().getRecordKeyword_0());
            		
            // InternalScn.g:2116:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:2117:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:2117:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:2118:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStructTypeAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_11);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStructTypeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getStructTypeAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalScn.g:2139:3: ( (lv_fields_3_0= ruleParameterDeclaration ) )
            // InternalScn.g:2140:4: (lv_fields_3_0= ruleParameterDeclaration )
            {
            // InternalScn.g:2140:4: (lv_fields_3_0= ruleParameterDeclaration )
            // InternalScn.g:2141:5: lv_fields_3_0= ruleParameterDeclaration
            {

            					newCompositeNode(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_8);
            lv_fields_3_0=ruleParameterDeclaration();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStructTypeRule());
            					}
            					add(
            						current,
            						"fields",
            						lv_fields_3_0,
            						"xtext.scn.cares.ubo.fr.Scn.ParameterDeclaration");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_47); 

            			newLeafNode(otherlv_4, grammarAccess.getStructTypeAccess().getSemicolonKeyword_4());
            		
            // InternalScn.g:2162:3: ( ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';' )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( ((LA26_0>=RULE_STRING && LA26_0<=RULE_ID)) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalScn.g:2163:4: ( (lv_fields_5_0= ruleParameterDeclaration ) ) otherlv_6= ';'
            	    {
            	    // InternalScn.g:2163:4: ( (lv_fields_5_0= ruleParameterDeclaration ) )
            	    // InternalScn.g:2164:5: (lv_fields_5_0= ruleParameterDeclaration )
            	    {
            	    // InternalScn.g:2164:5: (lv_fields_5_0= ruleParameterDeclaration )
            	    // InternalScn.g:2165:6: lv_fields_5_0= ruleParameterDeclaration
            	    {

            	    						newCompositeNode(grammarAccess.getStructTypeAccess().getFieldsParameterDeclarationParserRuleCall_5_0_0());
            	    					
            	    pushFollow(FOLLOW_8);
            	    lv_fields_5_0=ruleParameterDeclaration();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getStructTypeRule());
            	    						}
            	    						add(
            	    							current,
            	    							"fields",
            	    							lv_fields_5_0,
            	    							"xtext.scn.cares.ubo.fr.Scn.ParameterDeclaration");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    otherlv_6=(Token)match(input,15,FOLLOW_47); 

            	    				newLeafNode(otherlv_6, grammarAccess.getStructTypeAccess().getSemicolonKeyword_5_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

            otherlv_7=(Token)match(input,15,FOLLOW_48); 

            			newLeafNode(otherlv_7, grammarAccess.getStructTypeAccess().getSemicolonKeyword_6());
            		
            otherlv_8=(Token)match(input,18,FOLLOW_8); 

            			newLeafNode(otherlv_8, grammarAccess.getStructTypeAccess().getRightCurlyBracketKeyword_7());
            		
            otherlv_9=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getStructTypeAccess().getSemicolonKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStructType"


    // $ANTLR start "entryRuleNUMBER"
    // InternalScn.g:2203:1: entryRuleNUMBER returns [String current=null] : iv_ruleNUMBER= ruleNUMBER EOF ;
    public final String entryRuleNUMBER() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNUMBER = null;


        try {
            // InternalScn.g:2203:46: (iv_ruleNUMBER= ruleNUMBER EOF )
            // InternalScn.g:2204:2: iv_ruleNUMBER= ruleNUMBER EOF
            {
             newCompositeNode(grammarAccess.getNUMBERRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNUMBER=ruleNUMBER();

            state._fsp--;

             current =iv_ruleNUMBER.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNUMBER"


    // $ANTLR start "ruleNUMBER"
    // InternalScn.g:2210:1: ruleNUMBER returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleNUMBER() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalScn.g:2216:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalScn.g:2217:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalScn.g:2217:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalScn.g:2218:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalScn.g:2218:3: (kw= '-' )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==43) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalScn.g:2219:4: kw= '-'
                    {
                    kw=(Token)match(input,43,FOLLOW_49); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getNUMBERAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getNUMBERAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNUMBER"


    // $ANTLR start "entryRuleParameterDeclaration"
    // InternalScn.g:2236:1: entryRuleParameterDeclaration returns [EObject current=null] : iv_ruleParameterDeclaration= ruleParameterDeclaration EOF ;
    public final EObject entryRuleParameterDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameterDeclaration = null;


        try {
            // InternalScn.g:2236:61: (iv_ruleParameterDeclaration= ruleParameterDeclaration EOF )
            // InternalScn.g:2237:2: iv_ruleParameterDeclaration= ruleParameterDeclaration EOF
            {
             newCompositeNode(grammarAccess.getParameterDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameterDeclaration=ruleParameterDeclaration();

            state._fsp--;

             current =iv_ruleParameterDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameterDeclaration"


    // $ANTLR start "ruleParameterDeclaration"
    // InternalScn.g:2243:1: ruleParameterDeclaration returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) (otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']' )? ) ;
    public final EObject ruleParameterDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token lv_lowerBound_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_upperBound_6_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2249:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) (otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']' )? ) )
            // InternalScn.g:2250:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) (otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']' )? )
            {
            // InternalScn.g:2250:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) (otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']' )? )
            // InternalScn.g:2251:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= ':' ( ( ruleEString ) ) (otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']' )?
            {
            // InternalScn.g:2251:3: ( (lv_name_0_0= ruleEString ) )
            // InternalScn.g:2252:4: (lv_name_0_0= ruleEString )
            {
            // InternalScn.g:2252:4: (lv_name_0_0= ruleEString )
            // InternalScn.g:2253:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getParameterDeclarationAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_22);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParameterDeclarationRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,25,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getParameterDeclarationAccess().getColonKeyword_1());
            		
            // InternalScn.g:2274:3: ( ( ruleEString ) )
            // InternalScn.g:2275:4: ( ruleEString )
            {
            // InternalScn.g:2275:4: ( ruleEString )
            // InternalScn.g:2276:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterDeclarationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getParameterDeclarationAccess().getTypeTypeCrossReference_2_0());
            				
            pushFollow(FOLLOW_50);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalScn.g:2290:3: (otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']' )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==22) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalScn.g:2291:4: otherlv_3= '[' ( (lv_lowerBound_4_0= RULE_INT ) ) otherlv_5= ',' ( (lv_upperBound_6_0= ruleNUMBER ) ) otherlv_7= ']'
                    {
                    otherlv_3=(Token)match(input,22,FOLLOW_49); 

                    				newLeafNode(otherlv_3, grammarAccess.getParameterDeclarationAccess().getLeftSquareBracketKeyword_3_0());
                    			
                    // InternalScn.g:2295:4: ( (lv_lowerBound_4_0= RULE_INT ) )
                    // InternalScn.g:2296:5: (lv_lowerBound_4_0= RULE_INT )
                    {
                    // InternalScn.g:2296:5: (lv_lowerBound_4_0= RULE_INT )
                    // InternalScn.g:2297:6: lv_lowerBound_4_0= RULE_INT
                    {
                    lv_lowerBound_4_0=(Token)match(input,RULE_INT,FOLLOW_20); 

                    						newLeafNode(lv_lowerBound_4_0, grammarAccess.getParameterDeclarationAccess().getLowerBoundINTTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getParameterDeclarationRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"lowerBound",
                    							lv_lowerBound_4_0,
                    							"org.eclipse.xtext.common.Terminals.INT");
                    					

                    }


                    }

                    otherlv_5=(Token)match(input,23,FOLLOW_19); 

                    				newLeafNode(otherlv_5, grammarAccess.getParameterDeclarationAccess().getCommaKeyword_3_2());
                    			
                    // InternalScn.g:2317:4: ( (lv_upperBound_6_0= ruleNUMBER ) )
                    // InternalScn.g:2318:5: (lv_upperBound_6_0= ruleNUMBER )
                    {
                    // InternalScn.g:2318:5: (lv_upperBound_6_0= ruleNUMBER )
                    // InternalScn.g:2319:6: lv_upperBound_6_0= ruleNUMBER
                    {

                    						newCompositeNode(grammarAccess.getParameterDeclarationAccess().getUpperBoundNUMBERParserRuleCall_3_3_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_upperBound_6_0=ruleNUMBER();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getParameterDeclarationRule());
                    						}
                    						set(
                    							current,
                    							"upperBound",
                    							lv_upperBound_6_0,
                    							"xtext.scn.cares.ubo.fr.Scn.NUMBER");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    otherlv_7=(Token)match(input,24,FOLLOW_2); 

                    				newLeafNode(otherlv_7, grammarAccess.getParameterDeclarationAccess().getRightSquareBracketKeyword_3_4());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameterDeclaration"


    // $ANTLR start "entryRuleFunction"
    // InternalScn.g:2345:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalScn.g:2345:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalScn.g:2346:2: iv_ruleFunction= ruleFunction EOF
            {
             newCompositeNode(grammarAccess.getFunctionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;

             current =iv_ruleFunction; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalScn.g:2352:1: ruleFunction returns [EObject current=null] : ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) ) )? otherlv_8= ';' ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_0_0 = null;

        EObject lv_parameters_2_0 = null;

        EObject lv_parameters_4_0 = null;

        EObject lv_returnType_7_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2358:2: ( ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) ) )? otherlv_8= ';' ) )
            // InternalScn.g:2359:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) ) )? otherlv_8= ';' )
            {
            // InternalScn.g:2359:2: ( ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) ) )? otherlv_8= ';' )
            // InternalScn.g:2360:3: ( (lv_name_0_0= ruleEString ) ) otherlv_1= '(' ( ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )* )? otherlv_5= ')' (otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) ) )? otherlv_8= ';'
            {
            // InternalScn.g:2360:3: ( (lv_name_0_0= ruleEString ) )
            // InternalScn.g:2361:4: (lv_name_0_0= ruleEString )
            {
            // InternalScn.g:2361:4: (lv_name_0_0= ruleEString )
            // InternalScn.g:2362:5: lv_name_0_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getFunctionAccess().getNameEStringParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_4);
            lv_name_0_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFunctionRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_0_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_31); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalScn.g:2383:3: ( ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )* )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( ((LA30_0>=RULE_STRING && LA30_0<=RULE_ID)) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalScn.g:2384:4: ( (lv_parameters_2_0= ruleParameterDeclaration ) ) (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )*
                    {
                    // InternalScn.g:2384:4: ( (lv_parameters_2_0= ruleParameterDeclaration ) )
                    // InternalScn.g:2385:5: (lv_parameters_2_0= ruleParameterDeclaration )
                    {
                    // InternalScn.g:2385:5: (lv_parameters_2_0= ruleParameterDeclaration )
                    // InternalScn.g:2386:6: lv_parameters_2_0= ruleParameterDeclaration
                    {

                    						newCompositeNode(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_32);
                    lv_parameters_2_0=ruleParameterDeclaration();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFunctionRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_2_0,
                    							"xtext.scn.cares.ubo.fr.Scn.ParameterDeclaration");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2403:4: (otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) ) )*
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0==23) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // InternalScn.g:2404:5: otherlv_3= ',' ( (lv_parameters_4_0= ruleParameterDeclaration ) )
                    	    {
                    	    otherlv_3=(Token)match(input,23,FOLLOW_3); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getFunctionAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalScn.g:2408:5: ( (lv_parameters_4_0= ruleParameterDeclaration ) )
                    	    // InternalScn.g:2409:6: (lv_parameters_4_0= ruleParameterDeclaration )
                    	    {
                    	    // InternalScn.g:2409:6: (lv_parameters_4_0= ruleParameterDeclaration )
                    	    // InternalScn.g:2410:7: lv_parameters_4_0= ruleParameterDeclaration
                    	    {

                    	    							newCompositeNode(grammarAccess.getFunctionAccess().getParametersParameterDeclarationParserRuleCall_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_32);
                    	    lv_parameters_4_0=ruleParameterDeclaration();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFunctionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_4_0,
                    	    								"xtext.scn.cares.ubo.fr.Scn.ParameterDeclaration");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop29;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,13,FOLLOW_51); 

            			newLeafNode(otherlv_5, grammarAccess.getFunctionAccess().getRightParenthesisKeyword_3());
            		
            // InternalScn.g:2433:3: (otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==25) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalScn.g:2434:4: otherlv_6= ':' ( (lv_returnType_7_0= ruleReturnType ) )
                    {
                    otherlv_6=(Token)match(input,25,FOLLOW_3); 

                    				newLeafNode(otherlv_6, grammarAccess.getFunctionAccess().getColonKeyword_4_0());
                    			
                    // InternalScn.g:2438:4: ( (lv_returnType_7_0= ruleReturnType ) )
                    // InternalScn.g:2439:5: (lv_returnType_7_0= ruleReturnType )
                    {
                    // InternalScn.g:2439:5: (lv_returnType_7_0= ruleReturnType )
                    // InternalScn.g:2440:6: lv_returnType_7_0= ruleReturnType
                    {

                    						newCompositeNode(grammarAccess.getFunctionAccess().getReturnTypeReturnTypeParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_8);
                    lv_returnType_7_0=ruleReturnType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFunctionRule());
                    						}
                    						set(
                    							current,
                    							"returnType",
                    							lv_returnType_7_0,
                    							"xtext.scn.cares.ubo.fr.Scn.ReturnType");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getFunctionAccess().getSemicolonKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleReturnType"
    // InternalScn.g:2466:1: entryRuleReturnType returns [EObject current=null] : iv_ruleReturnType= ruleReturnType EOF ;
    public final EObject entryRuleReturnType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturnType = null;


        try {
            // InternalScn.g:2466:51: (iv_ruleReturnType= ruleReturnType EOF )
            // InternalScn.g:2467:2: iv_ruleReturnType= ruleReturnType EOF
            {
             newCompositeNode(grammarAccess.getReturnTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReturnType=ruleReturnType();

            state._fsp--;

             current =iv_ruleReturnType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnType"


    // $ANTLR start "ruleReturnType"
    // InternalScn.g:2473:1: ruleReturnType returns [EObject current=null] : ( () ( ( ruleEString ) ) ) ;
    public final EObject ruleReturnType() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalScn.g:2479:2: ( ( () ( ( ruleEString ) ) ) )
            // InternalScn.g:2480:2: ( () ( ( ruleEString ) ) )
            {
            // InternalScn.g:2480:2: ( () ( ( ruleEString ) ) )
            // InternalScn.g:2481:3: () ( ( ruleEString ) )
            {
            // InternalScn.g:2481:3: ()
            // InternalScn.g:2482:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getReturnTypeAccess().getReturnTypeAction_0(),
            					current);
            			

            }

            // InternalScn.g:2488:3: ( ( ruleEString ) )
            // InternalScn.g:2489:4: ( ruleEString )
            {
            // InternalScn.g:2489:4: ( ruleEString )
            // InternalScn.g:2490:5: ruleEString
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getReturnTypeRule());
            					}
            				

            					newCompositeNode(grammarAccess.getReturnTypeAccess().getTypeTypeCrossReference_1_0());
            				
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnType"


    // $ANTLR start "entryRuleExhaustive"
    // InternalScn.g:2508:1: entryRuleExhaustive returns [EObject current=null] : iv_ruleExhaustive= ruleExhaustive EOF ;
    public final EObject entryRuleExhaustive() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExhaustive = null;


        try {
            // InternalScn.g:2508:51: (iv_ruleExhaustive= ruleExhaustive EOF )
            // InternalScn.g:2509:2: iv_ruleExhaustive= ruleExhaustive EOF
            {
             newCompositeNode(grammarAccess.getExhaustiveRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExhaustive=ruleExhaustive();

            state._fsp--;

             current =iv_ruleExhaustive; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExhaustive"


    // $ANTLR start "ruleExhaustive"
    // InternalScn.g:2515:1: ruleExhaustive returns [EObject current=null] : (otherlv_0= 'Exhaustive exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) ;
    public final EObject ruleExhaustive() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_constraintFunctions_3_0 = null;

        EObject lv_constraintFunctions_4_0 = null;

        EObject lv_objectiveFunctions_8_0 = null;

        EObject lv_objectiveFunctions_9_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2521:2: ( (otherlv_0= 'Exhaustive exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) )
            // InternalScn.g:2522:2: (otherlv_0= 'Exhaustive exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            {
            // InternalScn.g:2522:2: (otherlv_0= 'Exhaustive exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            // InternalScn.g:2523:3: otherlv_0= 'Exhaustive exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,44,FOLLOW_52); 

            			newLeafNode(otherlv_0, grammarAccess.getExhaustiveAccess().getExhaustiveExplorationWithKeyword_0());
            		
            otherlv_1=(Token)match(input,45,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getExhaustiveAccess().getConstraintsKeyword_1());
            		
            otherlv_2=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getExhaustiveAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalScn.g:2535:3: ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( ((LA33_0>=RULE_STRING && LA33_0<=RULE_ID)) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalScn.g:2536:4: ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2536:4: ( (lv_constraintFunctions_3_0= ruleFunction ) )
                    // InternalScn.g:2537:5: (lv_constraintFunctions_3_0= ruleFunction )
                    {
                    // InternalScn.g:2537:5: (lv_constraintFunctions_3_0= ruleFunction )
                    // InternalScn.g:2538:6: lv_constraintFunctions_3_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getExhaustiveAccess().getConstraintFunctionsFunctionParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_constraintFunctions_3_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExhaustiveRule());
                    						}
                    						add(
                    							current,
                    							"constraintFunctions",
                    							lv_constraintFunctions_3_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2555:4: ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( ((LA32_0>=RULE_STRING && LA32_0<=RULE_ID)) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalScn.g:2556:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2556:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    // InternalScn.g:2557:6: lv_constraintFunctions_4_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getExhaustiveAccess().getConstraintFunctionsFunctionParserRuleCall_3_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_constraintFunctions_4_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getExhaustiveRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"constraintFunctions",
                    	    							lv_constraintFunctions_4_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,18,FOLLOW_53); 

            			newLeafNode(otherlv_5, grammarAccess.getExhaustiveAccess().getRightCurlyBracketKeyword_4());
            		
            otherlv_6=(Token)match(input,46,FOLLOW_11); 

            			newLeafNode(otherlv_6, grammarAccess.getExhaustiveAccess().getObjectivesKeyword_5());
            		
            otherlv_7=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_7, grammarAccess.getExhaustiveAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalScn.g:2587:3: ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( ((LA35_0>=RULE_STRING && LA35_0<=RULE_ID)) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalScn.g:2588:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2588:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) )
                    // InternalScn.g:2589:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    {
                    // InternalScn.g:2589:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    // InternalScn.g:2590:6: lv_objectiveFunctions_8_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getExhaustiveAccess().getObjectiveFunctionsFunctionParserRuleCall_7_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_objectiveFunctions_8_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getExhaustiveRule());
                    						}
                    						add(
                    							current,
                    							"objectiveFunctions",
                    							lv_objectiveFunctions_8_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2607:4: ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( ((LA34_0>=RULE_STRING && LA34_0<=RULE_ID)) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // InternalScn.g:2608:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2608:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    // InternalScn.g:2609:6: lv_objectiveFunctions_9_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getExhaustiveAccess().getObjectiveFunctionsFunctionParserRuleCall_7_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_objectiveFunctions_9_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getExhaustiveRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"objectiveFunctions",
                    	    							lv_objectiveFunctions_9_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getExhaustiveAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExhaustive"


    // $ANTLR start "entryRuleRandom"
    // InternalScn.g:2635:1: entryRuleRandom returns [EObject current=null] : iv_ruleRandom= ruleRandom EOF ;
    public final EObject entryRuleRandom() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRandom = null;


        try {
            // InternalScn.g:2635:47: (iv_ruleRandom= ruleRandom EOF )
            // InternalScn.g:2636:2: iv_ruleRandom= ruleRandom EOF
            {
             newCompositeNode(grammarAccess.getRandomRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRandom=ruleRandom();

            state._fsp--;

             current =iv_ruleRandom; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRandom"


    // $ANTLR start "ruleRandom"
    // InternalScn.g:2642:1: ruleRandom returns [EObject current=null] : (otherlv_0= 'Random exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) ;
    public final EObject ruleRandom() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_constraintFunctions_3_0 = null;

        EObject lv_constraintFunctions_4_0 = null;

        EObject lv_objectiveFunctions_8_0 = null;

        EObject lv_objectiveFunctions_9_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2648:2: ( (otherlv_0= 'Random exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) )
            // InternalScn.g:2649:2: (otherlv_0= 'Random exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            {
            // InternalScn.g:2649:2: (otherlv_0= 'Random exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            // InternalScn.g:2650:3: otherlv_0= 'Random exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,47,FOLLOW_52); 

            			newLeafNode(otherlv_0, grammarAccess.getRandomAccess().getRandomExplorationWithKeyword_0());
            		
            otherlv_1=(Token)match(input,45,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getRandomAccess().getConstraintsKeyword_1());
            		
            otherlv_2=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getRandomAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalScn.g:2662:3: ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( ((LA37_0>=RULE_STRING && LA37_0<=RULE_ID)) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalScn.g:2663:4: ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2663:4: ( (lv_constraintFunctions_3_0= ruleFunction ) )
                    // InternalScn.g:2664:5: (lv_constraintFunctions_3_0= ruleFunction )
                    {
                    // InternalScn.g:2664:5: (lv_constraintFunctions_3_0= ruleFunction )
                    // InternalScn.g:2665:6: lv_constraintFunctions_3_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getRandomAccess().getConstraintFunctionsFunctionParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_constraintFunctions_3_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRandomRule());
                    						}
                    						add(
                    							current,
                    							"constraintFunctions",
                    							lv_constraintFunctions_3_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2682:4: ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    loop36:
                    do {
                        int alt36=2;
                        int LA36_0 = input.LA(1);

                        if ( ((LA36_0>=RULE_STRING && LA36_0<=RULE_ID)) ) {
                            alt36=1;
                        }


                        switch (alt36) {
                    	case 1 :
                    	    // InternalScn.g:2683:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2683:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    // InternalScn.g:2684:6: lv_constraintFunctions_4_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getRandomAccess().getConstraintFunctionsFunctionParserRuleCall_3_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_constraintFunctions_4_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRandomRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"constraintFunctions",
                    	    							lv_constraintFunctions_4_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop36;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,18,FOLLOW_53); 

            			newLeafNode(otherlv_5, grammarAccess.getRandomAccess().getRightCurlyBracketKeyword_4());
            		
            otherlv_6=(Token)match(input,46,FOLLOW_11); 

            			newLeafNode(otherlv_6, grammarAccess.getRandomAccess().getObjectivesKeyword_5());
            		
            otherlv_7=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_7, grammarAccess.getRandomAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalScn.g:2714:3: ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )?
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( ((LA39_0>=RULE_STRING && LA39_0<=RULE_ID)) ) {
                alt39=1;
            }
            switch (alt39) {
                case 1 :
                    // InternalScn.g:2715:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2715:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) )
                    // InternalScn.g:2716:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    {
                    // InternalScn.g:2716:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    // InternalScn.g:2717:6: lv_objectiveFunctions_8_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getRandomAccess().getObjectiveFunctionsFunctionParserRuleCall_7_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_objectiveFunctions_8_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRandomRule());
                    						}
                    						add(
                    							current,
                    							"objectiveFunctions",
                    							lv_objectiveFunctions_8_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2734:4: ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( ((LA38_0>=RULE_STRING && LA38_0<=RULE_ID)) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // InternalScn.g:2735:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2735:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    // InternalScn.g:2736:6: lv_objectiveFunctions_9_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getRandomAccess().getObjectiveFunctionsFunctionParserRuleCall_7_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_objectiveFunctions_9_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRandomRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"objectiveFunctions",
                    	    							lv_objectiveFunctions_9_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop38;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getRandomAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRandom"


    // $ANTLR start "entryRuleOneByOne"
    // InternalScn.g:2762:1: entryRuleOneByOne returns [EObject current=null] : iv_ruleOneByOne= ruleOneByOne EOF ;
    public final EObject entryRuleOneByOne() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneByOne = null;


        try {
            // InternalScn.g:2762:49: (iv_ruleOneByOne= ruleOneByOne EOF )
            // InternalScn.g:2763:2: iv_ruleOneByOne= ruleOneByOne EOF
            {
             newCompositeNode(grammarAccess.getOneByOneRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneByOne=ruleOneByOne();

            state._fsp--;

             current =iv_ruleOneByOne; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneByOne"


    // $ANTLR start "ruleOneByOne"
    // InternalScn.g:2769:1: ruleOneByOne returns [EObject current=null] : (otherlv_0= 'OneByOne exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) ;
    public final EObject ruleOneByOne() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_constraintFunctions_3_0 = null;

        EObject lv_constraintFunctions_4_0 = null;

        EObject lv_objectiveFunctions_8_0 = null;

        EObject lv_objectiveFunctions_9_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2775:2: ( (otherlv_0= 'OneByOne exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) )
            // InternalScn.g:2776:2: (otherlv_0= 'OneByOne exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            {
            // InternalScn.g:2776:2: (otherlv_0= 'OneByOne exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            // InternalScn.g:2777:3: otherlv_0= 'OneByOne exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,48,FOLLOW_52); 

            			newLeafNode(otherlv_0, grammarAccess.getOneByOneAccess().getOneByOneExplorationWithKeyword_0());
            		
            otherlv_1=(Token)match(input,45,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getOneByOneAccess().getConstraintsKeyword_1());
            		
            otherlv_2=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getOneByOneAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalScn.g:2789:3: ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( ((LA41_0>=RULE_STRING && LA41_0<=RULE_ID)) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalScn.g:2790:4: ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2790:4: ( (lv_constraintFunctions_3_0= ruleFunction ) )
                    // InternalScn.g:2791:5: (lv_constraintFunctions_3_0= ruleFunction )
                    {
                    // InternalScn.g:2791:5: (lv_constraintFunctions_3_0= ruleFunction )
                    // InternalScn.g:2792:6: lv_constraintFunctions_3_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getOneByOneAccess().getConstraintFunctionsFunctionParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_constraintFunctions_3_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneByOneRule());
                    						}
                    						add(
                    							current,
                    							"constraintFunctions",
                    							lv_constraintFunctions_3_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2809:4: ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( ((LA40_0>=RULE_STRING && LA40_0<=RULE_ID)) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // InternalScn.g:2810:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2810:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    // InternalScn.g:2811:6: lv_constraintFunctions_4_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getOneByOneAccess().getConstraintFunctionsFunctionParserRuleCall_3_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_constraintFunctions_4_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getOneByOneRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"constraintFunctions",
                    	    							lv_constraintFunctions_4_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,18,FOLLOW_53); 

            			newLeafNode(otherlv_5, grammarAccess.getOneByOneAccess().getRightCurlyBracketKeyword_4());
            		
            otherlv_6=(Token)match(input,46,FOLLOW_11); 

            			newLeafNode(otherlv_6, grammarAccess.getOneByOneAccess().getObjectivesKeyword_5());
            		
            otherlv_7=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_7, grammarAccess.getOneByOneAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalScn.g:2841:3: ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( ((LA43_0>=RULE_STRING && LA43_0<=RULE_ID)) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalScn.g:2842:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2842:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) )
                    // InternalScn.g:2843:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    {
                    // InternalScn.g:2843:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    // InternalScn.g:2844:6: lv_objectiveFunctions_8_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getOneByOneAccess().getObjectiveFunctionsFunctionParserRuleCall_7_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_objectiveFunctions_8_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneByOneRule());
                    						}
                    						add(
                    							current,
                    							"objectiveFunctions",
                    							lv_objectiveFunctions_8_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2861:4: ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    loop42:
                    do {
                        int alt42=2;
                        int LA42_0 = input.LA(1);

                        if ( ((LA42_0>=RULE_STRING && LA42_0<=RULE_ID)) ) {
                            alt42=1;
                        }


                        switch (alt42) {
                    	case 1 :
                    	    // InternalScn.g:2862:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2862:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    // InternalScn.g:2863:6: lv_objectiveFunctions_9_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getOneByOneAccess().getObjectiveFunctionsFunctionParserRuleCall_7_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_objectiveFunctions_9_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getOneByOneRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"objectiveFunctions",
                    	    							lv_objectiveFunctions_9_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop42;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getOneByOneAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneByOne"


    // $ANTLR start "entryRuleDifferential"
    // InternalScn.g:2889:1: entryRuleDifferential returns [EObject current=null] : iv_ruleDifferential= ruleDifferential EOF ;
    public final EObject entryRuleDifferential() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDifferential = null;


        try {
            // InternalScn.g:2889:53: (iv_ruleDifferential= ruleDifferential EOF )
            // InternalScn.g:2890:2: iv_ruleDifferential= ruleDifferential EOF
            {
             newCompositeNode(grammarAccess.getDifferentialRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDifferential=ruleDifferential();

            state._fsp--;

             current =iv_ruleDifferential; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDifferential"


    // $ANTLR start "ruleDifferential"
    // InternalScn.g:2896:1: ruleDifferential returns [EObject current=null] : (otherlv_0= 'Differential exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) ;
    public final EObject ruleDifferential() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_10=null;
        EObject lv_constraintFunctions_3_0 = null;

        EObject lv_constraintFunctions_4_0 = null;

        EObject lv_objectiveFunctions_8_0 = null;

        EObject lv_objectiveFunctions_9_0 = null;



        	enterRule();

        try {
            // InternalScn.g:2902:2: ( (otherlv_0= 'Differential exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' ) )
            // InternalScn.g:2903:2: (otherlv_0= 'Differential exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            {
            // InternalScn.g:2903:2: (otherlv_0= 'Differential exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}' )
            // InternalScn.g:2904:3: otherlv_0= 'Differential exploration with' otherlv_1= 'constraints' otherlv_2= '{' ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )? otherlv_5= '}' otherlv_6= 'objectives' otherlv_7= '{' ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )? otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,49,FOLLOW_52); 

            			newLeafNode(otherlv_0, grammarAccess.getDifferentialAccess().getDifferentialExplorationWithKeyword_0());
            		
            otherlv_1=(Token)match(input,45,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getDifferentialAccess().getConstraintsKeyword_1());
            		
            otherlv_2=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getDifferentialAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalScn.g:2916:3: ( ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )* )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( ((LA45_0>=RULE_STRING && LA45_0<=RULE_ID)) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalScn.g:2917:4: ( (lv_constraintFunctions_3_0= ruleFunction ) ) ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2917:4: ( (lv_constraintFunctions_3_0= ruleFunction ) )
                    // InternalScn.g:2918:5: (lv_constraintFunctions_3_0= ruleFunction )
                    {
                    // InternalScn.g:2918:5: (lv_constraintFunctions_3_0= ruleFunction )
                    // InternalScn.g:2919:6: lv_constraintFunctions_3_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getDifferentialAccess().getConstraintFunctionsFunctionParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_constraintFunctions_3_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDifferentialRule());
                    						}
                    						add(
                    							current,
                    							"constraintFunctions",
                    							lv_constraintFunctions_3_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2936:4: ( (lv_constraintFunctions_4_0= ruleFunction ) )*
                    loop44:
                    do {
                        int alt44=2;
                        int LA44_0 = input.LA(1);

                        if ( ((LA44_0>=RULE_STRING && LA44_0<=RULE_ID)) ) {
                            alt44=1;
                        }


                        switch (alt44) {
                    	case 1 :
                    	    // InternalScn.g:2937:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2937:5: (lv_constraintFunctions_4_0= ruleFunction )
                    	    // InternalScn.g:2938:6: lv_constraintFunctions_4_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getDifferentialAccess().getConstraintFunctionsFunctionParserRuleCall_3_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_constraintFunctions_4_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDifferentialRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"constraintFunctions",
                    	    							lv_constraintFunctions_4_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop44;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,18,FOLLOW_53); 

            			newLeafNode(otherlv_5, grammarAccess.getDifferentialAccess().getRightCurlyBracketKeyword_4());
            		
            otherlv_6=(Token)match(input,46,FOLLOW_11); 

            			newLeafNode(otherlv_6, grammarAccess.getDifferentialAccess().getObjectivesKeyword_5());
            		
            otherlv_7=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_7, grammarAccess.getDifferentialAccess().getLeftCurlyBracketKeyword_6());
            		
            // InternalScn.g:2968:3: ( ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )* )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( ((LA47_0>=RULE_STRING && LA47_0<=RULE_ID)) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalScn.g:2969:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) ) ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    {
                    // InternalScn.g:2969:4: ( (lv_objectiveFunctions_8_0= ruleFunction ) )
                    // InternalScn.g:2970:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    {
                    // InternalScn.g:2970:5: (lv_objectiveFunctions_8_0= ruleFunction )
                    // InternalScn.g:2971:6: lv_objectiveFunctions_8_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getDifferentialAccess().getObjectiveFunctionsFunctionParserRuleCall_7_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_objectiveFunctions_8_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDifferentialRule());
                    						}
                    						add(
                    							current,
                    							"objectiveFunctions",
                    							lv_objectiveFunctions_8_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:2988:4: ( (lv_objectiveFunctions_9_0= ruleFunction ) )*
                    loop46:
                    do {
                        int alt46=2;
                        int LA46_0 = input.LA(1);

                        if ( ((LA46_0>=RULE_STRING && LA46_0<=RULE_ID)) ) {
                            alt46=1;
                        }


                        switch (alt46) {
                    	case 1 :
                    	    // InternalScn.g:2989:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:2989:5: (lv_objectiveFunctions_9_0= ruleFunction )
                    	    // InternalScn.g:2990:6: lv_objectiveFunctions_9_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getDifferentialAccess().getObjectiveFunctionsFunctionParserRuleCall_7_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_objectiveFunctions_9_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getDifferentialRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"objectiveFunctions",
                    	    							lv_objectiveFunctions_9_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop46;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_10=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getDifferentialAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDifferential"


    // $ANTLR start "entryRuleUserPolicy"
    // InternalScn.g:3016:1: entryRuleUserPolicy returns [EObject current=null] : iv_ruleUserPolicy= ruleUserPolicy EOF ;
    public final EObject entryRuleUserPolicy() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUserPolicy = null;


        try {
            // InternalScn.g:3016:51: (iv_ruleUserPolicy= ruleUserPolicy EOF )
            // InternalScn.g:3017:2: iv_ruleUserPolicy= ruleUserPolicy EOF
            {
             newCompositeNode(grammarAccess.getUserPolicyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUserPolicy=ruleUserPolicy();

            state._fsp--;

             current =iv_ruleUserPolicy; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUserPolicy"


    // $ANTLR start "ruleUserPolicy"
    // InternalScn.g:3023:1: ruleUserPolicy returns [EObject current=null] : (otherlv_0= 'UserPolicy exploration' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'with' otherlv_3= 'constraints' otherlv_4= '{' ( ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )* )? otherlv_7= '}' otherlv_8= 'objectives' otherlv_9= '{' ( ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )* )? otherlv_12= '}' ) ;
    public final EObject ruleUserPolicy() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_12=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_constraintFunctions_5_0 = null;

        EObject lv_constraintFunctions_6_0 = null;

        EObject lv_objectiveFunctions_10_0 = null;

        EObject lv_objectiveFunctions_11_0 = null;



        	enterRule();

        try {
            // InternalScn.g:3029:2: ( (otherlv_0= 'UserPolicy exploration' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'with' otherlv_3= 'constraints' otherlv_4= '{' ( ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )* )? otherlv_7= '}' otherlv_8= 'objectives' otherlv_9= '{' ( ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )* )? otherlv_12= '}' ) )
            // InternalScn.g:3030:2: (otherlv_0= 'UserPolicy exploration' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'with' otherlv_3= 'constraints' otherlv_4= '{' ( ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )* )? otherlv_7= '}' otherlv_8= 'objectives' otherlv_9= '{' ( ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )* )? otherlv_12= '}' )
            {
            // InternalScn.g:3030:2: (otherlv_0= 'UserPolicy exploration' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'with' otherlv_3= 'constraints' otherlv_4= '{' ( ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )* )? otherlv_7= '}' otherlv_8= 'objectives' otherlv_9= '{' ( ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )* )? otherlv_12= '}' )
            // InternalScn.g:3031:3: otherlv_0= 'UserPolicy exploration' ( (lv_name_1_0= ruleEString ) ) otherlv_2= 'with' otherlv_3= 'constraints' otherlv_4= '{' ( ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )* )? otherlv_7= '}' otherlv_8= 'objectives' otherlv_9= '{' ( ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )* )? otherlv_12= '}'
            {
            otherlv_0=(Token)match(input,50,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getUserPolicyAccess().getUserPolicyExplorationKeyword_0());
            		
            // InternalScn.g:3035:3: ( (lv_name_1_0= ruleEString ) )
            // InternalScn.g:3036:4: (lv_name_1_0= ruleEString )
            {
            // InternalScn.g:3036:4: (lv_name_1_0= ruleEString )
            // InternalScn.g:3037:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getUserPolicyAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_54);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUserPolicyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"xtext.scn.cares.ubo.fr.Scn.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_2=(Token)match(input,51,FOLLOW_52); 

            			newLeafNode(otherlv_2, grammarAccess.getUserPolicyAccess().getWithKeyword_2());
            		
            otherlv_3=(Token)match(input,45,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getUserPolicyAccess().getConstraintsKeyword_3());
            		
            otherlv_4=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_4, grammarAccess.getUserPolicyAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalScn.g:3066:3: ( ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )* )?
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( ((LA49_0>=RULE_STRING && LA49_0<=RULE_ID)) ) {
                alt49=1;
            }
            switch (alt49) {
                case 1 :
                    // InternalScn.g:3067:4: ( (lv_constraintFunctions_5_0= ruleFunction ) ) ( (lv_constraintFunctions_6_0= ruleFunction ) )*
                    {
                    // InternalScn.g:3067:4: ( (lv_constraintFunctions_5_0= ruleFunction ) )
                    // InternalScn.g:3068:5: (lv_constraintFunctions_5_0= ruleFunction )
                    {
                    // InternalScn.g:3068:5: (lv_constraintFunctions_5_0= ruleFunction )
                    // InternalScn.g:3069:6: lv_constraintFunctions_5_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getUserPolicyAccess().getConstraintFunctionsFunctionParserRuleCall_5_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_constraintFunctions_5_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUserPolicyRule());
                    						}
                    						add(
                    							current,
                    							"constraintFunctions",
                    							lv_constraintFunctions_5_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:3086:4: ( (lv_constraintFunctions_6_0= ruleFunction ) )*
                    loop48:
                    do {
                        int alt48=2;
                        int LA48_0 = input.LA(1);

                        if ( ((LA48_0>=RULE_STRING && LA48_0<=RULE_ID)) ) {
                            alt48=1;
                        }


                        switch (alt48) {
                    	case 1 :
                    	    // InternalScn.g:3087:5: (lv_constraintFunctions_6_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:3087:5: (lv_constraintFunctions_6_0= ruleFunction )
                    	    // InternalScn.g:3088:6: lv_constraintFunctions_6_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getUserPolicyAccess().getConstraintFunctionsFunctionParserRuleCall_5_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_constraintFunctions_6_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getUserPolicyRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"constraintFunctions",
                    	    							lv_constraintFunctions_6_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop48;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_7=(Token)match(input,18,FOLLOW_53); 

            			newLeafNode(otherlv_7, grammarAccess.getUserPolicyAccess().getRightCurlyBracketKeyword_6());
            		
            otherlv_8=(Token)match(input,46,FOLLOW_11); 

            			newLeafNode(otherlv_8, grammarAccess.getUserPolicyAccess().getObjectivesKeyword_7());
            		
            otherlv_9=(Token)match(input,17,FOLLOW_17); 

            			newLeafNode(otherlv_9, grammarAccess.getUserPolicyAccess().getLeftCurlyBracketKeyword_8());
            		
            // InternalScn.g:3118:3: ( ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )* )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( ((LA51_0>=RULE_STRING && LA51_0<=RULE_ID)) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalScn.g:3119:4: ( (lv_objectiveFunctions_10_0= ruleFunction ) ) ( (lv_objectiveFunctions_11_0= ruleFunction ) )*
                    {
                    // InternalScn.g:3119:4: ( (lv_objectiveFunctions_10_0= ruleFunction ) )
                    // InternalScn.g:3120:5: (lv_objectiveFunctions_10_0= ruleFunction )
                    {
                    // InternalScn.g:3120:5: (lv_objectiveFunctions_10_0= ruleFunction )
                    // InternalScn.g:3121:6: lv_objectiveFunctions_10_0= ruleFunction
                    {

                    						newCompositeNode(grammarAccess.getUserPolicyAccess().getObjectiveFunctionsFunctionParserRuleCall_9_0_0());
                    					
                    pushFollow(FOLLOW_17);
                    lv_objectiveFunctions_10_0=ruleFunction();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getUserPolicyRule());
                    						}
                    						add(
                    							current,
                    							"objectiveFunctions",
                    							lv_objectiveFunctions_10_0,
                    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalScn.g:3138:4: ( (lv_objectiveFunctions_11_0= ruleFunction ) )*
                    loop50:
                    do {
                        int alt50=2;
                        int LA50_0 = input.LA(1);

                        if ( ((LA50_0>=RULE_STRING && LA50_0<=RULE_ID)) ) {
                            alt50=1;
                        }


                        switch (alt50) {
                    	case 1 :
                    	    // InternalScn.g:3139:5: (lv_objectiveFunctions_11_0= ruleFunction )
                    	    {
                    	    // InternalScn.g:3139:5: (lv_objectiveFunctions_11_0= ruleFunction )
                    	    // InternalScn.g:3140:6: lv_objectiveFunctions_11_0= ruleFunction
                    	    {

                    	    						newCompositeNode(grammarAccess.getUserPolicyAccess().getObjectiveFunctionsFunctionParserRuleCall_9_1_0());
                    	    					
                    	    pushFollow(FOLLOW_17);
                    	    lv_objectiveFunctions_11_0=ruleFunction();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getUserPolicyRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"objectiveFunctions",
                    	    							lv_objectiveFunctions_11_0,
                    	    							"xtext.scn.cares.ubo.fr.Scn.Function");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop50;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_12=(Token)match(input,18,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getUserPolicyAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUserPolicy"


    // $ANTLR start "entryRuleEString"
    // InternalScn.g:3166:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalScn.g:3166:47: (iv_ruleEString= ruleEString EOF )
            // InternalScn.g:3167:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalScn.g:3173:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalScn.g:3179:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalScn.g:3180:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalScn.g:3180:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==RULE_STRING) ) {
                alt52=1;
            }
            else if ( (LA52_0==RULE_ID) ) {
                alt52=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 52, 0, input);

                throw nvae;
            }
            switch (alt52) {
                case 1 :
                    // InternalScn.g:3181:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalScn.g:3189:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEInt"
    // InternalScn.g:3200:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalScn.g:3200:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalScn.g:3201:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalScn.g:3207:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalScn.g:3213:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalScn.g:3214:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalScn.g:3214:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalScn.g:3215:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalScn.g:3215:3: (kw= '-' )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==43) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalScn.g:3216:4: kw= '-'
                    {
                    kw=(Token)match(input,43,FOLLOW_49); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleELong"
    // InternalScn.g:3233:1: entryRuleELong returns [String current=null] : iv_ruleELong= ruleELong EOF ;
    public final String entryRuleELong() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleELong = null;


        try {
            // InternalScn.g:3233:45: (iv_ruleELong= ruleELong EOF )
            // InternalScn.g:3234:2: iv_ruleELong= ruleELong EOF
            {
             newCompositeNode(grammarAccess.getELongRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleELong=ruleELong();

            state._fsp--;

             current =iv_ruleELong.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleELong"


    // $ANTLR start "ruleELong"
    // InternalScn.g:3240:1: ruleELong returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleELong() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalScn.g:3246:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalScn.g:3247:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalScn.g:3247:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalScn.g:3248:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalScn.g:3248:3: (kw= '-' )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==43) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalScn.g:3249:4: kw= '-'
                    {
                    kw=(Token)match(input,43,FOLLOW_49); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getELongAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getELongAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleELong"


    // $ANTLR start "entryRuleEBoolean"
    // InternalScn.g:3266:1: entryRuleEBoolean returns [String current=null] : iv_ruleEBoolean= ruleEBoolean EOF ;
    public final String entryRuleEBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEBoolean = null;


        try {
            // InternalScn.g:3266:48: (iv_ruleEBoolean= ruleEBoolean EOF )
            // InternalScn.g:3267:2: iv_ruleEBoolean= ruleEBoolean EOF
            {
             newCompositeNode(grammarAccess.getEBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEBoolean=ruleEBoolean();

            state._fsp--;

             current =iv_ruleEBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEBoolean"


    // $ANTLR start "ruleEBoolean"
    // InternalScn.g:3273:1: ruleEBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleEBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalScn.g:3279:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalScn.g:3280:2: (kw= 'true' | kw= 'false' )
            {
            // InternalScn.g:3280:2: (kw= 'true' | kw= 'false' )
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==52) ) {
                alt55=1;
            }
            else if ( (LA55_0==53) ) {
                alt55=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 55, 0, input);

                throw nvae;
            }
            switch (alt55) {
                case 1 :
                    // InternalScn.g:3281:3: kw= 'true'
                    {
                    kw=(Token)match(input,52,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalScn.g:3287:3: kw= 'false'
                    {
                    kw=(Token)match(input,53,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getEBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEBoolean"


    // $ANTLR start "entryRuleEDouble"
    // InternalScn.g:3296:1: entryRuleEDouble returns [String current=null] : iv_ruleEDouble= ruleEDouble EOF ;
    public final String entryRuleEDouble() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDouble = null;


        try {
            // InternalScn.g:3296:47: (iv_ruleEDouble= ruleEDouble EOF )
            // InternalScn.g:3297:2: iv_ruleEDouble= ruleEDouble EOF
            {
             newCompositeNode(grammarAccess.getEDoubleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEDouble=ruleEDouble();

            state._fsp--;

             current =iv_ruleEDouble.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDouble"


    // $ANTLR start "ruleEDouble"
    // InternalScn.g:3303:1: ruleEDouble returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEDouble() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;
        Token this_INT_3=null;
        Token this_INT_7=null;


        	enterRule();

        try {
            // InternalScn.g:3309:2: ( ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) )
            // InternalScn.g:3310:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            {
            // InternalScn.g:3310:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            // InternalScn.g:3311:3: (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            {
            // InternalScn.g:3311:3: (kw= '-' )?
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==43) ) {
                alt56=1;
            }
            switch (alt56) {
                case 1 :
                    // InternalScn.g:3312:4: kw= '-'
                    {
                    kw=(Token)match(input,43,FOLLOW_55); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            // InternalScn.g:3318:3: (this_INT_1= RULE_INT )?
            int alt57=2;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==RULE_INT) ) {
                alt57=1;
            }
            switch (alt57) {
                case 1 :
                    // InternalScn.g:3319:4: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_27); 

                    				current.merge(this_INT_1);
                    			

                    				newLeafNode(this_INT_1, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_1());
                    			

                    }
                    break;

            }

            kw=(Token)match(input,31,FOLLOW_49); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getEDoubleAccess().getFullStopKeyword_2());
            		
            this_INT_3=(Token)match(input,RULE_INT,FOLLOW_56); 

            			current.merge(this_INT_3);
            		

            			newLeafNode(this_INT_3, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_3());
            		
            // InternalScn.g:3339:3: ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            int alt60=2;
            int LA60_0 = input.LA(1);

            if ( ((LA60_0>=54 && LA60_0<=55)) ) {
                alt60=1;
            }
            switch (alt60) {
                case 1 :
                    // InternalScn.g:3340:4: (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT
                    {
                    // InternalScn.g:3340:4: (kw= 'E' | kw= 'e' )
                    int alt58=2;
                    int LA58_0 = input.LA(1);

                    if ( (LA58_0==54) ) {
                        alt58=1;
                    }
                    else if ( (LA58_0==55) ) {
                        alt58=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 58, 0, input);

                        throw nvae;
                    }
                    switch (alt58) {
                        case 1 :
                            // InternalScn.g:3341:5: kw= 'E'
                            {
                            kw=(Token)match(input,54,FOLLOW_19); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_4_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalScn.g:3347:5: kw= 'e'
                            {
                            kw=(Token)match(input,55,FOLLOW_19); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getEKeyword_4_0_1());
                            				

                            }
                            break;

                    }

                    // InternalScn.g:3353:4: (kw= '-' )?
                    int alt59=2;
                    int LA59_0 = input.LA(1);

                    if ( (LA59_0==43) ) {
                        alt59=1;
                    }
                    switch (alt59) {
                        case 1 :
                            // InternalScn.g:3354:5: kw= '-'
                            {
                            kw=(Token)match(input,43,FOLLOW_49); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEDoubleAccess().getHyphenMinusKeyword_4_1());
                            				

                            }
                            break;

                    }

                    this_INT_7=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_7);
                    			

                    				newLeafNode(this_INT_7, grammarAccess.getEDoubleAccess().getINTTerminalRuleCall_4_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDouble"


    // $ANTLR start "ruleTimeUnits"
    // InternalScn.g:3372:1: ruleTimeUnits returns [Enumerator current=null] : ( (enumLiteral_0= 'mms' ) | (enumLiteral_1= 'ms' ) | (enumLiteral_2= 's' ) | (enumLiteral_3= 'h' ) ) ;
    public final Enumerator ruleTimeUnits() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalScn.g:3378:2: ( ( (enumLiteral_0= 'mms' ) | (enumLiteral_1= 'ms' ) | (enumLiteral_2= 's' ) | (enumLiteral_3= 'h' ) ) )
            // InternalScn.g:3379:2: ( (enumLiteral_0= 'mms' ) | (enumLiteral_1= 'ms' ) | (enumLiteral_2= 's' ) | (enumLiteral_3= 'h' ) )
            {
            // InternalScn.g:3379:2: ( (enumLiteral_0= 'mms' ) | (enumLiteral_1= 'ms' ) | (enumLiteral_2= 's' ) | (enumLiteral_3= 'h' ) )
            int alt61=4;
            switch ( input.LA(1) ) {
            case 56:
                {
                alt61=1;
                }
                break;
            case 57:
                {
                alt61=2;
                }
                break;
            case 58:
                {
                alt61=3;
                }
                break;
            case 59:
                {
                alt61=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 61, 0, input);

                throw nvae;
            }

            switch (alt61) {
                case 1 :
                    // InternalScn.g:3380:3: (enumLiteral_0= 'mms' )
                    {
                    // InternalScn.g:3380:3: (enumLiteral_0= 'mms' )
                    // InternalScn.g:3381:4: enumLiteral_0= 'mms'
                    {
                    enumLiteral_0=(Token)match(input,56,FOLLOW_2); 

                    				current = grammarAccess.getTimeUnitsAccess().getMmsEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTimeUnitsAccess().getMmsEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalScn.g:3388:3: (enumLiteral_1= 'ms' )
                    {
                    // InternalScn.g:3388:3: (enumLiteral_1= 'ms' )
                    // InternalScn.g:3389:4: enumLiteral_1= 'ms'
                    {
                    enumLiteral_1=(Token)match(input,57,FOLLOW_2); 

                    				current = grammarAccess.getTimeUnitsAccess().getMsEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTimeUnitsAccess().getMsEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalScn.g:3396:3: (enumLiteral_2= 's' )
                    {
                    // InternalScn.g:3396:3: (enumLiteral_2= 's' )
                    // InternalScn.g:3397:4: enumLiteral_2= 's'
                    {
                    enumLiteral_2=(Token)match(input,58,FOLLOW_2); 

                    				current = grammarAccess.getTimeUnitsAccess().getSEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getTimeUnitsAccess().getSEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalScn.g:3404:3: (enumLiteral_3= 'h' )
                    {
                    // InternalScn.g:3404:3: (enumLiteral_3= 'h' )
                    // InternalScn.g:3405:4: enumLiteral_3= 'h'
                    {
                    enumLiteral_3=(Token)match(input,59,FOLLOW_2); 

                    				current = grammarAccess.getTimeUnitsAccess().getHEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getTimeUnitsAccess().getHEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeUnits"


    // $ANTLR start "ruleTypeFile"
    // InternalScn.g:3415:1: ruleTypeFile returns [Enumerator current=null] : ( (enumLiteral_0= 'csv' ) | (enumLiteral_1= 'json' ) ) ;
    public final Enumerator ruleTypeFile() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalScn.g:3421:2: ( ( (enumLiteral_0= 'csv' ) | (enumLiteral_1= 'json' ) ) )
            // InternalScn.g:3422:2: ( (enumLiteral_0= 'csv' ) | (enumLiteral_1= 'json' ) )
            {
            // InternalScn.g:3422:2: ( (enumLiteral_0= 'csv' ) | (enumLiteral_1= 'json' ) )
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==60) ) {
                alt62=1;
            }
            else if ( (LA62_0==61) ) {
                alt62=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 62, 0, input);

                throw nvae;
            }
            switch (alt62) {
                case 1 :
                    // InternalScn.g:3423:3: (enumLiteral_0= 'csv' )
                    {
                    // InternalScn.g:3423:3: (enumLiteral_0= 'csv' )
                    // InternalScn.g:3424:4: enumLiteral_0= 'csv'
                    {
                    enumLiteral_0=(Token)match(input,60,FOLLOW_2); 

                    				current = grammarAccess.getTypeFileAccess().getCsvEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTypeFileAccess().getCsvEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalScn.g:3431:3: (enumLiteral_1= 'json' )
                    {
                    // InternalScn.g:3431:3: (enumLiteral_1= 'json' )
                    // InternalScn.g:3432:4: enumLiteral_1= 'json'
                    {
                    enumLiteral_1=(Token)match(input,61,FOLLOW_2); 

                    				current = grammarAccess.getTypeFileAccess().getJsonEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTypeFileAccess().getJsonEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTypeFile"


    // $ANTLR start "ruleTimeStamp"
    // InternalScn.g:3442:1: ruleTimeStamp returns [Enumerator current=null] : ( (enumLiteral_0= 'timed' ) | (enumLiteral_1= 'untimed' ) ) ;
    public final Enumerator ruleTimeStamp() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalScn.g:3448:2: ( ( (enumLiteral_0= 'timed' ) | (enumLiteral_1= 'untimed' ) ) )
            // InternalScn.g:3449:2: ( (enumLiteral_0= 'timed' ) | (enumLiteral_1= 'untimed' ) )
            {
            // InternalScn.g:3449:2: ( (enumLiteral_0= 'timed' ) | (enumLiteral_1= 'untimed' ) )
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==62) ) {
                alt63=1;
            }
            else if ( (LA63_0==63) ) {
                alt63=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 63, 0, input);

                throw nvae;
            }
            switch (alt63) {
                case 1 :
                    // InternalScn.g:3450:3: (enumLiteral_0= 'timed' )
                    {
                    // InternalScn.g:3450:3: (enumLiteral_0= 'timed' )
                    // InternalScn.g:3451:4: enumLiteral_0= 'timed'
                    {
                    enumLiteral_0=(Token)match(input,62,FOLLOW_2); 

                    				current = grammarAccess.getTimeStampAccess().getTimedEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTimeStampAccess().getTimedEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalScn.g:3458:3: (enumLiteral_1= 'untimed' )
                    {
                    // InternalScn.g:3458:3: (enumLiteral_1= 'untimed' )
                    // InternalScn.g:3459:4: enumLiteral_1= 'untimed'
                    {
                    enumLiteral_1=(Token)match(input,63,FOLLOW_2); 

                    				current = grammarAccess.getTimeStampAccess().getUntimedEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTimeStampAccess().getUntimedEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeStamp"

    // Delegated rules


    protected DFA8 dfa8 = new DFA8(this);
    static final String dfa_1s = "\12\uffff";
    static final String dfa_2s = "\5\uffff\1\11\4\uffff";
    static final String dfa_3s = "\1\5\2\40\2\4\1\17\4\uffff";
    static final String dfa_4s = "\1\6\2\40\1\65\2\37\4\uffff";
    static final String dfa_5s = "\6\uffff\1\1\1\4\1\3\1\2";
    static final String dfa_6s = "\12\uffff}>";
    static final String[] dfa_7s = {
            "\1\1\1\2",
            "\1\3",
            "\1\3",
            "\1\5\2\7\30\uffff\1\6\13\uffff\1\4\10\uffff\2\10",
            "\1\5\32\uffff\1\6",
            "\1\11\17\uffff\1\6",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "783:2: (this_SimpleDoubleParameterSet_0= ruleSimpleDoubleParameterSet | this_SimpleIntParameterSet_1= ruleSimpleIntParameterSet | this_SimpleBoolParameterSet_2= ruleSimpleBoolParameterSet | this_SimpleStringParameterSet_3= ruleSimpleStringParameterSet )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0F00000000000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000020040060L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000400040000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000040060L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000080000000010L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000080080000010L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0030000000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000002060L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000802000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000000410000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000002000040000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x3000000000000000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0xC000000000021000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000000000021000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000000020000060L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000008000000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000000000008060L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000000002008000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0000000080000010L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x00C0000000000002L});

}