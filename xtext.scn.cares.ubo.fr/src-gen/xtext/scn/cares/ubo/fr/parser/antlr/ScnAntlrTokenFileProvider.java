/*
 * generated by Xtext 2.16.0-SNAPSHOT
 */
package xtext.scn.cares.ubo.fr.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class ScnAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("xtext/scn/cares/ubo/fr/parser/antlr/internal/InternalScn.tokens");
	}
}
