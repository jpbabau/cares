package design.csys.cares.ubo.fr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;

import fr.ubo.cares.dcl.InterfacePort;
import fr.ubo.cares.dcl.ParameterInstanciation;
import fr.ubo.cares.sys.Component;
import fr.ubo.cares.sys.CompositeComponent;
import fr.ubo.cares.sys.LeafComponent;
import fr.ubo.cares.sys.ProvidedInterfacePort;
import fr.ubo.cares.sys.RequiredInterfacePort;

/**
 * The services class used by VSM.
 */
public class Services {
    
    /**
    * See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24 for documentation on how to write service methods.
    */
    public Collection<ProvidedInterfacePort> getProvidedInterfacePort( LeafComponent lc) {
    	ArrayList<ProvidedInterfacePort> self = new ArrayList<ProvidedInterfacePort>();
    	ArrayList<InterfacePort> ins = new ArrayList<InterfacePort>();
    	for(RequiredInterfacePort rip : lc.getRequiredPorts()) {
    		ins.add( rip.getInterface());
    	}
    	
    	if(lc.eContainer() instanceof CompositeComponent) {
    		CompositeComponent cc = (CompositeComponent)lc.eContainer();
    		for(Component c : cc.getComponents()) {
    			if(c instanceof LeafComponent) {
    				LeafComponent clc = (LeafComponent) c;
	    			if( clc != lc) {
	    				for(ProvidedInterfacePort pip : clc.getProvidedPorts()) {
		    				for(InterfacePort i : ins) {
		    					if(i.getInterface().equals(pip.getInterface().getInterface()))
		    						self.add(pip);
		    				}
	    				}
	    			}
    			}
    		}
    	}
   	return self;
    }
    public String getValue( EObject o) {
    	if(o instanceof LeafComponent) return "LeafComponent";
    	else if (o instanceof ParameterInstanciation) {
    		ParameterInstanciation pi = (ParameterInstanciation) o;
    		return pi.getValue();
    	}
    	return o.getClass().getName();
    }
}
