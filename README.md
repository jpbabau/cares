# Cares

The Cares project provides model editors and java tools to develop and run model-based and component-based CPS simulators

## Getting Started

See for CARES users the [UserInstallationGuide.pdf](UserInstallationGuide.pdf) and the [SimulatorDeveloperManual.pdf](SimulatorDeveloperManual.pdf) for more details on how to install and use the different tools to build a simulator.

See for CARES developers the [DeveloperInstallationGuide.pdf](DeveloperInstallationGuide.pdf) and the [CaresDeveloperManual.pdf](CaresDeveloperManual.pdf) for more details on how to install and use the different tools to implement CARES evolutions.

### Prerequisites

The Cares project is based on the ObeoDesigner tool, version 11.1

## Built With

* [EMF](https://www.eclipse.org/modeling/emf/) for modeling purpose
* [Xtext](https://www.eclipse.org/Xtext/) for textual editor

* [Sirius](https://www.eclipse.org/sirius/) for graphical editor
* [Acceleo](https://www.eclipse.org/acceleo/) for code generation
* [Java 8](https://www.java.com/fr/download/faq/java8.xml) for coding


## Authors

* **Jean-Philippe Babau** - [web site](http://lab-sticc.univ-brest.fr/~babau/)
* **Loic Salmon** - 
* **Pierre-Yves Pillain** -

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the [LICENSE](LICENSE) file for details
