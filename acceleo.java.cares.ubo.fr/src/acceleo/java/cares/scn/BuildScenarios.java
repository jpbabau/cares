package acceleo.java.cares.scn;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import fr.ubo.cares.dcl.JavaType;
import fr.ubo.cares.dcl.Output;
import fr.ubo.cares.dcl.ParameterDeclaration;
import fr.ubo.cares.dcl.SimpleType;
import fr.ubo.cares.dcl.StructType;
import fr.ubo.cares.dcl.Type;
import fr.ubo.cares.scn.*;
import fr.ubo.cares.sys.LeafComponent;
import fr.ubo.cares.sys.LeafOutputPort;

public class BuildScenarios {

	int currentIndex = 0;
	String ca = "\t\t}\n\t\tcatch (Exception e) {s = s + \"error\";}\n";

	public List<Scenario> computeRun(Simulation simu) {
		List<Scenario> lscn = new ArrayList<Scenario>();
		for (Scenario scenario : simu.getScenarios()) {
			for (int i = 0; i < scenario.getNumber(); i++) {
				lscn.add(scenario);	
			}
		}
		return lscn;
	}
	public List<LeafComponent> getLeafComponents(LogObservation lo) {
		List<LeafComponent> lcn = new ArrayList<LeafComponent>();
		for (LogColumn lc : lo.getColumns()) {
			LeafOutputPort lop = lc.getData();
			LeafComponent lct = (LeafComponent)lop.eContainer();
			if(!lcn.contains( lct)) {
				System.out.println("ajout de " + lct.getType().getName());
				lcn.add( lct);
			}
		}
		return lcn;
	}
	
	public List<Scenario> computeAll(Simulation simu) {
		List<Scenario> lscn = new ArrayList<Scenario>();
		for (Scenario scenario : simu.getScenarios()) {
			lscn.add(scenario);	
		}
		return lscn;
	}
	public String setParameterChange(ParameterChange param) {
		String s = "";
		String ss = firstUpperCase(param.getParameter().getParameter().getName());
		LeafComponent lc = (LeafComponent)param.getParameter().eContainer();
		if (param instanceof SimpleDoubleParameterSet) {
			SimpleDoubleParameterSet action = (SimpleDoubleParameterSet) param;
			s = lc.getName() + "().set" + ss + "( " + action.getValue() + ")";
		}
		else if (param instanceof SimpleIntParameterSet) {
			SimpleDoubleParameterSet action = (SimpleDoubleParameterSet) param;
			s = lc.getName() + "().set" + ss + "( " + action.getValue() + ")";		}
		else if(param instanceof SimpleBoolParameterSet) {
			SimpleBoolParameterSet action = (SimpleBoolParameterSet) param;
			s = lc.getName() + "().set" + ss + "( " + action.isValue() + ")";		}
		else if(param instanceof SimpleStringParameterSet) {
			SimpleStringParameterSet action = (SimpleStringParameterSet) param;
			String sr = lc.getName() + "().set" + ss + "( \"" + action.getValue() + "\")";
			s = sr.replace("\\", "/");}
		else {
			s = "parameterChange type not managed : " + param.getClass().getName();
			System.err.println("parameterChange type not managed : " + param.getClass().getName());		
		}
		return s;
	}
	public String rootAction(Action param) {
		String s = "";
	if(param instanceof XOR) {
		XOR action = (XOR) param;
		LeafComponent lcr = (LeafComponent) action.getRequiredInterfacePort().eContainer();
		LeafComponent lc = (LeafComponent) action.getProvidedInterfacePort().eContainer();
		s =  lcr.getName() + "().getAppli().set" + action.getRequiredInterfacePort().getInterface().getName() + "( root.get_" + lc.getName() + "())";
	}
	else if(param instanceof ServiceCall) {
		ServiceCall action = (ServiceCall) param;
		s = action.getComponent().getName() + "()." + action.getFunctionCall().getFunction().getName() + "()";}
	else if(param instanceof ComponentStop) {
		ComponentStop action = (ComponentStop) param;
		s = action.getComponent().getName() + "().stop()";}
	else if(param instanceof ComponentStart) {
		ComponentStart action = (ComponentStart) param;
		s = action.getComponent().getName()+ "().start()";}
	else if(param instanceof ParameterChange) {
		ParameterChange paramC = (ParameterChange) param;
		s = setParameterChange(paramC);}
	else {
		s = "parameterChange type not managed : " + param.getClass().getName();
		System.err.println("rootAction type not managed : " + param.getClass().getName());}
	return  s;
	}
	public String getTypeLeafComponentName( LogColumn cl) {
		LeafOutputPort lop = cl.getData();
		LeafComponent lc = (LeafComponent)lop.eContainer();
		String name = lc.getType().getName();
		//System.out.println("getTypeLeafComponentName " + cl.getData().getName() + " " + name);
		return name;
	}

	public String getTypeLeafComponent( LogColumn cl) {
		LeafOutputPort lop = cl.getData();
		Output lc = lop.getOutput();
		//System.out.println("getTypeLeafComponent :" + lc.getType().getName());
		return lc.getType().getName();
	}
	
	public String getOutputname( LogColumn cl) {
		LeafOutputPort lop = cl.getData();
		Output lc = lop.getOutput();
		return lc.getName();
	}
	
	public String getLogObservationComponentName( LogObservation lo) {
		String name = lo.getName();
	    String s1 = name.substring(0, 1).toUpperCase();
	    return s1 + name.substring(1);
	}
	public String writeColumn( LogColumn cl) {
		LeafOutputPort lop = cl.getData();
		Output lc = lop.getOutput();
		Type t = cl.getData().getOutput().getType();
		String ty = "\t\ttry{";
		String ca = "\t\t}\n\t\tcatch (Exception e) {\n\t\t\ts = s + \"error\";}\n";
		String ss = "\n\t\t\ts = s + inst" + firstUpperCase( lc.getName());
		String s = "";
		try {
			if(t instanceof StructType) {
				StructType st = (StructType) t;
				for(ParameterDeclaration pd : st.getFields()) {
					/* OK pour SimpleType
					String f = "\n" + ss + ".get" + firstUpperCase(pd.getName()) + "() + \";\";";
					s = s + f;
					*/
					s = s + writeField( pd, "\n" + ss);
					System.out.println(pd.getType().getName() + " => s : " + s );//+ " f : " + " ss : " + ss + "\n\n\n");
				}
			}
			else {
				if(t instanceof JavaType) {
					JavaType jt = (JavaType) t;
					Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
					for( Field f : c.getFields()) {
						s = ss + "." + f.getName() + " + \";\";\n" ;
					}
				}
				else {
					s = ss + ".toString()" + " + \";\";\n";//
				}
					
			}
			String str =  ty + s + ca;
			s = str; 
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("s.lenth = " + s.length());
		return s;
	}
	public String writeField( ParameterDeclaration pd, String ss) throws ClassNotFoundException {
		Type t = pd.getType();
		String s = "";
		if(t instanceof StructType) {
			StructType st = (StructType) t;
			for(ParameterDeclaration pdd : st.getFields()) {
				String f =  ss + ".get" + firstUpperCase(pd.getName()) + "()";
				s = s + writeField( pdd, f);
			}
		}
		else {
			if(t instanceof JavaType) {
				JavaType jt = (JavaType) t;
				Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
				for( Field f : c.getFields()) {
					s = s + ss + ".get" + firstUpperCase(pd.getName())  + "()." + f.getName() + " + \";\";\n";
				}
			}
			else {
				s = ss + ".get" + firstUpperCase(pd.getName()) + "() + \";\";";// ss + "." + pd.getName() + " + \";\";\n";
			}
		}
		return s;
	}
	public String writeHeaderColumns( LogColumn cl) {
		Type t = cl.getData().getOutput().getType();
		String ss =  firstUpperCase( t.getName());
		String s = "\"";
		try {
			if(t instanceof StructType) {
				StructType st = (StructType) t;
				for(ParameterDeclaration pd : st.getFields()) {
					String f = firstUpperCase(pd.getName());
					s = s + writeHeaderColumn( pd, f);
					//System.out.println("s : " + s + " f : " + f + " ss : " + ss + "\n\n\n");
				}
				//on retire le dernier ";" s�parateur
				int ls = s.length();
				String l = s.substring(0, ls - 1);
				s = l;
			}
			else {
				if(t instanceof JavaType) {
					JavaType jt = (JavaType) t;
					Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
					for( Field f : c.getFields()) {
						s = s + ss + "." + f.getName();// + " + \";\";\n";
					}
				}
				else {
					s =  s + cl.getData().getOutput().getName();// + \";\";\n";
				}
					
			}
			s = s + "\"";
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
	public String writeHeaderColumn( ParameterDeclaration pd, String ss) throws ClassNotFoundException {
		Type t = pd.getType();
		String s = "";
		if(t instanceof StructType) {
			StructType st = (StructType) t;
			for(ParameterDeclaration pdd : st.getFields()) {
				String f =  ss + "." + firstUpperCase(pd.getName());
				s = s + writeField( pdd, f);
			}
		}
		else {
			if(t instanceof JavaType) {
				JavaType jt = (JavaType) t;
				Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
				for( Field f : c.getFields()) {
					s = s + ss + "." + f.getName() + ";";
				}
			}
			else {
				s = ss + "." + pd.getName() + ";";
			}
		}
		return s;
	}
	public String writeJsonColumn( LogColumn cl) {
		Type t = cl.getData().getOutput().getType();
		String instanceName =  "inst" + firstUpperCase( getOutputname(cl));
		String prefixe =  getTypeLeafComponent(cl) ;
		String s = "\n";
		try {
			if(t instanceof StructType) {
				StructType st = (StructType) t;
				//s = s + "\t\t\tjsonGenerator.writeArrayFieldStart(\"" + t.getName() + "\");\n\t\t\tjsonGenerator.writeStartObject();\n";
				String ss = "";
				for(ParameterDeclaration pd : st.getFields()) {
					ss = ss + writeJsonField( pd, instanceName, prefixe);
					//System.out.println("s : " + s + " f : " + f + " ss : " + ss + "\n\n\n");
				}
				//s = s + ss + "\t\t\tjsonGenerator.writeEndArray();\n\t\t\tjsonGenerator.writeEndObject();\n";
				s = s + ss;
			}
			else {
				if(t instanceof JavaType) {
					JavaType jt = (JavaType) t;
					Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
					s = s + "\t\tjsonGenerator.writeArrayFieldStart(" + jt.getPackageName() + "." + jt.getClassName() + ");";
					for( Field f : c.getFields()) {
						s = s + "\t\t\tjsonGenerator.writeNumber(" +  instanceName + "." + f.getName() + ");\n";
					}
					s = s + "\t\t\tjsonGenerator.writeEndArray();\n\n";
				}
				else {
					s = s + "\t\t\tjsonGenerator.writeNumberField(\"" +   cl.getData().getOutput().getName() + "\", " + instanceName + ");\n";
				}
					
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
	public String writeJsonField( ParameterDeclaration pd, String instance, String prefixe) throws ClassNotFoundException {
		Type t = pd.getType();
		String s = "";
		String instanceName = instance + ".get" + firstUpperCase( pd.getName()) + "()";
		if(t instanceof StructType) {
			StructType st = (StructType) t;
			s = s + "\t\t\tjsonGenerator.writeArrayFieldStart(" + prefixe + t.getName() + ");";
			String ss = "";
			for(ParameterDeclaration pdd : st.getFields()) {
				String p = prefixe + t.getName();
				ss = ss + writeJsonField( pdd, instanceName, p);
				System.err.println("writeJsonField type does not parsed : " + t.getName() + "\n\n\n");
			}
			s = s + ss + "\t\t\tjsonGenerator.writeEndArray()\n\n";
		}
		else {
			if(t instanceof JavaType) {
				JavaType jt = (JavaType) t;
				Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
				//s = s + "\t\t\tjsonGenerator.writeArrayFieldStart(\"" + jt.getPackageName() + "." + jt.getClassName() + "\");\n";
				s = s + "\t\t\tjsonGenerator.writeArrayFieldStart(\"" + prefixe + pd.getName() + "\");\n";
				String ss = "";
				for( Field f : c.getFields()) {
					ss = ss + "\t\t\tjsonGenerator.writeNumber(" +  instanceName + "." + f.getName() + ");\n";
				}
				s = s + ss + "\t\t\tjsonGenerator.writeEndArray();\n\n";
			}
			else {
				s = s + "\t\t\tjsonGenerator.writeNumberField(\"" +  t.getName() + "\", " + instanceName + ");\n";
			}
				
		}
		return s;
	}
	public String getTimeSerieParser( LogObservation lo) {
		//il existe une variable jsonParser et l'instruction suivante  a �t� ex�cut�e "JsonToken jt = jsonParser.nextToken();" 
		//puis l'instruction "String name = jsonParser.getCurrentName();"
		String s = "";
		String si = "";
		String next =  "\n\t\t\telse if(";
		boolean first = true;
		String rsts = "\t\t\t\tif(ptd == null) ptd = new " + this.firstUpperCase( lo.getName()) + "();\n";
		if(this.getTimeStamp(lo)) {
			si = "\t\t\tif(\"time\".equals(name)){\n" + rsts + "\t\t\t\tjsonParser.nextToken();\n\t\t\t\tptd.setTime( jsonParser.getDoubleValue());}";
		}
	    else {
	        si = "\t\t\tif(";
	    }
	 	for (LogColumn lc : lo.getColumns()) {
	 		if(first) {first = false;s = si;}
	 		if(lc.getData().getOutput().getType() instanceof SimpleType ) {
		        String is =  "\""+	lc.getData().getOutput().getName() + "\".equals(name)){\n";
				//String st = rsts + "\t\t\t\tjsonParser.nextToken();\n" + "\t\t\t\tptd.set" + this.firstUpperCase( this.getOutputname( lc)) + "(jsonParser.getDoubleValue());}";
	 			s = s + next + is + parseSimpleTypeJson( lc, rsts);
	 		}
 			else if (lc.getData().getOutput().getType() instanceof JavaType) {
 		        String is = next + "\""+	this.getTypeLeafComponent(lc) + lc.getData().getOutput().getType().getName() + "\".equals(name)){\n" + rsts + "\t\t\t\tjsonParser.nextToken();\n";
				JavaType jt = (JavaType)lc.getData().getOutput().getType();
				s =  s + is + parseJavaTypeJson( jt,  lc, rsts);
 			}
 			else if (lc.getData().getOutput().getType() instanceof StructType) {
				//s = s + "\t\t\tjsonGenerator.writeArrayFieldStart(\"" + t.getName() + "\");\n\t\t\tjsonGenerator.writeStartObject();\n";
				StructType st = (StructType) lc.getData().getOutput().getType();
				for(ParameterDeclaration pd : st.getFields()) {
					String is = next + "\""+	this.getTypeLeafComponent(lc) + pd.getName() + "\".equals(name)){\n" + rsts + "\t\t\t\tjsonParser.nextToken();\n";
					//System.err.println("getTimeSerieParser " + pd.getType().getName() + " => " +  pd.getType().getClass().getTypeName());
					if(pd.getType() instanceof StructType) {
						System.err.println("getTimeSerieParser Type does not parsed :" + pd.getType().getName());
						return null;
					}
					else if(pd.getType() instanceof JavaType) {
						JavaType jt = (JavaType)pd.getType();
						//System.err.println("getTimeSerieParser JavaType " +jt.getName());
						s = s + is + parseJavaTypeJson( jt, lc, pd.getName());
					}
					else if(pd.getType() instanceof SimpleType) {
						SimpleType sst = (SimpleType) pd.getType();
						//System.err.println("getTimeSerieParser SimpleType " + sst.getName());
						s = s + is + parseSimpleTypeJson( lc, rsts);
					}
					else  {
						System.err.println("getTimeSerieParser Type does not knowned :" + pd.getType().getName());
						return null;
					}
				}
				//s = s + ss + "\t\t\tjsonGenerator.writeEndArray();\n\t\t\tjsonGenerator.writeEndObject();\n";
 			}
	 	}
		return s;
	}
	public String parseSimpleTypeJson(LogColumn lc, String rsts) {
		return rsts + "\t\t\t\tjsonParser.nextToken();\n" + "\t\t\t\tptd.set" + this.firstUpperCase( this.getOutputname( lc)) + "(jsonParser.getDoubleValue());}";
	}
	public String parseJavaTypeJson(JavaType jt, LogColumn lc, String field) {
		//System.err.println("parseJavaTypeJson " + jt.getName());
	    String s = "";
		try {
			Class<?> c = Class.forName(jt.getPackageName() + "." + jt.getClassName());
			//s = s + "\t\t\t\tjsonParser.nextToken();\n\t\t\t\t" + jt.getClassName() + " inst" + jt.getClassName() + "  = new " + jt.getClassName() + "();";
			s = s + "\t\t\t\t" + jt.getClassName() + " inst" + jt.getClassName() + "  = new " + jt.getClassName() + "();";
			for( Field f : c.getFields()) {
				s = s +"\n\t\t\t\tjsonParser.nextToken();\n\t\t\t\tinst" + jt.getClassName() + "." + f.getName() + " = jsonParser.getDoubleValue();";
			}
		    String end = "\n\t\t\t\tif(jsonParser.nextToken() != JsonToken.END_ARRAY) return null;";
			s = s + end + "\n\t\t\t\tptd.set" + this.firstUpperCase( this.getOutputname( lc)) + firstUpperCase( field) +" ( " + "inst" + jt.getClassName()  + ");}\n";
			return s;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}
	public String getSerieClassParser( LogObservation lo) {
		//TODO
		String s = "";
	 	for (LogColumn lc : lo.getColumns()) {
	 	    Type t = lc.getData().getOutput().getType();
	 		if(t instanceof SimpleType) {
	 			s = s + "\t" + t.getName() + "  inst" + this.firstUpperCase(lc.getData().getOutput().getType().getName()) + " = null;";}
 			else if(t instanceof SimpleType) ;
	 	    else if(t instanceof JavaType) ;
	 	}
		return s;
	}
	public String timeSerieDeclaration( LogColumn lc) {
		Type t = lc.getData().getOutput().getType();
		String si = "\n\t\t\t";
		String s = si;
		if(t instanceof StructType) {
			StructType st = (StructType) t;
			String ss = "";
			for(ParameterDeclaration pd : st.getFields()) {
				//on ne traite pas le cas r�cursif d'un STRUCTType qui contient une StructType ....
				t = pd.getType();
				if(t instanceof StructType) System.err.println("timeSerieDeclaration : TODO " + pd.getType().getName());
				else if(t instanceof JavaType) {
					JavaType jt = (JavaType) t;
					ss = ss + si + jt.getClassName() + " inst" + firstUpperCase(getOutputname(lc));
				}
				else {
					ss = ss + si + getTypeLeafComponent(lc) + " inst" + firstUpperCase(getOutputname(lc));
				}
			}
			s = ss;
		}
		else {
			if(t instanceof JavaType) {
				JavaType jt = (JavaType) t;
				s = s + jt.getClassName() + " inst" + firstUpperCase(getOutputname(lc));
			}
			else {
				s = s + getTypeLeafComponent(lc) + " inst" + firstUpperCase(getOutputname(lc));
			}
				
		}
		return s;
	}

	public boolean getTimeStamp( LogObservation lo) {
		if(lo.getTimeStamp() == TimeStamp.TIMED) {
			System.out.println(lo.getName() + " timed = true");
			return true;
		}
		else {
			System.out.println(lo.getName() + " untimed = false");
			return false;
		}
	}

	public boolean getCvsTypeFile( LogObservation lo) {
		if(lo.getType() == TypeFile.CSV) {
			System.out.println(lo.getName() + " => cvs");
			return true;
		}
		else {
			System.out.println(lo.getName() + " => json");
			return false;
		}
	}

public static String firstUpperCase( String s) {
	    String s1 = s.substring(0, 1).toUpperCase();
	    return s1 + s.substring(1);
	}

}
