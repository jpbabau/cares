package acceleo.java.cares.sys;

import fr.ubo.cares.dcl.TimeableObject;
import fr.ubo.cares.sys.*;

public class Frequency {
	
	int currentIndex = 0;
	
	public void setFrequency(CompositeComponent comp){
		double frequency = comp.getFrequency();
		for (Component it : comp.getComponents()) {
			if (it.isInheritFrequency()) {
				((TimeableObject)it).setFrequency(frequency);
			}
			if (it instanceof CompositeComponent) {
				setFrequency((CompositeComponent)it);
			}
		}
	}
}
