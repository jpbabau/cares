package acceleo.java.cares.sys;

import org.eclipse.emf.ecore.EObject;
import fr.ubo.cares.sys.*;
import fr.ubo.cares.dcl.*;

public class Utility {
	
	public String getCompleteName(LeafComponent cmp) {
		String name = cmp.getName();
		EObject it = cmp;
		while (!(it.eContainer() instanceof CaresSystem)) {
			it = it.eContainer();
			name = ((NamedElement)it).getName() + '_' + name;
		}
		return name;
	}
	
	public LeafOutputPort getPortProducer(LeafInputPort inpt) {
		DataLinkSource tmp = inpt.getSource().getSource();
		while (tmp instanceof CompositeOutputPort || tmp instanceof CompositeInputPort) {
			if (tmp instanceof CompositeOutputPort) {
				tmp = ((CompositeOutputPort) tmp).getSource().getSource();
			}
			if (tmp instanceof CompositeInputPort) {
				tmp = ((CompositeInputPort) tmp).getSource().getSource();
			}
		}
		return (LeafOutputPort) tmp;
	}
	
	public LeafComponent getProducer(LeafInputPort inpt) {
		LeafOutputPort tmp = getPortProducer(inpt);
		if (tmp != null) {
			return (LeafComponent)tmp.eContainer();
		} else {
			//TODO g�rer le cas de liens cass�s
		}
		return (LeafComponent)inpt.eContainer();
	}
	/*
	 * @author pyp
	 * Permet de v�rifier qu'un LeafComponent est conforme au LeafComponentType auquel il fait r�f�rence par son attribut type.
	 * @param leafCompoent le LeafComponent � v�rifier.
	 * 
	 */

	public boolean uriString( ParameterInstanciation param) {
		try
		{
			Double d = new Double( param.getValue());
			return false;
		}
		catch (NumberFormatException e) {
			Boolean b = Boolean.valueOf( param.getValue());
			if(b) { return false;}
			else { 
				int i = param.getValue().compareTo("false");
				if( i == 0) {return false;}
				else {return true;}
			}
		}
	}
	public String callLink( RequiredInterfacePort itf) {
		String s = "";
		//itf.callLink.eContainer().oclAsType(LeafComponent).name.toUpperFirst()
		for(ProvidedInterfacePort pip : itf.getCallLink()) {
			LeafComponent lc = (LeafComponent) pip.eContainer();
			s = lc.getName();
		    String s1 = s.substring(0, 1).toUpperCase();
		    System.out.println(s1 + s.substring(1));
		    return s1 + s.substring(1);
		}
		return s;
	}
	public boolean callLinks( RequiredInterfacePort itf) {
		if( itf.getCallLink().size() > 1) {
			//System.out.println(itf.getName() + " callLinks " + itf.getCallLink().size() + " => true");
			return true;
		}
		else {
			//System.out.println(itf.getName() + " callLinks " + itf.getCallLink().size() + " => false");
            return false;
		}
	}
}
